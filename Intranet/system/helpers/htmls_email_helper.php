<?php
$exemplo_arte = "
    <style type='text/css'>
    body {
    margin:0px;
    font-family:Verdane;
    font-size:12px;
    color: #666666;
    }
    a{
    color: #666666;
    text-decoration: none;
    }
    a:hover {
    color: #FF0000;
    text-decoration: none;
    }
    </style>
    <html>
      <meta charset='utf-8'>
      <table width='510' border='1' cellpadding='1' cellspacing='1' bgcolor='#FFFFFF'>
        <tr>
          <td height='50'><b>Intranet Reis Office</b></td>
        </tr>
        
        <tr>
          <td width='500'>Tipo de Arte:<b> </b></td>
        </tr>
        <tr>
          <td width='500'>A Quem se Destina o Material? <tag1> <b> </b></td>
        </tr>
        <tr>
          <td width='500'>Qual o Objetivo da Arte? <tag2><b> </b></td>
        </tr>
        <tr>
          <td width='500'>Algum Texto na Arte, Qual Texto? <tag3><b> </b></td>
        </tr>
        <tr>
          <td width='500'>Qual o principal ponto a ser Destacado? <tag4><b> </b></td>
        </tr>
        <tr>
          <td width='500'>Precisa colocar alguma Informação Técnica de Produto? Qual? <tag5><b> </b></td>
        </tr>
        <tr>
          <td width='500'>Precisa de Informação Legal na Arte? Qual? <tag6><b> </b></td>
        </tr>
      </table>
    </html>
";

return $exemplo_arte;
