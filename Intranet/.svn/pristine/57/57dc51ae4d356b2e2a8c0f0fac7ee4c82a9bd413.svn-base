<?php

class Senhas extends CI_Controller {

    /** Sistema de Login e Geração de Senhas - novoRH
     * 
     *  William Feliciano 
     */    
    
    function __construct() 
    {
        parent::__construct();        
        $this->load->model("Senhas_model", "mdsenha");
        
        $this->load->library('sistema');
        $this->load->library('form_validation');  
    }
    
    //FORM DADOS (1ªfase)
    //Cadastro
    public function index()
    {       
        if($this->input->post())
        {
            $this->form_validation->set_rules('txtnome', 'Nome', 'required');
            $this->form_validation->set_rules('txtmatricula', 'Matrícula', 'required');
            $this->form_validation->set_rules('txtemail', 'Email', 'required');
            
            if ($this->form_validation->run() !== FALSE) 
            {
                if(is_numeric($this->validarDados()) === TRUE)
                {
                    $this->validarCaDados($this->validarDados(), $this->input->post("txtemail"));                    
                }
                else
                {                  
                    $this->session->set_flashdata('erro', $this->validarDados());            
                }
            }           
        }                 
    
        $this->load->view("templates/header");
        $this->load->view("templates/barra");        
        $this->load->view("senhas/obterDados");
        $this->load->view("templates/footer");
    }
    
    //VALIDAÇÃO DADOS
    //Verificação Normal ou Patrão
    public function validarDados()
    {
        $dados = $this->input->post();       
        if(strpos($dados["txtemail"],'.') !== FALSE)
        {            
            return $this->validNomeEmail($dados);
        }
        else
        {
            return $this->validaPatrao($dados);            
        }        
    }

    //Validando Dados - Normal    
    public function validNomeEmail($dados = null)
    {
        $name = $this->tirarAcentos(strtolower($dados["txtnome"]));
        $nomes = explode(" ", $name);
        $email = explode(".", strtolower($dados["txtemail"]));
        $matricula = $dados["txtmatricula"];
                        
        if($nomes[0] === $email[0])
        {
            $verif = 'O sobrenome não coincide com o email informado!';
            foreach ($nomes as $nome)
            {                
                if(strtolower($email[1]) === strtolower($nome))
                {
                    $verif = true;                    
                }                
            }

            if($verif === true)
            {                
                $rst = $this->mdsenha->obterFuncDados(array("id","nome"),"rh_empregados", array("matricula" => $matricula));
                if(count($rst)>1)
                {
                    $verif = 'O nome não coincide com a Matrícula Informada!';
                    foreach ($rst as $func)
                    {
                        $func->nome = $this->tirarAcentos($func->nome);
                        $verif = $func->nome === strtoupper($name) ? $func->id : $verif ;
                    }
                }
                else
                {
                    $rst = $rst[0];
                    $rst->nome = $this->tirarAcentos($rst->nome);

                    $verif = $rst->nome === strtoupper($name) ? $rst->id : 'O nome e/ou matrícula não coincide com o da Base de Dados!' ;
                }
            }            
            return $verif;            
        }
        else
        {
            return 'O nome não coincide com o email informado!';
        }        
    }
    
    //Validando Dados - Patrão
    public function validaPatrao($dados = null)
    {
        $nome = $this->tirarAcentos(strtolower($dados["txtnome"]));
        $nomes = explode(" ", $nome);
        $email = strtolower($dados["txtemail"]);
        $matricula = $dados["txtmatricula"];
        
        if(($email === $nomes[0])AND(strpos($email, '.') !== TRUE))
        {                       
            $rst = $this->mdsenha->obterFuncDados(array("id","nome","matricula"), "rh_empregados", array("matricula" => $matricula));
            $rst = $rst[0];
            $rst->nome = $this->tirarAcentos($rst->nome);
            
            if($rst->nome === strtoupper($nome) AND ($rst->matricula === $matricula))
            {  
                return $rst->id;
            }
            else
            {
                return 'O nome e/ou matrícula não coincide com o da Base de Dados!';
            }                
        }
        else
        {
            return 'O email não coincide com o nome informado';
        }
    }
    
    //Concluindo Validação    
    public function validarCaDados($id = null, $email = null)
    {
        $resul = $this->mdsenha->obterFuncDados("*", "rh_empregados_senhas", array("id_empregado" => $id))[0];
        if(empty($resul))       
        {
            //$this->mdsenha->newFuncDados(array("alterar_senha" => 1, "id_empregado" => $id, "ativo" => 0), "rh_empregados_senhas");
            $this->mdsenha->newFuncDados(array("id_empregado" => $id, "tipo_contato" => 13, "valor" => $email."@reisoffice.com.br"), "rh_empregados_contatos");
            redirect(base_url()."senhas/cadastrar/$id", "refresh");           
        }
        else
        {
            $cod = $resul->id;
            $this->mdsenha->updFuncDados(array("alterar_senha" => 1, "ativo" => 0), "rh_empregados_senhas", array("id" => $cod));
            redirect(base_url()."senhas/cadastrar/$id", "refresh");
        }
    }
    
    
    
    //FORM E VALIDAÇÃO DE SENHAS (2ªfase)
    //Cadastro    
    public function cadastrar($id = null)
    {
        if(empty($id))
        {
            $id = $this->input->post("codfunc");
        }

        $this->data["funcionario"] = $this->mdsenha->obterFuncDados("*", "rh_empregados", array("id" => $id))[0];
        $this->data["email"] = $this->mdsenha->obterFuncDados("valor", "rh_empregados_contatos", array("id_empregado" => $id, "tipo_contato" => 13))[0];
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('txtsenha1', 'Primeira Senha', 'required');
            $this->form_validation->set_rules('txtsenha2', 'Confirmação de Senha', 'required|matches[txtsenha1]|callback_contarSenha['.$this->input->post("txtsenha2").']');                

            if ($this->form_validation->run() !== FALSE)
            {                
                $campos = array(
                    "alterar_senha" => 0, 
                    "ativo"         => 1,
                    "senha"         => md5($this->input->post("txtsenha2")),
                    "data_ativacao" => $this->horaCerta()
                );           
                if($this->mdsenha->updFuncDados($campos, "rh_empregados_senhas", array("id_empregado" => $id)))
                {
                    if($this->sistema->enviarEmail($this->prepRem(), $this->prepDest($this->data["funcionario"]->nome, $this->data["email"]->valor, $this->input->post("txtsenha2"))) !== FALSE)
                    {
                        redirect(base_url()."senhas/realizado", "refresh");
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro ao enviar Email");
                        redirect(base_url()."senhas/index", "refresh");                        
                    }
                }
                else
                {
                    $this->session->set_flashdata('erro', "Erro de conexão com a Base de Dados, tente novamente mais Tarde!");
                    redirect(base_url()."senhas/index");
                }                
            }                
        }

        $this->load->view("templates/header");
        $this->load->view("templates/barra");        
        $this->load->view("senhas/obterSenhas", $this->data);
        $this->load->view("templates/footer");        
    }
    
    
    
    //FINALIZAÇÃO  
    //Mensagem 
    public function realizado()
    {
        $this->load->view("templates/header");
        $this->load->view("templates/barra");
        $this->load->view("senhas/congratulations");
        $this->load->view("templates/footer");
    }
    
    //Preparação Email
    public function prepDest($nome = null, $email = null, $senha = null)
    {
        return (object) array(
            'email' => $email,
            'nome' => $nome,
            'mensagem' => array("titulo" => $nome, "email" => $email, "senha" => $senha),
            //'cc' => ' ', 
            'assunto' => 'Seu novo Login'            
        );
    }
    
    public function prepRem()
    {
        return (object) array(
            'email' => 'sistema.solicitacao@reisoffice.com.br',
            'nome'  => 'Login Sistemas'
        );
    }
    
    
    
    
    
    //Funções úteis de uso Geral
    function tirarAcentos($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }
    
    function contarSenha($senha = null)
    {
        if(strlen($senha)>16)
        {
            $this->form_validation->set_message('contarSenha', 'A senha deve Ser menor que 16 caracteres');
            return false;
        }
        elseif(strlen($senha)<7)
        {
            $this->form_validation->set_message('contarSenha', 'A senha deve ter no Mínimo 8 caracteres');
            return false;
        }
        else
        {
            return true;
        }
    }
    
    function horaCerta()
    {
        date_default_timezone_set('America/Sao_Paulo');
        return date('Y-m-d H:i:s', time());
    }
    
    //Teste de Visual da View do Email
    public function emailSenha()
    {
        $this->load->view("email/templateSenhas");
    }    
}

