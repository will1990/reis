<?php

class Social_model extends CI_Model
{
    /*  Intranet Reis Office / SOCIAL
     * 
     *  William Feliciano
     */
    
    public function obter_pag_social($id = 2375)
    {
        $conteudo = get_post($id);
        $conteudo->post_title = $id === 2375 ? '' : '<h3>'.$conteudo->post_title.'</h3><br/>';
        
        return $conteudo;
    }
    
    /*public function obter_projetos($id = 2375)
    {
        $args = array('hide_empty' => 0,
                      'orderby' => 'name',
                      'child_of' => $id,  
                      'post_status' => 'publish');
        
        $pages = get_pages($args);
        
        $projetos = array();

        foreach ($pages as $item) {
            $projetos[$item->post_parent][$item->ID] = array("titulo" => utf8_encode($item->post_title),
                "link" => $item->ID);
        }
        
        return $projetos;
    }*/
    
    public function obter_doacoes()
    {
        $args = array("category" => 41, "numberposts" => -1, "orderby" => "name", "order" => "ASC");        
        $doacoes = get_posts($args);
                
        return $doacoes;
    }
    
    
}
