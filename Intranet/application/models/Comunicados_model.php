<?php

/* ver erro de comparar datas e ettc */

class Comunicados_model extends CI_Model {

    function __construct() {
        $this->load->helper(array('form', 'url', 'auth'));
        $this->load->library('form_validation');
    }

    private $datainicial;
    private $datafinal;
    private $categorias;

    public function filtro() {
        //pega os posts do departamentos
        $this->datainicial = $_POST["DataInicial"];
        $this->datafinal = $_POST["DataFinal"];
        $this->categorias = $_POST["Categorias"];

        //mensagem personalizada
        $this->form_validation->set_message('required', 'O campo "%s" precisa ser preenchido!');

        $this->form_validation->set_rules('DataInicial', 'data inicial', 'required');
        $this->form_validation->set_rules('DataFinal', 'data final', 'required');
        $this->form_validation->set_rules('Categorias', 'categorias', 'required');
        //verifica data
        if (($this->form_validation->run() == FALSE)) {
            return false;
        } else {
            return true;
        }
    }

    //pega post pelo seu id
    public function get_post_by_id($pst_id) {

        if ($pst_data = get_post_field('post_date', $pst_id)) {
            $pst_conteudo = get_post_field('post_content', $pst_id);
            $pst_conteudo = (strpos($pst_conteudo, "<img") !== false) ? $this->sistema->img_mobile($pst_conteudo) : $pst_conteudo;
            $postAux[] = array(
                "titulo" => get_post_field('post_title', $pst_id),
                "categoria" => get_the_category($pst_id)[0]->name,
                "data_postagem" => date('d/m/Y H:i', strtotime($pst_data)),
                "conteudo" => $pst_conteudo,
            );
            return $postAux;
        }
        return false;
    }
    
    //pega post por data e departamento especifico
    public function get_post_by_depto() {
        $this->datainicial = $_POST["DataInicial"];
        $this->datafinal = $_POST["DataFinal"];
        $this->categorias = $_POST["Categorias"];

        $i = 0;
        //pega categorias
        $categs = $this->get_all_categories(array('exclude' => 41));        
        foreach ($categs as $categ) {
            //verifica se a categoria pega do wordpres corresponde a categoria informada pelo usuario
            if ($categ->category_nicename === $this->categorias) {
                $posts = query_posts("category__in=$categ->cat_ID");
                foreach ($posts as $post) {
                    $post->post_date = date('Y/m/d', strtotime($post->post_date));
                    $this->datainicial = date('Y/m/d', strtotime($this->datainicial));
                    $this->datafinal = date('Y/m/d', strtotime($this->datafinal));
                    //if ($this->verificaData($this->datainicial, $this->datafinal, $post->post_date) == true) {
                    if ($post->post_date >= $this->datainicial && $post->post_date <= $this->datafinal) {
                        $i++;
                        $postAux[] = array(
                            "base_url" => "comunicados/id/" . $post->ID . "",
                            "titulo" => $post->post_title,
                            "data_postagem" => $this->dataMySqlParaPtBr($post->post_date),
                        );
                    }
                }
            }
        }

        if ($i == 0) {
            return false;
        }
        return $postAux;
    }

    //pega
    public function get_all_posts($args) {
        $categ = $this->get_all_categories();
        foreach ($categ as $categs) {
            $posts = query_posts("category__in=$categs->cat_ID&{$args}");                     
            foreach ($posts as $post) {       
                $postAux[] = array(
                    "base_url" => "comunicados/id/$post->ID",
                    "titulo" => $post->post_title,
                    "data_postagem" => $post->post_date = $this->dataMySqlParaPtBr($post->post_date),
                    "ID" => $post->ID,
                    "categoria" => $categs->cat_name,
                );
            }
        }

        return $postAux;
    }

    //busca todas categorias
    public function get_all_categories() {
        $categs = get_categories(array('orderby' => 'nome', 'parent' => 0, 'hide_empty' => 0, 'exclude' => 41));
        return $categs;
    }

    //converter datas para dd/mm/yyyy
    public function dataMySqlParaPtBr($dataMySql) {
        $data = new DateTime($dataMySql);
        return $data->format("d/m/Y");
    }

    

}
