<?php

class RamalDeptos extends CI_Model {

    private $nome;
    private $pessoas;

    public function __construct($nome) {
        $this->nome = $nome;
    }

    public function getAllDepto() {
        return $this->pessoas;
    }
    //pesquisa
    public function getSearchDepto($pesquisa) {
        $resultados = false;
        
        foreach ($this->pessoas as $key => $value) {
                //faz uma busca da palavra chave no conjunto de ramais do departamento
                foreach ($value as $key2 => $value2) {
                    //se achar um trecho corresponde da pesquisa nos valores do ramais(nome, cargo, numero do ramal), atribui a lista de resultados.
                    if (stripos($value2, $pesquisa) !== FALSE) {
                        $resultados[] = $value;
                    } else {
                        //aqui verifica se a pesquisa corresponde ao setor
                        if (stripos($this->nome, $pesquisa) !== FALSE) {
                            return $this->pessoas;
                        }
                    }
                }
            
        }
        //retorna o conjunto de dados ou 
        return $resultados;
    }
    //adiciona um novo ramal
    public function setDepto($nome, $cargo, $ramal) {
        $this->pessoas[] = array("nome" => $nome, "cargo" => $cargo, "ramal" => $ramal);
    }
    //nome do setor
    public function getNome() {
        return $this->nome;
    }

}
