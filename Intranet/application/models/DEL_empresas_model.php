<?php

class Empresas_model extends CI_Model{	
	
    public function buscaTodos(){
		/*$this->db->where("vendido", false);*/	
		return $this->db->get("empresas")->result_array();
	}
	
    public function salva($empresa){
		
		$this->db->insert("empresas", $empresa);
		
	}
        
    public function busca($id){
        return $this->db
    	->get_where("empresas", array(
            "id" => $id,
            ""         
                ))
        ->row_array();               
    }
    
    
    public function buscasetor($id){
        return $this->db
    	->get_where("setores", array(
            "id" => $id,
            ""         
                ))
        ->row_array();               
    }
    
    public function buscafuncionario($id){
        
        $this->db->select("nome, matricula, id, foto, id_setor");
        $rst = $this->db->get_where("usuarios", "id = $id")->row_array();
        
        return $rst;
    }
    
    public function buscaFuncionarios($id){
        
        $this->db->select("nome, matricula, id, id_setor");
        $rst = $this->db->get_where("usuarios", "id_empresa = $id")->result();
        
        if($rst)
        {
            foreach($rst as $item)
            {
                $item->cafe = $this->get_cafe($item->id);
                $item->almoco = $this->get_almoco($item->id);
                
                if($item->id_setor == 7)
                {
                    $item->valor_almoco = 2;
                }    
                else{                    
                    $item->valor_almoco = 1;                    
                }
            }
        }
        
        
        return $rst;
    }
    
    public function buscaDadosFuncionarios($id){
   
        $this->db->select("nome, matricula, id, id_setor");
        $this->db->order_by("nome", "asc");
        $rst = $this->db->get_where("usuarios", "id_setor = $id")->result();
        if ($rst)
        {
            foreach($rst as $item)
            {
                $item->cafe = $this->get_cafe($item->id);
                $item->almoco = $this->get_almoco($item->id);
                
                if($item->id_setor == 7)
                {
                    $item->valor_almoco = 2;
                }    
                else{                    
                    $item->valor_almoco = 1;                    
                }
                
            }    
        }    
        return $rst;
    }
    
    
    public function get_cafe($id, $mes = 0)
    {
        $mes = ($mes == 0) ? date("m") : $mes;
        $this->db->select("COUNT(*) total");
        $this->db->where("id_usuario = $id");
        $this->db->where("MONTH(data) = '$mes'");
        $this->db->where("TIME(data) <= '08:00:00'");
        $rst = $this->db->get("diario")->row();
        if($rst)
            return $rst->total;
        else
            return 0;
    }
    
    public function get_almoco($id, $mes = 0)
    {
        $mes = ($mes == 0) ? date("m") : $mes;
        $this->db->select("COUNT(*) total");
        $this->db->where("id_usuario = $id");
        $this->db->where("MONTH(data) = '$mes'");
        $this->db->where("TIME(data) >= '11:00:00'");
        $rst = $this->db->get("diario")->row();
        if($rst)
            return $rst->total;
        else
            return 0;
    }
    
    
    
    public function funcionario_almoco($id) 
    {        
        //$this->db->select("DATE_FORMAT(data, '%d/%m/%Y') dt, DATE_FORMAT(data, '%H:%i') hr", false);
        $this->db->select("id_usuario,id_refeicao,DATE_FORMAT(data, '%d/%m/%Y') dt, DATE_FORMAT(data, '%H:%i') hr", false);
        $this->db->from("diario");        
        $this->db->where("id_usuario = $id");
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $item)
            {
                $item->refeicao = $this->get_refeicao($item->id_refeicao);
                
            } 
        
        
            
        return $result;
        
    }        
    
    public function setor_almoco($id) 
    {        
        //$this->db->select("DATE_FORMAT(data, '%d/%m/%Y') dt, DATE_FORMAT(data, '%H:%i') hr", false);
        $this->db->select("id_usuario,id_refeicao,DATE_FORMAT(data, '%d/%m/%Y') dt, DATE_FORMAT(data, '%H:%i') hr", false);
        $this->db->from("diario");        
        $this->db->where("id_setor = $id");
        $query = $this->db->get();
        $result = $query->result();
        
        foreach($result as $item)
            {
                $item->refeicao = $this->get_refeicao($item->id_refeicao);
                $item->funcionario = $this->get_funcionario($item->id_usuario);
            } 
        
        
        return $result;
        
    }    
    
    
    public function get_funcionarios($id){
        
        $this->db->select("nome , id, id_setor");
        $this->db->from("usuarios");
        $this->db->where("id_setor = $id");
        $query = $this->db->get();
        $result = $query->result();
        
         foreach($result as $item)
            {
               $item->almocos = $this->funcionario_almoco($item->id);
               $item->quantidade = $this->qtdeRefeicoes($item->id);
               
                if($item->id_setor == 7)
                {
                    $item->valor_almoco = 2;
                }    
                else{                    
                    $item->valor_almoco = 1;                    
                }
            } 
        
        return $result;
    }
    
    public function get_refeicao($id)
    {        
        $rst = $this->db->get_where("refeicoes", "id = $id")->row();        
        return $rst;        
    }
    
    
    
    public function empresafuncionario($id)
    {
	$this->db->select("empresas.titulo");
        $this->db->from("empresas");
        $this->db->join("usuarios", "usuarios.id_empresa = empresas.id");
        $this->db->where("usuarios.id" , $id);
        $query = $this->db->get();
        $rst=$query->row();                     
        return $rst;		
    }
    
    
    
     public function setorfuncionario($id){
	
         $this->db->select("setores.nome");
         $this->db->from("setores");
         $this->db->join("usuarios", "usuarios.id_setor = setores.id");
         $this->db->where("usuarios.id" , $id);
         $query = $this->db->get();
         $rst=$query->row();                     
         return $rst;		
    }
    
    public function cargofuncionario($id){
	
        $this->db->select("cargos.titulo");
        $this->db->from("cargos");
        $this->db->join("usuarios", "cargos.id = usuarios.id_cargo");
        $this->db->where("usuarios.id" , $id);       
        $query = $this->db->get();
        $rst = $query->row();                     
        return $rst;		
    }
    
    public function buscaTodosSetores($id){
	
        $rst = $this->db->get_where("setores", "id_empresa = $id")->result_array();
        return $rst;		
        
    }
    
    public function buscaSetores(){
        
        $rst = $this->db->get("setores");
        
    }    
    
	
    public function buscaTodosFuncionarios($id){
        
        $rst = $this->db->get_where("usuarios", "id_setor = $id")->result_array();
        return $rst;
    }
	    
    public function buscaFuncionarios1(){
        
        $rst = $this->db->get("usuarios");
    }    
      
    public function buscaFuncionariosEmpresa($id){
        
        $rst = $this->db->get_where("usuarios", "id_empresa = $id")->result_array();
        return $rst;
               
    }    
   
    
    public function buscaTudo($id){
        $rst = "SELECT USU.*, EMP.titulo empresa, CAR.titulo cargo, STR.nome setor "
			+ "FROM usuarios USU "
                	+ "JOIN empresas EMP on USU.id_empresa = EMP.id "
                        + "JOIN cargos CAR on USU.id_cargo = CAR.id "
			+ "JOIN setores STR on USU.id_setor = STR.id "
			+ "WHERE EMP.id = $id";
    }    
    
    public function qtdeRefeicoes($id){
        
        $this->db->select("COUNT(*)cont");
        $this->db->from("diario");
        $this->db->where("id_usuario = $id");
        $query = $this->db->get();
        $rst = $query->row();
        
        return $rst;
        
        
        
        
    }
}