<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Busca_model extends CI_Model {

    /**
     * Intranet Reis Office
     * Cintia Masumi
     */
    function __construct() {
        parent::__construct();
    }  
    
    public function buscar($s) {       
        
        $busca_param = array(
            's' => $s,
            'category__not_in' => array(16,41), //categorias de posts renegadas
            'post__not_in' => array(84,94,2375)//paginas ou posts renegados
        );
        $resultado = new WP_Query($busca_param);
        $view = '';
        if (count($resultado->posts) > 0) 
        {
            foreach ($resultado->posts as $item) 
            {
                $scan = get_post($item->ID)->post_type;
                $post_parent = get_post($item->ID)->post_parent;
                $get_category = get_the_category($item->ID)[0]->term_taxonomy_id;
                  
                if ($scan == "post" && $get_category == 28) //verificação do faq atraves do post id
                {
                    list($act , $titulo) = explode("*",$item->post_title);
                    $view .= $this->view_busca(base_url("informacoes/index/$act"),$titulo);
                }
                elseif ($s == "galeria" || $s == "galeria de fotos") 
                {
                    $view .= $this->view_busca (redirect(base_url("galeria")));
                }
                elseif($s == "ramais" || $s === "ramal")
                {
                    $view .= $this->view_busca(redirect(base_url("ramais")));
                }
                elseif($scan == "post")
                {
                    $view .= $this->view_busca(base_url("comunicados/id/$item->ID"), $item->post_title);
                }
                elseif ($scan == "page" && $post_parent == 84) 
                {
                    $post_content = get_post($item->ID)->post_content; //aqui fica o link
                    $view .=  $this->view_busca ($post_content, $item->post_title, '_blank' );
                }
                elseif($item->ID == 1173)
                {
                    $view .= $this->view_busca(base_url("cardapio"),"Cardápio");
                }
                elseif ($scan == "page" )
                {
                    $view .= $this->view_busca(base_url("departamentos/index/$item->ID"), $item->post_title,'_blank');
                }
            }
        }        
        else 
        {
           $view .= '<div><p style="font-size:25px; ">Nenhum resultado encontrado. </p></div>';
        }
        return $view;
    }
    
    public function view_busca($link, $titulo, $blank = '') 
    {
        return '<div><p><h4><a href="' . $link . '" target="'.$blank.'">' . $titulo . '</a></h4></p>
                <button style="float:bottom; margin-left:1000px;" type="button" class="btn btn-primary"><a style="color:#ffffff" href="' . $link. '" target="'.$blank.'">Ver mais</a></button></div>
                <hr/>';
    }
}