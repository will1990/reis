<?php

class Galeria_model extends CI_Model
{
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    
    //MENU GALERIA
    //Trazendo Pastas e Níveis
    
    public function obter_menu_gal($caminho)
    {
        $res_pastas = array();
        
        if($caminho)
        {
            $album_dir = opendir($caminho);
            while(($dir = readdir($album_dir)) !== false)
            {
                if(is_dir($caminho.$dir) && $dir != '.' && $dir != '..')
                {
                    $res_pastas[] = $dir;                    
                }
            }
        }  
        return $res_pastas;
    }    
}
	