<?php

class Noticias_model extends CI_Model 
{
    /*  Intranet Reis Office / NOTICIAS
     * 
     *  William Feliciano
     */
        
    function __construct() 
    {
        parent::__construct();
        $this->base = $this->load->database('portal', TRUE);     
    }
    
    // Carrega as 10 últimas noticias do site Reis Office      
    public function obter_noticias_recentes($qtde = 10)
    {   
        $this->base->select("cd_noticia, ds_chamada_noticia, nm_noticia, nm_foto, dt_noticia, MONTH(dt_noticia) mes, DAY(dt_noticia) dia, YEAR(dt_noticia) ano");
        $this->base->order_by("cd_noticia", "desc");        
        $rst = $this->base->get_where("noticia", "cd_categoria = 1", $qtde)->result();
        
        if($rst)
        {
            foreach($rst as $item)
            {
                $item->nm_noticia = str_replace('chr37','%', $item->nm_noticia);
                $item->data = $item->dia.'/'.$item->mes.'/'.$item->ano;
                $url = "https://www.reisoffice.com.br/arquivos/noticias/thumb/".$item->nm_foto;
                $item->img = getimagesize($url) ? $url : base_url("images/img_all/logo_reis.png");
            }
        }
        
        return $rst;
    }    
    
    //Carrega a Noticia para Leitura
    public function obter_noticia($id)
    {
        $this->base->select("ds_integra_noticia, nm_noticia, MONTH(dt_noticia) mes, DAY(dt_noticia) dia, YEAR(dt_noticia) ano");
        $this->base->from("noticia");
        $this->base->where("cd_noticia", $id);
        $qry = $this->base->get();
        $rst = $qry->row();
        
        if($rst)
        {
            $rst->nm_noticia = str_replace('chr37','%', $rst->nm_noticia);
            $rst->data = $rst->dia.'/'.$rst->mes.'/'.$rst->ano;
            $rst->noticia = $this->tratar_noticia($rst->ds_integra_noticia);
        }
        
        return $rst;
    }
    
    //Tratamento de Conteúdo da Noticia para Leitura
    public function tratar_noticia($conteudo = null)
    {
        $conteudo = str_replace('chr60br /chr62', '<br/>', $conteudo);
        $conteudo = str_replace('chr37', '%', $conteudo);
                
        return $conteudo;
    }
}




    