<?php

class Teste_model extends CI_Model {
    /*  Intranet Reis Office / ESPAÇO DESTINADO PARA TESTES
     * 
     *  Rafael Oliveira
     *  William Feliciano 
     */

    // Página Index
    // Trazendo os Aniversariantes de Reis Office
    /* public function get_algo() {

      //$tab[1] = get_post(1891);
      //$tab[2] = get_post(1782);
      //$tab[3] = get_post_format(1891);

      $tab = get_pages(array('orderby' => 'name', 'hide_empty' => 0, 'exclude' => array(84, 1399)));

      /* $this->db = $this->load->database('rh', TRUE);

      $this->db->select("nome, data_admissao");
      $this->db->from("rh_empregados");
      $this->db->where("DATE_FORMAT(data_admissao, '%m') = 09 AND DATE_FORMAT(data_admissao, '%Y')< 2016 ");
      $qry = $this->db->get();
      $tab = $qry->result_array();

      $mes = date('m', strtotime('today'));

      print_r($mes); */






    /* return $tab;
      } */

    //conexao
    public function conectar() {
        $dbh = new PDO('mysql:host=192.168.0.89;dbname=wordpress', 'reis', 'Reis2015');
        return $dbh;
    }

    public function getLog2() {
        $data1 = $this->datePickerToMysql($_POST['inicio']);
        $data2 = $this->datePickerToMysql($_POST['fim']);

        if ($_POST['inicio'] <= $_POST['fim'] && (!empty($_POST['inicio']) && !empty($_POST['fim']))) {

            /* Alterado Fagner */
            $sql = "SELECT DATE(DATA) 'data'
                    , CASE LEFT(url,45)
                        WHEN 'http://intranet.reisoffice.com.br/' THEN 'Home'
                        ELSE 'Comunicados'
                      END 'local'
                    , COUNT(ip) AS 'quantidade' 
                    FROM log_intranet 
                    WHERE DATE(DATA) BETWEEN DATE('$data1') AND DATE('$data2') 
                    AND metodo != 'getAniversariantes' 
                    AND (url = 'http://intranet.reisoffice.com.br/' OR url LIKE 'http://intranet.reisoffice.com.br/comunicados%')
                    GROUP BY DATE(DATA), LEFT(url,45)";
            $rst = $this->db->query($sql)->result();
            $var = "<table id='tabPrincipais' class='table table-bordered table-striped'><thead><tr>";
            $var.= "<th>DATA</th>";
            $var.= "<th>Home</th>";
            $var.= "<th>Comunicados</th></thead><tbody></tr>";

            //echo "<pre>";print_r($rst);
            if ($rst) {
                $dia_atual = "";
                $span = 1;
                $locaisQuantia = 0;
                foreach ($rst as $row) {

                    $dia = $this->mysqToBrHu3($row->data);
                    if ($dia_atual != $dia && !empty($dia_atual)) {
                        $var = str_replace("{X}", $span, $var);
                        $span = 1;
                    }

                    if ($dia_atual == $dia)
                        $span++;

                    if ($locaisQuantia == 0) {
                        $var.= "<tr>";
                        if ($span == 1)
                            $var.= "<td>$dia</td>";
                    }
                    $var.= "<td>$row->quantidade</td>";

                    $dia_atual = $dia;

                    $locaisQuantia++;
                    if ($locaisQuantia == 2) {

                        $locaisQuantia = 0;
                        $var.= "</tr>";
                    }
                }
                $var = str_replace("{X}", $span, $var);
            }

            $var.= "</tbody></table>";
            return $var;

            /* try {
              $conexao = $this->conectar();
              $var = "<table id='tabPrincipais' class='table table-bordered table-striped'><thead><tr>";
              $var.= "<th>DATA</th>";
              $var.= "<th>QUANTIDADE</th>";
              foreach ($conexao->query("SELECT CAST(DATA AS DATE) AS 'data',COUNT(DISTINCT ip) AS 'quantidade' FROM log_intranet WHERE DATE(DATA) BETWEEN DATE('{$data1}') AND DATE('{$data2}') AND metodo != 'getAniversariantes'  GROUP BY DATE(DATA)") as $row) {
              $var.= "<tr>";
              $var.= "<td>{$this->mysqToBrHu3($row['data'])}</td>";
              $var.= "<td>{$row['quantidade']}</td>";
              $var.= "</tr>";
              }

              $var.= "</table>";
              return $var;
              } catch (PDOException $e) {
              print "Error!: " . $e->getMessage() . "<br/>";
              die();
              } */
        } else {
            return "verifique as datas, algo deu errado !";
        }
    }

    //get log
    public function getLog() {
        $data1 = $this->datePickerToMysql($_POST['inicio']);
        $data2 = $this->datePickerToMysql($_POST['fim']);

        if ($_POST['inicio'] <= $_POST['fim'] && (!empty($_POST['inicio']) && !empty($_POST['fim']))) {

            /* Alterado Fagner */
            $sql = "SELECT DATE(DATA) 'data'
                    , CASE LEFT(url,45)
                        WHEN 'http://intranet.reisoffice.com.br/' THEN 'Home'
                        ELSE 'Comunicados'
                      END 'local'
                    , COUNT(ip) AS 'quantidade' 
                    FROM log_intranet 
                    WHERE DATE(DATA) BETWEEN DATE('$data1') AND DATE('$data2') 
                    AND metodo != 'getAniversariantes' 
                    AND (url = 'http://intranet.reisoffice.com.br/' OR url LIKE 'http://intranet.reisoffice.com.br/comunicados%')
                    GROUP BY DATE(DATA), LEFT(url,45)";
            $rst = $this->db->query($sql)->result();

            $var = "<table id='tabPrincipais' class='table table-bordered table-striped'><thead><tr>";
            $var.= "<th>DATA</th>";
            $var.= "<th>LOCAL</th>";
            $var.= "<th>QUANTIDADE</th></thead><tbody></tr>";

            if ($rst) {
                $dia_atual = "";
                $span = 1;
                foreach ($rst as $row) {
                    $dia = $this->mysqToBrHu3($row->data);
                    if ($dia_atual != $dia && !empty($dia_atual)) {
                        $var = str_replace("{X}", $span, $var);
                        $span = 1;
                    }

                    if ($dia_atual == $dia)
                        $span++;

                    $var.= "<tr>";

                    if ($span == 1)
                        $var.= "<td rowspan='{X}'>$dia</td>";

                    $var.= "<td>$row->local</td>";
                    $var.= "<td>$row->quantidade</td>";
                    $var.= "</tr>";
                    $dia_atual = $dia;
                }
                $var = str_replace("{X}", $span, $var);
            }

            $var.= "</tbody></table>";
            return $var;

            /* try {
              $conexao = $this->conectar();
              $var = "<table id='tabPrincipais' class='table table-bordered table-striped'><thead><tr>";
              $var.= "<th>DATA</th>";
              $var.= "<th>QUANTIDADE</th>";
              foreach ($conexao->query("SELECT CAST(DATA AS DATE) AS 'data',COUNT(DISTINCT ip) AS 'quantidade' FROM log_intranet WHERE DATE(DATA) BETWEEN DATE('{$data1}') AND DATE('{$data2}') AND metodo != 'getAniversariantes'  GROUP BY DATE(DATA)") as $row) {
              $var.= "<tr>";
              $var.= "<td>{$this->mysqToBrHu3($row['data'])}</td>";
              $var.= "<td>{$row['quantidade']}</td>";
              $var.= "</tr>";
              }

              $var.= "</table>";
              return $var;
              } catch (PDOException $e) {
              print "Error!: " . $e->getMessage() . "<br/>";
              die();
              } */
        } else {
            return "verifique as datas, algo deu errado !";
        }
    }

    public function datePickerToMysql($dataMySql) {
        $pieces = explode("-", $dataMySql);
        return "$pieces[2]-$pieces[1]-$pieces[0]";
    }

    public function mysqToBrHu3($dataMySql) {
        $pieces = explode("-", $dataMySql);
        return "$pieces[2]-$pieces[1]-$pieces[0]";
    }

}
