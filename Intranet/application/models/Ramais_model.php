<?php

require_once "RamalDeptos.php";

class Ramais_model extends CI_Model{

    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
        //private $deptos = array("DIRETORIA", "RECURSOS HUMANOS", "ADMINISTRAÇÃO", "SYSTEMA ANALYSIS - BOHM/ E-COMMERCE/ QUALIDADE",
        //"COMPRAS", "MARKETING", "LOGÍSTICA", "TECNOLOGIA DA INFORMAÇÃO E TELECOMUNICAÇÕES", "CONECTIVIDADE", "TELEFONES REIS OFFICE",
        //"COMERCIAL", "LICITAÇÕES", "ASSISTÊNCIA TÉCNICA", "T.I DESENVOLVIMENTO", "VENDAS", "EQUIPE XEQUE MATE", "EQUIPE ALIANÇA", "EQUIPE VIBRAÇÃO", "EQUIPE SINERGIA");
    
    
    function __construct() {       
        parent::__construct();
        //$this->load->helper('url');       
        
        $this->rh = $this->load->database("base", true);
    }

    public function obterRamais()
    {
        $this->rh->select("empregados.nome, deptos.titulo depto, interno.id_empregado, interno.ramal, interno.skype, contato.valor email", false);
        $this->rh->from("rh_empregados_contatos_internos AS interno");
        $this->rh->join("rh_empregados AS empregados", "interno.id_empregado = empregados.id", "left");
        $this->rh->join("rh_empregados_contatos AS contato", "interno.id_empregado = contato.id_empregado", "left");
        $this->rh->join("rh_empregados_contratos AS contrato", "interno.id_empregado = contrato.id_empregado", "left");
        $this->rh->join("rh_departamentos AS deptos", "contrato.id_departamento = deptos.id", "left");               
        $qry = $this->rh->get();
        $rst = $qry->result();
        
        foreach ($rst as $row):
            if(empty($row->id_empregado)):
                $row->nome = $row->skype;
                $row->skype = "";
            endif;
        endforeach;
        
        return $rst;
    }
}