<?php

class Email_model extends CI_Model {

    private $email="";
    private $nome="";
    private $mensagem="";
    private $destinatario="";
    private $cc="";
    private $assunto="";
    private $anexos;

    function getEmail() {
        return $this->email;
    }

    function getNome() {
        return $this->nome;
    }

    function getMensagem() {
        return $this->mensagem;
    }

    function getDestinatario() {
        return $this->destinatario;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getAnexos() {
        return $this->anexos;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    function setDestinatario($destinatario) {
        $this->destinatario = $destinatario;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setAnexos($anexos) {
        $this->anexos[] = $anexos;
    }
    function setCc($cc) {
        $this->cc = $cc;
    }
    function getCc() {
        return $this->cc;
    }
    function getObject() {
        return (object) array(
            'email' => $this->email,
            'nome' => $this->nome,
            'mensagem' => array("titulo" => $this->assunto, "mensagem" => $this->mensagem),
            'destinatario' => $this->destinatario,
            'cc' =>$this->cc, 
            'assunto' => $this->assunto,
            'anexos' => $this->anexos
        );
    }

}
