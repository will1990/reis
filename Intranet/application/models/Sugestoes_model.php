<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sugestoes_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);//
         $this->base = $this->load->database('base', TRUE);//pega informações 
    }

    public function envia_sugestao_form() {

        $data = (object) $this->input->post();
        $this->db->set('NOME',  $this->session->userdata('ROIntranetDados')->nome );
        $this->db->set('EMAIL', $this->session->userdata('ROIntranetDados')->email );
        $this->db->set('SUGESTOES',$data->sugestao);
        
        if($this->db->insert('tabela_sugestao'))
            
        {
            return TRUE;
            
        }else
        {
            return FALSE;
        }
    }
    public function pega_email($id)
    {   
       $this->base->select ('valor');//campo do banco 
       $this->base->where ('id_empregado',$id);
       $rts = $this->base->get ('rh_empregados_contatos')->row(); 
       return $rts->valor;//somente o email 
       
    }
    
   /* public function get_contratos($id)
    {
        print_r($id);
        $sql ="SELECT a.id_empregado, b.titulo cargo, c.titulo depto "
                . "FROM rh_empregados_contratos AS a INNER JOIN rh_cargos AS b ON a.id_cargo = b.id "
                . "INNER JOIN rh_departamentos AS c ON a.id_departamento = c.id  WHERE id_empregado =$id";
        $rst=$this->base->query($sql)->row();
        return $rst;
        
        
    }*/

    public function get_contratos($id)
    {
       // print_r($id);
        /*$sql="SELECT a.id_empregado, b.titulo cargo, c.titulo depto "
                . "FROM rh_empregados_contratos AS a INNER JOIN rh_cargos AS b ON a.id_cargo = b.id "
                . "INNER JOIN rh_departamentos AS c ON a.id_departamento = c.id  WHERE a.id_empregado =$id";*/
        
        $this->base->select("rh_empregados_contratos.id_empregado, rh_cargos.titulo cargo, rh_departamentos.titulo depto");
        $this->base->from("rh_empregados_contratos");
        $this->base->join('rh_cargos', 'rh_empregados_contratos.id_cargo = rh_cargos.id');
        $this->base->join('rh_departamentos', 'rh_empregados_contratos.id_departamento = rh_departamentos.id');
        $this->base->where('rh_empregados_contratos.id_empregado', $id);

        $rst = $this->base->get();
        $rst = $rst->row();
        //print_r($rst);
       // exit;
        return $rst;
    }
    
    }
