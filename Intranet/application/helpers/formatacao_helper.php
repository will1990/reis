<?php

//Páginas (Elogios)
function quebraTrechoDivisor($trecho, $divisor){
    if(strpos($trecho, $divisor) !== false):
        $trecho = explode($divisor, $trecho);
    endif;
    return $trecho;
}

//Páginas (Elogios)
function tratarDataView($trecho){
    if(strpos($trecho, "-") !== false):
        $dt = quebraTrechoDivisor($trecho, "-");
        return InverterData($dt);
    elseif(strpos($trecho, "/") !== false):
        $dt = quebraTrechoDivisor($trecho, "/");
        return InverterData($dt);
    else:
        return $trecho;
    endif;
}

//Páginas ()
function inverterData($dtarray){
    if(strlen($dtarray[0])>3):
        return $dtarray[2].'/'.$dtarray[1].'/'.$dtarray[0];
    else:
        return $dtarray[0].'/'.$dtarray[1].'/'.$dtarray[2];
    endif;        
}

