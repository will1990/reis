<?php 

function autoriza() {
	$ci = get_instance();
	$usuarioLogado = $ci->session->userdata("usuario_logado");
	if(!$usuarioLogado) {
		
		$ci->session->set_flashdata("danger", "Você precisa estar logado !");
		redirect("/");
	}
	
	return $usuarioLogado;
	
	
}

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

?>