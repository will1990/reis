<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Elogios extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model("OutrasPags_model", "mdoutras");       
    }   
    
    
    //ELOGIOS
    //Montando a Página de Elogios - PADRÃO    
    public function index() 
    {
        $this->data["elogios"] = $this->mdoutras->obter_elogios();
        $this->data["depoimentos"] = $this->mdoutras->obterDepoimentos();
               
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/elogios", $this->data);
        $this->load->view("template/footer");
        $this->load->view("welcome/js/tabsOutrasPags");
    }
}


