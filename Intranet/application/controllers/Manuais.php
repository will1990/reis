<<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manuais extends CI_Controller {

    /**
     * Intranet Reis Office
     * William Feliciano
     */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Manuais_model","mdmanual");
    }    
    
    public function index()
    {
        $this->data['test'] = $this->mdmanual->test_manual();
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/manuais", $this->data);
        $this->load->view("template/footer");        
    }
}
