<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//define("EVTPATH", "https://www.reisoffice.com.br/portalws/index.php/revenda/ws/eventos_site");
define("EVTPATH", "https://portal.reisoffice.com.br/revenda/ws/eventos_site");

class Eventos extends CI_Controller
{
    /*  Intranet Reis Office / EVENTOS
     * 
     *  William Feliciano
     */
    
    public function index()
    {
        $this->data["eventos"] = $this->formatar_eventos(EVTPATH);
                
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/eventos/lista", $this->data);
        $this->load->view("template/footer"); 
    }
    
    public function formatar_eventos($url = null)
    {
        //$arq = fopen($url,"r");
        $ch = curl_init();
        // informar URL e outras funções ao CURL

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // acessar a URL
        $conteudo = curl_exec($ch);
                        
        return (array) json_decode($conteudo);
    }
    
}