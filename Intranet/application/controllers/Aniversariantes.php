<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aniversariantes extends CI_Controller {
    /**
     * Intranet Reis Office
     * William Feliciano 
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Index_model","mdindex");
    }    
    
    public function index() 
    {
        //$this->data['nivers'] = $this->mdniver->obter_nivers();
        //$this->data["test"] = $this->getAniversariantes();
        
        $this->load->view('template/header');
        $this->load->view("template/navbar"); 
        $this->load->view('welcome/aniv/aniversariantes',$this->data);       
        $this->load->view('welcome/aniv/script',$this->data);
        $this->load->view('template/footer');
    }
    
    public function getAniversariantes() 
    {
        $mes = $this->input->post("mes");
        $mes = $mes === ''? 01 : $mes ;
        $this->data["funcionarios"] = $this->mdindex->get_pessoa($mes);
        $this->load->view('welcome/aniv/lista', $this->data);       
    }

}
