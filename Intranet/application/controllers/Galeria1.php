<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define("GALERYPATH", "/media/mkt-fotos/");

class Galeria1 extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    public function index()
    {
        $album = $this->input->get('album');
        $galeria = $this->input->get('galeria'); 
        $evento = $this->input->get('evento'); 
        
        
        $data["albuns"]=$this->albuns();     
        $data["galerias"]=$this->galerias($album);
        $data["eventos"]=$this->eventos($album,$galeria);
        $data["fotos"]=$this->grade_img($album,$galeria,$evento);
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/galeria1", $data);
        $this->load->view("template/footer");
        $this->load->view("welcome/js/scripts");
    }
    
     
        
    public function albuns()
    {
        $albuns = array();
                
        if($album_dir = opendir(GALERYPATH)):
            
            while(($dir = readdir($album_dir)) !== false):
            
                if(is_dir(GALERYPATH.$dir) && $dir != '.' && $dir != '..'):                
                    
                    $albuns[] = $dir;
                
                endif;                                
            endwhile;
        endif;
        
        return $albuns;
    }
    
    public function galerias($album)
    {
        $galerias = array();
        
        /*if($album)
        {*/       
            $album .= '/';

            if($galeria_dir = opendir(GALERYPATH.$album))
            {
                while(($gal = readdir($galeria_dir)) !== false):

                    if(is_dir(GALERYPATH.$album.$gal) && $gal != '.' && $gal != '..'):

                        $galerias[] = $gal;

                    endif;                
                endwhile;       
            }           
        //}
                        
        return $galerias;
                     
    }
    
    public function eventos($album, $galeria)
    {
        $eventos = array();
        
        /*if($galeria)
        {*/
            $album .= '/';
            $galeria .= '/';

            if($evento_dir = opendir(GALERYPATH.$album.$galeria)):

                while(($evt = readdir($evento_dir)) !== false):

                    if(is_dir(GALERYPATH.$album.$galeria.$evt) && $evt != '.' && $evt != '..'):

                        $eventos[] = $evt;

                    endif;
                endwhile;
            endif;
        //}
        
        return $eventos;
                   
    }
    
    public function grade_img($album, $galeria, $evento)
    {
        if($galeria || $evento)                    
        {
            $titulo = ($evento !== null)? $evento : $galeria ;
            
            $titulo = str_replace('_',' ',$titulo);
            
            $fotos = array();
            
            $pasta = $album.'/'.$galeria.'/';
            $galeria = $evento.'/';

            $count = 0;
            
            if($fotos_dir = opendir(GALERYPATH.$pasta.$galeria))
            {
                while(($dir3 = readdir($fotos_dir)) !== false)
                {
                    if($count <= 100)
                    {
                        if($dir3 != '.' && $dir3 != '..')
                        {
                            if(file_exists(GALERYPATH.$pasta.$galeria.$dir3))
                            {
                                $ext = strtolower(pathinfo(GALERYPATH.$pasta.$galeria.$dir3, PATHINFO_EXTENSION));
                                if($ext == 'jpg' || $ext == 'png' || $ext == 'gif')
                                {
                                    $path = str_replace("/","::",GALERYPATH.$pasta.$galeria.$dir3);
                                    $fotos[] = '<div class="col-md-4 col-sm-6 col-xs-12"><a data-fancybox-href="'.base_url("galeria1/imagem/".$path."/".$ext).'" data-fancybox-group="galeria" class="fancybox"><img src="'.base_url("galeria1/imagem/".$path."/".$ext).'" class="img-responsive img-rounded"></a><br></div>';
                                }
                            }
                        }
                        $count++;
                    }
                }              
            }
        }
        
        return array($this->fotos = $fotos, 
                     $this->titulo = $titulo);
    }
    
    public function imagem($path, $ext)
    {
        $path = str_replace("::","/",$path);
        if($ext == 'jpg')
        {
            $im = imagecreatefromjpeg(urldecode($path));
            header("Content-type: image/jpg");
            imagejpeg($im);
        }
    }
    
}

