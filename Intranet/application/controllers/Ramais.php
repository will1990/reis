<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ramais extends CI_Controller {

    /**
     * Intranet Reis Office
     * William Feliciano
     */
    function __construct() {
        parent::__construct();
        $this->load->model("Ramais_model", "mdramal");        
    }

    public function index() 
    {       
        $this->data["ramais"] = $this->mdramal->obterRamais();        
                
        $this->load->view("template/header");
        $this->load->view("template/navbar");        
        $this->load->view("welcome/ramais/ramais", $this->data);
        $this->load->view("template/footer");        
        $this->load->view("welcome/js/tabsOutrasPags", $this->data);
    }
    
    
   
}
