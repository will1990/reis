<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Teste extends CI_Controller {

    /**
     * Intranet Reis Office
     * William Feliciano
     */
    function __construct() {
        parent::__construct();
        $this->load->model("Teste_model", "mdteste");
    }

    public function acesso() {
        $this->load->view("template/header");
        $this->load->view("welcome/testes/login");
    }

    public function index() {
        //$data['teste'] = $this->mdteste->get_algo();

        //$data["test"] = get_post(616);
        
        //$args = array("category" => 26, "numberposts" => 20, "orderby" => "name", "order" => "ASC");
        //$data["elogio"] = get_posts($args);
                
        //echo md5('12345678');
        
        //echo rand(01,80);
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        //$this->load->view("welcome/testes/teste", $data);
        $this->load->view("template/footer");
        //$this->load->view("welcome/testes/jsteste", $data);
        
    }

    //deixarei temporariamente o log aqui by rafael
    public function log() {
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/log/log");
        $this->load->view("template/footer");
        
    }
      public function consultaLog() {
        $data['msg'] = $this->mdteste->getLog2();
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/log/log", $data);
        $this->load->view("template/footer");
        $this->load->view("welcome/deptos/js/tabsPrincipais");
    }
}
