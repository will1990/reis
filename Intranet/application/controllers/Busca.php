<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Busca extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * Cintia Masumi
     */
    
    function __construct() {
        parent::__construct();
       
        $this->load->model("Busca_model", "m_busca");
        
    }   

    public function index() 
    {   
        
        
        $this->data['s'] = $this->input->post('s');
        $this->data['resultado'] = $this->m_busca->buscar($this->data['s']);
        
        
        $this->load->view("template/header");
        $this->load->view("template/navbar"); 
        $this->load->view("welcome/busca/resultado", $this->data);
        $this->load->view("template/footer");
    }
    
   /* public function refina()
    {
        if( get_cat_name($item->categoria = get_the_category($item->ID)[0]->term_id))
        {        }
    }*/
    

}