<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cardapio extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model("OutrasPags_model", "mdoutras");       
    }   
    
    
    //CARDÁPIO CUCINARE
    //Montando a Página de Cardápio - PADRÃO    
    public function index() 
    {
        $conteudo = get_post(1173);        
        $conteudo = $conteudo->post_content;
        
        if(strpos($conteudo, "[table id=") !== false)
        {           
            $tab = $this->mdoutras->obter_tabelas_gerais($conteudo);
            $tabela = $this->mdoutras->montar_tabelas_gerais($tab["tabela"]);
            
            $conteudo = str_replace("[table id=".$tab['id']." /]", $tabela, $conteudo);           
        }
        
        if(strpos($conteudo, "<img ") !== false)
        {
            $conteudo = str_replace('<img ', '<img class="img-responsive" ', $conteudo);
        }
        
        if(strpos($conteudo, "&nbsp;") !== false)
        {
            $conteudo = str_replace("&nbsp;", "<p class='espacamento'></p>", $conteudo);      
        }
        
        $this->data["conteudo"] = $conteudo;
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/cardapio", $this->data);
        $this->load->view("template/footer");
        $this->load->view("welcome/js/tabsOutrasPags");
    }
}
