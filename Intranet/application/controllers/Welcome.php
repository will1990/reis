<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{    
    /*  Intranet Reis Office / INDEX
     * 
     *  William Feliciano
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model("Index_model", "m_index");
        $this->load->model("Noticias_model", "mdnews");
        $this->data['aniversariantes'] = $this->m_index->get_pessoa();    
    }
    
    public function index()
    {
        $this->data["categ"]= $this->m_index->get_all_categories();
        $this->data["banners"] = $this->m_index->get_banners();
        $this->data["posts"] = $this->m_index->get_posts();        
        $this->data["boletim"] = $this->m_index->obter_boletins();
        $this->data["destaque"] = $this->m_index->obter_ultima_foto();
        $this->data["noticias"] = $this->mdnews->obter_noticias_recentes(2);
                
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/index", $this->data);
        $this->load->view("template/footer");
        $this->load->view("welcome/js/scripts");
    }
    
}
