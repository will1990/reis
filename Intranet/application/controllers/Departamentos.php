<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Departamentos extends CI_Controller {

    /* Intranet Reis Office / DEPTOS 
     * 
     * Rafael Oliveira
     * William Feliciano     
     */
    
    private $menu;
   
    function __construct() {
        parent::__construct();
        //$this->load->helper("conteudo");
        $this->load->model("Deptos_model","m_deptos");
        $this->menu = $this->m_deptos->get_menu(); 
        $this->load->model("Departamentos_model_forms", "m_depto"); 
        $this->load->model("OutrasPags_model", "mdsalas");      
    }
    
    public function valida_form_dinamico()
    { 
        if (!empty(filter_input(INPUT_POST, 'metodo'))){
            $metodo = filter_input(INPUT_POST, 'metodo');
            if($metodo === 'pesq_salas')
            {                
                $dtdia = $this->input->post('data_salas');                
            }
            else
            {
                if (method_exists($this->m_depto, $metodo)) {
                    $str = $this->m_depto->{$metodo}();
                } else {
                    $str = "pagina não existe";
                }
            }
        } else {
            $str = "pagina não informada";
        }      
        
        if($metodo === 'pesq_salas')
        {            
            /*if(!empty($this->mdsalas->obter_reservas($dtdia)))
            {*/
                $this->data["reservas"] = $this->mdsalas->obter_reservas($dtdia);
                $this->load->view("welcome/deptos/conteudo_tab_salas", $this->data);
            /*}
            else 
            {
                ?>
                <div class='alert alert-success'><h4 style="color: #346321;">Sem Agendamentos!</h4></div>
                <?php
            } */ 
        }
        else
        {
            if(!empty(validation_errors()))
            {
                echo "<div class='alert alert-danger'><strong>Atenção!</strong> <br><br>";
                echo validation_errors();
                echo "</div> ";
            }
            else
            {
                echo "<div class='alert alert-info'><strong>Sucesso!</strong> <br><br>";
                echo $str;
                echo "</div> ";
            }            
        }
    }

    //DEPTOS GERAL
    //Montando a Página de Deptos - PADRÃO
    public function index($id = 0, $sub_id = '') 
    {
        $menu["menu"] = $this->show_menu_fixo($this->menu, $id);
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/deptos/menu_deptos", $menu); 
        
        if($id == 0)
        {
            $this->load->view("welcome/deptos/index_deptos");
        }
        else 
        {
            $data["sub"] = ($sub_id) ? base_url("departamentos/pagina/$sub_id") : "";
            $data["conteudo"] = $this->paginas_pai($id);
            $this->load->view("welcome/deptos/departamentos", $data);             
        } 
        
        $this->load->view("template/footer");
        $this->load->view("welcome/js/pagina", $data); 
        $this->load->view("welcome/deptos/js/tabsPrincipais", $data);
    }

    //LADO DIREITO
    //Montando o Conteúdo da Pagina Principal dos Deptos - PAGINAS MÃE
    public function paginas_pai($id = 0)
    {
        $query = $this->m_deptos->obt_tit_cont_depto($id);
        $conteudo = $query->post_content;
        
        if(strpos($conteudo, "[table id=") !== false)
        {
            $tab = $this->m_deptos->obter_tabelas_principais($conteudo);
            $tabela = $this->m_deptos->montar_tabelas_principais($tab["tabela"]);
            
            $conteudo = str_replace("[table id=".$tab['id']." /]", $tabela, $conteudo);           
        }
                
        return array("conteudo" => $conteudo, "titulo" => $query->post_title);       
    }
    
    //Montando o Conteúdo das Páginas Pertecentes aos Deptos - PAGINAS FILHAS
    public function pagina($id = 0) 
    {
        $this->load->view("welcome/js/scripts");
        
        $query = $this->m_deptos->obt_tit_cont_depto($id);
        $conteudo = $query->post_content;
                
        if($id == 84)
        {            
            $this->data["boletim"] = $this->m_deptos->obter_boletins();
            $this->load->view("welcome/deptos/conteudo_boletim",$this->data);
            $conteudo = '';            
        }
        
        if($id == 497)
        {                       
            $this->data["conteudo"] = $conteudo;           
            $this->load->view("welcome/deptos/conteudo_pesq_salas", $this->data);
            $this->load->view("welcome/deptos/conteudo_form_sala", $this->data);
            $conteudo = '';            
        }

        if($id == 559)
        {
            $this->load->view("welcome/deptos/conteudo_form_arte", $this->data);
            $conteudo = '';
        }
        
        if($id == 557)
        {            
            $this->load->view("welcome/deptos/conteudo_form_brinde", $this->data);
            $conteudo = '';
        }
        
        if($id == 2586)
        {
            $this->data["condicoes"] = $this->m_deptos->obter_condicoes();
            $this->load->view("welcome/deptos/cond_comercial_abas", $this->data);
            $this->load->view("welcome/deptos/cond_comercial_pages", $this->data);
            $this->load->view("welcome/deptos/js/abasCondComerciais");
            $conteudo = '';
        }
        
        if(strpos($conteudo, "[table id=") !== false)
        { 
            $tab = $this->m_deptos->obter_tabelas_principais($conteudo);
            $tab["conteudo"] = $conteudo;
            $this->load->view("welcome/deptos/conteudo_tables", $tab);            
            $conteudo = '';
        }
        else    
        {            
            $this->data["conteudo"] = $conteudo;
            $this->load->view("welcome/deptos/conteudo_outros", $this->data);
        }  
        
        return array("conteudo" => $conteudo, "titulo" => $query->post_title);
    }
    
    
    //LADO ESQUERDO - MENU
    //Montando o Menu das Principais Páginas - PAGINAS MÃE
    public function show_menu_fixo($arr_menu, $id_pai = 0) {
        $menu = "";

        $div1 = '';
        $active = '';
        $parent = '#MainMenu';

        foreach ($arr_menu[0] as $id_menu => $item) {
            $active = ($id_menu == $id_pai) ? "active" : "";
                        
            $href = base_url("departamentos/index/$id_menu#conteudo"); //"#item$id_menu";
            $menu.=$div1 . '<a href="' . $href . '" class="list-group-item ' . $active . '" data-toggle="" data-parent="' . $parent . '">' . $item["titulo"];            
            if (isset($arr_menu[$id_menu]) && $id_menu == $id_pai && $id_pai > 0) {
                //$parent='#SubMenu1';
                $div1 = '';
                $menu.='</a>' . $this->show_menu($arr_menu, $id_pai);
                $conteudo = '#subM';
            } else {
                $menu.='</a>';
                $div1 = '';
                $parent = '#MainMenu';
                //$menu.='</div>';
            }

            //$menu.='</div>';
        }

        //$menu .='</div>';

        return $menu;
    }

    //Montando o Menu das Páginas Pertencentes aos Deptos (Java) - PAGINAS FILHAS
    public function show_menu($arr_menu, $id_pai = 0, $base = 0) {
        $menu = ($base > 0) ? '<div class="collapse" style="border-radius:0px;" id="item' . $id_pai . '">' : "";

        foreach ($arr_menu[$id_pai] as $id_menu => $item) {
            //$active = ($item->ID == @$id) ? "active" : "" ;
            $collapse = (isset($arr_menu[$id_menu]) AND (in_array($id_menu, $this->m_deptos->filhasrenegadas()) !== TRUE)) ? "collapse" : "";


            $div1 = '<div style="border-radius:0px">';
            $parent = '#SubMenu1';

            //$href = (isset($arr_menu[$id_menu])) ? (($id_menu == 84) ? base_url("departamentos/pagina/$id_menu") : "#item$id_menu") : base_url("departamentos/pagina/$id_menu");
            //$bold = ((isset($arr_menu[$id_menu])) && ($id_menu != 84)) ? "<b>" : " ";
            //$linkMenu = (isset($arr_menu[$id_menu])) ? (($id_menu == 84) ? "linkMenu" : "" ) : "linkMenu";
            
            if(isset($arr_menu[$id_menu]) AND (in_array($id_menu, $this->m_deptos->filhasrenegadas()) !== TRUE))
            {
                $href = "#item$id_menu";
                $linkMenu = " ";
                $bgcolor = "";
                $ftcolor = "";
                $subactive = "list-group-item-warning active";
            }
            else
            {
                $href = base_url("departamentos/pagina/$id_menu");
                $linkMenu = "linkMenu";
                $bgcolor = "background-color:#FCEDD8;";
                $ftcolor = "color:#E47B38";
                $subactive = "";
            }
                        
            $menu.= $div1 . '<a href="' . $href . '" style="border-radius:0px;  '.$bgcolor.$ftcolor.'" class="' . $linkMenu . ' list-group-item '.$subactive.'" data-toggle="' . $collapse . '" data-parent="' . $parent . '">' . str_repeat("-", $base) . ">" . $item["titulo"];
            if ((isset($arr_menu[$id_menu])) AND (in_array($id_menu, $this->m_deptos->filhasrenegadas()) !== TRUE)) {
                //$parent='#SubMenu1';
                $div1 = '';
                $menu.='<i class="fa fa-caret-down pull-right"></i></a></b>' . $this->show_menu($arr_menu, $id_menu, $base + 1);
            } else {
                $menu.='</a>';
                $div1 = '';
                $parent = '#MainMenu';
                //$menu.='</div>';
            }

            $menu.='</div>';
        }

        $menu .= ($base > 0) ? '</div>' : "";

        return $menu;
    }   
}
