<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller
{
    /*  Intranet Reis Office / NOTICIAS
     * 
     *  William Feliciano
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Noticias_model","mdnews");
    }
    
    public function index()
    {
        $this->data["noticias"] = $this->mdnews->obter_noticias_recentes();
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/noticias/lista", $this->data);
        $this->load->view("template/footer");        
    }
    
    public function ler($id = null)
    {       
        $this->data["noticia"] = $this->mdnews->obter_noticia($id);
                
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/noticias/noticia", $this->data);
        $this->load->view("template/footer"); 
    }
}