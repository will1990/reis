<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informacoes extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model("OutrasPags_model", "mdoutras");       
    }   
    
    
    //INFORMAÇÕES UTEIS
    //Montando a Página de Informações - PADRÃO    
    public function index($act)            
    {
        $this->data["act"] = (!empty($act))? $act : "";
        $this->data["faqs"] = $this->mdoutras->obter_faqs();      
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/informacoes", $this->data);
        $this->load->view("template/footer");
        $this->load->view("welcome/js/tabsOutrasPags");
    }
}