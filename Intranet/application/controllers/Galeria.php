<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define("GALERYPATH", "/media/mkt-fotos/");

class  Galeria extends CI_Controller
{    
    /**
     * Intranet Reis Office
     * William Feliciano
     */
    
    
    //private $menu;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("galeria_model","m_gal");  
    }

    //GALERIA FOTOS
    //Montando a Página de Galeria - PADRÃO
    public function index($pasta1 = '',$pasta2 = '',$pasta3 = '')  
    {       
        $this->data = $this->obter_conteudo($pasta1,$pasta2,$pasta3);
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/galeria/ini_container_galeria");
        $this->load->view("welcome/galeria/menu_galeria",$this->data);  
                
        if($pasta2 && $pasta3 == '')
        {
            $this->data["titulo"] = $this->data["pasta2"];
            $this->load->view("welcome/galeria/grade_fotos",$this->data);
        }
        elseif($pasta2 && $pasta3)
        {
            $this->data["titulo"] = $this->data["pasta3"];
            $this->load->view("welcome/galeria/grade_fotos",$this->data);
        }
        else
        {
            $this->load->view("welcome/galeria/index_galeria"); 
        }      
        
        $this->load->view("welcome/galeria/fim_container_galeria");
        $this->load->view("template/footer");
        $this->load->view("welcome/js/scripts");
    }
    
    //Obter as Pastas em Cada Nível - MENU
    private function obter_conteudo($pasta1 = '',$pasta2 = '',$pasta3 = '')
    {
        $link = GALERYPATH;        
        $this->data["pastas1"] = $this->m_gal->obter_menu_gal($link);
        
        $link .= $pasta1;
        $link .= '/';
        $this->data["pastas2"] = $this->m_gal->obter_menu_gal($link);
        $this->data["pasta1"] = $pasta1;
        
        $pasta2 = urldecode(utf8_encode(utf8_decode($pasta2)));
        $link .= $pasta2;
        $link .= '/';
        $this->data["pastas3"] = $this->m_gal->obter_menu_gal($link);
        $this->data["fotos"] = $this->montar_grade_img($link);
        $this->data["pasta2"] = $pasta2;
                
        $pasta3 = urldecode(utf8_encode(utf8_decode($pasta3)));
        $link .= $pasta3; 
        $link .= '/';        
        $this->data["pastas4"] = $this->m_gal->obter_menu_gal($link);
        $this->data["fotos"] = $this->montar_grade_img($link);                    
        $this->data["pasta3"] = $pasta3;  
        
        return $this->data;       
    }    
    
    //LADO DIREITO
    //Montando a Grade de Fotos
    private function montar_grade_img($link)
    {
        $fotos = array();
        
        $count = 0;  
        
        $fotos_cnt = opendir($link);
        if($fotos_cnt !== false)
        {
            // Conta quantas fotos exesitem no diretorio
            $arquivos = 0;
            while(($dir_count = readdir($fotos_cnt)) !== false)             
            {
                if($dir_count != '.' && $dir_count != '..')
                    $arquivos++;
            }
            closedir($fotos_cnt);
        }
        
        $fotos_dir = opendir($link);
        if($fotos_dir !== FALSE && $arquivos > 0)
        {   
            // Divide a quantidade de fotos em 3 colunas
            $colunas = array(floor($arquivos / 3), floor($arquivos / 3), floor($arquivos / 3));
            $sobra = $arquivos % 3;
            for($c = 0; $c < $sobra; $c++)
                $colunas[$c] += 1;  
            
            $arquivos_coluna = 0;
            $coluna_atual = 0;
            while(($dir3 = readdir($fotos_dir)) !== false)             
            {     
                if($count <= 200)
                {
                    if($dir3 != '.' && $dir3 != '..')
                    {
                        if(file_exists($link.$dir3))
                        {
                            $ext = strtolower(pathinfo($link.$dir3, PATHINFO_EXTENSION));
                            if($ext == 'jpg' || $ext == 'png' || $ext == 'gif')
                            {
                                /*list($largura, $altura) = getimagesize($link.$dir3);
                                if($altura > 1200)
                                {
                                    $dir3 = rename($link.$dir3, $link.'w_'.$dir3);
                                    $path = str_replace("/","::",$link.$dir3);
                                }
                                else 
                                {*/
                                    $path = str_replace("/","::",$link.$dir3);                                                      
                                //}
                                //$fotos[] = '<div class="col-md-4 col-sm-6 col-xs-12"><a href="#" data-fancybox-href="'.base_url("galeria/imgopen/".$path."/".$ext).'" data-fancybox-group="galeria" class="fancybox"><img src="'.base_url("galeria/imagem/".$path."/".$ext).'" alt="test" class="img-responsive img-rounded"></a><br></div>';
                                $fotos[$coluna_atual][] = '<div class="img-responsive"><a href="#" data-fancybox-href="'.base_url("galeria/imgopen/".$path."/".$ext).'" data-fancybox-group="galeria" class="fancybox"><img src="'.base_url("galeria/imagem/".$path."/".$ext).'" alt="Imagem" class="img-responsive img-rounded"></a><br></div>';
                                $arquivos_coluna++;
                                if($arquivos_coluna == $colunas[$coluna_atual])
                                {
                                    $arquivos_coluna = 0;
                                    $coluna_atual++;
                                }
                            }                            
                        }
                    }
                    $count++;
                }
            }              
        }      
                
        return $fotos;
    }
    
    /*public function obter_tamanho($path)
    {
        $imagem = str_replace("::","/",$path);
        
        
        // vamos obter as dimensões da imagem
        list($largura, $altura) = getimagesize($imagem);
                
        
        // exibe as informações
        $echo = "largura: " . $largura . " pixels 
              altura: " . $altura . " pixels";
        return $echo;
    }*/
    
    
    
    //IMAGEM
    //Obtendo Imagem para a Grade
    public function imagem($path, $ext)
    {
        $path = str_replace("::","/",$path);
          
        if($ext == 'jpg')
        {
            $im = imagecreatefromjpeg(urldecode($path));
            
            /*$largura = imagesX($im);
            $altura = imagesY($im);
            
            if($altura > 1600)
            {
                $nv_altura = 1200;
                $nova = ImageCreateTrueColor($largura, $nv_altura);
                imagecopyresampled($nova, $im, 0, 0, 0, 0, $largura, $nv_altura, $largura, $altura);
                
                $this->mostrar_img_jpg($nova);                
            }
            else
            { */          
                $this->mostrar_img_jpg($im); 
            //}
        }
        if($ext == 'png')
        {
            $im =  imagecreatefrompng(urldecode($path));
            $this->mostrar_img_png($im);
        }
        if($ext == 'gif')
        {
            $im =  imagecreatefromgif(urldecode($path));
            $this->mostrar_img_gif($im); 
        }
    }
    
    //Obtendo Imagem para Visualização - FANCYBOX
    public function imgopen($path, $ext)
    {
        $path = str_replace("::","/",$path);
        if($ext == 'jpg')
        {
            $im = imagecreatefromjpeg(urldecode($path));
            $this->mostrar_img_jpg($im);
        }
        if($ext == 'png')
        {
            $im =  imagecreatefrompng(urldecode($path));
            $this->mostrar_img_png($im);
        }
        if($ext == 'gif')
        {
            $im =  imagecreatefromgif(urldecode($path));
            $this->mostrar_img_gif($im); 
        }
    }
    
    //Saída da Imagem (pelo Formato)
    public function mostrar_img_jpg($img)
    {
        header("Content-type: image/jpg");
        imagejpeg($img); 
    }
    public function mostrar_img_png($img)
    {
        header("Content-type: image/png");
        imagepng($img); 
    }
    public function mostrar_img_gif($img)
    {
        header("Content-type: image/gif");
        imagegif($img); 
    }
}
    
    
