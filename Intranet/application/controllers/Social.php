<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends CI_Controller 
{
    /*  Intranet Reis Office / SALAS
     * 
     *  William Feliciano
     */
    
    function __construct() {
        parent::__construct();
        $this->load->model("Social_model", "mdsocial");      
    }
    
    public function index($act = null, $id = null)
    {
        /*if($act == 'view')
        {*/
            $doacoes = $this->mdsocial->obter_doacoes(); 
            $act = 'view';
        /*}
        else
        {
            $doacoes = '';
            $act = '';
        }*/
        
        if($id !== null)
        {
            $conteudo = $this->mdsocial->obter_pag_social($id);
            $act2 = $id;
        }
        else
        {
            $conteudo = $this->mdsocial->obter_pag_social();
            $act2 = null;
        }
        
        $conteudo->post_content = $this->tratar_conteudo($conteudo->post_content); 
                
        //$this->data["projetos"] = $this->mdsocial->obter_projetos();
        $this->data["act"] = $act;
        $this->data["act2"] = $act2;
        $this->data["doacoes"] = $doacoes; 
        $this->data["index"] = $conteudo;
        
        $this->load->view("template/header");
        $this->load->view("template/navbar");
        $this->load->view("welcome/social/ini_container_social", $this->data);
        
        $this->load->view("welcome/social/menu_social", $this->data);       
        $this->load->view("welcome/social/index_social", $this->data);
        
        $this->load->view("welcome/social/fim_container_social");
        $this->load->view("template/footer"); 
    }  
    
    public function tratar_conteudo($conteudo = null)
    {
        if(strpos($conteudo, "[table id=") !== false)
        {
            $this->load->modal("OutrasPags_modal", "mdoutras");
            
            $tab = $this->mdoutras->obter_tabelas_gerais($conteudo);
            $tabela = $this->mdoutras->montar_tabelas_gerais($tab["tabela"]);
            
            $conteudo = str_replace("[table id=".$tab['id']." /]", $tabela, $conteudo);           
        }
        
        if(strpos($conteudo, "<img ") !== false)
        {
            $conteudo = str_replace('<img ', '<img class="img-responsive" ', $conteudo);
        }
        
        if(strpos($conteudo, "&nbsp;") !== false)
        {
            $conteudo = str_replace("&nbsp;", "<p class='espacamento'></p>", $conteudo);      
        }
        return $conteudo;
    }
}

