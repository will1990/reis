        <style>    
            .container{
                background-image: url('<?=base_url("images/img_index/fundo_login.png")?>');
                background-repeat: no-repeat;
                background-size: cover;                
            }            
        </style>
        <div class="container" style="height: 920px;">
            <div class="col-md-12" style="height: 310px;"></div>
            <div class="col-md-12">
                <?=$message?>
                <?=validation_errors()?>
                <form class="form-horizontal" action="<?=base_url("login")?>" method="POST">
                    <div class="input-group input-group-lg col-md-4 col-md-offset-4">
                        <span class="input-group-addon" id="basic-addon1">Reis Office:</span>        
                        <select class="form-control" id="empresa" name="empresa" required>
                            <option value="1" <?= (set_value("empresa") == "1") ? "selected" : ""?>>Comercial</option>
                            <option value="2" <?= (set_value("empresa") == "2") ? "selected" : ""?>>Serviços</option>
                        </select>
                    </div>
                    <br/>
                    <div class="input-group input-group-lg col-md-4 col-md-offset-4">
                        <span class="input-group-addon" id="basic-addon1">Login:</span>        
                        <input class="form-control" name="usuario" type="text" placeholder="Matrícula ou CPF" value="<?=set_value("usuario")?>" />
                    </div>
                    <br/>
                    <div class="input-group input-group-lg col-md-4 col-md-offset-4">
                        <span class="input-group-addon" id="basic-addon1">Senha:</span>        
                        <input class="form-control" name="senha" type="password" placeholder="Senha" />
                    </div>
                    <br/>
                    <?php if($this->session->userdata("ROIntranetInterno")): ?>                    
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group text-right">
                                <label class="checkbox"><input type="checkbox" name="senha_lemb" /> Lembrar senha</label>                      
                            </div>     
                        </div>
                        <br/>
                    <?php endif; ?>                    
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group text-center">
                            <a class="btn btn-default" style="width: 150px;" href="<?=base_url("registro/senhas")?>">Esqueci a Senha</a>&nbsp;&nbsp;&nbsp;
                            <button class="btn btn-primary" style="width: 80px;" name="submit" type="submit">Entrar</button>                        
                            <input name="destino" type="hidden" value="<?=set_value("destino", $destino)?>" />
                        </div>              
                    </div>
                </form> 
            </div>
        </div>
    </body>
</html>





