<div class="container">
    <div class="col-md-4 col-md-offset-4">
        <p style="line-height: 100px">&nbsp;</p>
        <form class="form-horizontal" method="post" action="<?php echo site_url("admin/index") ?>">
            <?php //echo $message ?>
            <?php //echo validation_errors(); ?>
            <fieldset>
                <!-- Sign In Form -->
                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="usuario">Informe seu CPF ou Nº de Matrícula</label>
                    <div class="controls">
                        <input required="" id="usuario" name="usuario" type="text" class="form-control" placeholder="Nº de Matrícula ou CPF" class="input-medium" value="<?php echo set_value("usuario") ?>" required="">
                    </div>
                </div>

                <!-- Password input-->
                <div class="control-group">
                    <label class="control-label" for="senha">Senha:</label>
                    <div class="controls">
                        <input required="" id="senha" name="senha" class="form-control" type="senha" placeholder="Informe a senha" class="input-medium">
                    </div>
                </div>

                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="signin"></label>
                    <div class="controls">
                        <button id="signin" name="signin" class="btn btn-success">Entrar</button>
                        <a href="" class="pull-right">Esqueci a Senha</a>
                    </div>
                    <?php if($this->session->userdata("ROIntranetInterno")): ?>
                        <div class="controls text-center">   
                            <p>&nbsp;</p>
                            <a href="http://192.168.0.89/wordpress/wp-admin" class="btn btn-lg btn-info">Administração da Intranet</a>                       
                        </div>
                    <?php endif; ?>
                </div>
            </fieldset>
        </form>
        <p style="line-height: 100px">&nbsp;</p>
    </div>
</div> <!-- /container -->