<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">NOTÍCIAS</h1>
        </div>
    </div>
    <div class="container">
        <br/><br/>
        <div class="row">
            <h2 class="media-heading col-md-10 titulo_noticia"><?=$noticia->nm_noticia?></h2>
            <h4 class="col-md-2 text-right" style="margin-top: 10px;"><i class="fa fa-calendar"></i>&nbsp;&nbsp;<small><?=$noticia->data?></small><br/></h4>
        </div>
        <hr/>
        <section style="font-size: 14pt; margin-top: -15px;">
            <?=$noticia->noticia;?>       
        </section>
        <br/>
        <a href="<?=base_url("noticias")?>" class="btn btn-primary btn-lg">Voltar</a>   
        <br/><br/><br/><br/>
    </div>
</div>