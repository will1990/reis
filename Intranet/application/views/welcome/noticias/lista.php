<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">NOTÍCIAS</h1>
        </div>
    </div>
    <div class="container">        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="geral">
                <br/>
                <?php
                //print_r($noticias);
                foreach ($noticias as $noticia)
                {                    
                    ?>
                    <div class="media container">
                        <div class="media-left media-middle">
                            <a href="<?=base_url("noticias/ler/$noticia->cd_noticia")?>">
                                <img class="media-object" src="<?=$noticia->img?>" style="margin: 5px 10px 30px auto;" width="110" alt="<?=$noticia->nm_noticia?>">                                                                
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="<?= base_url("noticias/ler/$noticia->cd_noticia")?>" class="post_link">
                                <h3 class="media-heading"><?=$noticia->nm_noticia?></h3>
                                <h4 style="margin-top: 10px;">
                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;<small><?=$noticia->data?></small><br/>                                    
                                </h4>
                            </a>
                        </div>
                    </div>
                    <hr/>
                    <?php
                }
                ?>
            </div> 
        </div>      
        <div class="col-md-12">
            <a href="https://www.reisoffice.com.br/noticias.asp" target="_blank" class="btn btn-primary btn-lg">Ver Todos</a>            
            <br/><br/><br/><br/> 
        </div>
    </div>
</div>