
<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">ANIVERSARIANTES DE REIS OFFICE</h1>
        </div>
    </div>
    <div class="container">
        <br/>
        <div class="panel-group" id="accordion">
            <ul class="nav nav-tabs">
                <li class="active" style="width: 100px;"><a data-toggle="tab" href="#menu0">Home</a></li>
                <?php
                $meses = array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");

                $cont = 1;
                foreach ($meses as $mes)
                {
                                
                    echo "<li><a data-toggle='tab' onclick='busca_aniversariante($cont)' href='#menu$cont'>$mes</a></li>";                    
                    $cont++;
                }
                ?>           
            </ul>

            <div class="tab-content">
                <div id="menu0" class="tab-pane fade in active text-center">
                    <br/>
                        <h2> Escolha um Mês&nbsp;&nbsp;&nbsp;↑&nbsp;↑</h2>
                    <br/>
                </div>
                <?php
                
                for($num=1;$num<=12;$num++)
                {
                    echo "<div id='menu$num' class='tab-pane fade text-center'> <br/><br/><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i>  </div>";
                }
                
                ?>        
            </div>            
        </div>
    </div>    
</div>