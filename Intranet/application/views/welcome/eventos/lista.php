<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">EVENTOS</h1>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12">            
            <br/>       
            <?php
            $meses = array("JAN","FEV","MAR","ABR","MAI","JUN","JUL","AGO","SET","OUT","NOV","DEZ");

            foreach ($eventos as $ordem)
            {
                foreach ($ordem as $evento)
                {
                    foreach($evento->DATAS as $data)
                    {                         
                        list($dia, $mes, $ano) = explode("/", $data->DATA_EVENTO);
                        $mes = $meses[$mes-1];
                        $hrfim = ($data->HORA_TERMINO !== '')? ' às '.$data->HORA_TERMINO :'';
                        ?>
                        <div class="row">
                            <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1 text-center" style="background-color: #e47b38;">                                
                                <h1 style="color: #fff; margin-bottom: -15px; margin-top: 5px;"><?=$dia?></h1>
                                <h3 style="color: #fff;"><?=$mes?></h3>
                            </div>
                            <div class="col-xs-9 col-sm-10 col-md-11 col-lg-11">                                
                                <h3 class="media-heading"><?=$evento->TITULO?></h3>
                            </div>    
                            <div class="col-xs-12 col-sm-10 col-md-11 col-lg-11">    
                                <h4>
                                    <?=$evento->LOCAL?> / <?=$evento->FORMATO?>
                                    <br/>
                                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;<small><?=$data->HORA_INICIO.$hrfim?> hs</small>                                        
                                </h4>                                
                            </div>
                        </div>
                        <hr/>
                        <?php
                    }
                }
            }

            //echo '<pre>';
            //print_r($eventos);
            //echo '</pre>';

            ?>
        </div>        
    </div>
</div>