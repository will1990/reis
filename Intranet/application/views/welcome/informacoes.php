<?php
/*
* Intranet Reis Office
* William Feliciano
*/
?>
<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">INFORMAÇÕES ÚTEIS</h1>
        </div>
    </div>
    
    <!-- Layout Simplificado  -->
    <div class="container">
        <br>
        <div class="col-md-12">
            <p> Nesta página segue relação das dúvidas mais comuns dos colaboradores. 
                Observe que algumas das respostas direcionam o colaborador ao procedimento 
                adequado ou a páginas onde podem elucidar as questões. </p>
        </div>
        <br>
        <br>
        <?php
        //print_r($faqs);
             
        if(!empty($faqs))
        {            
            ?>
            <br/>
            <div class="panel-group" id="accordion">
                <?php
                $total = count($faqs);
                for($fq=1;$fq<=$total;$fq++)
                {
                    $in = $fq == $act ? "in" : "";
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$fq?>"><?=$faqs[$fq]->post_title?></a>
                            </h4>
                        </div>
                        <div id="collapse<?=$fq?>" class="panel-collapse collapse <?=$in?>">
                            <div class="panel-body">
                                <?=$faqs[$fq]->post_content?> 
                            </div>
                        </div>
                    </div>
                    <?php            
                }
                ?>
            </div>
            <br/>
            <?php
        }
        else
        {
            echo "<br/><div class='col-md-12'><h1>Sem Informações!</h1></div><br/><br/>";
        } 
        ?>  
    </div>    
</div>


