<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">            
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    /**
                     * Intranet Reis Office
                     * William Feliciano
                     */
                    foreach ($banners as $key => $banner):
                        ?>                    
                        <div class="item <?= ($key == 0) ? "active" : "" ?>">
                            <a href='<?= $banner["base_url"]; ?>'>                           
                                <?= $banner["conteudo"]; ?>                                
                            </a>
                        </div>

                    <?php endforeach; ?>    

                </div>
                <a href="#carousel" class="left carousel-control" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a href="#carousel" class="right carousel-control" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
            <!-- <pre><//?php print_r($banners) ?></pre> -->
        </div>
    </div>   
        
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                    <!-- Mural de Destaques / ULTIMOS POSTS PUBLICADOS E QUE TENHAM IMAGENS -->                    
                    <div class="row">
                        <h3 class="col-xs-12 col-md-12 col-lg-12">DESTAQUES</h3>
                    </div>                    
                    <div class="row">
                        <div class="text-center col-xs-12 col-sm-4 col-md-2 col-lg-4 img-responsive">
                            <a href='<?= base_url("comunicados/id/{$destaque[0]['ID']}") ?>'><?=$destaque[0]["foto"]?></a>                     
                        </div>
                        <div class="visible-xs" style="margin-top: 10px; margin-botton: 10px;">&nbsp;</div>
                        <div class="text-center col-xs-12 col-sm-4 col-md-offset-2 col-md-2 col-lg-offset-0 col-lg-4 img-responsive">
                            <a href='<?= base_url("comunicados/id/{$destaque[1]['ID']}") ?>'><?=$destaque[1]["foto"]?></a> 
                        </div>
                        <div class="visible-xs" style="margin-top: 10px; margin-botton: 10px;">&nbsp;</div>
                        <div class="text-center col-xs-12 col-sm-4 col-lg-4 img-responsive visible-xs visible-sm visible-lg">
                            <a href='<?= base_url("comunicados/id/{$destaque[2]['ID']}") ?>'><?=$destaque[2]["foto"]?></a> 
                        </div>                        
                    </div>      
                    <br/>
                    <br/>
                    
                    <!-- Mural de Publicações dos Setores / ULTIMOS POSTS PUBLICADOS EM GERAL -->                    
                    <div class="row">
                        <div class="col-md-12">
                            <h3>TUDO QUE ACONTECE NA REIS OFFICE</h3>                            

                            <!-- SELECT -->
                            <select class="form-control" name="opt">
                                <option value="geral">Todos os Departamentos</option>
                                <?php
                                foreach ($categ as $cat):
                                    if ($cat->category_nicename != "sem-categoria") {
                                        ?>                        
                                        <option value="<?= $cat->category_nicename ?>"><?= $cat->cat_name ?></option>                        
                                        <?php
                                    }
                                endforeach;
                                ?>                        
                            </select> 

                            <h3 class="espaco"> </h3>

                            <!-- LISTA DE ABAS -->
                            <ul class="nav nav-tabs hide" role="tablist">
                                <li role="presentation" class="active"><a href="#geral" id="link_geral" aria-controls="geral" role="tab" data-toggle="tab">Todos os Deptos.</a></li>
                                <?php
                                //$cats = get_categories(array('orderby' => 'name', 'parent' => 0, 'hide_empty' => 0));                    
                                foreach ($categ as $cat):
                                    if ($cat->category_nicename !== "sem-categoria") {
                                        ?>                        
                                        <li role="presentation">
                                            <a href="#<?= $cat->category_nicename ?>" id="link_<?= $cat->category_nicename ?>" aria-controls="t1" role="tab" data-toggle="tab"><?= $cat->cat_name ?></a>
                                        </li>
                                        <?php
                                    }
                                endforeach;
                                ?>
                            </ul>  

                            <!-- ACTIVE POSTS - PRIMEIRA ABA -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="geral">
                                    <?php
                                    //$posts = get_posts(['category__not_in' => [16,1]]);
                                    //$posts = get_posts(array('category__not_in' => [16,1],'numberposts' => 5 ));

                                    foreach ($posts as $post)
                                    {
                                        $ico = $this->sistema->img_data(get_the_post_thumbnail($post->ID, array(64, 64)));
                                        $img = ($ico !== null)? $ico :'<img class="media-object" src="'.base_url("images/img_index/icone_comunicados.png").'" width="64" alt="...">';
                                        
                                        $conteudo = $post->post_content;
                                        if(strpos($conteudo, "<a href=")!== FALSE)
                                        {
                                            $frase = strstr("<a href=", $conteudo);
                                            $expec = (strlen($frase)>0)? strip_tags(substr($frase,0,40))."... >>" : "Ver mais >>" ;
                                        }
                                        elseif(strpos($conteudo, "<img")!== FALSE)
                                        {
                                            $frase = strstr("<img", $conteudo);
                                            $expec = (strlen($frase)>0)? strip_tags(substr($frase,0,40))."... >>" : "Ver mais >>" ;
                                        }
                                        else
                                        {                                            
                                            $expec = strip_tags(substr($conteudo,0,40))."... >>";
                                        }
                                        ?>
                                        <div class="media">
                                            <div class="media-left media-middle">
                                                <a href="<?= base_url("comunicados/id/$post->ID") ?>">
                                                    <?=$img?>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <a href="<?= base_url("comunicados/id/$post->ID") ?>" class="post_link" >
                                                    <h4 class="media-heading"><?= $post->post_title ?></h4>                                            
                                                    <p>
                                                        <small><i class="fa fa-calendar-o"></i> <?= $post->post_date = date('d/m/Y', strtotime($post->post_date)) ?></small><br/>
                                                        <?=$expec?>
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                        <hr/>
                                        <?php
                                    }
                                    ?>
                                </div> 

                                <!-- LISTA DE POSTS -->
                                <?php
                                foreach ($categ as $cat)
                                {
                                    ?>
                                    <div role="tabpanel" class="tab-pane" id="<?= $cat->category_nicename ?>">
                                        <?php
                                        $posts_sel = query_posts("category__in=$cat->cat_ID&showposts=5");
                                        if($posts_sel)
                                        {
                                            foreach ($posts_sel as $post)
                                            {
                                                $ico = get_the_post_thumbnail($post->ID, array(64, 64));
                                                $img = ($ico !== '')? $ico :'<img class="media-object" src="'.base_url("images/img_index/icone_comunicados.png").'" width="64" alt="...">';

                                                $conteudo = $post->post_content;
                                                if(strpos($conteudo, "<a href=")!== FALSE)
                                                {
                                                    $frase = strstr("<a href=", $conteudo);
                                                    $expec = (strlen($frase)>0)? strip_tags(substr($frase,0,40))."... >>" : "Ver mais >>" ;
                                                }
                                                elseif(strpos($conteudo, "<img")!== FALSE)
                                                {
                                                    $frase = strstr("<img", $conteudo);
                                                    $expec = (strlen($frase)>0)? strip_tags(substr($frase,0,40))."... >>" : "Ver mais >>" ;
                                                }
                                                else
                                                {                                            
                                                    $expec = strip_tags(substr($conteudo,0,40))."... >>";
                                                }                                     
                                                ?>
                                                <div class="media">
                                                    <div class="media-left media-middle">
                                                        <a href="<?= base_url("comunicados/id/$post->ID") ?>">
                                                            <?=$img?>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <a href="<?= base_url("comunicados/id/$post->ID") ?>" class="post_link" >
                                                            <h4 class="media-heading"><?= $post->post_title ?></h4>
                                                            <p>
                                                                <small><i class="fa fa-calendar-o"></i> <?= $post->post_date = date('d/m/Y', strtotime($post->post_date)) ?></small><br/>
                                                                <?=$expec?>
                                                            </p>
                                                        </a>
                                                    </div>
                                                </div>
                                                <hr/>
                                                <?php
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            <h4 class="text-center"><?= $cat->cat_name ?></h4>
                                            <p class="text-center">
                                                Nenhuma mensagem disponível deste Departamento                         
                                            </p>                                
                                            <hr/>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div> 
                        </div>
                    </div>
                    
                    <!-- Mural de Noticias / ULTIMAS 2 NOTICIAS PUBLICADAS NO SITE -->         
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?= base_url("noticias")?>"><h3>NOTÍCIAS</h3></a>
                            <?php 
                            if($noticias)
                            {
                                foreach ($noticias as $noticia)
                                {
                                    ?>
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="<?=base_url("noticias/ler/$noticia->cd_noticia") ?>">
                                                <img class="media-object" src="<?=$noticia->img?>" style="margin: auto 10px 30px auto;" width="110" alt="<?=$noticia->nm_noticia?>">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <a href="<?=base_url("noticias/ler/$noticia->cd_noticia") ?>" class="post_link" >
                                                <h4 class="media-heading tit_news_home"><?=$noticia->nm_noticia?></h4>
                                                <p style="margin-top: 15px;"><small><i class="fa fa-calendar"></i> <?=$noticia->data?></small></p>
                                            </a>
                                        </div>
                                    </div>
                                    <hr/>
                                    <?php
                                }                         
                            }                           
                            ?>                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12" style="border-left: 2px solid #ccc"> 
                            <h3>ANOS DE EMPRESA</h3>
                            <?php
                            if($aniversariantes)
                            {
                                shuffle($aniversariantes);
                                $i = 0;
                                foreach ($aniversariantes as $row) 
                                {    
                                    
                                    $i++;  
                                    if($i==5)
                                    {
                                        break;
                                    }
                                    if($row['foto']!= null) 
                                    {
                                        $image = base64_encode($row['foto']); 
                                    }
                                    if($row['foto'] == null)
                                    {
                                        $image = 'iVBORw0KGgoAAAANSUhEUgAAAOkAAADZCAMAAADyk+d8AAAAbFBMVEX///8yicgth8dZnNAjhMYqhscdgsX7/f73+/0agcXp8vmlyOUmh8fg7Padw+KhxePY5/Px9/vO4fA4jcpDksyPut7L3+9rptV6r9lRmc+vzuc7j8u61evC2u2Httzj7vZwqdZhodONuN2As9v/d1uUAAAPt0lEQVR4nO1dDZOiPAzW0oKAuHwqXyqu//8/HmiD4AJNoV25mX1m3nvvZlQITZMnaRI2mz/84Q9/+MMf/vCHP3wIu8A7lvkhuVen2xOn9JK4+dl3AuvTN6cK19C9nLIiYiarQRh5ov47ZZSSKDbSpPSdT9/mMuxC9xRvKWVkS7Yj4FJH2b30Pn2/87ALDwYzKRuX8U1gZppRlV8/fd+ScMpqazKcjD1xqZ0l4e7Tt49FkN/qPSkrZSstM4vL8T8QNjhXezpbTBDWjhN/3SbZT+JRpa3tDqUmNc3GBtf/q//e2OKRT1N2y4NPizOK840MilnLZdP4dE/c8hz6vu95Xv3n8ZwfvlOjsO3a1wx8jdDiskprbOWx+fN+Sb1yceqer6O6aO38MrlFpv3zIdVaUPm/KQMGOzeib3fauMno5CIt6fX8bezpD2mZeQs137oULDd6W8/agtZS+nImNDgnGTHfNJmsSFarLPpy1otZpOd59sRzje2bdhC7Wsd+PRrm29aM5or5hOfeWF+NGbt8nhdf07ebMo188V1ZfvK2HVjhfti/fm17LIHSVJGx3JWZ3ZWV0OyTZtjPuluK0G2iUsnCU49TEvr9qWW1ErsnZ+SqZqteZdKuxhRHxRfAwc/Mrpx6NpJXdUk0MZMPUP+cdW6BRgddJNU/dXcINX57twZVR3MJveMCaMvxSje5pA98J+7Zw+zrc9axw4zky+5cEn78WlBi4p6zl6dGVIczjD4TSbQGK4x7KXxKlrvvXu7+i4ap7DA2xlzxF6xjku3NgZClSaqQ7CB6VE7VWVZq/BqNSF47h9g38WWvSfyTu/ekpfFB8DNl9LLCLPodJmydXjaXMfGuqd3ie5gzIC21BQFa0FlWYpaqpJmAk70eLkKPjoaNzLUw+zQta046qnRQJtAYvOJ14zQR2Qb/NBCdj8tqppNBi9eJJcy7QqEGbz1q75xFZ8GHg4RI5s7228PUw9tdXg+Opirl+oHwlaemhsg7HGPx/nxHTeQnrU3+enTmSaO36QhqpiJelsjL2YCxr8lbiFpZmT5Rw3376IUWIcjMYUnEy2qepphl8LKI7KZSug589roZkXM5Rguy2zSaNEy3VlR6UijeC97+pY0iW1TO09z2SbLJzXpvRTUrdfK1cNo9SoQUJZc/fHoDm3yWyWtV1VvgIGsFLUQsNV+2og/sJ/fHoTUCZqJQyAbWDfYdmd5EGxUr2lxmeoccXgqsOIpL4ZfJXiToWcGKPjCpOq2ohClNuXyBthBTpLqhDP+bAtlOPtOkvaWtwnP0c/urVGSMnEKRoPXF4klycoebYpmy5NK1vTYVRUvWjY7c9wywavJaFVxKnQFuza4pTC8cFAq63dqT1mZngJUU3xcOl/bZCS36cS4FHAahk1vVidslUJIxPNv859hNxKitWNkm5Zc0Jq8Xgj8jhQKyH7QRaSxM6SZKdbcBnfaWeWuVFATmJ74ZCBVqyNUevePZiKbzN6+dJaLiQrRPbdo6PHBaWJ0zBHaZvmYG1ywWHiI4EVyxEn42VGuOniD7aV7gACOjC/U3hUcm3qSbm2Jz9IRoUUt4vsv0tyVHVEwufQ2624AIiDYsBllClawYBP0WfzjVsqRiLx4A/6STCahpAOMRMNAHHGR5qzRIJLg4KB5hs89rAgnd7QTHqiE8mgD9RVhN0S8gGLSVaVpSRArQAXJjzySFPuiuwHv3PqwDVGT3XbjTafI4CiACqEhBbRDTh1B9W4UyZ3maEKx3jPiwZWhT3iZZL7p8a5RmLSosqY15To5GQWsITT8k9ObQB791yJhPnzUqLyb89HhwMWdRK1B91El7ookgPYHgBOAn5HeqDzEu7pRHD+cFELGb80AFpY9qvrk64pZ0py4jOCgpYge1zl/Sp4IzRh7cORpi8K6kW3EkBQ6dCmKfd0AAjlT7UK+kW4JIXlfPRUXxnA64KyYZLhNVajW9OOINeUm5gxpYI0HCqsWXZkkx9UdAXnBuEZCCJiCTM99anUz9xDGENOePW4bn78COYXf3Xbf2YoJsCMnRd715bbvpQ/gOUs1rylDnwXfQRHya+ySr8dolRS1TKM2TAqDL6IOddUi64YclmMzBE6C8Jtoz6ZaU4goieZCMV1/ug7f4uGAVFqkmv8CTkPZlx7N8WGe60R3K4PcRqC+yoCWUVl7tzEF4Fs9x4E8ckybZtGeDMlxDNxvE1qf4EJDjmhs5q2ISZdH+Chh+A4uHYDjuCwEbhlW339G7pmhWCsYU52fOYF0kDnRWEIk/wU/ecKdR3CmJU49dnLRKij/bBz+DqsbiNy13cqW+wqELPFnb8NNBhjDWAfdJtlT3zVnb+VMDiSIcztbEBUX1+nN2v5eqG3AineorEZzwIxpMipCvDjGkKnwsnWlQGZPBsyWYE9+DjJ3ufO2TJ1AdBLwYKxKbJFB0yVI8T6ekEv7Oyvh3hGwA8k7SRzn61FfuqJtzB3F40hbPyY7FcPWd/ksVZcPuExpfhxskRP1RH4EumkRiKdvIgw2xIgBXly/uuWuKUU254psj1ncAcZTvLvL0SCp7+uDx7wndDPe8MjlTQKVFVAzb6YLvIsJE248TWJngFODpCFJll3Sz40dKtuh793nu9AEdGUJT9pG3kop4O4Syc1rOg7168ytftstjMWF6mxOA6Q66MXwp96kzmvyB5Am+CTVMsmfo/NuGYv2dUwl4ed6DKJUEda5s3uQwX0Xz3gvS5qhBgrM0VjGTDHKo5YT2HGsBkgpMmTWX9gIqhTGNOcOpt8RXLOl2maSBuupXcSfSILCSwmVmT/i7qmL6TDrIeAK9pgu1V1kDKolmliX/nqR1kKCke3ruZCCsRSr4hZbMp1Qh6vyeaJBUEOy1/nRRm+NyUedW1G/aiiFhpTlwpGVTpY7L0r9MOF9gAnccR1rGe1+4Zgv8Kr0t6cbjMYqQMaNTawLs0tkaTC9LOtwhxy7Ms11w+xmBLzaL7rNo2UNu41PRToczRQVDPnxjxrLap4XTGeAETVilwQt7VXQjb6w8ktyttFg8dI53zIqLn+EAah7nfEdwx06mezxeW8GAU+gJEeYGedprUTNn7/cqipzDwvZKRkzzfhZxp0QABWnKJlJ6l0L4sg7CzFjRlGNI4wrPCndKSFIfQX4jE8LWYm5PparZKRd0Ghfckdp5lFf3FJk/h4M2I0FpUS2fQf4ClGmI75+TqTmp7WkEx68qY6b5eDHH8yUd9j6r3FDpjONWJ8V01sWeVc2BFfjH/PD9wFd+9NS/7YqfFW4LsZpAMwaiUGCNgCIahO+48kyS8FxjANbn30RwwJrehjduZ5qkwI2FgwglkWeyNhliMYyZSbHH531ck63ZDMVTqPQNwzKF05p72EE/HiZl4cqX99ZyXvgbRBSOcQ+fY8tNmQn0fPYLjvgAcZSYA7E7bFtXSdiXEoO6S9oxTzRGB3KwTKgWgoAnzfB1Ov1BvcQ0FAz8Pse095NITYFtipiesmlb4RGNvA/s7u9hKKHpQiPsnez+bzIckwmghQC3StB7GqE+7RcDMSijyQLm41wGYj1TOAN68wpkEJ25DaAIBZWJdEfOwc29lM3swPmmg/E724sXCnossSOMOXVEVI1YF3ssRGneNjPDDPv3/Vg8S8RzXaHTB1ulwbPgpBB9MJicGEnY/lRKKXEd3o3K+VAUwXDvtq0Ymy5uW4MEa+JkotRJ7fjvIdLp7I5pIUqxUWPyyaWyVACOoQTlV9cYkyNiLE5L4ZZ1yqqYerdF+2vZhKhQ3SlB79qW1Sll8dADI+ugNLuXowril2lM319IN/pbE+FYidTF7rV5tdhUVVL3tQ5iNG+sZVl6yEPPCTgcr45V04zaiDd4dH5pvEIJWllkeCy3vhN1kg5Kdfv3SB55Bnv/BLNtcyDhIsSoAkNtpNTk13ZU25gRC+QFVQZ2G95U0Boq0WP5mi4ytreVDiSWxvC4yLb1v5IQtDPaY1hTLp8UdKRsUnKACgCG9g6zDfX1gZIYmMPVzjKTTIDB2dy2GPheiPQI+jAwqx7mg0qfh8Jgu4FDdIXj0Wfj57x0nv4iiPRnH7C/B9qmVBbMzcb725Eg+4mMwbsAR/OjDiDX3CmNxJvhgSWdUV4EE03fx0Bc1Ra2zkfPLZRgQeeMxofm2TeTrnOEohS6yWsoGNtOj5UfgcMtbH9YbP5pB/NCJ6UHbk+2w5IDxkB0SUega0rvHLTFou3sYDovowP58G5CXPcoGSm0+wruSnYuXQsoNX8VPYRrErTlD0eYqijsexoFxNrAO15vDloHnvuq5XMLXtRxhDEm3EuV6zFHTzzGAyU/VG8GoCnvOUpS44TpmWhyY5DfWzYQ/wp560cF9OqW9BHUwBZbWK/Q9r/Yvvp3qigAMYDgLK2gevVvRWtc0u229e+L32bmQyzKUuNj0iAgNxBnEO3grrUw+0EoeZWZ3nHLioB4+40YjqpXzWkErRQI2nnl1Wqh5IVXDb7XkE6Zwl5VvYzWkUAKsLgR5IU1ZAPHMTtWG0KooatfFWa2qY5hRUmVN8jPwhFA3yuBloHM7N2fwH2dosqN0MPhtEJfQ2ZNBxDhs2emgyAK/UsXu7WJSlS94fUHAtXDRhZCm6BrE1WjoM2rgdejwLZOQTfrscB6rG4P6Sr8KkGUwC7GYQWROSsUNhiOI/94Rolh6rdVIIw+a4LNSjGpH4fzSRNMbOVtlFO4j1al6waTmV6vAuXPaQ1EC96uYRq/3iV4Nd6qco2boR63/lkQUd8AjIB1IL1lrTREiru8d5BJMw3XwMDvLSuht1Jti7Bz6LUgkEXtRstgHczushKa5eqaMr1L1HPcNP7QgvLbufUYEzGjRElexzqfWE9Otj8o7ymXRNlvciHMvMm1Aw3AS4q+uSNm9UusaAqNZXq7rX11nq/Fjpu9N1t8zBK9I7hs35wrs/dp6cir287/Muy3CILQWHuAhod3fz9IbkaM3A5nCWl3Xn7PfoyjIaZKK6cC3oX86O8hlEa3S+mLt60TumlGBqaV2GuTs4FzGJj7ROq1pUWWfpXhdWAgxy7wjnlyiodmsjRdYoZiD60KQZ7ZQ7FrLS6rHW8UG6f08p0kh8MhSS736pZFEWG0ZtBD4QKl6Urs0CD8+35sPlBN1hvGzthjwky9hgMEvrOcsfsxQoTErrxt6aI2k/phFJdfyZ4sxjU/oZpJBxee7ePLcX1WaBTOufEYckmYWko7uh3C/0jMJ2r/mMa02Y8ojaXm3kjOnx/GMxO7a3m4xcx82J9hEbdNU2qUpe5R/Vio34blhGWSGoVpP3przfa/+t/7+HT/OiO4xX8FK7j6x7LMc9fN87w8h77z323JP/zhD3/4gxT+Ab4P4vOefRUFAAAAAElFTkSuQmCC';
                                    }
                                    ?>
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <?php                                   
                                            echo '<img class="media-object img-rounded" src="data:image/png;base64,' .$image. '"  width="44" alt="">';                                 
                                            ?>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">
                                                <?=$row['nome']?>
                                            </h5>
                                            <p>
                                                <small>
                                                    <?php
                                                    $pieces = explode("-", $row['data_admissao']); //FIZEMOS EXPLODE PARA SEPARAR O ANO EM ARRAY PARA SUBTRAIR PELO ANO, N SABEMOS O PQ MAS TAVA RETORNANDO A IDADE
                                                    $anos = date('Y', strtotime('today')) - $pieces[0]; //$dado=date('y', strtotime($row['DATA_ADMISSAO'])) ;    
                                                    if($anos > 1)
                                                    {
                                                        echo "Faz ".$anos." anos";
                                                    }
                                                    else
                                                    {
                                                        if($anos == 1)
                                                        {  
                                                            echo "Faz ".$anos." ano" ;
                                                        }
                                                    }
                                                    ?>
                                                </small>
                                                <!--<br>
                                                <small>Setor</small>-->
                                            </p>                              
                                        </div>
                                    </div>                                
                                    <?php 
                                }
                                ?>
                                <p class="text-right dist_link"><small><a href="<?=base_url("aniversariantes")?>">Ver todos</a></small></p>
                                <h3 class="espaco"> </h3> 
                                <?php
                            }
                            else
                            {
                                echo '<br>';                                
                                echo '<h4>No data found</h4>';
                                echo '<br><br>';
                            }
                            ?>                            
                        </div>
                    </div>
                    
                    <div class="row visible-xs visible-md visible-lg" style="border-left: 2px solid #ccc">                                            
                        <a href="http://192.168.0.89/rhadmin/login"><img src="<?=base_url("images/img_index/avaliacao_de_desempenho.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <a href="<?=base_url("elogios")?>"><img src="<?=base_url("images/img_index/atalho_elogios.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <div class="visible-xs" style="margin-top: 1px; margin-botton: 1px;">&nbsp;</div>
                        <a href="<?=base_url("departamentos/index/616/497") ?>"><img src="<?=base_url("images/img_index/atalho_salas.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <div class="visible-xs" style="margin-top: 1px; margin-botton: 1px;">&nbsp;</div>
                        <a href="<?=base_url("eventos")?>"><img src="<?=base_url("images/img_index/atalho_eventos.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <div class="visible-xs" style="margin-top: 1px; margin-botton: 1px;">&nbsp;</div>
                        <a href="<?=base_url("departamentos/index/616/630") ?>"><img src="<?=base_url("images/img_index/atalho_videos.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <div class="visible-xs" style="margin-top: 1px; margin-botton: 1px;">&nbsp;</div>
                        <a href="<?=base_url("departamentos/index/602/3510") ?>"><img src="<?=base_url("images/img_index/atalho_biblioteca.png")?>" class='img-responsive col-xs-12 col-lg-12'></a>
                        <div class="visible-md visible-lg" style="margin-top: 15px; margin-botton: 15px;">&nbsp;</div>
                        <div class="visible-xs" style="margin-top: 1px; margin-botton: 1px;">&nbsp;</div>
                        <a href="<?=base_url("departamentos/index/602/64") ?>"><img src="<?=base_url("images/img_index/atalho_pagto.png") ?>" class='img-responsive col-xs-12 col-lg-12' ></a>                           
                    </div>
                    
                    <div class="row visible-sm">
                        <div class="col-sm-6">
                            <a href="<?=base_url("elogios")?>"><img src="<?=base_url("images/img_index/atalho_elogios.png")?>" class='img-responsive col-sm-12'></a>
                            <div class="visible-sm" style="margin-top: 1px;">&nbsp;</div>
                            <a href="<?=base_url("departamentos/index/616/497") ?>"><img src="<?=base_url("images/img_index/atalho_salas.png")?>" class='img-responsive col-sm-12'></a> 
                            <div class="visible-sm" style="margin-top: 1px;">&nbsp;</div>
                            <a href="<?=base_url("eventos")?>"><img src="<?=base_url("images/img_index/atalho_eventos.png")?>" class='img-responsive col-sm-12'></a>  
                            <div class="visible-sm" style="margin-top: 1px;">&nbsp;</div>
                            <a href="<?=base_url("departamentos/index/616/630") ?>"><img src="<?=base_url("images/img_index/atalho_videos.png")?>" class='img-responsive col-sm-12'></a>
                        </div>
                        <div class="col-sm-6">
                            <a href="<?=base_url("departamentos/index/602/64") ?>"><img src="<?=base_url("images/img_index/atalho_pagto.png") ?>" class='img-responsive col-sm-12' ></a> 
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="container">        
        <h3>ACESSO RÁPIDO</h3>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <?php
                $esp_meio = "padding:2px";
                ?>
                <!-- Destaques AZUL -->
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="<?= base_url("departamentos/index/612/15") ?>"><img src="<?= base_url("images/img_index/acesso_links.png") ?>" class="img-responsive"></a>			
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="http://reisoffice.com.br/helpdesk/" target="_blank"><img src="<?= base_url("images/img_index/acesso_helpdesk.png") ?>" class="img-responsive"></a>			
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="https://mail.reisoffice.com.br/webmail/login/" target="_blank"><img src="<?= base_url("images/img_index/acesso_webmail.png") ?>" class="img-responsive"></a>
                </div>   
                <!-- Destaques LARANJA -->
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="https://erp.reisoffice.com.br/eAnalysis/" target="_blank"><img src="<?= base_url("images/img_index/acesso_business.png") ?>" class="img-responsive"></a>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="http://192.168.0.89/vendas/index.php/login" target="_blank"><img src="<?= base_url("images/img_index/acesso_vendas.png") ?>" class="img-responsive"></a>			
                </div>            
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="https://erp.reisoffice.com.br/PortalService/" target="_blank"><img src="<?= base_url("images/img_index/acesso_service.png") ?>" class="img-responsive"></a>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="http://portal.reisoffice.com.br/" target="_blank"><img src="<?= base_url("images/img_index/acesso_revendas.png") ?>" class="img-responsive"></a>
                </div>            
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="http://www.reisoffice.com.br/solucoes/reembolso/" target="_blank"><img src="<?= base_url("images/img_index/acesso_reembolso.png") ?>" class="img-responsive"></a>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="http://www.reisoffice.com.br/solucoes/arquivodigital" target="_blank"><img src="<?= base_url("images/img_index/acesso_arquivo.png") ?>" class="img-responsive"></a>			
                </div>
                <!-- Destaques VERDE -->
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="<?= base_url("departamentos/index/616/497") ?>"><img src="<?= base_url("images/img_index/acesso_salas.png") ?>" class="img-responsive"></a>			
                </div>
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="<?= base_url("ramais") ?>"><img src="<?= base_url("images/img_index/acesso_ramais.png") ?>" class="img-responsive"></a>			
                </div> 
                <div class="col-xs-4 col-sm-2 col-md-1 col-lg-1" style="<?= $esp_meio ?>">
                    <a href="<?= base_url("cardapio") ?>"><img src="<?= base_url("images/img_index/acesso_cardapio.png") ?>" class="img-responsive"></a>
                </div>        
            </div>                           
        </div>
        
    </div>
    <h3 class="espaco"> </h3> 
</div>

