    <h4><b>Solicitações de Arte</b></h4>
    <br>
    <div id="resultado"></div>
    <?=form_open_multipart('departamentos/valida_form_dinamico',array('id' => 'form_arte','class' => 'form-horizontal','name' => 'solic_arte', 'method' => 'post')); ?>
    <?php //form_open_multipart('departamentos/valida_form_dinamico');?>
       <div class="form-group">
            <label class="control-label col-sm-4" for="email">* Seu Nome:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" placeholder="Nome" name="nome">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">* Seu Email:</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="email" placeholder="Email" name="email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">* Qual o Tipo de Arte?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="tipo_arte">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">* A Quem se Destina o Material?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="destinacao_arte">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">* Qual o Objetivo da Arte?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="objetivo_arte">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="email">Algum Texto na Arte, Qual Texto?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="texto_arte">
            </div>
        </div>
        <div class="form-group">                    
            <label class="control-label col-sm-4" for="email">Qual o principal ponto a ser destacado?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" name="ponto_arte">
            </div>
        </div>
        <div class="form-group">                    
            <label class="control-label col-sm-4" for="email">Alguma Referência de Arte/Imagem?</label>
            <div class="col-sm-6">
                <input type="file" id="arq_arte" name="arq_arte">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="comment">Precisa colocar alguma Informação Técnica de Produto? Qual?</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="inf_tec_arte" rows="5" id="comment"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="comment">Precisa de Informação Legal na Arte. Qual?</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="inf_legal_arte" rows="5" id="comment"></textarea>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-md-3"></div>        
            <div class="col-md-5">      
                <button type="submit" id="bot_arte" name="bot_arte" class="btn btn-warning btn-lg bot_arte">Solicitar</button>
            </div>
        </div>
        <div class="visible-xs">
            <br/><br/>
        </div>
        <input type="hidden" name="metodo" value="solicita_arte_form">        
        <?=  form_close()?>        
    </div>    
</div>
<?php
