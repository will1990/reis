<?php 
$arr_boletins = $boletim;
    ?>

    <h4><b>Boletim Informativo</b></h4>
    <div class="col-xs-12 col-lg-12">        
        <?php $boletim_ultimo = $arr_boletins[0] ?>
        <div class="row">
            <h5><?=$boletim_ultimo['titulo']?></h5>                          
            <a href="<?=$boletim_ultimo['url']?>" target="_blank">
                <div class="col-xs-offset-0 col-lg-offset-2 col-xs-12 col-lg-10">
                    <?=get_the_post_thumbnail($boletim_ultimo['id'], 'large', array("class" => "img-responsive"))?>
                </div>
            </a>           
        </div>
        <br/>
        <div class="row">
            <h4><b>Anteriores</b></h4>
            <?php
            for($cont=1;$cont<=3;$cont++)
            {
                $boletins_recentes = $arr_boletins[$cont];
                ?>
                <div class="col-xs-12 col-sm-4 col-lg-4 text-center">
                    <div class="imgs_boletins">
                        <a href="<?=$boletins_recentes['url']?>" target="_blank">
                            <?=get_the_post_thumbnail($boletins_recentes['id'],'medium', array("class" => "img-responsive"))?>
                        </a>
                        <h5><?=$boletins_recentes['titulo']?></h5>                        
                    </div>                     
                </div>              
                <?php
            }       
            ?>                
        </div>
        <br/><br/>
        <div class="row">
            <h4><b>Todas as Edições</b></h4>            
            <table id='tbId' class='table display'>
                <tbody>                        
                    <?php
                    foreach($arr_boletins as $boletins)
                    {
                        ?>
                        <tr>
                            <td><a href="<?=$boletins['url']?>" target="_blank"><?=$boletins['titulo']?></a></td>
                        </tr>
                        <?php
                    }
                    ?>               
                </tbody>
            </table>            
        </div>
    </div>
    
    </div>    
</div>
<?php 