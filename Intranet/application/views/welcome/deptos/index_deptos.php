        <div class="col-md-9">
            <div class="visible-xs col-xs-12">
                <br/><br/>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="col-md-12">
                <p>Para manter sempre a alta qualidade de suas operações a Reis Office é uma empresa que se divide internamente em vários departamentos, responsáveis por desenvolver atividades específicas que contribuem para o bom funcionamento da empresa.</p>
                <p>No menu ao lado é possível conhecer melhor a natureza de cada departamento e sua importância estratégica para a empresa, bem como a relação de todos os colaboradores e suas funções. Além disso, há várias funcionalidades importantes para os colaboradores, que vão desde informações sobre benefícios a solicitações de salas de treinamento.</p>
                <p>Conhecendo melhor os demais departamentos é possível aumentar ainda mais a sintonia para que a empresa continue prosperando. Assim, todos contribuem cada um com uma parcela que somada faz o sucesso da Reis Office.</p>
            </div>
            <div class="visible-xs">
                <br/><br/>
            </div>
        </div>
    </div>    
</div>
    

