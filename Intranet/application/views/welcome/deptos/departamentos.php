        <div class="col-xs-12 col-sm-8 col-md-9">
            <div class="visible-xs col-xs-12" id="conteudo">
                <br/><br/><br/>
            </div>
            <h3><?=$conteudo['titulo']?></h3>
            <?php     
                
                // Aplica formatação em Imagens em POSTS
                $img = str_replace('<img ', '<img class="img-responsive" ', $conteudo['conteudo']);

                // Aplica o espacamento entre paragrafos
                $conteudo['conteudo'] = str_replace("&nbsp;", "<p class='espacamento'></p>", $img);

                echo "<div id='divContent'>".$conteudo['conteudo']."</div><br/><br/>";
                echo "<input type='hidden' class='linkMenu' id='sub_redirect' value='$sub'/>";
            
            ?>
            <div class="visible-xs visible-sm">
                <br/><br/>
            </div>
        </div>        
    </div>    
</div>