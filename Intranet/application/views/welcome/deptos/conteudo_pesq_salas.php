<!--<h4><b>Verifique a disponibilidade</b></h4>-->
<img src="<?=base_url("images/img_index/salas_header.png")?>" class="img-responsive"/>
<br/>
<div class="col-md-12">
    <form class="form-horizontal" role="form" id="cons_salas" action="<?=base_url("departamentos/valida_form_dinamico")?>" name="filtro_salas" method="POST">
        <div class="form-group">
            <label class="control-label col-xs-12 col-sm-4 col-md-3 col-lg-4" for="email"><h3>Selecione o Dia:</h3></label>
            <div class="col-xs-7 col-sm-4 col-md-3 col-lg-3">
                <div class="visible-lg visible-md visible-sm" style="margin-top: 22px;"></div>
                <div class="visible-xs" style="margin-top: 8px;"></div>
                <input class="form-control" name="data_salas" value="<?=date('d-m-Y')?>" id="datepicker3">                        
            </div>
            <div class="col-xs-4 col-sm-4 col-md-6 col-lg-5">
                <div class="visible-lg visible-md visible-sm" style="margin-top: 10px;"></div>
                <input type="hidden" name="metodo" value="pesq_salas"/>
                <button type="submit" id="bot_salas" class="btn btn-primary btn-lg bot_sala">Verificar</button>
            </div>
        </div>        
    </form>
    <br/><br/>
</div>

<div class="col-md-12" id="consulta"> 

</div>
<br/>
<br/>
