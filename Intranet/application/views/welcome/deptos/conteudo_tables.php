<?php

$source = json_decode($tabela->post_content);

echo '<h4><b>'.$tabela->post_title.'</b></h4>';

$content = "<div class='box-body table-responsive'>";
$content .= "<table id='TBConteudo' class='table table-bordered table-striped'><thead><tr>";
foreach ($source[0] as $item)
{
    if($item != '')
    {
        $content .= "<th>$item</th>";
    }
}

$content .= "</thead><tbody>";
foreach ($source as $key => $linha)
{
    if($key > 0)
    {
        $content .= "<tr>";
        foreach ($linha as $item)
        {
            if($item != '')
            {
                $content .= "<td>$item</td>";
            }
        }
        $content .= "</tr>";
    }
}
$content .= "</tbody></table></div>";

$conteudo = str_replace("[table id=$id /]", $content, $conteudo);

$conteudo .= '<div class="visible-xs visible-sm"><br/><br/></div>';

echo $conteudo."<br/><br/>";