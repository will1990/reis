<script>
    $(function () {
        $.datepicker.regional['pt-BR'] = {
            closeText: 'Fechar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
                'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#datepicker,#datepicker1,#datepicker2,#datepicker3,#datepicker4").datepicker({dateFormat: 'dd-mm-yy',
            changeYear: true
        });


    });
</script> 
<br><br>
<h4><b>Solicitação</b></h4>

<div class="content">    
<?php
   //echo $conteudo;
?>
</div>
<br>
<br>
<form class="form-horizontal" role="form" id="form_sala" action="<?= base_url("departamentos/valida_form_dinamico") ?>" name="solic_sala" method="POST">
    <div id="resultado">    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Seu Nome:</label>
        <div class="col-md-5">
            <input type="text" class="form-control" id="email" placeholder="Nome" name="nome">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Seu Email:</label>
        <div class="col-md-5">
            <input type="email" class="form-control" id="email" placeholder="Email" name="email">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Data do Evento:</label>
        <div class="col-md-5">
            <input class="form-control" name="data_sala" placeholder="DD-MM-YYYY" id="datepicker2">
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Horário das:</label> 
        <div class="col-md-2">
            <input type="time" class="form-control" id="email" placeholder="00:00"  name="hora_inicio">
        </div>        
        <div class="horario_salas">
            <label class="control-label col-lg-1" for="email"> As:</label>        
            <div class="col-md-2">                    
                <input type="time" class="form-control" id="email" placeholder="00:00"  name="hora_termino">
            </div>
        </div>    
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Quantidade de Pessoas:</label>
        <div class="col-md-2">
            <input type="number" class="form-control" id="email" placeholder="Qtde" name="pessoas_sala">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Tipo de Evento:</label>
        <div class="col-md-5">
            <select class="form-control" name="evento_sala">
                <option value=""></option>
                <option value="Treinamento">Treinamento</option>
                <option value="Reunião">Reunião</option>
                <option value="Palestra">Palestra</option>
                <option value="Outros">Outros **</option>
            </select>
            <p class="espec_coment">** Especificar nos Comentários</p>            
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* O que será Utilizado?</label>
        <div class="col-md-8">
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Projetor">Projetor</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Som">Som</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="FLIPchart">FLIPchart</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Notebook">Notebook</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Água">Água</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Café">Café</label>
            <label class="checkbox-inline"><input type="checkbox" name="rec_sala[]" value="Outros">Outros</label>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="email">* Disposição da Sala:</label>
        <div class="col-md-5">
            <select class="form-control" name="disp_sala">
                <option value=""></option>
                <option value="Escolar">Escolar</option>
                <option value="Reunião">Reunião</option>
                <option value="Círcular">Círcular</option>
                <option value="FormatoU">Formato U</option>
                <option value="Sem Mesas">Sem Mesas</option>
            </select>
        </div>                    
    </div>
    <div class="form-group">
        <label class="control-label col-md-3" for="comment">Comentários:</label>
        <div class="col-md-5">
            <textarea class="form-control" name="coment_sala" rows="5" id="comment"></textarea>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <div class="col-md-3"></div>        
        <div class="col-md-5">        
            <button type="submit" id="bot_sala" name="bot_sala" class="btn btn-warning btn-lg bot_sala">Solicitar</button>
        </div>
    </div>
    <div class="visible-xs">
        <br/><br/>
    </div>
    <input type="hidden" name="metodo" value="sala_treinamento_form">
</form>        
</div>    
</div>

<?php
for($count=0;$count<=3;$count++)
{
    echo "<br>";
}
