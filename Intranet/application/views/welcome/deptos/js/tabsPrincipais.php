<script type="text/javascript">
    $(function() {
        $("#tabPrincipais").dataTable({
            "bStateSave": true,
            "processing": true,
            /*"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],*/
            "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" },
            //"aaSorting": [[ 2, "asc" ],[ 3, "asc" ]],
            /*"aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 4, 5 ] }
            ]*/
        });       
    });   
</script>