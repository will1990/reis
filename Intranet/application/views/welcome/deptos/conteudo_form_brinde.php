    <script>
        $(function () {
            $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
                    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                    'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};

            $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

            $("#datepicker,#datepicker1,#datepicker2,#datepicker3,#datepicker4").datepicker({dateFormat: 'dd-mm-yy',
                changeYear: true
            });


        });
    </script> 

    <h4><b>Solicitações de Brindes</b></h4>
    <br>
    <div id="resultado"></div>          
    <form class="form-horizontal" role="form" id="form_brinde" action="<?=base_url("departamentos/valida_form_dinamico")?>" name="solic_brinde" method="POST">
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">* Seu Nome:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" placeholder="Nome" name="nome">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">* Seu Email:</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="email" placeholder="Email" name="email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">* Qual o Brinde?</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="email" placeholder="Brinde ou Código" name="brinde">
            </div>                    
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">* Quantidade:</label>
            <div class="col-sm-3">
                <input type="number" class="form-control" id="email" placeholder="Qtde" name="pessoas_brinde">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="email">* Para Qual Data:</label>
            <div class="col-sm-5">
                <input class="form-control" placeholder="DD-MM-YYYY" name="data_brinde" id="datepicker1">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="comment">Comentários:</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="coment_brinde" rows="5" id="comment"></textarea>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-md-3"></div>        
            <div class="col-md-5">      
                <button type="submit" id="bot_brinde" name="bot_brinde" class="btn btn-warning btn-lg">Solicitar</button>
            </div>
        </div>
        <div class="visible-xs">
            <br/><br/>
        </div>
        <input type="hidden" name="metodo" value="solicita_brinde_form">        
    </form>                 
    
    </div>    
</div>
<?php 
