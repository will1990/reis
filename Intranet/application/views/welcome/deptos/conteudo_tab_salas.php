<?php

$cores = array("autor_thomas" => "#57C78D", 
               "autor_julio"  => "#577EC7",
               "autor_arthur" => "#C7C7C7",
               "autor_aline"  => "#B6CF6E"  );

$bordas = array("autor_thomas" => "#184921", 
                "autor_julio"  => "#1D2B58",
                "autor_arthur" => "#5F5F5F",
                "autor_aline"  => "#998333"  );
    
?>
<section class="content" style="margin-botton: 50px;">
    <table width="100%" style="border: 2px #e47b38 solid">
        <thead height="30px">
            <tr>
                <td class="text-center" width="10%" style="border: 2px solid #e47b38; color: #e47b38;">Horário</td>                            
                <td class="text-center" width="30%" style="border: 2px solid #e47b38;"><h3 style="font-weight: bold;">Sala 01</h3></td>
                <td class="text-center" width="30%" style="border: 2px solid #e47b38;"><h3 style="font-weight: bold;">Sala 02</h3></td>
                <td class="text-center" width="30%" style="border: 2px solid #e47b38;"><h3 style="font-weight: bold;">Sala 03</h3></td>                            
            </tr>
        </thead>                    
        <tbody>
        <tr height="5px" colspan="4"><tr/>
        <?php                

        $range = range(strtotime("08:00"),strtotime("18:00"), 30*60);
    
        // Faixas de horario
        $ce = 0;
        while ($ce <= 19)
        {
            $tabela = "";
            $time = $range[$ce];
            $tr = '<tr height="30px">';
            $tabela .= '<td class="text-center" style="border: 2px #e47b38 solid; color: #e47b38;">'.date("H:i", $time)."\n".'</td>';
                        
            // Salas
            $ci = 1;                
            while ($ci <= 3)
            {                    
                $style = '';
                $conteudo = '';
                $colspan = '';                
                
                $r = 0;
                $td = '<td class="text-center" ';
                $ftd = '>';
                $etd = '</td>';
                
                foreach ($reservas as $reserva)
                {                                        
                    $sala = 'sala'.$ci;
                    $inicio = strtotime($reserva->inicio);
                    $fim = strtotime($reserva->fim);
                    $fdiv = explode(':', $reserva->fim);
                    $hr = $fdiv[0]-1;
                    $min = $fdiv[1]+30;
                    $fdiv = strtotime($hr.':'.$min);

                    if(($reserva->$sala == 'ok')&&($time >= $inicio)&&($time < $fim))
                    {
                        $colspan = "";
                        
                        if($reserva->qtdesalas > 1)
                        {
                            $colspan .= ' colspan="'.$reserva->qtdesalas.'" ';
                            $ci += ($reserva->qtdesalas - 1);
                        }                               
                        
                        if($reserva->linhas > 1)
                        {
                            $colspan .= ' rowspan="'.$reserva->linhas.'" ';
                            if($r == 0)
                            {
                                $r = $reserva->linhas;
                            }
                        }
                        
                        $style .= ' style="background-color: '.$cores[$reserva->autor].'; border: 2px solid '.$bordas[$reserva->autor].';"';
                        
                        if($time == $inicio)
                        {
                            $conteudo .= '<h4>'.$reserva->post_title.'</h4>';
                            $conteudo .= '<h5>'.$reserva->post_content.'</h5>';
                        }
                        
                        if(($time > $inicio) AND ($time < $fim))
                        {                            
                            if($r > 0)
                            {
                                $r--;
                                $colspan = '';
                                $style = '';
                                $conteudo .= '';
                                $td = '';
                                $ftd = '';
                                $etd = '';
                            }
                        }
                    }
                    else
                    {                            
                        $style .= '';
                        $conteudo .= '';                            
                    }                                   
                }
                
                $tabela .= $td.$colspan.$style.$ftd.$conteudo.$etd;
                //$tabela .= '<td class="text-center"'.$colspan.$style.'>'.$conteudo.'</td>';                    
                    
                $ci++;
            }    
            
            
            echo $tr.$tabela.'</tr>';
            
            $ce++;            
        }      
        ?>  
        </tbody>
    </table>
</section>
<br/><br/><br/>
    