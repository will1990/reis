<div class="col-md-3">
    <h3>ALBUNS</h3>
    <div class="list-group">
        <!-- Método Atual - MENU -->
        <div class="list-group">
            <?php 
            rsort($pastas1);
            foreach($pastas1 as $pst1)
            {                
                if($pst1 == $pasta1)
                {
                    $active = 'active';
                    $open = '-open';
                }
                else
                {
                    $active = null;
                    $open = null;
                }                           
                ?>
                <a href="<?=base_url("galeria/index/$pst1")?>" class="list-group-item <?=$active?>">
                    <i class="fa fa-folder<?=$open?> fa-lg" aria-hidden="true"></i>
                    &nbsp;&nbsp;&nbsp;&nbsp;<?=str_replace('_',' ',$pst1)?>
                </a>
                <?php
                if($pst1 == $pasta1)
                {
                    rsort($pastas2);
                    foreach($pastas2 as $pst2)
                    {
                        if($pst2 == $pasta2)
                        {
                            $subactive = 'list-group-item-warning active';
                            $open = '-open';
                        }
                        else
                        {
                            $subactive = null;
                            $open = '-o';
                        }
                        ?>
                        <a href="<?=base_url("galeria/index/$pst1/$pst2")?>" class="list-group-item <?=$subactive?>">
                            <i class="fa fa-folder<?=$open?> fa-lg"></i>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=str_replace('_',' ',$pst2)?>
                        </a>
                        <?php 
                        if($pst2 == $pasta2)
                        {
                            rsort($pastas3);
                            foreach($pastas3 as $pst3)
                            {
                                if($pst3 == $pasta3)
                                {
                                    $subsubact = 'list-group-item-warning';
                                    $open = '<i class="fa fa-folder-open-o fa-lg"></i>';
                                }
                                else
                                {
                                    $subsubact = null;
                                    $open = null;
                                }                                            
                                ?>
                                <a href="<?=base_url("galeria/index/$pst1/$pst2/$pst3")?>" class="list-group-item <?=$subsubact?>">                              
                                    <?=$open?>&nbsp;&nbsp;<?=str_replace('_',' ',$pst3)?>
                                </a>
                                <?php                                            
                            }
                        }
                    }
                }
            }                    
            ?>
        </div>           
    </div>
</div>
        
    
