<script type="text/javascript">
    $(".fancybox").fancybox({
        type: "image",
        openEffect: 'none',
        closeEffect: 'none'
    });
    
    $(document).ready(function () 
    {
        $("select[name=opt]").change(function () 
        {
            opt = $(this).val();
            $("#link_" + opt).trigger('click');
        });
    });
</script>

