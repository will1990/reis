<script type="text/javascript">
    $(document).ready(function() {
        
        $(".linkMenu").on("click", function(e) {
            e.preventDefault();
            url = $(this).attr('href');
            if(url == null)
                url = $(this).val();
            $.ajax({
                url: url,
                success: function(response) {
                    $("#divContent").html(response);
                    mostrar_salas();
                    $("#TBConteudo").DataTable({
                        "bStateSave": true,
                        "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
                        "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" },
                        "aaSorting": [[ 1, "asc" ]],
                        "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 0 ] }
                        ]
                    });
                }
            });            
        });
        
                   
        $("#divContent").on("submit", "#cons_salas", function(e){
            e.preventDefault();
            mostrar_salas();
        });
        
        
        
            
        $("#divContent").on("submit", "#form_sala", function(e){
            e.preventDefault();
            postData = $("#form_sala").serialize();
            //alert("Enviar solicitacao de Sala");
            url = $("#form_sala").attr("action");            
            $.ajax({
                url: url,
                type: "post",
                data: postData,
                success: function(result){
                    $("#resultado").html(result);
                },
                error: function(x, y, err){
                    console.log(x);
                }
            });
        });
        
        $("#divContent").on("submit", "#form_arte", function(e){
            e.preventDefault();
            formData = $("#form_arte").serializeArray();
            var postData = new FormData();
            postData.append('arq_arte', $('#arq_arte')[0].files[0]);
            $.each(formData, function(idx, obj){
                postData.append(obj.name, obj.value);
            });            
            
            console.log(formData)
            //alert("Enviar solicitacao de Arte");
            url = $("#form_arte").attr("action");            
            $.ajax({
                url: url,
                type: "POST",
                data: postData,
                contentType: false,
                processData: false,
                success: function(result){
                    $("#resultado").html(result);
                },
                error: function(x, y, err){
                    console.log(x);
                }
            });
        });
        
        $("#divContent").on("submit", "#form_brinde", function(e){
            e.preventDefault();
            postData = $("#form_brinde").serialize();
            //alert("Enviar solicitacao de Brinde");
            url = $("#form_brinde").attr("action");            
            $.ajax({
                url: url,
                type: "post",
                data: postData,
                success: function(result){
                    $("#resultado").html(result);
                },
                error: function(x, y, err){
                    console.log(x);
                }
            });
        });
                        
        <?=(!empty($sub)) ? "$('#sub_redirect').trigger('click')" : "" ?>
   });
   
   function mostrar_salas()
   {
        postData = $("#cons_salas").serialize();
        //alert("Enviar Data");
        url = $("#cons_salas").attr("action");            
        $.ajax({
            url: url,
            type: "post",
            data: postData,
            success: function(result){
                $("#consulta").html(result);
            },
            error: function(x, y, err){
                console.log(x);
            }
        });
    }
   
   
    
</script>