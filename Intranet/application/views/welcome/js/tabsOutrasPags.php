<script type="text/javascript">
    $(function() {
        $("#tabConteudos").dataTable({
            "bStateSave": true,
            "processing": true,
            /*"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]],*/
            "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" },
            //"aaSorting": [[ 2, "asc" ],[ 3, "asc" ]],
            /*"aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 4, 5 ] }
            ]*/
        });
        
        /*Tabelas da Página Elogios*/
        $("#tabElogios1").dataTable({
            "bFilter": false,
            //"bStateSave": true,
            //"processing": true,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" },
            //"aaSorting": [[ 2, "asc" ],[ 3, "asc" ]],
            /*"aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 4, 5 ] }
            ]*/
        });
        
        $("#tabElogios2").dataTable({
            "bFilter": false,
            //"sDom": true,
            //"bStateSave": true,
            //"processing": true,
            "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Todos"]],
            "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" },
            //"aaSorting": [[ 2, "asc" ],[ 3, "asc" ]],
            /*"aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 4, 5 ] }
            ]*/
        });
        
        /*Tabela da Página Ramais*/
        $("#tabRamais").dataTable({
            "bFilter": true,
            "bStateSave": true,
            "processing": true,
            "sDom": '<"row"<"col-md-12"rt>><"row"<"col-md-3"l><"col-md-4"i><"col-md-5"p>><"clear">',
            //"iDisplayLength": 15, 
            "aLengthMenu": [[5, 10, 20], [5, 10, 20]],
            "oLanguage": { "sUrl": "<?php echo base_url() ?>assets/datatables/language/pt_BR.txt" }
            //"aaSorting": [[ 2, "asc" ],[ 3, "asc" ]],
            /*"aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 4, 5 ] }
            ]*/            
        });      
        
        $("#searchRamal").on("input", function (e) {
           e.preventDefault();
           $('#tabRamais').DataTable().search($(this).val()).draw();
        });
        
        
        
    });   
</script>