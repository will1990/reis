<?php
/*
 * Intranet Reis Office
 * William Feliciano
 */
?>
<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">COMUNICADOS</h1>
        </div>
    </div>

    <!--   Layout Simplificado -->
    <div class="container">     
        <?php
        if (empty($field)) {
            ?>                    

            <!--Form com os filtros de exibições-->
            <div class="row">
                <div class="bootstrap-iso">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">


                                <label for="inputWarning"><?php echo validation_errors() ?></label>

                                <!-- Form code begins -->
                                <form method="post" action="<?= base_url("Comunicados/filtro") ?>" class="form-inline">
                                    <div class="form-group"> <!-- Date input -->
                                        <label class="control-label" for="date">De</label>
                                        <input class="form-control"  name="DataInicial" placeholder="DD/MM/YYY" id="datepicker"/>
                                    </div>
                                    <div class="form-group"> <!-- Date input -->
                                        <label class="control-label" for="date">Até</label>
                                        <input class="form-control"  name="DataFinal" placeholder="DD/MM/YYY" id="datepicker1"/>
                                    </div> 

                                    <!-- retorna categorias em um select -->    
                                    <div class="form-group" > <!-- Date input -->
                                        <label class="control-label" for="date">Departamento:</label>
                                        <select class="form-control" name="Categorias">
                                            <?php
                                            //categ são categorias vindas do model
                                            foreach ($categ as $cat):
                                                if ($cat->category_nicename != "sem-categoria") {
                                                    ?>                        
                                                    <option name="Categorias" value="<?= $cat->category_nicename ?>"><?= $cat->cat_name ?></option>                        
                                                    <?php
                                                }
                                            endforeach;
                                            ?>                        
                                        </select>
                                    </div>       

                                    <?php echo form_submit('submitFiltro', 'Filtrar', 'class="btn btn-warning"'); ?>
                                </form>
                                <!-- Form code ends -->

                            </div>
                            <br>
                            <div class="col-md-12">
                                Abaixo segue a relação dos últimos comunicados publicados por cada departamento. Todas as informações aqui disponíveis são revisadas e aprovadas pelo respectivo gestor.
                            </div>
                            <br>    
                        </div>
                    </div>
                    <div></div>           
                    <br>
                    <?php
                    //busca os comunicados //max 5 por depto
                    foreach ($categ as $categ):
                        if (($categ->category_nicename !== "sem-categoria") && ($categ->category_nicename !== "elogio")) {
                            ?>
                            <h3><?= $categ->cat_name ?></h3>            
                            <?php
                            if ($postagens):
                                foreach ($postagens as $post):
                                    ?>
                                    <?php if (strcmp($post['categoria'], $categ->cat_name) === 0): ?>
                                        <a href="<?= base_url("comunicados/id/{$post['ID']}") ?>"><h4 class="font_comunicado"><?= $post['data_postagem'] . ' - ' . $post['titulo'] ?></h4></a>
                                        <?php
                                    endif;
                                endforeach;
                            endif;
                            echo "<br>";
                        }
                    endforeach;
                } else {
                    ?> 
                    <!-- mostra um post especifico pela id -->                  
                    <div class="col-md-12">  
                        <?php
                        foreach ($field as $resultado) {
                            echo "<h3>{$resultado['titulo']}</h3>";
                            $data = str_replace(" ", " ás ", $resultado['data_postagem']);
                            echo "<h5>{$data} &nbsp;&nbsp;-&nbsp;&nbsp;Por {$resultado['categoria']}</h5>";
                            echo "<br/>";
                            echo $resultado['conteudo'];
                        }
                        ?>
                    </div>
                    <div class="col-md-12">
                        <hr> 
                        <a href="<?= base_url("welcome") ?>" class="btn btn-primary btn-lg">Home</a>
                        &nbsp;&nbsp;
                        <a href="<?= base_url("comunicados") ?>" class="btn btn-warning btn-lg">Comunicados</a> 
                        <br><br><br><br>
                    </div>
                    <br>                    
    <?php
}
?>
            </div>   


        </div>
















