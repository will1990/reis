<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">ELOGIOS</h1>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row" style="min-height: 500px;">        
            <div class="col-md-6">
                <h3>Elogios</h3>
                <table id='tabElogios1' class='table table-bordered table-striped'>
                    <thead>
                        <tr>
                            <th width="5px">Data</th>
                            <th width="93px">Nome</th>
                            <th width="2px">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($elogios)):
                            foreach ($elogios as $elogio):                                
                                ?>
                                <tr>
                                    <td><?=$elogio->data?></td>
                                    <td><?=$elogio->titulo?></td>
                                    <td><a data-toggle="modal" data-target=".<?=$elogio->ID?>"><i class="btn btn-default fa fa-eye btn-xs"></i></a></td>
                                </tr>
                                <!-- Modal -->
                                <div id="mod" class="modal fade <?=$elogio->ID?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b><?=$elogio->data."&nbsp;|&nbsp;".$elogio->titulo?></b></h4>                                                        
                                            </div>                                    
                                            <div class="modal-body text-center">
                                                <?=$elogio->post_content?>
                                            </div>
                                            <div class="modal-footer"></div>                                    
                                        </div>
                                    </div>
                                </div> 
                                <?php
                            endforeach;
                        endif;
                        ?>                        
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h3>Depoimentos (Portal de Revendas)</h3>
                <table id='tabElogios2' class='table table-bordered table-striped'>
                    <thead>
                        <tr>                            
                            <th width="5px">Data</th>
                            <th width="93px">Nome</th>
                            <th width="2px">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($depoimentos)):
                            foreach ($depoimentos as $depo):                                
                                ?>
                                <tr>
                                    <td><?=$depo->DATA_COMENTARIO?></td>
                                    <td><?=$depo->NOME?></td>
                                    <td><a data-toggle="modal" data-target=".<?=$depo->ID?>"><i class="btn btn-default fa fa-eye btn-xs"></i></a></td>
                                </tr>
                                <!-- Modal -->
                                <div id="mod" class="modal fade <?=$depo->ID?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b><?=$depo->DATA_COMENTARIO."&nbsp;|&nbsp;".$depo->NOME?></b></h4>                                                        
                                            </div>                                    
                                            <div class="modal-body text-center">
                                                <?=$depo->COMENTARIO?>
                                            </div>
                                            <div class="modal-footer"></div>                                   
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>        
        </div>
    </div>
    <div class="container">
        <section class="col-md-12">
            <?php          
            //echo '<pre>';
            //print_r($elogios);            
            //echo '</pre>';            
            ?>  
        </section>
    </div>
    <br/>
</div>
