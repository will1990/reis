<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">GALERIA DE FOTOS</h1>
        </div>
    </div>
    <div class="container">
        <div class="col-md-3">
            <h3>ALBUNS</h3>

            <?php                    
            // Método Símples em Funcionamento com LINK GET
            /*if(isset($_GET['direc']))
            {
                $album = $_GET['direc'];

                $album .= '/';

                //echo $album;

                if($galerias_dir = opendir(GALERYPATH.$album))
                {
                    while(($dir2 = readdir($galerias_dir)) !== false)
                    {
                        if(is_dir(GALERYPATH.$album.$dir2) && $dir2 != '.' && $dir2 != '..')
                        {
                            echo '[P2] <a href="'.base_url("galeria?pst=$album&galery=$dir2").'">'.$dir2.'</a><br>';
                        }                     
                    }              
                }
                else
                    echo "OPS P2!";
            }
            else if(isset($_GET['pst']))
            {
                $galeria = $_GET['galery'];
                $pasta = $_GET['pst'];

                $galeria .= '/';

                if($topicos_dir = opendir(GALERYPATH.$pasta.$galeria))
                {
                    while(($dir3 = readdir($topicos_dir)) !== false)
                    {
                        if(is_dir(GALERYPATH.$pasta.$galeria.$dir3) && $dir3 != '.' && $dir3 != '..')
                        {                                    
                            $pasta = $pasta.$galeria;
                            echo '[P3] <a href="'.base_url("galeria?pst=$pasta&galery=$dir3").'">'.$dir3.'</a><br>';
                        }                     
                    }
                }
                else
                {
                    echo 'OPS P3!';
                }

            }
            else
            {
                if($albuns_dir = opendir(GALERYPATH))
                {
                    while(($dir = readdir($albuns_dir)) !== false)
                    {
                        if(is_dir(GALERYPATH.$dir) && $dir != '.' && $dir != '..')
                        {
                            echo '[P1] <a href="'.base_url("galeria?direc=$dir").'">'.$dir.'</a><br>';


                        }
                    }              
                }
                else  
                    echo "OPS P1!";
            }*/

            ?>


            <!-- Método Atual - MENU -->
            <div class="list-group">
                <?php
                foreach($albuns as $album):
                    if($_GET['album'] == $album)
                    //if($this->veralbum === $album)
                    {
                        $active = 'active';
                        $open = '-open';
                    }
                    else
                    {
                        $active = null;
                        $open = null;
                    }                           
                    ?>
                    <a href="<?=base_url("galeria1?album=$album")?>" class="list-group-item <?=$active?>">
                    <!--<a href="<?//=base_url("galeria/index/$album")?>" class="list-group-item <?//=$active?>">-->
                        <i class="fa fa-folder<?=$open?> fa-lg" aria-hidden="true"></i>
                            &nbsp;&nbsp;&nbsp;&nbsp;<?=str_replace('_',' ',$album)?>
                    </a>
                    <?php
                    if($_GET['album'] == $album):
                    //if($this->veralbum === $album):
                        foreach($galerias as $galeria):
                            if($_GET['galeria'] == $galeria)
                            //if($this->vergaleria === $galeria)
                            {
                                $subactive = 'list-group-item-warning active';
                                $open = '-open';
                            }
                            else
                            {
                                $subactive = null;
                                $open = '-o';
                            }
                            ?>
                            <a href="<?=base_url("galeria1?album=$album&galeria=$galeria")?>" class="list-group-item <?=$subactive?>">
                            <!--<a href="<?//=base_url("galeria/index/$album/$galeria")?>" class="list-group-item <?//=$subactive?>">-->
                                <i class="fa fa-folder<?=$open?> fa-lg"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=str_replace('_',' ',$galeria)?>
                            </a>
                            <?php 
                            if($_GET['galeria'] == $galeria):
                            //if($this->vergaleria === $galeria):
                                foreach($eventos as $evento):
                                    if($_GET['evento'] == $evento)
                                    //if($evento)
                                    {
                                        $subsubact = 'list-group-item-warning';
                                        $open = '<i class="fa fa-folder-open-o fa-lg"></i>';
                                    }
                                    else
                                    {
                                        $subsubact = null;
                                        $open = null;
                                    }                                            
                                    ?>
                                    <a href="<?=base_url("galeria1?album=$album&galeria=$galeria&evento=$evento")?>" class="list-group-item <?=$subsubact?>">
                                    <!--<a href="<?//=base_url("galeria/index/$album/$galeria/$evento")?>" class="list-group-item <?//=$subsubact?>">-->                              
                                        <?=$open?>&nbsp;&nbsp;<?=str_replace('_',' ',$evento)?>
                                    </a>
                                    <?php                                            
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                endforeach;
                ?>
            </div>                                 
        </div>                
        <div class="col-md-9">     
            <?php

            if($this->fotos)
            {
                ?>
                <h3>FOTOS</h3>
                <h4><b><?=$this->titulo?></b></h4>
                <?php

                foreach ($this->fotos as $foto):
                    echo $foto;
                endforeach; 
            }

            // Mostrando as Imagens
            /*if(isset($_GET['galeria']))                    
            {
                $album = $_GET['album'];
                $galeria = $_GET['galeria'];
                $evento = $_GET['evento'];

                $titulo = ($evento !== null)? $evento : $galeria ;
                ?>
                <h3>FOTOS</h3>
                <h4><b><?= str_replace('_',' ',$titulo)?></b></h4>
                <?php                        

                $pasta = $album.'/'.$galeria.'/';
                $galeria = $evento.'/';

                $count = 0;

                if($fotos_dir = opendir(GALERYPATH.$pasta.$galeria))
                {
                    while(($dir3 = readdir($fotos_dir)) !== false)
                    {
                        if($count <= 100)
                        {
                            if($dir3 != '.' && $dir3 != '..')
                            {
                                if(file_exists(GALERYPATH.$pasta.$galeria.$dir3))
                                {
                                    $ext = strtolower(pathinfo(GALERYPATH.$pasta.$galeria.$dir3, PATHINFO_EXTENSION));
                                    if($ext == 'jpg' || $ext == 'png' || $ext == 'gif')
                                    {
                                        $path = str_replace("/","::",GALERYPATH.$pasta.$galeria.$dir3);
                                        //$img = file_get_contents(GALERYPATH.$pasta.$galeria.$dir3, true);                                                
                                        echo "<div class='col-md-4 col-sm-6 col-xs-12'>";
                                        //echo '<img src="data:image;base64,'.base64_encode($img).'" class="img-responsive img-rounded"><br>';
                                        //echo '<a href="'.base_url("galeria/imagem/".$path."/".$ext).'" rel="galeria" class="fancybox"><img src="'.base_url("galeria/imagem/".$path."/".$ext).'" class="img-responsive img-rounded"></a><br>';
                                        echo '<a href="#" data-fancybox-href="'.base_url("galeria/imagem/".$path."/".$ext).'" data-fancybox-group="galeria" class="fancybox"><img src="'.base_url("galeria/imagem/".$path."/".$ext).'" class="img-responsive img-rounded"></a><br>';
                                        echo "</div>";
                                    }
                                }

                            }
                            $count++;
                        }
                    }              
                }
            }*/


            else
            {
                ?>
                <div class="col-md-12">
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <p>Veja fotos de eventos internos e externos, treinamentos e premiações nesta área. 
                        E não se esqueça de avisar aos colegas que aquela foto que vocês tanto esperavam já está disponível. </p>
                    </div>                            
                </div>
                <br>
                <?php
            }
            ?>

        </div>
    </div>
</div>