<?php
/*
 * Intranet Reis Office
 * Amanda Silva
 * Cintia Nomura
 */
?>
<div class="container-fluid">
    <div class="row" id="home" style="background: #537f95">
        <div class="container">
            <h1 style="color: white">SUGESTÕES</h1>
        </div>
    </div>
</div>

<div class="container">
    <!-- Layout Simplificado  -->

    <br>
    <div class="col-lg-push-12">
        <p style="font-size: 35px;text-align: center;color:#537f95;"> Alguma dúvida, sugestão ou crítica? </p>
        <p style="font-size: 35px;text-align: center;color:#537f95">Preencha o formulário e fale conosco.</p>
    </div><br/><br/>
    <div class="row text-center">
        <div class="col-lg-offset-2 col-lg-8 col-lg-offset-2">
        <?php 
        if($this->session->flashdata('msg'))
        {
            echo $this->session->flashdata('msg');
        }
    ?>
            </div>
      
    </div>
    <br>
    <br>

    <div class="panel-group" id="accordion">
        <form class="form-horizontal" role="form" id="form_sgst" action='<?= base_url("sugestoes/envia_sugestao_form") ?>' name="sugestoes" method="POST">
            <div class="form-group">
                <label class="control-label col-sm-3" for="email" >Seu Nome:</label>
                <div class="col-sm-6">
                    <?="<h5>$login->nome</h5>"?> <!--$login = ['login']mostra a informação -->
                                       
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="email">Seu Email Corporativo:</label>
                <div class="col-sm-6">
                    <?="<h5>$login->email</h5>"?> <!--$login = ['login'] -->
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="comment">Sugestão:</label>
                <div class="col-sm-6">
                    <textarea class="form-control" name="sugestao" rows="5" id="comment"></textarea>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-md-3"></div>        
                <div class="col-md-7">      
                    <button style="float: right"type="submit" id="bot_sgst" name="bot_sgts" class="btn btn-warning btn-lg ">Enviar</button>
                </div>
            </div>
            <div class="visible-xs">
                <br/><br/>
            </div>
        
        </form>  
    </div>
    <br/>
    <?php
    ?>  
</div>    


