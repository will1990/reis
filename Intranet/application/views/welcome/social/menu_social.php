<div class="col-md-3">
    <br/>
    <div class="row">
        <div class="col-xs-offset-3 col-xs-9 col-md-offset-3 col-md-9 col-sm-offset-0 col-sm-3">
            <img class="img-responsive" src="<?=base_url("images/img_index/logo_social.png")?>"/> 
            <br/>
        </div>
        <div class="col-md-12 col-sm-9">        
            <h3>PROJETOS</h3>        
            <?php 
                //echo '<pre>';
                //print_r($doacoes);
                //echo '</pre>';
            ?>


            <div class="list-group">
                <!-- Método Atual - MENU -->
                <?php
                /*if($act == 'view')
                {*/
                    $active = 'active';
                /*}
                else
                {
                    $active = '';
                }*/
                ?>

                <a href="<?=base_url("social/index/view")?>" class="list-group-item <?=$active?>">
                    Creche São Rafael
                </a>
                <div class="list-group">
                    <?php
                    //if($act == 'view')
                    if($doacoes)
                    {
                        foreach($doacoes as $doacao)
                        {
                            if($doacao->ID == $act2)
                            {
                                $subactive = 'list-group-item-warning active';                        
                            }
                            else
                            {
                                $subactive = null;                        
                            }
                            ?>                
                            <a href="<?=base_url("social/index/view/$doacao->ID")?>" class="list-group-item <?=$subactive?>">
                                <?=$doacao->post_title?>
                            </a>
                            <?php                         
                        }
                    }                    
                    ?>
                </div>      
            </div>
        </div>
    </div>
</div>

