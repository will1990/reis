    <?php if($this->session->userdata("ROIntranetInterno")): ?>
        <footer class="section section-primary">
            <style type="text/css">
                .botaoRodape{                    
                    color: #e47b38;
                    font-size: 20px;
                    background-color: #fff;
                    border-radius: 5px;
                    font-weight: bold;
                }
                .botaoRodape:hover{
                    color: #fff;
                    background-color: #e47b38;
                    text-decoration: none;
                    border-radius: 5px;
                    font-weight: bold;
                    /*class="btn btn-md botaoRodape";*/
                }
                .linksugestao{
                    color: #fff;
                    background-color: #e47b38;
                    /*text-decoration: none;
                    border-radius: 5px;
                    font-weight: bold;*/
                    font-size: 20px;                    
                }
                
                
            </style>
            <div class="container"> 
                <div class="row">            
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <h1>Dúvidas e Sugestões?</h1>
                        <p>Tire suas dúvidas ou deixe suas sugestões sobre o<br/>que gostaria de ver na Intranet.<br/>Sua mensagem é muito bem vinda!</p><a class="linksugestao" href="<?= base_url("sugestoes")?>">CLIQUE AQUI</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right">
                        <p>Acesse em seu horário de almoço:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                        <a href="https://www.facebook.com/ReisOfficeProducts" target="_blank"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                        <a href="https://www.linkedin.com/company/reis-office-products-ltda-?trk=biz-companies-cym" target="_blank"><i class="fa fa-3x fa-fw fa-linkedin-square text-inverse"></i></a>
                        <a href="https://www.youtube.com/user/ReisOfficeProducts" target="_blank"><i class="fa fa-3x fa-fw fa-youtube-play text-inverse"></i></a>
                        <a href="https://plus.google.com/+reisoffice/posts" target="_blank"><i class="fa fa-3x fa-fw fa-google-plus-square text-inverse"></i></a>
                        <a href="https://twitter.com/reisofficereal" target="_blank"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>                
                    </div>
                </div>
            </div>
        </footer>
    <?php endif; ?>
    <?php// endif; ?>

    <!--<script src="<?//=base_url("assets/js/jquery-2.1.4.js") ?>"></script>-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="<?= base_url("assets/js/bootstrap.min.js")?>"></script>
    <script type="text/javascript" charset="utf8" src="<?= base_url("assets/DataTables-1.10.10/js/jquery.dataTables.js") ?>"></script>
    <script type="text/javascript" charset="utf8" src="<?= base_url("assets/DataTables-1.10.10/js/dataTables.bootstrap.js") ?>"></script>

    <!-- Fancybox -->
    <!--<script src="http://code.jquery.com/jquery-1.8.2.js"></script>-->
    <!--<script src="http:/\/code.jquery.com/ui/1.9.0/jquery-ui.js"></script>-->
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?= base_url("assets/fancyapps/lib/jquery.mousewheel-3.0.6.pack.js")?>"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?= base_url("assets/fancyapps/source/jquery.fancybox.js?v=2.1.5")?>"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url("assets/fancyapps/source/jquery.fancybox.css?v=2.1.5")?>" media="screen" />

    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?= base_url("assets/fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5")?>" />
    <script type="text/javascript" src="<?= base_url("assets/fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5")?>"></script>

    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="<?= base_url("assets/fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7")?>" />
    <script type="text/javascript" src="<?= base_url("assets/fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7")?>"></script>

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="<?= base_url("assets/fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.6")?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            /*
             *  Simple image gallery. Uses default settings
             */

            /*$('.fancybox').click(function(e){
                e.preventDefault();
                url = $(this).attr("href");
                $.fancybox.open({
                    type: "image",
                    href: url
                });
            });*/

            

            /*
             *  Different effects
             */


        });
    </script>
    <script>
                $(function () {
                    $.datepicker.regional['pt-BR'] = {
                        closeText: 'Fechar',
                        prevText: '&#x3c;Anterior',
                        nextText: 'Pr&oacute;ximo&#x3e;',
                        currentText: 'Hoje',
                        monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
                            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                            'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                        dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
                        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                        weekHeader: 'Sm',
                        dateFormat: 'dd/mm/yy',
                        firstDay: 0,
                        isRTL: false,
                        showMonthAfterYear: false,
                        yearSuffix: ''};

                    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

                    $("#datepicker,#datepicker1,#datepicker2,#datepicker3,#datepicker4").datepicker({dateFormat: 'dd-mm-yy',
                        changeYear: true
                    });


                });
            </script> 

    </body>
</html>
