<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>         
        <meta name="viewport" content="width=device-width">
        <title>Intranet Reis Office</title>
        
        <!--<link rel="shortcut icon" type="image/x-icon" href="https:/\/www.reisoffice.com.br/favicon.ico">-->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url("images/favicon.ico")?>">

        <link rel="stylesheet" href="<?= base_url("assets/css/Intranet.css") ?>">        
        <link rel="stylesheet" href="<?= base_url("assets/css/font-awesome.min.css") ?>">
                
        <!-- Data Table -->
        <link rel="stylesheet" href="<?= base_url("assets/DataTables-1.10.10/css/dataTables.bootstrap.min.css")?>">

        <!-- Data picker -->        
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">       
        
        <!-- Fancybox -->
        <style>
            .navbar-header button{
                color:#fff;
            }
            .fullscreen{
                width: 100%
            }
            main{
                padding-top:50px;
            }
            h3{
                color: #e47b38;			
            }

            label{
                color: #e47b38;			
            }
            header{
                color: #e47b38;	
            }
            .hotqcontent {
                display:none;
            }
            
            .titulo_noticia{
                color: #e47b38;	
            }        
            
            
            /*Pagina Aniversariantes*/
            .cab_niver{
                margin-top: 8px;
            }
            .h_leg{
                margin-top: 17px;
            }
            
            /*Noticias*/
            .tit_news_home{
                color: #e47b38;	
            }
            
            
            .dist_link{                
                margin-right: 10px;
                font-size: 15px;
            }
            
            /*Pagina Elogios*/
            .esp_elogio{
                margin-top: 10px;
            }
            .tit_elogio{
                height: 40px;
            }
            .desc_elogio{
                margin-bottom: 15px;
            }  
            
            /*Menu Dispositivos*/
            .tam_fonte{
                font-size: 13pt;
            }
            
            /*Esconder atributo baixar de Vídeos em HTML5*/
            video.formatVideo::-webkit-media-controls-enclosure{
                overflow: hidden;
            }
            video.formatVideo::-webkit-media-controls-panel{
                width: calc(100% + 32px);
            }
        </style>
    </head>  
    <body>
