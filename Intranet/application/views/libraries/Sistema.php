<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Classe com funções básicas para o sistema
 *
 * @author fagnervalerio
 */
class Sistema {

    private $ip;
    
    // -- Construtor
    public function __construct() {
        /*
         * Criando uma instância do CodeIgniter para poder acessar
         * banco de dados, sessionns, models, etc...
         */
        $this->CI = & get_instance();
                
        // Identifica o IP
        $ip = $this->CI->input->ip_address();
        $ip_part = explode(".", $ip);
        array_push($ip_part, $ip);
        $this->CI->data["ip"] = $ip_part;        
        $this->ip = $ip_part;
        
        if($ip_part[3] != '243' && $ip_part[3] != '195' && $ip_part[3] != '77' && $ip_part[3] != '223' && $ip_part[0] != '10')
        {
            redirect("http://192.168.0.89/intranet/");
            exit;            
        }        
        
        $this->check_login();
        $this->log();                      
    }
    
    public function check_login()
    {
        if(($this->ip[0] == 192 && $this->ip[1] == 168) || ($this->ip[0] == 10 && $this->ip[1] == 0))
        {
            $this->CI->session->set_userdata("ROIntranetInterno", true);            
            
            // Verifica se o colaborador ja se identificou em seu equipamento
            if(get_cookie($this->ip[4]))
            {
                $this->CI->session->set_userdata("ROIntranetColaborador", $this->CI->get_cookie($this->ip[4]));
                $this->CI->session->set_userdata("ROIntranetColaboradorText", "Sair");
                $this->CI->session->set_userdata("ROIntranetColaboradorLink", base_url("login/logout"));
            }
            else
            {
                $this->CI->session->set_userdata("ROIntranetColaborador", "Colaborador");
                $this->CI->session->set_userdata("ROIntranetColaboradorText", "Identifique-se");
                $this->CI->session->set_userdata("ROIntranetColaboradorLink", base_url("login/"));
            }
            
            return true;
        }
        else
        {
            $this->CI->session->set_userdata("ROIntranetInterno", false);
            if($this->CI->router->class != login && $this->CI->session->userdata("ROIntranetSessao") === false)
            {
                redirect("login");
            }
            else
            {
                return true;
            }
        }        
    }

    public function enviar_email($remetente, $destinatario) 
    {
        $this->CI->load->library('email');

        $mail_config["protocol"] = "smtp";
        $mail_config["smtp_host"] = "mail.reisoffice.com.br";
        $mail_config["smtp_user"] = "spamtrap";
        $mail_config["smtp_pass"] = "Ro12333321";
        $mail_config["smtp_port"] = "587";
        $mail_config["mailtype"] = "html";
        $mail_config['crlf'] = "\r\n";
        $mail_config['newline'] = "\r\n";
        $this->CI->email->initialize($mail_config);

        $this->CI->email->from($remetente->email, html_entity_decode($remetente->nome));
        $this->CI->email->to($destinatario->email);
        if(!empty($destinatario->cc)){
             $this->CI->email->cc($destinatario->cc);
        }
        $this->CI->email->subject(html_entity_decode($destinatario->assunto));
        $msg = $this->CI->load->view("email/modeloHtml", $destinatario->mensagem, TRUE);
        $this->CI->email->message($msg);
        
        if (isset($destinatario->anexos)){
            foreach ($destinatario->anexos as $anexo)
            {
                $this->CI->email->attach($anexo['full_path']);
            }
        }
        else
        {
            //echo "não há anexos";
        }
        //print_r($destinatario->mensagem);
        //print_r($destinatario->anexos);
        //exit;
        if ($this->CI->email->send()) {
            $debug = html_entity_decode($this->CI->email->print_debugger(array("headers")));
            file_put_contents(APPPATH . "logs/email_debug.txt", $debug);
            //echo $debug;
            //exit;
            // Grava LOG
            //$this->log("EML", json_encode(array("msg"=> substr($debug,0,3000), "remetente"=>$remetente->email, "destinatario"=>$destinatario->email)));

            return TRUE;
        } else {
            $debug = html_entity_decode($this->CI->email->print_debugger(array("headers")));
            file_put_contents(APPPATH . "logs/email_debug.txt", $debug);
            //echo $debug;
            // Grava LOG
            //$this->log("EML", json_encode(array("msg"=>$debug, "remetente"=>$remetente->email, "destinatario"=>$destinatario->email)));

            return FALSE;
        }
    }
    public function log() 
    {
        $classe = $this->CI->router->class;
        $metodo = $this->CI->router->method;
                
        $metodos_nao_logar =array('imagem','imgopen','getAniversariantes', 'getByDepto', 'buscaRamais');
        $classe_nao_logar = array('teste','teste1');
        
        $metodo_pos = $this->substrArra($metodo, $metodos_nao_logar );
        $classe_pos = $this->substrArra($classe, $classe_nao_logar );
        
        if(($metodo_pos === false) && ($classe_pos === false))
        {
            $this->CI->load->model("Sistema_model", "m_sistema");
            $logData = array(
                "ip" => $this->CI->input->ip_address(),
                "classe" => $this->CI->router->class,
                "metodo" => $this->CI->router->method,
                "url" => current_url()
            );
            $this->CI->m_sistema->insereLog($logData);
        }
    }
    
    public function substrArra($atual, $requerido){
        foreach ($requerido as $value){
            if(($pos = substr_count($atual, $value))> 0)
                return true;
        }
        return false;
    }
}