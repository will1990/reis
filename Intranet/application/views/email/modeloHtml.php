<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />        
        <meta name="viewport" content="width=device-width">
        <title>Intranet Reis Office</title>

        <link rel="stylesheet" href="<?= base_url("assets/css/Intranet.css") ?>">
        <link rel="stylesheet" href="<?= base_url("assets/css/font-awesome.min.css") ?>">

        <!-- Fancybox -->

        <style>
            .navbar-header button{
                color:#fff;
            }
            .fullscreen{
                width: 100%
            }
            main{
                padding-top:50px;
            }
            h3{
                color: #e47b38;			
            }

            label{
                color: #e47b38;			
            }
            header{
                color: #e47b38;	
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

        </style>
    </head>  
    <body>
        <div class="row">
            <div class="col-md-12">
                <img src="http://www.reisoffice.com.br/images/logo.png" height="60" style="float:left" />
            </div>
            <div class="col-md-12">
                <h1><?php echo $titulo; ?></h1>
            </div>
            <div class="col-md-12">
                <?php
                $can_foreach = is_array($mensagem) || is_object($mensagem);
                if ($can_foreach) {
                    foreach (array_keys($mensagem) as $index => $key) {

                        echo stripslashes($mensagem[$key]);
                    }
                }
                ?>
            </div>    
        </div>
    </body>
</html>