<script type="text/javascript">
    //script responsavel pelas tabelas (DataTables)
    $(document).ready(function(){
        $('#tabsAdmin').DataTable({
            language: {                
                url: "<?= base_url('assets/plugins/datatables/pt_BR.txt')?>"       
            }
        });
    //});
    
    //script responsavel pela formatação das datas nos forms (DatePicker)
    //$(function() {
        $( "#dtIniAdm, #dtFimAdm ").datepicker({
            showOn: "button",
            buttonImage: "calendario.png",
            buttonImageOnly: true,
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    });
    
    //script responsavel por Mostrar Imagem escolhida no Form
    function readURL(input, id) 
    {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+id)
             .attr('src', e.target.result)
             ;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
</script>
