<div class="container"> 
    <div class="row blocks-2">
        <div class="span3b">
            <div class="block-2"> 
                <h3 style="border:0px; padding:0px; margin:0px;"><img src="<?=base_url("images/icons/revenda.png")?>"> REVENDAS</h3>
                <p> Seja uma revenda e conhe&ccedil;a porque somos o parceiro ideal para seu neg&oacute;cio. Veja todos os produtos. </p>
                <a class="btn btn-large btn-inverse" href="<?=base_url("produtos")?>" style="padding:10px 15px; font-size:16px;">VEJA MAIS</a> 
            </div>
        </div>
        <div class="span3b">
            <div class="block-2">       
                <h3 style="border:0px; padding:0px; margin:0px;"><img src="<?=base_url("images/icons/outsourcing.png")?>"> OUTSOURCING</h3>
                <p> Conhe&ccedil;a nossas solu&ccedil;&otilde;es de impress&atilde;o, outsourcing e loca&ccedil;&atilde;o de equipamentos para toda S&atilde;o Paulo. </p>
                <a class="btn btn-large btn-inverse" href="<?=base_url("solucoes")?>" style="padding:10px 15px; font-size:16px;">VEJA MAIS</a> 
            </div>
        </div>
        <div class="span3b">
            <div class="block-2">       
                <h3 style="border:0px; padding:0px; margin:0px;"><img src="<?=base_url("images/icons/licitacoes.png")?>"> LICITA&Ccedil;&Otilde;ES</h3>
                <p> Fale com nossa equipe especializada em org&atilde;os p&uacute;blicos para venda de equipamento e loca&ccedil;&atilde;o. </p>
                <a class="btn btn-large btn-inverse" href="<?=base_url("governo")?>" style="padding:10px 15px; font-size:16px;">VEJA MAIS</a> 
            </div>
        </div>
        <div class="span3b">
            <div class="block-2">        
                <h3 style="border:0px; padding:0px; margin:0px;"><img src="<?=base_url("images/icons/lojavirtual.png")?>"> LOJA VIRTUAL</h3>
                <p> Clique para acessar nossa loja virtual e confira nossa linha completa de produtos de maneira on-line. </p>
                <a href="<?=LOJA_VIRTUAL?>" target="_blank" class="btn btn-large btn-inverse" style="padding:10px 15px; font-size:16px;">VEJA MAIS</a> 
            </div>
        </div>
    </div>
    <div class="row wrap" style="margin-bottom:20px;"> 
        <div class="span8" style="width:780px; ">
            <h2>&Uacute;LTIMAS NOT&Iacute;CIAS</h2>
            <div class="row">
                <div class="span4 blog-corp" style="margin-bottom:20px;margin-left:20px;">
                    <div class="row">
                        <div class="span1"> 
                            <i style="width:88px; min-height:88px;border:1px solid #EEE; display:block;"><img class="blogimg" height="88" src="https://www.reisoffice.com.br/arquivos/noticias/thumb/2016_11_18_13_59_9_24633.jpg" style=" max-height:87px; float:none; border:0px;"></i> 
                            <br><br>
                        </div>
                        <div class="span3">
                            <h3 style="line-height:25px; margin-bottom:8px; padding-bottom:5px;"><a href="noticia-interna.asp?cd_noticia=187"  title="Reis Office, solução de outsourcing" style="text-align:justify">Reis Office, solução de outsourcing...</a></h3>
                            <span class="date">09/11/2016</span>
                            <p style="text-align:justify"> </p>
                        </div>
                    </div>
                </div>
                <div class="span4 blog-corp" style="margin-bottom:20px;margin-left:20px;">
                    <div class="row">
                        <div class="span1"> 
                            <i style="width:88px; min-height:88px;border:1px solid #EEE; display:block;"><img class="blogimg" height="88" src="https://www.reisoffice.com.br/arquivos/noticias/thumb/2016_11_18_14_1_40_13522.jpg" style=" max-height:87px; float:none; border:0px;"></i> 
                            <br><br>
                        </div>
                        <div class="span3">
                            <h3 style="line-height:25px; margin-bottom:8px; padding-bottom:5px;"><a href="noticia-interna.asp?cd_noticia=188"  title="Como se destacar da concorrência com novas tecnologias no outsourcing de impressão" style="text-align:justify">Como se destacar da concorrência com novas tecnologias no outsourcing de impressão...</a></h3>
                            <span class="date">09/11/2016</span>
                            <p style="text-align:justify"> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="span4c">
            <h3>SHOW ROOM 360&ordm;</h3>
            <div id="carousel-example-generic"> 
                <div  role="listbox" style="border:1px solid #eeeeee; padding:10px; height:294px; width:94%;">
                    <div class="item active">
                        <a href="<?=base_url("home/showroom")?>"><img src="<?=base_url("images/showroom1.jpg")?>" width="100%" style="border:none"></a>
                    </div>         
                </div>
                <a class="carousel-control left" href="#carousel-example-generic" role="button" data-slide="prev">&lsaquo;</a> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a> <a class="carousel-control right" href="#carousel-example-generic" role="button" data-slide="next">&rsaquo;</a> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a> 
            </div>
        </div>
    </div>    
    
    <?=(isset($nossas_marcas)) ? $nossas_marcas : ""?>
    
</div>

