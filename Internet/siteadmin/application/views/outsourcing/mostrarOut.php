<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Outsourcing&nbsp;<small><?=$label?></small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li>Outsourcing</li>
            <li class="active"><?=$page?></li>
        </ol>
    </section>   

    <section class="content">
        <div class="col-lg-12">
            <?php
            if(validation_errors())
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.validation_errors().'</h4></div>';
            }
            
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }           
            
            $imgSup = $outsourcing->img_sup !== null ? $pathBaseOut.$outsourcing->img_sup : $pathBaseOut."notImg.jpg" ;
            $imgOpt = $outsourcing->img_opc !== null ? $pathBaseOut.$outsourcing->img_opc : $pathBaseOut."notImg.jpg" ;            
            ?>
        </div>
        <div class="col-lg-12"> 
            <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title">Mostrar Outsourcing</h2>
                </div><!-- /.box-header -->
                <div class="box-body">                
                    <div class="row">                                                      
                        <section class="col-md-6 col-lg-5">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-lg-2">
                                    Titulo:                                
                                </label>
                                <div class="col-md-12 col-lg-10">
                                    <h4><?=$outsourcing->titulo?></h4>
                                </div>
                            </div>
                        </section>    
                        <section class="col-md-6 col-lg-7">    
                            <div class="form-group">
                                <label class="control-label col-md-12 col-lg-2">
                                    Descrição:                                
                                </label>
                                <div class="col-md-12 col-lg-9 well">
                                    <?=$outsourcing->descricao?>
                                </div>
                                <div class="visible-lg col-lg-1"></div>
                            </div>
                        </section>
                    </div>
                    <hr/>
                    <div class="row">
                        <section class="col-md-6 col-lg-6 form-horizontal">
                            <div class="form-group">                            
                                <label class="col-lg-3 text-right">
                                    Imagem topo Outsourcing:
                                </label>                                    
                                <div class="col-lg-6" style="height: 120px;">                              
                                    <img class="img-responsive" src="<?=$imgSup?>"/>
                                </div>
                            </div>                                                               
                            <div class="form-group">
                                <label class="control-label col-md-4 col-lg-2 text-right">
                                    Texto Conteúdo:                              
                                </label>
                                <div class="col-md-8 col-lg-9 well">
                                    <?=$outsourcing->conteudo?>
                                </div>
                            </div>                                
                        </section>
                        <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                            <div class="form-group">                            
                                <label class="col-lg-3 text-right">
                                    Imagem final Outsourcing:
                                </label>                                    
                                <div class="col-lg-6" style="height: 120px;">                                
                                    <img class="img-responsive" src="<?=$imgOpt?>"/>
                                </div>                      
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-lg-2 text-right">
                                    Texto final Opcional:                                
                                </label>
                                <div class="col-md-8 col-lg-9 well">
                                    <?=$outsourcing->conteudo_opc?>
                                </div>
                            </div>                                
                        </section>                            
                    </div>                        
                    <br/>
                    <hr/>
                    <div class="col-lg-12">                
                        <a href="<?=base_url("outsourcing/listar")?>" class="btn btn-default btn-lg">Voltar</a>
                    </div>                
                </div>    
            </div>                      
        </div>
    </section>
</div>