<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Outsourcing&nbsp;<small><?=$label?></small>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php
            echo $conjunto->botaoAdd;
            ?> 
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url("painel")?>"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li class="active">Outsourcing</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <?php
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }            
            ?>
        </div>
        <div class="box-body table-responsive">            
            <table width="100%" id="tabsAdmin" class="table table-bordered table-striped">
                <thead>                    
                    <tr>
                        <th width="1%"><i class="fa <?=$conjunto->botoesEdit->icon?> fa-1x"></i></th>
                        <?= $conjunto->botoesDel !== false ? '<th width="1%"><i class="fa fa-trash fa-1x"></i></th>' : null ?>
                        <?= $conjunto->botoesOnOff !== false ? '<th width="3%">Ativo</th>' : null ?>
                        <th width="35%">Titulo</th>
                        <th width="60%">Descrição</th>                                                                               
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach($outsourcings as $outsourcing)
                    {
                        $botaoDel = ($conjunto->botoesDel !== false) ? '<td><a href="'.base_url("outsourcing/remover/$outsourcing->id").'"><i class="btn btn-default fa fa-trash btn-xs"></i></a></td>': null ;                        
                        $botaoOnOffView = $outsourcing->ativo > 0 ? '<i class="btn btn-primary btn-xs" style="width:32px;">ON</i>' : '<i class="btn btn-warning btn-xs" style="width:32px;">OFF</i>';
                        $botaoOnOff = ($conjunto->botoesOnOff !== false) ? '<td><a href="'.base_url("outsourcing/onoff/$outsourcing->id").'">'.$botaoOnOffView.'</a></td>' : null ;
                        ?>
                        <tr>
                            <td><a href="<?=base_url('outsourcing/'.$conjunto->botoesEdit->url.'/'.$outsourcing->id)?>"><i class="btn btn-default <?=$conjunto->botoesEdit->icon?> btn-xs"></i></a></td>
                            <?=$botaoDel?>
                            <?=$botaoOnOff?>
                            <td><?=$outsourcing->titulo?></td>
                            <td><?=$outsourcing->descricao?></td>                                                                                                                              
                        </tr>                        
                        <?php 
                    }
                    
                    ?>            
                </tbody>
            </table>            
        </div>       
    </section>   
</div>