<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Premios / Soluções&nbsp;<small><?=$label?></small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li>Premios / Soluções</li>
            <li class="active"><?=$page?></li>
        </ol>
    </section>   

    <section class="content">
        <div class="col-lg-12">
            <?php
            if(validation_errors())
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.validation_errors().'</h4></div>';
            }
            
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }            
            ?>
        </div>
        <div class="col-lg-12">           
            <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title">Ver <?=$premio->label?></h2>
                </div><!-- /.box-header -->
                <div class="box-body">                
                    <div class="row">                                                      
                        <section class="col-md-6 col-lg-6 form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-lg-3 text-right">
                                    Título:                                
                                </label>
                                <div class="col-md-8 col-lg-7">
                                    <h4><?=$premio->nome?></h4>
                                </div>
                            </div>
                        </section>    
                        <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">    
                            <div class="form-group">
                                <label class="control-label col-md-4 col-lg-3">
                                    Tipo:
                                </label>                                
                                <div class="col-md-8 col-lg-7">
                                    <h4><?=$optTipo[$premio->tipo]?></h4>                                       
                                </div>
                            </div>
                        </section>
                    </div>
                    <hr/>
                    <div class="row">
                        <section class="col-md-6 col-lg-6 form-horizontal">
                            <div class="form-group">                            
                                <label class="col-md-4 col-lg-3 text-right">
                                    Imagem de Exibição:
                                </label>                                                               
                                <div class="col-md-8 col-lg-9">                                
                                    <img class="img-responsive" src="<?=$pathBasePre.$premio->img?>"/>
                                </div>                      
                            </div>                       
                        </section>
                        <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                            <div class="form-group">                            
                                <label class="col-md-4 col-lg-3 text-right">
                                    Imagem do Rodapé:
                                </label>                                                              
                                <div class="col-md-8 col-lg-9">                                
                                    <img class="img-responsive" src="<?=$pathBasePre.$premio->thumb?>"/>
                                </div>                      
                            </div>                       
                        </section>                            
                    </div>                    
                    <br/>
                    <hr/>
                    <div class="col-lg-12">                
                        <a href="<?=base_url("premios/listar")?>" class="btn btn-default btn-lg">Voltar</a>
                    </div>             
                </div>    
            </div>                       
        </div>
    </section>
</div>