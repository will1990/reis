<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Premios / Soluções&nbsp;<small><?=$label?></small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li>Prêmios / Soluções</li>
            <li class="active"><?=$page?></li>
        </ol>
    </section>   

    <section class="content">
        <div class="col-lg-12">
            <?php
            if(validation_errors())
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.validation_errors().'</h4></div>';
            }
            
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }            
            ?>
        </div>
        <div class="col-lg-12"> 
            <form class="form form-vertical" action="<?=base_url("premios/editar/$premio->id")?>" method="post" enctype="multipart/form-data">
                <div class="box box-primary">
                    <div class="box-header">
                        <h2 class="box-title">Editar <?=$premio->label?></h2>
                    </div><!-- /.box-header -->
                    <div class="box-body">                
                        <div class="row">                                                      
                            <section class="col-md-6 col-lg-6 form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-lg-3 text-right">
                                        Nome:                                
                                    </label>
                                    <div class="col-md-8 col-lg-7">
                                        <input class="form-control" value="<?=$premio->nome?>" name="preNome"/>
                                    </div>
                                </div>
                            </section>    
                            <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">    
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-lg-3">
                                        Tipo:
                                    </label>                                
                                    <div class="col-md-6 col-lg-5">
                                        <select class="form-control" name="preTipo">
                                            <?php
                                            foreach ($optTipo as $id => $opt):
                                                $select = $premio->tipo == $id ? 'selected=""':'';
                                                ?>
                                                <option value="<?=$id?>" <?=$select?> ><?=$opt?></option>
                                                <?php
                                            endforeach;                                            
                                            ?>
                                        </select>                                    
                                    </div>
                                    <div class="col-md-2 col-lg-4"></div>
                                </div>
                            </section>
                        </div>
                        <hr/>
                        <div class="row">
                            <section class="col-md-6 col-lg-6 form-horizontal">
                                <div class="form-group">                            
                                    <label class="col-lg-3 text-right">
                                        Escolha uma Imagem para Exibição na Galeria:
                                    </label>
                                    <div class="col-lg-9">                         
                                        <input type="file" id="image" name="preImg" class="btn btn-default" onchange="readURL(this,'new_pre_img');" />                           
                                    </div>
                                </div>
                                <div class="form-group">                                
                                    <div class="col-lg-offset-3 col-md-4 col-lg-9 dist_sup">                                
                                        <img id="new_pre_img" class="img-responsive" src="<?=$pathBasePre.$premio->img?>"/>
                                    </div>                      
                                </div>                       
                            </section>
                            <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                                <div class="form-group">                            
                                    <label class="col-lg-3 text-right">
                                        Escolha a miniatura para o Rodapé:
                                    </label>
                                    <div class="col-lg-9">                         
                                        <input type="file" id="image" name="preThumb" class="btn btn-default" onchange="readURL(this,'new_pre_thumb');" />                           
                                    </div>
                                </div>
                                <div class="form-group">                                
                                    <div class="col-lg-offset-3 col-md-4 col-lg-9 dist_sup">                                
                                        <img id="new_pre_thumb" class="img-responsive" src="<?=$pathBasePre.$premio->thumb?>"/>
                                    </div>                      
                                </div>                       
                            </section>                            
                        </div>
                        <input type="hidden" name="preImgOld" value="<?=$premio->img?>"/>
                        <input type="hidden" name="preThmOld" value="<?=$premio->thumb?>"/>
                        <br/>
                        <hr/>
                        <div class="col-lg-12">
                            <input class="btn btn-primary btn-lg" type="submit" name="preAct" value="Atualizar"/>
                        </div>                
                    </div>    
                </div>
            </form>             
        </div>
    </section>
</div>