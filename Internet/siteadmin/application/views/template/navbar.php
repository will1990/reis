<header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">       
        <img src="<?=base_url('images/img_template/logo_reis.png')?>" class="img-responsive" alt="ReisOffice"/>        
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <?php            
            //$label = $_SERVER['REQUEST_URI'];               
        ?>
        
        <!-- Titulo Barra -->
        <li class="nav col-xs-8 col-sm-7 col-md-9 col-lg-10 text-center"><div class="barra_titulo">Gerenciamento do Site</div></li>
        <!--<li class="nav col-xs-8 col-sm-7 col-md-9 col-lg-10 text-center"><div class="barra_titulo"><?//=$label?></div></li>-->
        
        <!-- Usuário Opções Barra -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!--<img src="<?//= base_url('images/img_template/favicon.png') ?>" class="user-image" alt="User Image">-->
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="hidden-xs"><?=$login->fname?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= base_url('images/img_template/favicon.png') ?>" class="img-circle" alt="User Image">
                            <p><?=$login->nome?> <small><?=$login->nm_grupo?></small></p>
                        </li>                       
                        <!-- Menu Body -->
                        <!--<li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Alterar Senha</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Alterar Nome</a>
                                </div>
                            </div> -->
                            <!-- /.row -->
                        <!-- </li> -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                            </div>
                            <div class="pull-right">
                                <a href="<?=base_url('login/logout')?>" class="btn btn-default btn-flat">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>         
            </ul>
        </div>
    </nav>
</header>
