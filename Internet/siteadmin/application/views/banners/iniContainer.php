<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Banners&nbsp;<small><?= !empty($label)? $label->descricao : null ?></small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li>Banners</li>
            <li class="active"><?=$page?></li>
        </ol>
    </section>   

    <section class="content">
        <div class="col-lg-12">
            <?php
            if(validation_errors())
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.validation_errors().'</h4></div>';
            }
            
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }            
            ?>
        </div>