<div class="col-lg-12">
    <div class="box box-primary">
        <form class="form form-vertical" action="<?=base_url("banners/novo")?>" method="post">
            <div class="box-header">
                <h2 class="box-title">Tipo de Banner</h2>
            </div><!-- /.box-header -->
            <div class="box-body">                
                <div class="row">
                    <section class="col-md-12 col-lg-12 form-horizontal">
                        <div class="form-group">
                            <label class="control-label text-right col-md-2 col-lg-2">
                                Selecione o Tipo:
                            </label>                            
                            <div class="col-md-4 col-lg-4">
                                <select class="form-control" name="bannTipo" onChange="javascript: this.form.submit()">
                                    <option value=""></option>                                    
                                    <?php                                     
                                    
                                    foreach ($tipos as $id => $tipo)
                                    {
                                        $selected = $id == $tipo->id ? "selected=''" : " " ;
                                        echo "<option $selected value='$tipo->id'>$tipo->valor</option>";
                                    }
                                    
                                    ?>                        
                                </select>                                
                            </div>
                            <div class="col-lg-6"></div>                                    
                        </div>                            
                    </section>
                </div>
            </div>
        </form>
    </div>
</div>
