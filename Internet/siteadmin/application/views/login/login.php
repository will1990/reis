        <div class="container-fluid">
            <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4 col-sm-8 col-sm-offset-2">  
                <div class="row text-center">
                    <div class="col-lg-12">
                        <img src="<?=base_url('images/img_template/logo_reis.png')?>" class="img-responsive col-xs-8 col-sm-7 col-md-6 col-lg-5" alt="ReisOffice"/>  
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
                        <?= ($message) ? '<div class="alert alert-warning">'.$message.'</div>' : ''?>
                        <?= validation_errors()?>
                    </div>
                </div>
                <div class="panel panel-info">            
                    <div class="panel-heading">                
                        <div class="panel-title">Sistema de Administração do Conteúdo do Site</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><!--<a href="#">Esqueceu a senha?</a>--></div>
                    </div>

                    <div style="padding-top:30px" class="panel-body">
                        <div style="display:none" id="login-alert" class="col-sm-12"></div>                
                        <form id="loginform" class="form-horizontal" action="<?=base_url('login')?>" method="post" role="form">
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="login-username" type="text" class="form-control" value="<?= set_value("loginUser")?>" name="loginUser" placeholder="Usuário">                                        
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="login-password" type="password" class="form-control" value="<?= set_value("loginPass")?>" name="loginPass" placeholder="Senha">
                            </div>

                            <div style="margin-top:10px" class="form-group col-lg-12">               
                                <!-- Button -->
                                <div class="controls col-xs-4 col-sm-4 col-md-3 col-lg-3">
                                    <input type="submit" name="loginAct" class="btn btn-primary btn-lg" value="Login"/>                         
                                </div>                        
                                <div class="checkbox col-xs-8 col-sm-8 col-md-9 col-lg-9">
                                    <label>
                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Lembrar
                                    </label>
                                </div>
                            </div>                          
                        </form>
                    </div>                     
                </div>
                <!--<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>-->
            </div>    
        </div>
    </div>
</html>
