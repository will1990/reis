<div class="col-lg-12">    
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Ver Destaque</h2>
        </div><!-- /.box-header -->
        <div class="box-body">                
            <div class="row">                                                      
                <section class="col-md-6 col-lg-6 form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-4 text-right">
                            URL do Botão (Veja Mais):                                
                        </label>
                        <div class="col-md-4 col-lg-8 dist_sup">
                            <h4><?=$destaque->url?></h4>
                        </div>
                    </div> 
                    <hr/>
                    <div class="form-group text-left">
                        <label class="control-label col-md-4 col-lg-4 text-right">
                            Título:                                
                        </label>
                        <div class="col-md-8 col-lg-8">
                            <h4><?=$destaque->titulo?></h4>
                        </div>
                    </div>
                    <?php                    
                    if($tipo == 10)
                    {
                        ?>
                        <hr/>
                        <div class="form-group">
                            <label class="control-label col-lg-2 text-right">Descrição: </label>                                
                            <div class="col-lg-10 well">
                                <?=$destaque->texto?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </section>
                <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3 text-right">
                            Imagem:                                
                        </label>
                        <div class="col-md-4 col-lg-8 dist_sup">                            
                            <img class="img-responsive" src="<?=$destaque->img?>" alt="Banner Index"/>
                        </div>
                    </div>
                </section>
                <input type="hidden" name="dstqTipo" value="<?=$destaque->tipo?>"/>
            </div>
            <hr/>
            <div class="col-lg-12">                
                <a href="<?=base_url("destaques/listar/$label->valor")?>" class="btn btn-default btn-lg">Voltar</a>
            </div>
        </div>    
    </div>
                
</div>    
