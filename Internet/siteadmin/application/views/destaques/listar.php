<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Destaques&nbsp;<small><?=$label->descricao?></small>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php
            echo $conjunto->botaoAdd;
            ?>            
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Painel</a></li>
            <li class="active">Destaques</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <?php
            if(!empty($this->session->flashdata('sucesso')))
            {
                echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
                $this->session->set_flashdata('sucesso', '');
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }
            
            if(!empty($this->session->flashdata('feito')))
            {
                echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
                $this->session->set_flashdata('erro', '');
            }            
            ?>
        </div>
        <div class="box-body table-responsive">            
            <table width="100%" id="tabsAdmin" class="table table-bordered table-striped">
                <thead>                    
                    <tr>
                        <th width="1%"><i class="fa <?=$conjunto->botoesEdit->icon?> fa-1x"></i></th>
                        <?= $conjunto->botoesDel !== false ? '<th width="1%"><i class="fa fa-trash fa-1x"></i></th>' : null ?>
                        <?= $conjunto->botoesOnOff !== false ? '<th width="3%">Ativo</th>' : null ?>
                        <th width="16%">Nome Imagem</th>
                        <th width="19%">Link Botão</th>               
                        <th width="20%">Título</th>
                        <th width="40%">Texto</th>                                       
                    </tr>
                </thead>
                <tbody>
                    <?php
                    
                    foreach($destaques as $destaque)
                    {
                        $botaoDel = ($conjunto->botoesDel !== false) ? '<td><a href="'.base_url("destaques/remover/$destaque->id").'"><i class="btn btn-default fa fa-trash btn-xs"></i></a></td>': null ;
                        $botaoOnOffView = $destaque->ativo > 0 ? '<i class="btn btn-primary btn-xs" style="width:32px;">ON</i>' : '<i class="btn btn-warning btn-xs" style="width:32px;">OFF</i>';
                        $botaoOnOff = ($conjunto->botoesOnOff !== false) ? '<td><a href="'.base_url("destaques/onoff/$destaque->id").'">'.$botaoOnOffView.'</a></td>' : null ;
                        ?>
                        <tr>
                            <td><a href="<?=base_url('destaques/'.$conjunto->botoesEdit->url.'/'.$destaque->id)?>"><i class="btn btn-default <?=$conjunto->botoesEdit->icon?> btn-xs"></i></a></td>
                            <?=$botaoDel?>
                            <?=$botaoOnOff?>
                            <td><?=$destaque->img?></td>
                            <td><?=$destaque->url?></td>
                            <td><?=$destaque->titulo?></td>
                            <td><?=$destaque->texto?></td>                                                                            
                        </tr>                        
                        <?php 
                    }
                    
                    ?>            
                </tbody>
            </table>            
        </div> 
        
        <?php
        //print_r($destaques);
        ?>
        
    </section>   
</div>

