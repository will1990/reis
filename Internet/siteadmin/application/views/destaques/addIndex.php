<div class="col-lg-12"> 
    <form class="form form-vertical" action="<?=base_url("destaques/novo/$tipo")?>" method="post" enctype="multipart/form-data">
        <div class="box box-primary">
            <div class="box-header">
                <h2 class="box-title">Novo Destaque</h2>
            </div><!-- /.box-header -->
            <div class="box-body">                
                <div class="row">                                                      
                    <section class="col-md-6 col-lg-6 form-horizontal">                    
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-4 text-right">
                                URL do Botão (Veja Mais):                                
                            </label>
                            <div class="col-md-4 col-lg-6 dist_sup">
                                <input class="form-control" value="<?=$retorno["dstqUrl"]?>" type="text" name="dstqUrl"/>
                            </div>
                        </div> 
                        <hr/>
                        <div class="form-group text-left">
                            <label class="control-label col-md-3 col-lg-3 text-right">
                                Título:                                
                            </label>
                            <div class="col-md-8 col-lg-7">
                                <input class="form-control" value="<?=$retorno["dstqTitulo"]?>" name="dstqTitulo"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-lg-3">Descrição: </label>                                
                            <div class="col-md-8 col-lg-7">
                                <textarea class="form-control" name="dstqTexto" style="height: 150px;"><?=$retorno["dstqTexto"]?></textarea> 
                            </div>                                                
                            <br/>                                                               
                        </div>
                    </section>
                    <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">                        
                        <div class="form-group">                                
                            <div class="col-lg-offset-3 col-md-12 col-lg-9 dist_sup">    
                                <img id="mini_foto_new" class="img-responsive" src="<?=$pathBaseDstq.'notImg.jpg'?>"/>                       
                            </div>                      
                        </div>                        
                        <hr/>
                        <div class="form-group">                            
                            <label class="col-lg-3 text-right">
                                Escolha uma Imagem:
                            </label>
                            <div class="col-lg-9">                         
                                <input type="file" id="image" name="dstqImg" class="btn btn-default" onchange="readURL(this,'mini_foto_new');" />                           
                            </div>
                        </div>  
                    </section>
                    <input type="hidden" name="dstqTipo" value="<?=$tipo?>"/>
                </div>
                <br/>
                <hr/>
                <div class="col-lg-12">
                    <input class="btn btn-primary btn-lg" type="submit" name="dstqAct" value="Publicar"/>
                </div>                
            </div>    
        </div>
    </form>             
</div>    
    