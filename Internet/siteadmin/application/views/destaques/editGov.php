<div class="col-lg-12"> 
    <form class="form form-vertical" action="<?=base_url("destaques/editar/$destaque->id")?>" method="post" enctype="multipart/form-data">
        <div class="box box-primary">
            <div class="box-header">
                <h2 class="box-title">Editar Destaque</h2>
            </div><!-- /.box-header -->
            <div class="box-body">                
                <div class="row">                                                      
                    <section class="col-md-6 col-lg-6 form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-4 text-right">
                                URL do Botão (Veja Mais):                                
                            </label>
                            <div class="col-md-4 col-lg-8 dist_sup">
                                <input class="form-control" value="<?=$destaque->url?>" type="text" name="dstqUrl"/>
                            </div>
                        </div> 
                        <hr/>
                        <div class="form-group text-left">
                            <label class="control-label col-md-4 col-lg-4 text-right">
                                Título:                                
                            </label>
                            <div class="col-md-8 col-lg-8">
                                <input class="form-control" value="<?=$destaque->titulo?>" name="dstqTitulo"/>
                            </div>
                        </div>                                                    
                    </section>
                    <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                        <div class="form-group">                                
                            <div class="col-md-12 col-lg-12 dist_sup">    
                                <img id="mini_foto_new" class="img-responsive" src="<?=$pathBaseDstq.$destaque->img?>"/>                       
                            </div>                      
                        </div>                        
                        <hr/>
                        <div class="form-group">                            
                            <label class="col-lg-3 text-right">
                                Escolha uma Imagem para Alterar Existente:
                            </label>
                            <div class="col-lg-9">                         
                                <input type="file" id="image" name="dstqImg" class="btn btn-default" onchange="readURL(this,'mini_foto_new');" />                           
                            </div>
                        </div>                    
                    </section>
                    <input type="hidden" name="dstqTipo" value="<?=$destaque->tipo?>"/>
                    <input type="hidden" name="dstqImgOld" value="<?=$destaque->img?>"/>
                </div>
                <br/>
                <hr/>
                <div class="col-lg-12">
                    <input class="btn btn-primary btn-lg" type="submit" name="dstqAct" value="Atualizar"/>
                </div>                
            </div>    
        </div>
    </form>             
</div>   
