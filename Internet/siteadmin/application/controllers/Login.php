<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
    /**
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Login_model","mdlogin");
                
        // Recupera a mensagem de status, se houver
	$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Recupera dados do usuario, se houver
        $this->data["dados"] = $this->session->userdata("dados_inter");
    }
    
    //LOGIN
    //Formulário de Login - Interrompe sessões ao Executar Obrigatoriamente
    public function index()
    {      
        // Termina a sessão obrigatoriamente
        $this->logout(FALSE);
        
        // Valida as informações do usuário        
        $this->form_validation->set_rules('loginUser', 'Usuário', 'required|numeric');
        //$this->form_validation->set_rules('loginPass', 'Senha', 'required');
        $this->form_validation->set_rules('loginPass', 'Senha', 'required|callback_login_user');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        
        if($this->form_validation->run($this) !== FALSE)
        {    
            redirect(base_url("painel"));                     
        }
        else
        {            
            $this->load->view("login/header");
            $this->load->view("login/login", $this->data);
        }
    }
    
    //Validação de Login - Abertura de Sessão
    public function login_user($pass)            
    { 
        $user = $this->input->post("loginUser");
        
        for($emp = 1; $emp <= 2; $emp++)
        {
            $url = 'http://intranet.reisoffice.com.br/registro/senhas/validar_usuario/'.$user.'/'.$pass.'/'.$emp;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FILETIME, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $json = curl_exec($ch);
            $obj = json_decode($json); 
            if($obj->result !== false)
                break;
        }        
        
        $login = $this->validaLogin($obj);
                
        if($login->logged === false)
        {
            $this->form_validation->set_message("login_user", $login->error);
            return false;
        }
        else
        {
            $this->mdlogin->registroLogin($login->user_id);
            $this->session->set_userdata(array("is_logged_vendas" => true, "dados_inter" => $login));
            return true;
        }
        
    }
    
    public function validaLogin($obj = null)
    {  
        $data = array(  "logged"        => FALSE,
                        "error"         => "",
                        "nome"          => "",
                        "user_id"       => "",
                        "id_tipo"       => "",
                        "id_grupo"      => "",
                        "nm_grupo"      => "",
                        "fname"         => ""   );
                            
        if($obj->result !== false)
        {
            $query = $this->mdlogin->getLogin($obj->matricula);
                        
            if($query->result_id->num_rows > 0)
            {
                $result = $query->row();
                            
                $data['logged'] = true;
                $data['nome'] = $obj->nome;               
                $data['user_id'] = $result->id;
                $data['id_tipo'] = $result->id_tipo;
                $data['id_grupo'] = $result->id_grupo;
                $data['nm_grupo'] = $this->mdlogin->getGrupo($result->id_grupo);
                $data['fname'] = ucfirst(strtolower(explode(' ', $obj->nome)[0]));        
            }
            else
            {        
                $data['error'] = "Acesso não Permitido";                
            }
        }
        else
        {
            $data["error"] = $obj->error;
        }
        return (object)$data;
    }
    
    //LOGOUT
    //Destroi a Sessão
    public function logout($redir = TRUE)
    {
        //$dados = $this->data["dados"];
        $this->session->unset_userdata(array("is_logged_inter" => "", "dados_inter" => ""));
        
        if($redir)
        {
            session_destroy();                      
            redirect(base_url("login"));
        }
    }
    
    //Redirecionamento e Tratamento em caso de Falta de Autoridade para a Operação desejada
    public function noaut()
    {
        $this->session->set_flashdata("erro", "Falta autoridade!");        
        if(!empty($_SERVER['HTTP_REFERER']))
        {
            header("Location: ".$_SERVER['HTTP_REFERER']."");            
        }
        else
        {
            redirect(base_url("painel"));
        }
    }    
}