<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outsourcing extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Outsourcing_model","mdout");
        
        $this->data['pathBaseOut'] = substr(base_url(),0,-6)."images/conteudo/";
                
        // Recupera a mensagem de status, se houver
	//$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Pega a URL atual para Verificação e Salvamento de Método
        $this->sistema->verifMetodo(URL_ATUAL);
        
        // Verifica o Login e Traz a Sessão
        $this->data["login"] = ($this->sistema->checkLogin($this->session->userdata("dados_inter"), URL_ATUAL)!== false)? $this->session->userdata("dados_inter") : null ;
        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->session->userdata("dados_inter"));
    }
    
    //OUTSOURCING
    //Listando Todos os Outsourcings existentes
    public function listar()
    {        
        $this->data["outsourcings"] = $this->mdout->getAllOutsourcings();
        $this->data["label"] = "";
        $this->data["conjunto"] = $this->sistema->montarConjuntoBotoes($this->data["login"]);
       
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar', $this->data);
        $this->load->view('outsourcing/listar', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
        
    //Ativando / Desativando Outsourcings
    public function onoff($id = null)
    {        
        $resul = $this->mdout->getStatusOutsourcing($id);
        
        $act = $resul->ativo == 1 ? 0 : 1 ;      
        
        if($this->mdout->editStatusOutsourcing($id, $act) != false)
        {
            if($act === 1)
            {
                $this->session->set_flashdata('sucesso', "Outsourcing Ativo");                
            }                        
            else
            {
                $this->session->set_flashdata('feito', "Outsourcing Desativado");                
            }            
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro de Atualização, tente mais Tarde!");
        }
        
        redirect(base_url("outsourcing/listar"));
    }
        
    //Adicionando novo Outsourcing
    public function novo()
    {
        if($this->input->post("outAct"))
        {  
            $this->form_validation->set_rules('outTitulo', 'Titulo de Outsourcing', 'required');
            $this->form_validation->set_rules('outDesc', 'Descrição de Outsourcing', 'required');
            $this->form_validation->set_rules('outText', 'Conteúdo de Outsourcing ', 'required');             
                       
            if ($this->form_validation->run() !== false) 
            {
                $nome = date('yms').rand(1,5);
                $img = $_FILES["outImg"]["error"] == 0 ? $this->validArq("outImg", "Out", $nome):'';
                $imgOpt = $_FILES["outImgOpt"]["error"] == 0 ? $this->validArq("outImgOpt", "optOut", $nome):'';                
                
                if($this->mdout->addOutsourcing($img, $imgOpt) !== false)
                {
                    $this->session->set_flashdata('sucesso', "Outsourcing Publicado");                
                }
                else
                {
                    $this->session->set_flashdata('erro', "Erro na publicação, tente mais Tarde!");                
                }
                redirect(base_url() . "outsourcing/listar", "refresh");       
            }
            else 
            {                
                $this->formNewOut();
            }
        }
        else
        {
            $this->formNewOut();
        }       
    }
    
    public function formNewOut()
    {       
        $this->data["retorno"] = $this->retornoDados();
        $this->data["label"] = "Novo";        
        $this->data["page"] = "Novo";
       
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');        
        $this->load->view('outsourcing/addOut', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
    
    //Editando um Outsourcing Publicado
    public function editar($id = null)
    {
        if($this->input->post("outAct"))
        {           
            $this->form_validation->set_rules('outTitulo', 'Titulo de Outsourcing', 'required');
            $this->form_validation->set_rules('outDesc', 'Descrição de Outsourcing', 'required');
            $this->form_validation->set_rules('outText', 'Conteúdo de Outsourcing ', 'required');                    
                    
            if ($this->form_validation->run() != false) 
            {         
                $img = '';
                $imgOpt = '';
                $nome = date('yms').rand(1,5);                         
                if($_FILES["outImg"]["error"] == 0)
                {
                    $img = $this->validArq("outImg", "Out", $nome);
                    unlink(ARQ_PATH.'/../images/conteudo/'.$this->input->post("outImgOld"));
                }
                
                if($_FILES["outImgOpt"]["error"] == 0)
                {
                    $imgOpt = $this->validArq("outImgOpt", "optOut", $nome);                    
                    unlink(ARQ_PATH.'/../images/conteudo/'.$this->input->post("outImgOptOld"));
                }                       

                if($this->mdout->editOutsourcing($id, $imagem, $mini) != false)
                {
                    $this->session->set_flashdata('sucesso', "Outsourcing Atualizado");                
                }
                else
                {
                    $this->session->set_flashdata('erro', "Erro na atualização, tente mais Tarde!");                
                }                    
                redirect(base_url() . 'outsourcing/listar/', "refresh");                
            }
            else 
            {                
                $this->formEditOut($id);
            } 
        }
        else
        {
            $this->formEditOut($id);
        }
    }
    
    public function formEditOut($id = null)
    {
        $this->data["outsourcing"] = $this->mdout->getOutsourcing($id);
        $this->data["label"] = "Editar";
        $this->data["page"] = "Editar";
                
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');
        $this->load->view('outsourcing/editOut', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);        
    }
    
    //Remover Outsourcing pelo ID
    public function remover($id = null)
    {
        $outsourcing = $this->mdout->getOutsourcing($id);              
        if($this->mdout->delOutsourcing($outsourcing->id) !== false)
        {
            $outsourcing->img_sup !== null ? unlink(ARQ_PATH.'/../images/conteudo/'.$outsourcing->img_sup): '';
            $outsourcing->img_opc !== null ? unlink(ARQ_PATH.'/../images/conteudo/'.$outsourcing->img_opc): '';
            $this->session->set_flashdata('sucesso', "Prêmio Removido");                
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro na exclusão, tente mais Tarde!");                
        }
        redirect(base_url() . "outsourcing/listar/", "refresh");        
    }
     
    //Mostrar informações do Outsourcing pelo ID (Sem Autoridade)
    public function ver($id = null)
    {
        $this->data["outsourcing"] = $this->mdout->getOutsourcing($id);
        $this->data["label"] = "Mostrar";
        $this->data["page"] = "Mostrar";
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');        
        $this->load->view('outsourcing/mostrarOut', $this->data);       
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);        
    }
    
    
    //Funções Complementares
    //Retorno do POST com informações para o Form, caso tenha erro do form_validation
    public function retornoDados()
    {
        if(!empty($this->input->post("outAct")))
        {           
            return $this->input->post(); 
        }
        else
        {
            return null;
        }        
    }
    
    //Faz o upload de Imagens
    public function validArq($index = null, $prefixo = null, $nome = null)
    {
        $ext = pathinfo($_FILES[$index]["name"], PATHINFO_EXTENSION); 

        $this->load->library('upload');

        $configuracao = array(
         'upload_path'   => ARQ_PATH.'/../images/conteudo/',
         'allowed_types' => 'png|jpg|jpeg',
         'file_name'     => $prefixo.'_'.$nome.'.'.$ext,
         'max_size'      => '1000'
         );

        $this->upload->initialize($configuracao);

        if($this->upload->do_upload($index))
        {
            return $configuracao['file_name'];
        }
        else
        {            
            $this->session->set_flashdata('feito', $this->upload->display_errors());
            return false;
        }
    }
}
