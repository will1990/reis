<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Banners_model","mdbanner");
        
        $this->data["pathBaseBann"] = substr(base_url(),0,-6)."images/conteudo/";
                
        // Recupera a mensagem de status, se houver
	//$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Pega a URL atual para Verificação e Salvamento de Método
        $this->sistema->verifMetodo(URL_ATUAL);
        
        // Verifica o Login e Traz a Sessão
        $this->data["login"] = ($this->sistema->checkLogin($this->session->userdata("dados_inter"), URL_ATUAL)!== false)? $this->session->userdata("dados_inter") : null ;
        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->session->userdata("dados_inter"));
    }
    
    //BANNERS
    //Listando Todos os Banners de Determinada Categoria (Página)
    public function listar($tipo = null)
    {
        $link = $this->sistema_model->getCategs("banner", $tipo);  
        $this->data["banners"] = $this->mdbanner->getAllBanners($link->id);
        $this->data["label"] = $this->sistema_model->getType($link->id);
        $this->data["conjunto"] = $this->sistema->montarConjuntoBotoes($this->data["login"]);
                
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar', $this->data);
        $this->load->view('banners/listar', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
    
    //Ativando / Desativando Banners
    public function onoff($id = null)
    {        
        $resul = $this->mdbanner->getStatusBanner($id);
        $link = $this->sistema_model->getCategs("banner", $resul->tipo);
                
        if((!empty($resul->dt_inicio)) AND (!empty($resul->dt_fim)))
        {            
            $inicio = strtotime(invertData($resul->dt_inicio));
            $fim = strtotime(invertData($resul->dt_fim));     
            $hoje = strtotime(date('d/m/Y'));         
                        
            if(($hoje >= $inicio) AND ($hoje <= $fim))
            {
                $ac = $resul->ativo;
                $act = 3;
            }
            else
            {
                $ac = $resul->ativo == 1 ? 0 : 1 ;
                $act = $ac;
            }
        }
        else
        {            
            $ac = $resul->ativo == 1 ? 0 : 1 ;
            $act = $ac;
        }       
        
        if($this->mdbanner->editStatusBanner($id, $ac) != false)
        {
            if($act === 1)
            {
                $this->session->set_flashdata('sucesso', "Banner Ativo");                
            }
            elseif($act === 3)
            {
                $this->session->set_flashdata('erro', "Não é possivel Desativar Banners com Data de Vigência");
            }            
            else
            {
                $this->session->set_flashdata('feito', "Banner Desativado");                
            }            
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro de Atualização, tente mais Tarde!");
        }
        
        redirect(base_url() . "banners/listar/$link->valor");
    }
      
    //Adicionando novo Banner
    public function novo($cat = 0)
    {
        $data = (object)$this->input->post();
        
        if($cat > 0)
        {           
            $this->form_validation->set_rules('bannTitulo', 'Titulo do Banner', 'required');
            
            if(!empty($data->bannIni) OR (!empty($data->bannFim)))
            {
                $this->form_validation->set_rules('bannIni', 'Data de Início', 'required');
                $this->form_validation->set_rules('bannFim', 'Data de Término', 'required|callback_validaData');
            }
            
            if(($cat == 1) OR ($cat == 3))
            {
                $this->form_validation->set_rules('bannUrl', 'Url do Botão', 'required');
            }

            if ($this->form_validation->run() !== false) 
            {              
                $arquivo = $this->validArq("bannImg", "bann");
                if($arquivo !== false)
                {            
                    $link = $this->sistema_model->getCategs("banner", $cat);
                    $ativo = $this->verifAtivo();

                    if($this->mdbanner->addBanner($ativo, $arquivo) !== false)
                    {
                        $this->session->set_flashdata('sucesso', "Banner Publicado");                
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro na publicação, tente mais Tarde!");                
                    }
                    redirect(base_url() . "banners/listar/$link->valor", "refresh");
                }
                else
                {                    
                    $this->formBanner();
                }
            }
            else 
            {                
                $this->formBanner();
            }
        }
        else
        {
            $this->formBanner();
        }       
    }
    
    public function formBanner()
    {
        $data = (object)$this->input->post();
        if($data->bannTipo)
        {
            $tipo = $data->bannTipo;
            $this->data["tipo"] = $tipo;
        }        
        else
        {
            $tipo = 0;
            $this->data["tipo"] = $tipo;
        }
        
        $this->data["tipos"] = $this->sistema_model->getCategs("banner", null);       
        $this->data["retorno"] = $this->retornoDados();
        $this->data["label"] = $this->sistema_model->getType($tipo);        
        $this->data["page"] = "Novo";

        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');

        $this->load->view('banners/iniContainer', $this->data);
        $this->load->view('banners/addTipo', $this->data);        

        if($tipo == 1 OR $tipo == 3)
        {
            $this->load->view('banners/addIndexProd', $this->data);
        }

        if($tipo == 4)
        {
            $this->load->view('banners/addSocial', $this->data);
        }        

        $this->load->view('banners/fimContainer');
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }   
    
    //Editando um Banner Existente
    public function editar($id = null)
    {
        $data = (object)$this->input->post();
        
        if(!empty($data))
        {           
            $this->form_validation->set_rules('bannTitulo', 'Titulo do Banner', 'required');
            
            if(!empty($data->bannIni) OR (!empty($data->bannFim)))
            {
                $this->form_validation->set_rules('bannIni', 'Data de Início', 'required');
                $this->form_validation->set_rules('bannFim', 'Data de Término', 'required|callback_validaData');
            }
            
            if(($data->bannTipo == 1) OR ($data->bannTipo == 3))
            {
                $this->form_validation->set_rules('bannUrl', 'Url do Botão', 'required');
            }
                        
            if ($this->form_validation->run() != false) 
            {                
                $arquivo = ($_FILES["bannImg"]["error"] > 0)? true : $this->validArq("bannImg", "bann");
                if($arquivo !== false)
                {
                    if($_FILES["bannImg"]["error"] == 0)
                    {                        
                        unlink(ARQ_PATH.'/../images/conteudo/'.$data->bannImgOld);
                    }
                    
                    $resul = $this->mdbanner->getStatusBanner($id); 
                    $link = $this->sistema_model->getCategs("banner", $data->bannTipo);

                    if($this->mdbanner->editBanner($id, $resul->ativo, $arquivo) != false)
                    {
                        $this->session->set_flashdata('sucesso', "Banner Atualizado");                
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro na atualização, tente mais Tarde!");                
                    }                    
                    redirect(base_url() . 'banners/listar/'.$link->valor, "refresh"); 
                }
                else
                {
                    $this->formEditBanner($id);
                }
            }
            else 
            {                
                $this->formEditBanner($id);
            } 
        }
        else
        {
            $this->formEditBanner($id);
        }
    }
    
    public function formEditBanner($id)
    {
        $banner = $this->mdbanner->getBanner($id);
        $tipo = $banner->tipo;
        $this->data["label"] = $this->sistema_model->getType($tipo);
        $this->data["banner"] = $banner;        
        $this->data["page"] = "Editar";
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');

        $this->load->view('banners/iniContainer', $this->data);
        
        if($tipo == 1 OR $tipo == 3)
        {
            $this->load->view('banners/editIndexProd', $this->data);
        }

        if($tipo == 4)
        {
            $this->load->view('banners/editSocial', $this->data);
        }
        
        $this->load->view('banners/fimContainer');
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);        
    }
    
    //Remover Banner pelo ID
    public function remover($id = null)
    {
        $resul = $this->mdbanner->getStatusBanner($id);
        $link = $this->sistema_model->getCategs("banner", $resul->tipo);
               
        if($this->mdbanner->delBanner($id) !== false)
        {
            unlink(ARQ_PATH.'/../images/conteudo/'.$resul->img);
            $this->session->set_flashdata('sucesso', "Banner Removido");                
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro na exclusão, tente mais Tarde!");                
        }
        redirect(base_url() . "banners/listar/$link->valor", "refresh");        
    }
    
    //Mostrar informações do Banner pelo ID (Sem Autoridade)
    public function ver($id = null)
    {
        $banner = $this->mdbanner->getBanner($id);
        $banner->img = $this->data["pathBaseBann"].$banner->img;
        $this->data["label"] = $this->sistema_model->getType($banner->tipo);  
        $this->data["banner"] = $banner;        
        $this->data["page"] = "Mostrar";
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');
        $this->load->view('banners/iniContainer', $this->data);
        $this->load->view('banners/mostrarBann', $this->data);                
        $this->load->view('banners/fimContainer');
        $this->load->view('template/footer');          
    }
    
    
    //Funções Complementares
    //Retorno do POST com informações para o Form, caso tenha erro do form_validation
    public function retornoDados()
    {
        $data = (object)$this->input->post();
        
        if(!empty($data->bannAct))
        {           
            return $this->input->post(); 
        }
        else
        {
            return null;
        }        
    }   
    
    //Validando a o período de Data inserido nos Forms
    public function validaData()
    {
        $data = (object)$this->input->post();
        $inicio = strtotime($data->bannIni);
        $fim = strtotime($data->bannFim);
         
        print_r($inicio);
        print_r($fim);
        
        if($inicio <= $fim)
        {
            return true;
        }            
        else
        {
            $this->form_validation->set_message('validaData', 'A data de Início deve ser Menor que a data de Término');
            return false;
        }        
    }   
    
    //Validando o Status para o Novo Banner
    public function verifAtivo()
    {
        $data = (object)$this->input->post();
        
        if(!empty($data->bannIni) OR (!empty($data->bannFim)))
        {
            $ini = strtotime($data->bannIni);
            $fim = strtotime($data->bannFim);
            $hj = strtotime(date('d/m/Y'));                
            return (($hj >= $ini) AND ($hj <= $fim)) ? 1 : 0 ; 
        }
        else
        {
            return 0;
        }
    }
    
    //Faz o upload de Imagens
    public function validArq($index = null, $prefixo = null)
    {
        $ext = pathinfo($_FILES[$index]["name"], PATHINFO_EXTENSION); 

        $this->load->library('upload');

        $configuracao = array(
         'upload_path'   => ARQ_PATH.'/../images/conteudo/',
         'allowed_types' => 'png|jpg|jpeg',
         'file_name'     => $prefixo.'_'.date('yms').rand(1,5).'.'.$ext,
         'max_size'      => '1000'
         );

        $this->upload->initialize($configuracao);

        if($this->upload->do_upload($index))
        {
            return $configuracao['file_name'];
        }
        else
        {            
            $this->session->set_flashdata('feito', $this->upload->display_errors());
            return false;
        }
    }
}