<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
                
        // Verifica se tem permissao nesta pagina
        $this->data["login"] = $this->sistema->checkLogin($this->router->class, $this->router->method);
        
        // Recupera a mensagem de status, se houver
	$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
                        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->session->userdata("dados_inter"));        
    } 
    
    public function index()
    {
        //echo base_url();
        
        //print_R($this->data['menu']);
               
     
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar', $this->data);
        $this->load->view('painel/home', $this->data);
        $this->load->view('template/footer');
    }
}
