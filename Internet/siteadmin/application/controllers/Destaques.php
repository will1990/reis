<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques extends CI_Controller 
{
    /**
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Destaques_model","mddstqs");
        
        $this->data['pathBaseDstq'] = substr(base_url(),0,-6)."images/conteudo/";
                   
        // Recupera a mensagem de status, se houver
	//$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Pega a URL atual para Verificação e Salvamento de Método
        $this->sistema->verifMetodo(URL_ATUAL);
        
        // Verifica o Login e Traz a Sessão
        $this->data["login"] = ($this->sistema->checkLogin($this->session->userdata("dados_inter"), URL_ATUAL)!== false)? $this->session->userdata("dados_inter") : null ;
        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->session->userdata("dados_inter"));
    }   
    
    //DESTAQUES
    //Listando Todos os Destaques de Determinada Categoria (Página)
    public function listar($tipo = null)
    {
        $link = $this->sistema_model->getCategs("destaque", $tipo);
        $this->data["destaques"] = $this->mddstqs->getAllDstqs($link->id);
        $this->data["label"] = $this->sistema_model->getType($link->id);
        $this->data["conjunto"] = $this->sistema->montarConjuntoBotoes($this->data["login"]);
                
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar', $this->data);
        $this->load->view('destaques/listar', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
    
    //Ativando / Desativando Banners
    public function onoff($id = null)
    {        
        $resul = $this->mddstqs->getStatusDstqs($id);
        $link = $this->sistema_model->getCategs("destaque", $resul->tipo);
        $act = $resul->ativo == 1 ? 0 : 1 ;
               
        if($this->mddstqs->editStatusDstqs($id, $act) != false)
        {
            if($act == 1)
            {
                $this->session->set_flashdata('sucesso', "Destaque Ativo");                
            }                        
            else
            {
                $this->session->set_flashdata('feito', "Destaque Desativado");                
            }            
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro de Atualização, tente mais Tarde!");
        }       
        redirect(base_url() . "destaques/listar/$link->valor");
    }
    
    //Adicionando novo Destaque
    public function novo($cat = 0)
    {
        if($cat > 0)
        {       
            $this->form_validation->set_rules('dstqTitulo', 'Titulo do Destaque', 'required');
            $this->form_validation->set_rules('dstqUrl', 'Url do Botão', 'required');
                        
            if($cat == 1)
            {
                $this->form_validation->set_rules('dstqTexto', 'Descrição', 'required');
            }
                        
            if ($this->form_validation->run() != false) 
            {
                $arquivo = $this->validArq("dstqImg", "dstq");
                if($arquivo !== false)
                {
                    $link = $this->sistema_model->getCategs("destaque", $cat);

                    if($this->mddstqs->addDstqs(0, $arquivo) != false)
                    {
                        $this->session->set_flashdata('sucesso', "Destaque Publicado");                
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro na publicação, tente mais Tarde!");                
                    }
                    redirect(base_url() . "destaques/listar/$link->valor", "refresh");
                }
                else
                {
                    $this->formDstqs();
                }
            }
            else 
            {                
                $this->formDstqs();
            }
        }
        else
        {
            $this->formDstqs();
        }       
    }
    
    public function formDstqs()
    {
        if($this->input->post("dstqTipo"))
        {    
            $tipo = $this->input->post("dstqTipo");    
            $this->data["tipo"] = $tipo;
        }
        else
        {
            $tipo = 0;
            $this->data["tipo"] = $tipo;
        }
        
        $this->data["tipos"] = $this->sistema_model->getCategs("destaque", null);            
        $this->data["retorno"] = $this->retornoDados();
        $this->data["label"] = $this->sistema_model->getType($tipo);   
        $this->data["page"] = "Novo";

        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');

        $this->load->view('destaques/iniContainer', $this->data);
        $this->load->view('destaques/addTipo', $this->data);        

        if($tipo == 10)
        {
            $this->load->view('destaques/addIndex', $this->data);
        }

        if($tipo == 2)
        {           
            $this->load->view('destaques/addGov', $this->data);
        }        

        $this->load->view('destaques/fimContainer');
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
    
    //Editando um Destaque Existente
    public function editar($id = null)
    {        
        if(!empty($this->input->post()))
        {           
            $this->form_validation->set_rules('dstqUrl', 'Url do Botão', 'required');
            $this->form_validation->set_rules('dstqTitulo', 'Titulo do Destaque', 'required');
                    
            if($this->input->post("dstqTipo") == 1)
            {
                $this->form_validation->set_rules('dstqTexto', 'Descrição', 'required');
            }
                        
            if ($this->form_validation->run() != false) 
            {
                $arquivo = ($_FILES["dstqImg"]["error"] > 0)? true : $this->validArq("dstqImg", "dstq");
                if($arquivo !== false)
                {
                    if($_FILES["dstqImg"]["error"] == 0)
                    {                        
                        unlink(ARQ_PATH.'/../images/conteudo/'.$this->input->post("dstqImgOld"));
                    }
                    
                    $resul = $this->mddstqs->getStatusDstqs($id);
                    $link = $this->sistema_model->getCategs("destaque", $this->input->post("dstqTipo"));

                    if($this->mddstqs->editDstqs($id, $resul->ativo, $arquivo) != false)
                    {
                        $this->session->set_flashdata('sucesso', "Destaque Atualizado");                
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro na atualização, tente mais Tarde!");                
                    }
                    redirect(base_url() . 'destaques/listar/'.$link->valor, "refresh");
                }
                else
                {
                    $this->formEditDstqs($id);
                }
            }
            else 
            {                
                $this->formEditDstqs($id);
            } 
        }
        else
        {
            $this->formEditDstqs($id);
        }
    }
    
    public function formEditDstqs($id)
    {
        $destaque = $this->mddstqs->getDstq($id);
        $tipo = $destaque->tipo;
        $this->data["label"] = $this->sistema_model->getType($tipo);
        $this->data["destaque"] = $destaque;        
        $this->data["page"] = "Editar";
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');

        $this->load->view('destaques/iniContainer', $this->data);
        
        if($tipo == 10)
        {
            $this->load->view('destaques/editIndex', $this->data);
        }

        if($tipo == 2)
        {
            $this->load->view('destaques/editGov', $this->data);
        }
        
        $this->load->view('destaques/fimContainer');
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);        
    }
    
    //Remover Destaque pelo ID
    public function remover($id = null)
    {
        $resul = $this->mddstqs->getStatusDstqs($id);
        $link = $this->sistema_model->getCategs("destaque", $resul->tipo);
        
        if($this->mddstqs->delDstqs($id) !== false)
        {
            unlink(ARQ_PATH.'/../images/conteudo/'.$resul->img);
            $this->session->set_flashdata('sucesso', "Destaque Removido");                
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro na exclusão, tente mais Tarde!");                
        }
        redirect(base_url() . "destaques/listar/$link->valor", "refresh");        
    }
    
    //Mostrar informações do Destaque pelo ID (Sem Autoridade)
    public function ver($id = null)
    {
        $destaque = $this->mddstqs->getDstq($id);
        $destaque->img = $this->data['pathBaseDstq'].$destaque->img;
        $this->data["label"] = $this->sistema_model->getType($destaque->tipo);  
        $this->data["destaque"] = $destaque;        
        $this->data["page"] = "Mostrar";
        $this->data["tipo"] = $destaque->tipo;
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');
        $this->load->view('destaques/iniContainer', $this->data);
        $this->load->view('destaques/mostrarDstq', $this->data);                
        $this->load->view('destaques/fimContainer');
        $this->load->view('template/footer');               
    }
    
    
    //Funções Complementares
    //Retorno do POST com informações para o Form, caso tenha erro do form_validation
    public function retornoDados()
    {
        if(!empty($this->input->post("dstqAct")))
        {           
            return $this->input->post(); 
        }
        else
        {
            return null;
        }        
    }
    
    //Faz o upload de Imagens
    public function validArq($index = null, $prefixo = null)
    {
        $ext = pathinfo($_FILES[$index]["name"], PATHINFO_EXTENSION); 

        $this->load->library('upload');

        $configuracao = array(
         'upload_path'   => ARQ_PATH.'/../images/conteudo/',
         'allowed_types' => 'png|jpg|jpeg',
         'file_name'     => $prefixo.'_'.date('yms').rand(1,5).'.'.$ext,
         'max_size'      => '1000'
         );

        $this->upload->initialize($configuracao);

        if($this->upload->do_upload($index))
        {
            return $configuracao['file_name'];
        }
        else
        {            
            $this->session->set_flashdata('feito', $this->upload->display_errors());
            return false;
        }
    }
}
 