<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Premios extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model("Premios_model","mdpre");
        
        $this->data["pathBasePre"] = substr(base_url(),0,-6)."images/conteudo/";
                
        // Recupera a mensagem de status, se houver
	//$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Pega a URL atual para Verificação e Salvamento de Método
        $this->sistema->verifMetodo(URL_ATUAL);
        
        // Verifica o Login e Traz a Sessão
        $this->data["login"] = ($this->sistema->checkLogin($this->session->userdata("dados_inter"), URL_ATUAL)!== false)? $this->session->userdata("dados_inter") : null ;
        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->session->userdata("dados_inter"));
    }
    
    //PREMIOS
    //Listando Todos os Banners de Determinada Categoria (Página)
    public function listar()
    {        
        $this->data["premios"] = $this->mdpre->getAllPremios();
        $this->data["label"] = "";
        $this->data["conjunto"] = $this->sistema->montarConjuntoBotoes($this->data["login"]);
       
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar', $this->data);
        $this->load->view('premios/listar', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
        
    //Adicionando novo Premio
    public function novo()
    {
        if($this->input->post("preAct"))
        {           
            $this->form_validation->set_rules('preNome', 'Nome do Premio', 'required');             
            $this->form_validation->set_rules('preTipo', 'Selecione o Tipo', 'required');            
            
            if ($this->form_validation->run() !== false) 
            {
                $nome = date('yms').rand(1,5);
                $imagem = $this->validArq("preImg", "preImg", $nome);
                $mini = $this->validArq("preThumb", "preThm", $nome);
                
                if(($imagem !== false)AND($mini !== false))
                {
                    if($this->mdpre->addPremio($imagem, $mini) !== false)
                    {
                        $this->session->set_flashdata('sucesso', "Premio Publicado");                
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro na publicação, tente mais Tarde!");                
                    }
                    redirect(base_url() . "premios/listar", "refresh");
                }
                else
                {                    
                    $this->formPremio();
                }
            }
            else 
            {                
                $this->formPremio();
            }
        }
        else
        {
            $this->formPremio();
        }       
    }
    
    public function formPremio()
    {       
        $this->data["retorno"] = $this->retornoDados();
        $this->data["label"] = "Novo";        
        $this->data["page"] = "Novo";
        $this->data["optTipo"] = array("PRÊMIO","SOLUÇÃO");

        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');        
        $this->load->view('premios/addPremio', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);
    }
    
    //Editando um Prêmio Publicado
    public function editar($id = null)
    {
        if($this->input->post("preAct"))
        {           
            $this->form_validation->set_rules('preNome', 'Nome do Premio', 'required');                    
                            
            if ($this->form_validation->run() != false) 
            {
                $imagem = '';
                $mini = '';
                $nome = date('yms').rand(1,5);                         
                if($_FILES["preImg"]["error"] == 0)
                {
                    $imagem = $this->validArq("preImg", "preImg", $nome);
                    unlink(ARQ_PATH.'/../images/conteudo/'.$this->input->post("preImgOld"));
                }
                
                if($_FILES["preThumb"]["error"] == 0)
                {
                    $mini = $this->validArq("preThumb", "preThm", $nome);                    
                    unlink(ARQ_PATH.'/../images/conteudo/'.$this->input->post("preThmOld"));
                }
                       

                if($this->mdpre->editPremio($id, $imagem, $mini) != false)
                {
                    $this->session->set_flashdata('sucesso', "Prêmio Atualizado");                
                }
                else
                {
                    $this->session->set_flashdata('erro', "Erro na atualização, tente mais Tarde!");                
                }                    
                redirect(base_url() . 'premios/listar/', "refresh");                
            }
            else 
            {                
                $this->formEditPremio($id);
            } 
        }
        else
        {
            $this->formEditPremio($id);
        }
    }
    
    public function formEditPremio($id = null)
    {
        $this->data["premio"] = $this->mdpre->getPremio($id);
        $this->data["label"] = "Editar";
        $this->data["page"] = "Editar";
        $this->data["optTipo"] = array("","PRÊMIO","SOLUÇÃO");
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');
        $this->load->view('premios/editPremio', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/scripts', $this->data);        
    }
    
    //Remover Premio pelo ID
    public function remover($id = null)
    {
        $premio = $this->mdpre->getPremio($id);              
        if($this->mdpre->delPremio($premio->id) !== false)
        {
            unlink(ARQ_PATH.'/../images/conteudo/'.$premio->img);
            unlink(ARQ_PATH.'/../images/conteudo/'.$premio->thumb);
            $this->session->set_flashdata('sucesso', "Prêmio Removido");                
        }
        else
        {
            $this->session->set_flashdata('erro', "Erro na exclusão, tente mais Tarde!");                
        }
        redirect(base_url() . "premios/listar/", "refresh");        
    }
     
    //Mostrar informações do Banner pelo ID (Sem Autoridade)
    public function ver($id = null)
    {
        $this->data["premio"] = $this->mdpre->getPremio($id);
        $this->data["label"] = "Mostrar";
        $this->data["page"] = "Mostrar";
        $this->data["optTipo"] = array("","PRÊMIO","SOLUÇÃO");
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/sidebar');        
        $this->load->view('premios/mostrarPremio', $this->data);       
        $this->load->view('template/footer');              
    }
    
    
    //Funções Complementares
    //Retorno do POST com informações para o Form, caso tenha erro do form_validation
    public function retornoDados()
    {
        if(!empty($this->input->post("preAct")))
        {           
            return $this->input->post(); 
        }
        else
        {
            return null;
        }        
    }
    
    //Faz o upload de Imagens
    public function validArq($index = null, $prefixo = null, $nome = null)
    {
        $ext = pathinfo($_FILES[$index]["name"], PATHINFO_EXTENSION); 

        $this->load->library('upload');

        $configuracao = array(
         'upload_path'   => ARQ_PATH.'/../images/conteudo/',
         'allowed_types' => 'png|jpg|jpeg',
         'file_name'     => $prefixo.'_'.$nome.'.'.$ext,
         'max_size'      => '10000'
         );

        $this->upload->initialize($configuracao);

        if($this->upload->do_upload($index))
        {
            return $configuracao['file_name'];
        }
        else
        {            
            $this->session->set_flashdata('feito', $this->upload->display_errors());
            return false;
        }
    }
}
