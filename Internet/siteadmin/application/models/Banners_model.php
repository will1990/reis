<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners_model extends CI_Model 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    } 
    
    //BANNERS
    //Obter Banners de Determinada Categoria
    public function getAllBanners($tipo = 1)
    {
        $this->db->select("*");
        $this->db->from("site_banners");
        $this->db->where("tipo", $tipo);
        $qry = $this->db->get();
        $resul = $qry->result();
        
        foreach ($resul as $res)
        {
            $res->dt_inicio = (!empty($res->dt_inicio))? invertData($res->dt_inicio):'';
            $res->dt_fim = (!empty($res->dt_fim))? invertData($res->dt_fim):'';
        }
        
        return $resul;
    }
    
    //Obter Banner específico pelo ID
    public function getBanner($id = null)
    {
        $this->db->select("*");
        $this->db->from("site_banners");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $qry = $qry->result();
        $resul = $qry[0];
        
        $resul->dt_inicio = (!empty($resul->dt_inicio))? invertData($resul->dt_inicio):'';
        $resul->dt_fim = (!empty($resul->dt_fim))? invertData($resul->dt_fim):'';
        
        return $resul;
    }
    
    //Obter Status de Banner - ON/OFF
    public function getStatusBanner($id = null)
    {
        $this->db->select("ativo, img, tipo, dt_inicio, dt_fim");
        $this->db->from("site_banners");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $qry = $qry->result();       
        return $qry[0];       
    }
    
    //Alterar Status de Banner - ON/OFF
    public function editStatusBanner($id = null, $act = null)
    {
        $this->db->set("ativo", $act, false);
        $this->db->where("id", $id);
        if ($this->db->update("site_banners"))
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
    
    //Inserindo Novo Banner
    public function addBanner($ativo = 0, $arquivo = null) 
    {     
        $this->db->set("ativo", $ativo, false);
        $this->db->set("img", $arquivo);
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        
        if((!empty($_POST['bannIni'])) AND (!empty($_POST['bannFim'])))
        {
            $inicio = invertData($_POST['bannIni']);
            $fim = invertData($_POST['bannFim']);
            $this->db->set("dt_inicio", $inicio);
            $this->db->set("dt_fim", $fim);
        }
        else
        {
            $this->db->set("dt_inicio", null);
            $this->db->set("dt_fim", null);
        }
        
        $this->db->set("tipo", $_POST['bannTipo'], false);
        
        if(($_POST['bannTipo'] == 1) OR ($_POST['bannTipo'] == 3))
        {
            $this->db->set("url", $_POST['bannUrl']);
        }               
               
        $this->db->set("titulo", $_POST['bannTitulo']);
        $this->db->set("texto", $_POST['bannTexto']);
        
        if($this->db->insert('site_banners')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Editando/Atualizando Banner
    public function editBanner($id = null, $ativo = 0, $arquivo = null) 
    {     
        $this->db->set("ativo", $ativo, false);
        
        if($_FILES["bannImg"]["error"] == 0)
        {
            $this->db->set("img", $arquivo);
        }
        
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        
        if((!empty($_POST['bannIni'])) AND (!empty($_POST['bannFim'])))
        {
            $inicio = invertData($_POST['bannIni']);
            $fim = invertData($_POST['bannFim']);
            $this->db->set("dt_inicio", $inicio);
            $this->db->set("dt_fim", $fim);
        }
        else
        {
            $this->db->set("dt_inicio", null);
            $this->db->set("dt_fim", null);
        }
        
        $this->db->set("tipo", $_POST['bannTipo'], false);
        
        if(($_POST['bannTipo'] == 1) OR ($_POST['bannTipo'] == 3))
        {
            $this->db->set("url", $_POST['bannUrl']);
        }               
               
        $this->db->set("titulo", $_POST['bannTitulo']);
        $this->db->set("texto", $_POST['bannTexto']);
        $this->db->where("id", $id);
        
        if($this->db->update('site_banners')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Apagando Banner
    public function delBanner($id = null)
    {
        $this->db->where("id", $id);
        if($this->db->delete("site_banners"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }           
}

?>