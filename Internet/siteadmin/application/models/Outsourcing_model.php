<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Outsourcing_model extends CI_Model 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    } 
    
    //PREMIOS
    //Obter todos os Prêmios
    public function getAllOutsourcings()
    {
        $this->db->select("*");
        $this->db->from("site_outsourcing");        
        $qry = $this->db->get();
        return $qry->result();   
    }
       
    //Obter Status de Outsourcing - ON/OFF
    public function getStatusOutsourcing($id = null)
    {
        $this->db->select("ativo");
        $this->db->from("site_outsourcing");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $qry = $qry->result();       
        return $qry[0];       
    }
    
    //Alterar Status de Outsourcing - ON/OFF
    public function editStatusOutsourcing($id = null, $act = null)
    {
        $this->db->set("ativo", $act, false);
        $this->db->where("id", $id);
        if ($this->db->update("site_outsourcing"))
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
        
    //Obter Outsourcing específico pelo ID
    public function getOutsourcing($id = null)
    {
        $this->db->select("*");
        $this->db->from("site_outsourcing");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        return $qry->row();     
    }    
    
    //Inserindo Novo Outsourcing
    public function addOutsourcing($img = null, $imgOpt = null) 
    {
        $this->db->set("titulo", $_POST['outTitulo']);
        $this->db->set("descricao", $_POST['outDesc']);
        
        if($_FILES["outImg"]["error"] == 0)
        {
            $this->db->set("img_sup", $img);
        }
        
        if($_FILES["outImgOpt"]["error"] == 0)
        {
            $this->db->set("img_opc", $imgOpt);
        }
                      
        $this->db->set("conteudo", $_POST['outText']);
        $this->db->set("conteudo_opc", $_POST['outTextOpt']);
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->set("ativo", 0);
        
        if($this->db->insert('site_outsourcing')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Editando/Atualizando Outsourcing
    public function editOutsourcing($id = null, $img = null, $mini = null) 
    {  
        $this->db->set("titulo", $_POST['outTitulo']);
        $this->db->set("descricao", $_POST['outDesc']);
        
        if($_FILES["outImg"]["error"] == 0)
        {
            $this->db->set("img_sup", $img);
        }
        
        if($_FILES["outImgOpt"]["error"] == 0)
        {
            $this->db->set("img_opc", $imgOpt);
        }
               
        $this->db->set("conteudo", $_POST['outText']);
        $this->db->set("conteudo_opc", $_POST['outTextOpt']);
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->where("id", $id);
        
        if($this->db->update('site_outsourcing')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    } 
    
    //Apagando Outsourcing
    public function delOutsourcing($id = null)
    {
        $this->db->where("id", $id);
        if($this->db->delete("site_outsourcing"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }  
}