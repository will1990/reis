<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_model extends CI_Model
{
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    }
    
    //MÉTODOS
    //Obtem Método da Tabela MÉTODO
    public function getMetodo($classe, $metodo)
    {
        $this->db->where("classe", $classe);
        $this->db->where("metodo", $metodo);
        return $this->db->get("metodos");
    }
    
    //Obtem o ID do Método para Comparações e Montagem de Views
    public function getInfoMetodo($classe = null, $metodo = null)
    {
        $this->db->select("id, icon, url, parametro");
        $this->db->where("classe", $classe);
        $this->db->where("metodo", $metodo);
        $qry = $this->db->get("metodos");
        $query = $qry->row();
               
        if($query != null):            
            $query->url = remontarUrl($query->url);
            return $query;
        else:
            return null;
        endif; 
    }
    
    //Adiciona Novo Método a Tabela MÉTODO
    public function addMetodo($classe, $metodo)
    {
        $this->db->set("classe", $classe);
        $this->db->set("metodo", $metodo);
        $this->db->set("parametro", "NULL", false);
        $this->db->set("url", "$classe/$metodo");
        $this->db->set("modulo", "ADMIN");
        $this->db->insert('metodos');
    }
    
    //TIPOS
    //Obtem todos os tipos de Determinada Categoria ou Todos Existentes dependendo do Parâmetro
    public function getCategs($tipo = null, $entrada = null)
    {
        $this->db->select("*");
        $this->db->from("site_tipos");
        $this->db->where("tipo", $tipo);
        
        if($entrada !== null)
        {
            if(is_numeric($entrada) !== false)
            {
                $this->db->where("id", $entrada);
            }
            else
            {
                $this->db->where("valor", $entrada);
            }
            $qry = $this->db->get();
            return $qry->row();
        }
        else
        {
            $qry = $this->db->get();
            return $qry->result();
        }      
    }
        
    //PERMISSÕES
    //Verifica as permissões do Usuário Logado
    public function getPermUser($data = null)
    {       
        $this->db->select("*");
        $this->db->where("id_usuario", $data->user_id);
        $this->db->where("id_metodo", $data->metodo);
        $this->db->where("liberar", 1);
        $query = $this->db->get("permissoes_usuarios");
          
        if($query->result_id->num_rows > 0)
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Verifica as permissões do Grupo deste Usuário
    public function getPermGroup($data = null)
    {
        $this->db->select("*");        
        $this->db->where("id_grupo", $data->id_grupo);
        $this->db->where("id_metodo", $data->metodo);
        $this->db->where("liberar", 1);
        $query = $this->db->get("permissoes_grupos");
              
        if($query->result_id->num_rows > 0)
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //MENU
    //Obter as Classes para o Menu Principal da Sidebar (Agrupado)
    public function getCategsMenu()
    {
        $this->db->select("classe");
        $this->db->from("metodos");        
        $this->db->where("menu >", 0);
        $this->db->group_by("classe");
        $this->db->order_by("id", "ASC");
        $qry = $this->db->get();      
        return $qry->result();             
    }
    
    //Obter Classes/Categorias Pai para o Menu (Individual)
    public function getCategMenu($classe = null, $nivel = null)
    {
        $this->db->select("*");
        $this->db->from("metodos");
        if($classe !== null)
        {
            $this->db->where("classe", $classe);
        }        
        if($nivel !== null)
        {
            $this->db->where("menu", $nivel);
        }
        $qry = $this->db->get();
        
        if($qry->result_id->num_rows > 0)
        {
            $qry = $qry->row();
            $qry->url = ($qry->url !== '#')? explode("/", $qry->url)[3] : $qry->url ;
                        
            return $qry;
        }
        else
        {            
            return (object)array("metodo" => $classe);
        }
    }
    
    //Obter os SubMenus para o Menu Principal da Sidebar
    public function getSubCategsMenu($classe = null, $param = null, $nivel = null)
    {
        $this->db->select("*");
        $this->db->from("metodos");
        if($classe !== null)
        {
            $this->db->where("classe", $classe);
        }
        if($param !== null)
        {
            $this->db->where("parametro", $param);
        }
        if($nivel !== null)
        {
            $this->db->where("menu", $nivel);
        }
        $this->db->order_by("parametro", "ASC");
        $qry = $this->db->get();
        
        if($qry->result_id->num_rows > 0)
        {
            $resul = $qry->result();
            foreach ($resul as $res)
            {
                $res->url = remontarUrl($res->url);
            }
            return $resul;
        }
        else
        {
            return null;
        }
    }
    
    //Obter todos os Métodos permitidos Para o Usuário ou Grupo
    public function getMetodosPermitidos($tabela = null, $coluna= null, $id = null )
    {
        $this->db->select("id_metodo");
        $this->db->from($tabela);
        $this->db->where($coluna, $id);
        $this->db->where("liberar", 1);
        $qry = $this->db->get();
        $query = $qry->result();
        
        $resul = array();
        foreach ($query as $qry)
        {
            $resul[] .= $qry->id_metodo;
        }
        return $resul;
    }
    
    //GERAL - VIEWS
    //Obter Título de Barra superior através do Tipo de Listagem
    public function getType($tipo = null)
    {
        if($tipo != null)
        {
            $this->db->select("*");
            $this->db->from("site_tipos");
            $this->db->where("id", $tipo);
            $qry = $this->db->get();
            $qry = $qry->result();
            return $qry[0];            
        }
        else
        {
            return null;
        }
    }
}
