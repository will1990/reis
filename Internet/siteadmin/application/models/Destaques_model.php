<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques_model extends CI_Model
{ 
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        $this->db = $this->load->database('default', TRUE);
    }

    //DESTAQUES
    //Obter Destaques de Determinada Categoria
    public function getAllDstqs($tipo = 1)
    {
        $this->db->select("*");
        $this->db->from("site_destaques");
        $this->db->where("tipo", $tipo);
        $qry = $this->db->get();
        return $qry->result();      
    }
        
    //Obter Destaque específico pelo ID
    public function getDstq($id = null)
    {
        $this->db->select("*");
        $this->db->from("site_destaques");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $qry = $qry->result();
        return $qry[0];
    }    
    
    //Obter Status de Destaque - ON/OFF
    public function getStatusDstqs($id = null)
    {
        $this->db->select("ativo, tipo, img");
        $this->db->from("site_destaques");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $qry = $qry->result();       
        return $qry[0];       
    }
    
    //Alterar Status de Destaque - ON/OFF
    public function editStatusDstqs($id = null, $act = null)
    {
        $this->db->set("ativo", $act, false);
        $this->db->where("id", $id);
        if ($this->db->update("site_destaques"))
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
            
    //Inserindo Novo Destaque
    public function addDstqs($ativo, $arquivo = null) 
    {  
        $this->db->set("img", $arquivo);
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->set("url", $_POST['dstqUrl']);
        $this->db->set("ativo", $ativo, false);
        $this->db->set("tipo", $_POST['dstqTipo'], false);
        $this->db->set("titulo", $_POST['dstqTitulo']);
        
        if($_POST['dstqTipo'] == 10)
        {
            $this->db->set("texto", $_POST['dstqTexto']);
        }               
        
        if($this->db->insert('site_destaques')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Editando/Atualizando Destaque
    public function editDstqs($id = null, $ativo = 0, $arquivo = null) 
    {
        if($_FILES["dstqImg"]["error"] == 0)
        {
            $this->db->set("img", $arquivo);
        }        
        
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->set("url", $_POST['dstqUrl']);
        $this->db->set("ativo", $ativo, false);
        $this->db->set("tipo", $_POST['dstqTipo'], false);
        $this->db->set("titulo", $_POST['dstqTitulo']);
              
        if($_POST['dstqTipo'] == 10)
        {
            $this->db->set("texto", $_POST['dstqTexto']);
        }               
        
        $this->db->where("id", $id);
        
        if($this->db->update('site_destaques')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Apagando Destaque
    public function delDstqs($id = null)
    {
        $this->db->where("id", $id);
        if($this->db->delete("site_destaques"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}