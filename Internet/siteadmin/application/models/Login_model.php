<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
*   Site / Administração
*   William Feliciano     
**/

class Login_model extends CI_Model
{
    function __construct() 
    {
        $this->db = $this->load->database('default', TRUE);
    }
    
    //LOGIN
    //Consulta para Obter Informações do Usuário para Login
    public function getLogin($matricula)
    {            
        $this->db->select("id, ativo, id_tipo, id_grupo");                              
        $this->db->where("matricula", $matricula);
        $this->db->where("ativo", 1);
        return $this->db->get("usuarios");  
    }
    
    //Registra o ultimo Dia e Horário que o Login do Usuário foi realizado
    public function registroLogin($id = null)
    {
        $this->db->set("dt_login", date('Y-m-d h:m:s'));
        $this->db->where("id", $id);
        $this->db->update("usuarios");
    }
    
    //Consulta para Obter informações do Grupo que pertence o Usuário
    public function getGrupo($id = null)
    {
        $this->db->select("nome");
        $this->db->where("id", $id);
        $qry = $this->db->get("grupos");
        $query = $qry->row();
        return $query->nome;
    }
    
}

