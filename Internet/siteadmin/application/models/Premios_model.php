<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Premios_model extends CI_Model 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    } 
    
    //PREMIOS
    //Obter todos os Prêmios
    public function getAllPremios()
    {
        $this->db->select("*");
        $this->db->from("site_premios_solucoes");        
        $qry = $this->db->get();
        $resul = $qry->result();
        
        foreach ($resul as $res):
            if($res->tipo == 1):
                $res->tipo = 'PRÊMIO';
            elseif($res->tipo == 2):
                $res->tipo = 'SOLUÇÃO';
            else:
                $res->tipo = 'no';
            endif;                
        endforeach;
        
        return $resul;
    }
    
    //Obter Prêmio específico pelo ID
    public function getPremio($id = null)
    {
        $this->db->select("*");
        $this->db->from("site_premios_solucoes");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $res = $qry->row();

        if($res->tipo == 1):
            $res->label = 'Prêmio';
        elseif($res->tipo == 2):
            $res->label = 'Solução';
        else:
            $res->label = '';
        endif; 
        
        return $res;
    }    
    
    //Inserindo Novo Banner
    public function addPremio($imagem = null, $mini = null) 
    {     
        $this->db->set("img", $imagem);
        $this->db->set("thumb", $mini);
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->set("tipo", $_POST['preTipo']);
        $this->db->set("nome", $_POST['preNome']);
        if($this->db->insert('site_premios_solucoes')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Editando/Atualizando Prêmio
    public function editPremio($id = null, $img = null, $mini = null) 
    {       
        if($_FILES["preImg"]["error"] == 0)
        {
            $this->db->set("img", $img);
        }
        
        if($_FILES["preThumb"]["error"] == 0)
        {
            $this->db->set("thumb", $mini);
        }
               
        $this->db->set("dt_publicacao", date('Y/m/d h:m:s'));
        $this->db->set("tipo", $_POST['preTipo']);
        $this->db->set("nome", $_POST['preNome']);
        $this->db->where("id", $id);
        
        if($this->db->update('site_premios_solucoes')) 
        {            
            return true;            
        } 
        else
        {            
            return false;            
        }
    }
    
    //Apagando Prêmio
    public function delPremio($id = null)
    {
        $this->db->where("id", $id);
        if($this->db->delete("site_premios_solucoes"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }  
}