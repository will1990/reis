<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%'@ CodePage=1252 Language="VBScript"%> 

<%  
Response.Status = "301 Moved Permanently"
Response.AddHeader "Location", "https://www.reisoffice.com.br/site/assistencia-tecnica.asp"
Response.End
' 
' This file is saved in ANSI format on a U.S. English system locale. 
' The language of the system doesn't matter 
'  because you are setting @CodePage and Response.CodePage. 
' Otherwise, the system code page of the server would be the default. 

'Response.CodePage = 1252 
'Response.CharSet = "windows-1252"  response.Charset = "ISO-8859-1" 

'response.ContentType = "text/html" %>