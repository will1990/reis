<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outsourcing extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();       
        $this->load->model("Outsourcing_model","mdout");
        
        $this->data["pathbase"] = base_url("images/conteudo/");
               
        $this->data["tituloBarra"] = (object)array("titulo" => "Outsourcing", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> PORTAL SERVICE", "url" => PORTAL_SERVICE),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
    }
    
    //OUTSOURCING
    //Página principal de Outsourcing (Lista)
    public function index()
    {
        $this->data["topMarcador"] = 4;        
        
        $this->data["outsourcings"] = $this->mdout->getAllOutsourcings();
        $this->data["solucoes"] = $this->mdout->getAllSolucoes();
                          
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/index', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);     
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/jsAccordionOutsourcing');
    }
    
    //Montando Conteúdo Interno de Outsourcing
    public function ver($id = null)
    {
        $this->data["topMarcador"] = 0;
        
        $this->data["outsourcing"] = $this->mdout->getOutsourcing($id);
        $this->data["solucoes"] = $this->mdout->getAllSolucoes();
        
        $this->data["tituloBarra"]->titulo = $this->data["outsourcing"]->titulo;
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["outsourcing"]->titulo) < 41)? $this->data["tituloBarra"]->fontSize : '20px';
                
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
        $this->data["nossas_marcas"] = $this->load->view('template/suppliers', null, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/montarCategorias', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);  
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer');
    }
    
    //SOLUCOES
    //Página principal de Outsourcing (Lista)
    public function solucoes($slug = null)
    {       
        if($slug !== null):            
        
            $this->data["topMarcador"] = 0;
            $this->data["solucoes"] = $this->mdout->getAllSolucoes();
            $this->data["tituloBarra"] = (object)array("titulo" => $this->data["solucoes"]["$slug"]->titulo);
            $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'20px');
            $this->data["bread"] = $this->data["solucoes"]["$slug"]->nome;
                    
            $this->data["conteudo"] = $this->load->view("solucoes/$slug", $this->data, true);
            
            $this->load->view('template/header');
            $this->load->view('template/navbar', $this->data);
            $this->load->view('template/barrasup', $this->data);        
            $this->load->view('solucoes/index', $this->data);        
            $this->load->view('template/footer');
            
        else:
            
            redirect(base_url("outsourcing"),"refresh");
            
        endif;
    }
    
    //ASSISTÊNCIA
    //Página de Assistência
    public function assistencia()
    {
        $this->data["topMarcador"] = 0;
        $this->data["tituloBarra"]->titulo = "Assistência";
        
        $this->data["barraDir"] = $this->load->view("assistencia/barradir", $this->data, true);
                
        //Fazer a logica de cadastro de email do rodapé com Jquery, sem refresh
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('assistencia/assistencia', $this->data);                
        $this->load->view('template/footer');
        $this->load->view('js/jsMostrarAssist', $this->data);        
    }
    
    //EMAIL (FORM OUTSOURCING)
    //Salvando Informações do Contato e Enviando Email
    public function enviar()
    {
        $this->form_validation->set_rules('outNome', 'Nome', 'required');
        $this->form_validation->set_rules('outEmail', 'Email', 'required');
        $this->form_validation->set_rules('outFone', 'Telefone', 'required');
        $this->form_validation->set_rules('outCNPJ', 'CNPJ', 'required');
                
        if($this->form_validation->run() !== false) 
        {             
            $this->sistema->enviarEmail($this->prepRemEmail(), $this->prepDestEmail());
            
            $this->session->set_flashdata('success', "<h4>Informações enviadas</h4>Aguarde contato dos nossos Consultores!");
            redirect(base_url("outsourcing"));
        }
        else
        {
            $this->index();            
        }        
    }
        
    //Parâmetros de Destinatário
    public function prepDestEmail()
    {
        $cnpj = $this->input->post("outCNPJ");
        $label = strlen($cnpj) < 15 ? "CPF":"CNPJ";
        
        $view = '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                    <h3>Nome: '.$this->input->post("outNome").' </h3>
                    <h3>Email: '.$this->input->post("outEmail").' </h3>
                    <h3>Telefone: '.$this->input->post("outFone").' </h3>                           
                    <h3>'.$label.': '.$this->input->post("outCNPJ").' </h3>                                                                                   
                    <br/>
                    <h4>Mensagem</h4>
                    <h3> '.$this->input->post("outMsg").' </h3>
                </div>';
        
        
        return (object) array(
            'email' => array('SITE@reisoffice.com.br','julio.miranda@gmail.com','rodrigo@reisoffice.com.br','mariana.lima@reisoffice.com.br'),
            'nome' => $this->input->post("outName"),
            'mensagem' => array("titulo" => "Contato da Página Outsourcing (Site)", "view" => $view),                
            //'cc' => ' ', 
            'assunto' => 'Contato da Página Outsourcing (Site)'            
        );
    }
    
    //Parâmentros do Remetente
    public function prepRemEmail()
    {
        return (object) array(
            'email' => 'sistema.solicitacao@reisoffice.com.br',
            'nome'  => 'Página Outsourcing - Site Reis Office'
        );
    }
}