<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//Produtos
$route['produtos/imagens/(:any)/(:any)'] = 'produtos/imagens/$1/$2';
$route['produtos/imagem/(:any)/(:any)'] = 'produtos/imagem/$1/$2';
$route['produtos/imagem/(:any)/(:any)/(:any)'] = 'produtos/imagem/$1/$2/$3';

$route['produtos/download/(:any)'] = 'produtos/download/$1';

$route['produtos/produto_pagina/(:num)'] = 'produtos/produto_pagina/$1';

$route['produtos/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'produtos/index/$1/$2/$3/$4/$5';
$route['produtos/(:any)/(:any)/(:any)/(:any)'] = 'produtos/index/$1/$2/$3/$4';
$route['produtos/(:any)/(:any)/(:any)'] = 'produtos/index/$1/$2/$3';
$route['produtos/(:any)/(:any)'] = 'produtos/index/$1/$2';
$route['produtos/(:any)'] = 'produtos/index/$1';

// Outsourcing
// Rota de correção dos anúncios do google
$route['outsourcing/(:any)'] = function ($out_page)
{
    switch($out_page)
    {
        case "impressao-diagnosticos.asp":
        case "impressao-diagnosticos":
            return 'outsourcing/ver/9';
            break;
        case "outsourcing-servicos-impressao.asp":
        case "outsourcing-servicos-impressao":
            return 'outsourcing/ver/1';
            break;
        case "Softwares-customizados.asp":
        case "Softwares-customizados":
            return 'outsourcing/ver/13';
            break;
        case "Aluguel-de-leitores-e-coletores-de-dados.asp":
        case "Aluguel-de-leitores-e-coletores-de-dados":
            return 'outsourcing/ver/14';
            break;
        case "Solucao-de-Pulseiras-de-Identificacao.asp":
        case "Solucao-de-Pulseiras-de-Identificacao":
            return 'outsourcing/ver/15';
            break;
        case "Solucao-de-totens-com-informacoes-digitais.asp":
        case "Solucao-de-totens-com-informacoes-digitais":
            return 'outsourcing/ver/16';
            break;
        case "Solucao-para-condominios.asp":
        case "Solucao-para-condominios":                    
            return 'outsourcing/ver/17';
            break;
        case "nova":
            return 'outsourcing/nova';
            break;
        default:
            return 'outsourcing';
    }
};

$route['default_controller'] = 'home';
$route['404_override'] = 'home/desculpe';
$route['translate_uri_dashes'] = true;
