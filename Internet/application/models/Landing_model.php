<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */    
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    } 
    
    //CONTATOS
    //Adicionar contato de Landing Pages
    public function addContato()
    {
        $this->db->set("cli_nome", $_POST["txtNomeForm"]);
        $this->db->set("cli_email", $_POST["txtEmailForm"]);        
        $this->db->set("cli_empresa", $_POST["txtEmpreForm"]);
        $this->db->set("cli_telefone", $_POST["txtTelForm"]);
        $this->db->set("cli_obs", $_POST["txtObsForm"]);        
        $this->db->set("dt_solicitacao", date('Y/m/d h:m:s'));
        $this->db->set("nome_landing", $_POST["txtNomeLanding"]);
        
        $this->db->insert('site_forms_contatos');
    }   
}

