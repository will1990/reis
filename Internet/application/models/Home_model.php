<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
        $this->base = $this->load->database('portal', TRUE);
    }
    
    //HOME
    //Obter os Slides do Carousel (Banners)
    public function getBanns()  
    {       
        $this->db->select("*");
        $this->db->from("site_banners");
        $this->db->order_by("ordem", "asc");
        $this->db->where(array("tipo" => 1, "ativo" => 1));
        $qry = $this->db->get();
        $rst = $qry->result();    
        
        $ct = 0;
        foreach ($rst as $bann):
            if(!empty($bann->dt_inicio) AND !empty($bann->dt_fim)):
                if(strtotime($bann->dt_inicio) <= strtotime(date('Y-m-d')) AND (strtotime($bann->dt_fim) >= strtotime(date('Y-m-d')))):
                    $banners[$ct] = $bann;
                endif;
            else:
                $banners[$ct] = $bann;
                $ct++;
            endif;            
        endforeach;     
        
        return $banners;
    }
    
    //Obter os Destaques da Index
    public function getDstqs()
    {        
        $this->db->select("*");
        $this->db->from("site_destaques");
        $this->db->where("tipo", 10);
        $this->db->where("ativo", 1);
        //$this->db->order_by("dt_publicacao", "DESC");
        $this->db->limit(4);
        $qry = $this->db->get();
        return $qry->result();   
    }
    
    //CONTATO
    //Salvar contato de Form
    public function salvarContato()
    {
        list($ddd, $tel) = explode(")", $this->input->post("contFone"));
              
        $this->base->set("nm_contato", $this->input->post("contNome"));
        $this->base->set("ds_email", $this->input->post("contEmail"));        
        $this->base->set("cd_telefone", trim($tel));
        $this->base->set("dt_contato", date('Y/m/d h:m:s'));
        $this->base->set("ds_mensagem", $this->input->post("contMsg"));        
        $this->base->set("cd_ddd_contato", str_replace("(","",$ddd));                
        $this->base->insert('contato');   
    }
    
    //RODAPÉ e OUTROS CAMPOS UNITÁRIOS DE EMAIL
    //Cadastro de Email na Base de Dados
    public function addEmail()
    {              
        $data = array(           
            'email' => $this->input->post('txtEmail'),
            'data_cadastro' => date('Y/m/d h:m:s'),
            'ativo' => 1,
            'origem' => $this->input->post('origemEmail')
        );       
        
        if($this->base->insert('newsletter', $data)):
            return true;          
        else:
            return false;          
        endif;        
    }
    
    public function como_conheceu()
    {
        return array("Como conheceu a Reis Office", "Google", "Outros Sites", "Anúncios", "Folder", "Email-marketing", "Indicação", "Outros");
    }
}