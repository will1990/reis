<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias_model extends CI_Model 
{
    /**
    *  Site Reis Office
    *  William Feliciano     
    */
    
    function __construct() 
    {  
        parent::__construct();
        $this->base = $this->load->database('portal', TRUE); 
    }
    
    // NOTÍCIAS
    // Carrega Todas as noticias   
    public function getAllNoticias($qtde = 'all')
    {
        $this->base->select("cd_noticia, ds_chamada_noticia, nm_noticia, nm_foto, dt_noticia, MONTH(dt_noticia) mes, DAY(dt_noticia) dia, YEAR(dt_noticia) ano");
        $this->base->order_by("dt_noticia", "desc");
        $this->base->from("noticia");
        $this->base->where("cd_categoria", 1);
        if($qtde != 'all')
        {
            $this->base->limit($qtde);
        }
        $qry = $this->base->get();
        $rst = $qry->result();
        
        if($rst)
        {
            foreach($rst as $item)
            {
                $item->nm_noticia = str_replace('chr37','%', $item->nm_noticia);
                $item->data = $item->dia.'/'.$item->mes.'/'.$item->ano;
                $item->url = "https://www.reisoffice.com.br/arquivos/noticias/thumb/".$item->nm_foto; 
                $item->img = urlExists($item->url) !== false ? $item->url : base_url("images/visual/logo_reis.png");                
            }
        }
        
        return $rst;
    } 
        
    // Carrega a Noticia para Leitura
    public function getNoticia($id)
    {
        $this->base->select("ds_integra_noticia, nm_noticia, MONTH(dt_noticia) mes, DAY(dt_noticia) dia, YEAR(dt_noticia) ano");
        $this->base->from("noticia");
        $this->base->where("cd_noticia", $id);
        $qry = $this->base->get();
        $rst = $qry->row();
        
        if($rst)
        {
            $rst->nm_noticia = str_replace('chr37','%', $rst->nm_noticia);
            $rst->data = $rst->dia.'/'.$rst->mes.'/'.$rst->ano;
            $rst->noticia = $this->prepNoticia($rst->ds_integra_noticia);
        }
        
        return $rst;
    }
    
    // Tratamento de Conteúdo da Noticia para Leitura
    public function prepNoticia($conteudo = null)
    {
        $conteudo = str_replace('chr60br /chr62', '<br/>', $conteudo);
        $conteudo = str_replace('chr37', '%', $conteudo);
                
        return $conteudo;
    }
    
    
}

