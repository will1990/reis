<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revenda_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    //REVENDAS    
    //Banner
    //Obter as Soluções do Banner Abaixo
    public function getSolucoes()
    {
        $this->db = $this->load->database('default', TRUE);
        
        $this->db->select("*");
        $this->db->from("site_premios_solucoes");
        $this->db->where("tipo", 2);
        $qry = $this->db->get();
        return $qry->result();
    }
    
    //Eventos
    //Carrega as 3 últimas noticias para Lateral     
    public function getNoticias()
    {
        $this->base = $this->load->database('portal', TRUE);
        
        $this->base->select("cd_noticia, ds_chamada_noticia, nm_noticia, nm_foto, dt_noticia, MONTH(dt_noticia) mes, DAY(dt_noticia) dia, YEAR(dt_noticia) ano");
        $this->base->order_by("cd_noticia", "desc");        
        $rst = $this->base->get_where("noticia", "cd_categoria = 1", 3)->result();
        
        if($rst)
        {
            foreach($rst as $item)
            {
                $item->nm_noticia = str_replace('chr37','%', $item->nm_noticia);
                $item->data = $item->dia.'/'.$item->mes.'/'.$item->ano;
                $url = "https://www.reisoffice.com.br/arquivos/noticias/thumb/".$item->nm_foto;
                $item->img = ((!empty($url)) AND ($url !== null)) ? $url : base_url("images/visual/logo_reis.png"); 
            }
        }
        
        return $rst;
    }    
}
