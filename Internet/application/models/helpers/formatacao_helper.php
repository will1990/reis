<?php

// Funcao que formata os campos para serem mostrados ou salvos
function formatar($string, $tipo = "")
{
    if(empty($string))
        return false;
    
    // Limpa texto deixando somewnte os numeros
    $string = preg_replace("#[^0-9]#", "", $string);
    
    // Caso nao informe o tipo, detecta alguns padroes
    if (!$tipo)
    {
        switch (strlen($string))
        {
            case 10: 	$tipo = 'fone'; break;
            case 8: 	$tipo = 'cep';  break;
            case 11: 	$tipo = 'cpf';  break;
            case 14:    $tipo = 'cnpj'; break;
        }
    }
    
    // Formata no tipo escolhido
    switch($tipo)
    {
        case 'fone':
            if(strlen($string) == 10)
                $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . '-' . substr($string, 6);
            else
                $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 5) . '-' . substr($string, 7);
            break;
        case 'cep':
            $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
            break;
        case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
            break;
        case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12, 2);
            break;
        case 'dt2bd':
            $string = substr($string, -4) . '-' . substr($string, 2, 2) . '-' . substr($string, 0, 2);
            break;
        case 'bd2dt':
            $string = substr($string, -2) . '/' . substr($string, 4, 2) . '/' . substr($string, 0, 4);
            break;
        case 'st':
            $string = substr($string, 0, 2) .':'. substr($string, 2, 2);
            break;
        case 'ft':
            $string = substr($string, 0, 2) .':'. substr($string, 2, 2) .':'. substr($string, 4, 2);
            break;
    }
    return $string;
}

function somente_numeros($string)
{
    return preg_replace("#[^0-9]#", "", $string);
}

$orig = array("ç", "ã", "õ", "á", "à", "é", "í", "ó", "ú");
$mask = array("%cd%", "%at%", "%ot%", "%aa%", "%ac%", "%ea%", "%ia%", "%oa%", "%ua%");

function url_encode($url)
{
    global $orig;
    global $mask;
    //return str_replace($orig, $mask, urlencode($url));    
    return remover_acentos($url);
}

function url_decode($url)
{
    global $orig;
    global $mask;
    $url = urldecode($url);
    $url = str_replace($mask, $orig, $url);
    return $url;
}

function remover_acentos($string) {
    /*$string = preg_replace("/[áàâãä]/", "a", $string);
    $string = preg_replace("/[ÁÀÂÃÄ]/", "A", $string);
    $string = preg_replace("/[éèê]/", "e", $string);
    $string = preg_replace("/[ÉÈÊ]/", "E", $string);
    $string = preg_replace("/[íì]/", "i", $string);
    $string = preg_replace("/[ÍÌ]/", "I", $string);
    $string = preg_replace("/[óòôõö]/", "o", $string);
    $string = preg_replace("/[ÓÒÔÕÖ]/", "O", $string);
    $string = preg_replace("/[úùü]/", "u", $string);
    $string = preg_replace("/[ÚÙÜ]/", "U", $string);
    $string = preg_replace("/[ç]/", "c", $string);
    $string = preg_replace("/[Ç]/", "C", $string);
    $string = preg_replace("/[ñ]/", "n", $string);
    $string = preg_replace("/[Ñ]/", "N", $string);*/
    //$string = preg_replace("/[][><}{)(:;,!?*%~^`@]/+", "", $string);
    //$string = preg_replace("/ /", "_", $string);
    //$string = str_replace("/", "", $string);
    
    //$string = 'ÁÍÓÚÉÄÏÖÜËÀÌÒÙÈÃÕÂÎÔÛÊáíóúéäïöüëàìòùèãõâîôûêÇç';
    $string = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
    
    return $string;
}

function url_codificar($str)
{
    $str = str_replace(" / ", "_", $str);
    $str = str_replace("/", "__", $str);
    $str = str_replace("(", "|_", $str);
    $str = str_replace(")", "_|", $str);
    $str = str_replace("+", "--", $str);
    $str = remover_acentos($str);
    //$str = urlencode($str);
    return strtolower($str);
}

function url_decodificar($str)
{
    $str = urldecode($str);
    $str = str_replace("__", "/", $str);
    $str = str_replace("_", " / ", $str);        
    $str = str_replace("|_", "(", $str);
    $str = str_replace("_|", ")", $str);
    $str = str_replace("--", "+", $str);
    return $str;
}

function oracle_translate($cmp)
{
    return "TRANSLATE($cmp, 'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜáçéíóúàèìòùâêîôûãõëü', 'ACEIOUAEIOUAEIOUAOEUaceiouaeiouaeiouaoeu')";
}

function meta($index )
{
    $metas = array();
    $metas["home"] = array("title" => "Líder em Outsourcing e Soluções para Impressão"
                         , "description" => "Líder em Outsourcing, Equipamentos e Soluções para Impressão ✓ Mais de 30 anos de tradição ✓ Líder de Mercado ✓ Solicite Orçamento sem Custo");
    $metas["atas"] = array("title" => "Atas e Registros de Preços - Licitações"
                         , "description" => "Disponibilizamos as Stas de Resgistros de peço");
    $metas["assistencia"] = array("title" => "Assistência Técnica para Outsourcing e Revendas"
                        , "description" => "Veja como funciona Assistência Técnica para Outsourcing e Revendas ✓ Portal Service Chamados Técnicos Suporte");
    $metas["eventos"] = array("title" => "Cronograma de Eventos"
                        , "description" => "Conheça todos os eventos que a ReisOffice promove,suas datas e descrições ✓ Eventos ✓ Parceiros ✓ Funcionários");
    $metas["patrimonio"] = array("title" => "Identificação de Patrimônio"//atas das marcas 
                   , "description" => "Identificação de Patrimônio Identifique seus ativos de forma simples e rápida Software de edição");
    $metas["noticia"] = array("title" => "Listagem de Notícias"
                         , "description" => "Conheça todas as notícias relacionandas aos produtos e serviços da ReisOffice Notícias");
    $metas["etica"] = array("title" => "Conheça os princípios do nosso Código de Ética"
                         , "description" => "Profissionalismo, determinação e esforço pautam o sucesso de nossa companhia.Conheça nosso código de ética e seus princípios");
    $metas["premios"] = array("title" => "Prêmios e Qualificações"
                         , "description" => "Os prêmios conquistados pela Reis Office são uma prova da qualidade e compromisso da empresa com seus clientes, fornecedores e a sociedade a sua volta");
    $metas["showroom"] = array("title" => "Instalações e Show-Room"
                         , "description" => "DDispomos de instalações modernas de nova sede da Reis Office e o Show-Room destaca-se com diversos equipamentos das marcas Brother, Canon, HP, Kyocera e Oki.");
    $metas["social"] = array("title" => "Sustentabilidade e Responsabilidade Sócio Ambiental"
                         , "description" => "Conheça nossa política de Sustentabilidade e Responsabilidade Sócio Ambiental. Além de educação e uso consciente dos recursos, garantindo uma melhora na qualidade de vida.");
    $metas["ambiente"] = array("title" => "Meio Ambiente"
                         , "description" => "A Reis Office, como importante empresa de tecnologia do país, tem consciência de seu papel na sociedade. ");
    $metas["qualidade"] = array("title" =>"Política de Qualidade e Compromissos Reis Office",
                                "description" => "Conheça a política de qualidade que define as diretrizes para monitoramento e desenvolvimento do negócio");
    $metas["busca/index"] = array("title" =>'Resultado de Busca ' //$meta_title
                          ,"description" => "");
    $metas["contato/index"] = array ("title" =>'Contatos ' //$meta_title
                          ,"description" => "");
    
    if(isset($metas[$index]))
        return (object)$metas[$index];
    else
        return (object)$metas["home"];
}


/*Verificar se URL Existe*/
function urlExists($url) {

    $validar = get_headers($url);
    $validar = explode(" ",$validar[0]);
    $validar = $validar[1];
    
    if($validar == "302" || $validar == "200"):
        return true;
    else:
        return false;
    endif;    
}
