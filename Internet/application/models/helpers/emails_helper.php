<?php

function viewContato($post)
{   
    $view = '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["contNome"].' </h3>
                <h3>Email: '.$post["contEmail"].' </h3>
                <h3>Telefone: '.$post["contFone"].' </h3>
                <h3>CNPJ/CPF: '.$post["contCNPJ"].' </h3>';
    
    if(empty($post["govMarca"])):
        $view .= '<h3>Área de Interesse: '.$post["contObs"].' </h3>
                  <h3>Como conheceu a ReisOffice: '.$post["contConheceu"].' </h3>';                 
        $titulo = "Página Contato (Site)";
    else:
        $view .= '<h3>Marca: '.$post["govMarca"].' </h3>
                  <h3>Produto: '.$post["govProduto"].' </h3>';                      
        $titulo = "Contato de Licitações (Site)";
    endif;

    $view .= '  <br/>
                <h4>Mensagem:</h4>
                <h3> '.$post["contMsg"].' </h3>
            </div>';   
    
    return array("titulo" => $titulo, "view" => $view);        
}

function viewTrabalhe($post)
{
    return '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["trabNome"].' </h3>
                <h3>Email: '.$post["trabEmail"].' </h3>
                <h3>Telefone: '.$post["trabFone"].' </h3>
                <br/>
                <h4>Mensagem:</h4>
                <h3> '.$post["trabMsg"].' </h3>
            </div>';
}

function viewOutsourcing($post)
{
    $cnpj = $post["outCNPJ"];
    $label = strlen($cnpj) < 15 ? "CPF":"CNPJ";

    return '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["outNome"].' </h3>
                <h3>Email: '.$post["outEmail"].' </h3>
                <h3>Telefone: '.$post["outFone"].' </h3>                           
                <h3>'.$label.': '.$post["outCNPJ"].' </h3>                                                                                   
                <br/>
                <h4>Mensagem</h4>
                <h3> '.$post["outMsg"].' </h3>
            </div>';
}

function viewGoverno($post)
{
    return '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["govNome"].' </h3>
                <h3>Email: '.$post["govEmail"].' </h3>
                <h3>Telefone: '.$post["govFone"].' </h3>
                <h3>Empresa/Orgão: '.$post["govEmpresa"].' </h3>
                <h3>Material: '.$post["govMaterial"].' </h3>
                <h3>Quantidade: '.$post["govQtde"].' </h3>                                                                
                <br/>
                <h4>Observações</h4>
                <h3> '.$post["govObs"].' </h3>
            </div>';
        
}

function viewRevendas($post)
{
    return '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["revNome"].' </h3>
                <h3>Email: '.$post["revEmail"].' </h3>
                <h3>Telefone: '.$post["revFone"].' </h3>
                <h3>Empresa/Orgão: '.$post["revEmpresa"].' </h3>                                                                              
                <br/>
                <h4>Observações</h4>
                <h3> '.$post["revObs"].' </h3>
            </div>';
}

function viewLandings($post)
{
    return '<div style="font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;">
                <h3>Nome: '.$post["txtNomeForm"].' </h3>
                <h3>Email: '.$post["txtEmailForm"].' </h3>
                <h3>Telefone: '.$post["txtTelForm"].' </h3>
                <h3>Empresa/Orgão: '.$post["txtEmpreForm"].' </h3>
                <br/>
                <h4>Observações</h4>
                <h3> '.$post["txtObsForm"].' </h3>
            </div>';    
}