<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Governo_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    }
    
    //GOVERNO   
    //Destaques Index
    public function getAllDestaques()
    {
        $this->db->select("*");
        $this->db->from("site_destaques");
        $this->db->where("ativo", 1);
        $this->db->where("tipo", 2);
        $this->db->order_by("dt_publicacao", "DESC");
        $this->db->limit(3);
        $qry = $this->db->get();
        return $qry->result();
    }     
}
