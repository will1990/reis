<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Governo extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();       
        $this->load->model("Governo_model","mdgov");
        $this->data["pathbase"] = base_url("images/conteudo/");
        $this->data["pagina"] = null;
      
        $this->data["tituloBarra"] = (object)array("titulo" => "Licitações", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
            );
        $this->data["topMarcador"] = 5;  
    }
    
    //GOVERNO
    //Página Inicial de Governo
    public function index()
    {              
        $this->data["destaques"] = $this->mdgov->getAllDestaques();
           
        $this->data["barraDir"] = $this->load->view('governo/barradirForm', $this->data, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('governo/index', $this->data);
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('template/footer');        
    }
    
    //Página de Atas
    public function atas($marca = null)
    {
        $this->data["topMarcador"] = 0;
        $this->data["pagina"] = "atas";
        $this->data["tituloBarra"]->titulo = meta($this->data["pagina"])->title;
        
        if($marca === 'brother'):
            $label = "Brother";
            $view = 'listaBrother';
        elseif($marca === 'canon'):
            $label = "Canon";
            $view = 'listaCanon';
        elseif($marca === 'kyocera'):
            $label = "Kyocera";
            $view = 'listaKyocera';
        elseif($marca === 'oki'):
            $label = "Oki";
            $view = 'listaOki';
        else:
            $marca = null;
        endif;            
                       
        $this->data["barraDir"] = $this->load->view('governo/barradirForm', $this->data, true);
           
        $this->load->view('template/header', array("pagina"=>"atas"));
        $this->load->view('template/navbar', $this->data);        

        if($marca !== null):            
            $this->data["label"] = $label;
            $this->data["tituloBarra"]->titulo .= '&nbsp;'.$label; 
        
            $this->load->view('template/barrasup', $this->data);
            $this->data["listagem"] = $this->load->view("governo/$view", $this->data, true);
            $this->load->view("governo/listasAtas", $this->data);
        else:
            $this->load->view('template/barrasup', $this->data);
            $this->load->view("governo/atas", $this->data);
        endif;
        
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('template/footer');        
    }
    
    
    //Página de Patrimônio
    public function patrimonio()
    {
        $this->data["topMarcador"] = 0;
        $this->data["pagina"] = "patrimonio";
        $this->data["tituloBarra"]->titulo = meta($this->data["pagina"])->title;
              
        $this->data["barraDir"] = $this->load->view('governo/barradirForm', $this->data, true);
           
        $this->load->view('template/header', array("pagina" =>"patrimonio"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view("governo/patrimonio", $this->data);
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('template/footer');        
    }   
}