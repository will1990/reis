<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
        $this->load->helper("emails");
        $this->data['pagina'] = null;       
        $this->load->model("Home_model","mdhome");
        $this->load->model("Landing_model","mdland");
        
        $this->data["tituloBarra"] = (object)array("titulo" => "Obrigado pelo Contato", "fontSize" => "30px");
        $this->data["botoesBarra"] = array();
        
    }
    
    // SUCESSO
    // Tela única utilizada após sucesso no envio das informações
    public function index($msg = null)
    {
        $this->data["descricao"] = ($msg !== null)? $msg : "Aguarde Contato dos nossos Consultores";
        $this->data["js"] = $this->load->view('js/jsGoogleConversion', array("valor_conversao" => 275), true);
        
        $this->load->view('template/header', array("pagina"=>"contato/index"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);   
        $this->load->view('contato/sucessoContato', $this->data);
        $this->load->view('template/footer', $this->data);
    }    
       
    // ALL FORMS AND EMAILS
    // Uso em Contato, Trabalhe, Outsourcing, Governo, Revenda
    public function salvar()            
    {        
        $post = $this->input->post();
        if(!empty($post)):
            switch ($post["controller"]):
                case "contato":
                    
                    $this->mdhome->salvarContato();                     
                    $email = (object) array(
                        'email' => array('SITE@reisoffice.com.br','julio.miranda@gmail.com','rodrigo@reisoffice.com.br','mariana.lima@reisoffice.com.br'),
                        //'email' => array('fagner.valerio@reisoffice.com.br'),
                        //'email' => array('william.feliciano@reisoffice.com.br'),
                        'nome' => $post["contName"],
                        'mensagem' => viewContato($post),
                        //'cc' => ' ', 
                        'assunto' => viewContato($post)["titulo"]          
                    );                    
                    $this->sistema->enviarEmail($this->dadosRemetente("Página Contato"), $email);           
                    $this->index();
                    
                    break;                
                case "trabalhe":
                    
                    $this->form_validation->set_rules('trabArq', 'Arquivo', 'callback_validArq');              
                    if($this->form_validation->run() !== false):
                        $email = (object) array(
                            'email' => 'selecao@reisoffice.com.br',
                            //'email' => array('fagner.valerio@reisoffice.com.br'),
                            //'email' => array('william.feliciano@reisoffice.com.br'),
                            'nome' => $post["contName"],
                            'mensagem' => array("titulo" => "Contato de Trabalhe Conosco (Site)", "view" => viewTrabalhe($post)),
                            //'cc' => ' ', 
                            'assunto' => "Contato de Trabalhe Conosco (Site)",
                            'anexos' => $this->session->flashdata("arquivo"),          
                        );
                        $this->sistema->enviarEmail($this->dadosRemetente("Trabalhe Conosco"), $email);
                        $this->session->set_flashdata('arquivo', null); 
                        $this->index("Aguarde contato em caso de aprovação, boa sorte!");
                    else:
                        $this->session->set_flashdata('arquivo', validation_errors());
                        redirect(base_url("home/trabalhe")); 
                    endif;
                    
                    break;                    
                case "outsourcing":
                    
                    $email = (object) array(
                        'email' => array('SITE@reisoffice.com.br','julio.miranda@gmail.com','rodrigo@reisoffice.com.br','mariana.lima@reisoffice.com.br'),
                        //'email' => array('fagner.valerio@reisoffice.com.br'),
                        //'email' => array('william.feliciano@reisoffice.com.br'),
                        'nome' => $post["outName"],
                        'mensagem' => array("titulo" => "Contato da Página Outsourcing (Site)", "view" => viewOutsourcing($post)),                
                        //'cc' => ' ', 
                        'assunto' => 'Contato da Página Outsourcing (Site)'            
                    );
                    $this->sistema->enviarEmail($this->dadosRemetente("Página Outsourcing"), $email);          
                    $this->index();
                    
                    break;                
                case "governo":
                    
                    $email = (object) array(
                        'email' => array('SITE@reisoffice.com.br','julio.miranda@gmail.com','rodrigo@reisoffice.com.br','mariana.lima@reisoffice.com.br'),
                        //'email' => array('fagner.valerio@reisoffice.com.br'),
                        //'email' => array('william.feliciano@reisoffice.com.br'),
                        'nome' => $post["govName"],
                        'mensagem' => array("titulo" => "Contato da Página Governo (Site)", "view" => viewGoverno($post)),
                        //'cc' => ' ', 
                        'assunto' => 'Contato da Página Governo (Site)'         
                    );
                    $this->sistema->enviarEmail($this->dadosRemetente("Página Governo"), $email);           
                    $this->index();
                    
                    break;                
                case "revenda":
                                 
                    $email = (object) array(           
                        'email' => array('SITE@reisoffice.com.br','julio.miranda@gmail.com','rodrigo@reisoffice.com.br','mariana.lima@reisoffice.com.br'),
                        //'email' => array('fagner.valerio@reisoffice.com.br'),
                        //'email' => array('william.feliciano@reisoffice.com.br'),
                        'nome' => $post["revName"],
                        'mensagem' => array("titulo" => "Contato da Página Revendas (Site)", "view" => viewRevendas($post)),                
                        //'cc' => ' ', 
                        'assunto' => 'Contato da Página Revendas (Site)'            
                    );
                    $this->sistema->enviarEmail($this->dadosRemetente("Página Revendas"), $email);           
                    $this->index();
                    
                    break;
                case "landings":
                    
                    $this->mdland->addContato();                                       
                    $email = (object) array(
                        'email' => 'marketing@reisoffice.com.br',
                        //'email' => 'william.feliciano@reisoffice.com.br',
                        'nome' => $post["txtNameForm"],
                        'mensagem' => array("titulo" => $post["txtNomeLanding"], "view" => viewLandings($post)),                
                        //'cc' => ' ', 
                        'assunto' => 'Contato sobre '.$post["txtNomeLanding"]            
                    );
                    $this->sistema->enviarEmail($this->dadosRemetente("Landing Pages"), $email);           
                    $this->redirecionar();
                    
                    break;
                default:
                    $this->index();                                    
            endswitch;           
        else:
            redirect(base_url());
        endif;        
    }       
    
    //Parâmentros do Remetente padrão
    public function dadosRemetente($nome = null)
    {
        return (object) array(
            'email' => 'sistema.solicitacao@reisoffice.com.br',
            'nome'  => $nome.' - Site Reis Office'
        );
    }
    
    // PARA LANDINGS
    // Tratamento para redirecionamento correto após o envio de informações dos forms em Landings Pages
    public function redirecionar()
    {
        $nomes = array('Canon-G1100','Canon-G2100','Canon-G3100','Canon-G3102');
        if(array_search($this->input->post('txtNomeLanding'), $nomes) !== false)
        {
            $this->sucesso();
        }
        else
        {
            $this->index();
        }
    }
    
    //Tela de Sucesso para Landings com Iframe
    public function sucesso()
    {
        $this->load->view('contato/sucessoLanding');
    }
    
    // PARA TRABALHE CONOSCO
    // Faz o upload de Arquivos para Anexos
    public function validArq()
    {
        $nome = 'Arq_'.date('yms').rand(1,5);
        
        $ext = pathinfo($_FILES["trabArq"]["name"], PATHINFO_EXTENSION); 

        $this->load->library('upload');

        $configuracao = array(
         'upload_path'   => APPPATH.'/../uploads/',
         'allowed_types' => 'doc|docx|pdf',
         'file_name'     => $nome.'.'.$ext,
         'max_size'      => '1000'
         );

        $this->upload->initialize($configuracao);

        if($this->upload->do_upload("trabArq"))
        {
            $this->session->set_flashdata("arquivo", $this->upload->data());
            return true;
        }
        else
        {            
            $this->form_validation->set_message('validArq', $this->upload->display_errors());
            return false;
        }
    }    
}