<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fabricante extends CI_Controller {

    /**
     *  Saite Reis Office
     *    
     */
    function __construct() {
        parent::__construct();
        $this->load->model("Produtos_model", "m_produtos");
        //$this->load->model("Fabricante_model", "m_fabricante");

        $this->data["topMarcador"] = 6;
        $this->data['pagina'] = null;
        $this->data["nossas_marcas"] = ""; 
        
    }
    
    public function index($marca = false)
    {
        if($marca === false){
            redirect(base_url("produtos"));
        }
        
        $this->session->marca = strtoupper($marca);
        $this->data['menu'] = $this->m_produtos->get_menu();
        $this->data["menuEsq"] = $this->load->view('fabricante/menuesq', $this->data, true);
        $this->data["tituloBarra"] = (object) array("titulo" => "$marca", "fontSize" => "30px");
        $this->data["logo"] = base_url("images/logos/". strtolower($marca) .".jpg");
        $this->data["banner"] = base_url("images/landing/Marcas_Banners/". strtolower($marca) ."/Banner_Hotsite_Brother_01_A_prova-01.png");
        $this->data["botoesBarra"] = array(
            (object) array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object) array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object) array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('fabricante/barrasup', $this->data);
        //$this->load->view('fabricante/inicial', $this->data);
        $this->load->view("fabricante/fabricante", $this->data);
        $this->load->view('template/footer'); 
    }
    public function inicial ($marca)
    {

        $this->data['menu'] = $this->m_produtos->get_menu();
        $this->data["menuEsq"] = $this->load->view('fabricante/menuesq', $this->data, true);    
        $this->session->marca = strtoupper($marca);
        $this->data["logo"] = base_url("images/logos/". strtolower($marca) .".jpg");
        $this->data["banner"] = base_url("images/landing/Marcas_Banners/". strtolower($marca) ."Banner_Hotsite_Olivetti_01_A-01.png");
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('fabricante/barrasup', $this->data);
        $this->load->view('fabricante/inicial');
        $this->load->view('template/footer'); 
        

    }
    public function produtos($familia = '', $linha = '', $categoria = '',  $codigo = '')
    {    
        $this->data["menu"] = $this->m_produtos->get_menu();
        $this->data["tituloBarra"] = (object) array("titulo" => $this->session->marca, "fontSize" => "30px");
        $this->data["logo"] = base_url("images/logos/". strtolower($this->session->marca) .".jpg");
        $this->data["banner"] = base_url("images/landing/Marcas_Banners/". strtolower($this->session->marca) ."/Banner_Hotsite_Brother_01_A_prova-01.png");// vai ser chamado do banco de dados tabela lading_marca
        $this->data["botoesBarra"] = array(   //vai ser chamado do banco de dados links utilizar o Upper
            (object) array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object) array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object) array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        
        $this->data["chave_familia"] = (is_numeric($familia)) ? $familia : 0;
        $this->data["chave_linha"] = (is_numeric($linha)) ? $linha : 0;
        $this->data["chave_categoria"] = (is_numeric($categoria)) ? $categoria : 0;
        $this->data["menu_familia"] = $this->m_produtos->get_produto_familia($this->data["chave_familia"]);
        $this->data["menu_linha"] = $this->m_produtos->get_produto_linha($this->data["chave_linha"]);
        $this->data["menu_categoria"] = $this->m_produtos->get_produto_categoria($this->data["chave_categoria"]);
                
        $this->data["menuEsq"] = $this->load->view('fabricante/menuesq', $this->data, true);
        //$this->data["js"] = $this->load->view('produtos/jsMenu', $this->data, true);

        // Verifica como buscar os produtos arrumar
        if(!empty($codigo))
        {
            $view = "produto";            
            $this->data["codigo"] = (!is_numeric($linha)) ? $familia : $codigo;
            $this->data["produto"] = $this->m_produtos->get_produto_por($this->data["codigo"]);
            $this->data["detalhes"] = $this->m_produtos->get_produto_por_detalhe($this->data["codigo"]);
            $this->data["titulo_descricao"] = $this->data["produto"]->PRODUTO;
            
            $this->data["chave_familia"] = $this->data["produto"]->FAMILIA->CHAVE;
            $this->data["chave_linha"] = $this->data["produto"]->LINHA->CHAVE;
            $this->data["chave_categoria"] = $this->data["produto"]->CATEGORIA->CHAVE;
            $this->data["menu_familia"] = $this->data["produto"]->FAMILIA->FAMILIA;
            $this->data["menu_linha"] = $this->data["produto"]->LINHA->LINHA;
            $this->data["menu_categoria"] = $this->data["produto"]->CATEGORIA->CATEGORIA;
        } else {
            $view = "fabricante";
            $this->data["produtos"] = $this->m_produtos->get_produtos($familia, $linha, $categoria);
        }

        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('fabricante/barrasup', $this->data);
        $this->load->view("fabricante/$view", $this->data);
        $this->load->view('template/footer', $this->data);        
    }
}
    