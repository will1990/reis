<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
         
    //LANDINGS
    //Carregando Páginas através do Nome 
    public function page($nome = null)
    {    
        $this->data["nome"] = $nome;  
        switch($nome)
        {
            case "Multifuncional-Laser-Mono-imageRUNNER-1435-Canon":
                $this->data["form_top_margin"] = "105px";
                $this->data["botao_classe"] = "btn-lg btn-success";
                $this->data["botao_texto"] = "Enviar Solicitação";              
                break;
            case "Reembolso-de-Despesas":
                $this->data["form_top_margin"] = "10px";
                $this->data["botao_classe"] = "btn-lg btn-default";
                $this->data["botao_texto"] = "Enviar";         
                break;
            case "Follow-Me":
                $this->data["form_top_margin"] = "10px";
                $this->data["botao_classe"] = "btn-lg btn-default";
                $this->data["botao_texto"] = "Enviar";                
                break;
            case "Arquivo-Digital":
                $this->data["form_top_margin"] = "10px";
                $this->data["botao_classe"] = "btn-lg btn-default";
                $this->data["botao_texto"] = "Enviar";
                break;
            case "Canon-G1100":
                $this->data["iFrameWidth"] = "100%";
                $this->data["iFrameHeight"] = "600px";                
                $this->data["iFrame"] = $this->load->view('landings/iFrame', $this->data, true);                
                break;
            case "Canon-G2100":
                $this->data["iFrameWidth"] = "100%";
                $this->data["iFrameHeight"] = "600px";                
                $this->data["iFrame"] = $this->load->view('landings/iFrame', $this->data, true);                
                break;
            case "Canon-G3100":
                $this->data["iFrameWidth"] = "100%";
                $this->data["iFrameHeight"] = "600px";                
                $this->data["iFrame"] = $this->load->view('landings/iFrame', $this->data, true);                
                break;
            case "Canon-G3102":
                $this->data["iFrameWidth"] = "100%";
                $this->data["iFrameHeight"] = "600px";                
                $this->data["iFrame"] = $this->load->view('landings/iFrame', $this->data, true);                
                break;
            default:
                $this->data["form_top_margin"] = "0px";
                $this->data["botao_classe"] = "btn-default";
                $this->data["botao_texto"] = "Enviar";    
        }           
               
        $this->data["form"] = $this->load->view('landings/form', $this->data, true); 
        $this->load->view('landings/'.$nome, $this->data);      
    }   
    
    //Uso para Landings que utilizam formulários dentro de Iframes - Endereço URL
    public function formiframe($nome = null)
    {
        $this->data["nome"] = $nome;        
        $this->data["form_top_margin"] = "10px";
        $this->data["botao_classe"] = "btn-lg btn-default";
        $this->data["botao_texto"] = "Enviar";        
                    
        $this->data["form"] = $this->load->view('landings/form', $this->data, true);
        $this->load->view('landings/formComplemento', $this->data);
    }   
}
