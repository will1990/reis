<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller 
{
    /** 
    *  Site Reis Office
    *  William Feliciano     
    */
    
    function __construct()
    {       
        parent::__construct();
        $this->load->model("Noticias_model","mdnews");
        $this->data['pagina'] = null;
        
        $this->data["tituloBarra"] = (object)array("titulo" => "Listagem de Notícias Reis Office", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> PORTAL DE REVENDAS", "url" => PORTAL_REVENDAS),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        $this->data["topMarcador"] = 0;        
    }
    
    public function index()
    {
        $this->data["noticias"] = $this->mdnews->getAllNoticias();
          
        $this->data["barraDir"] = $this->load->view('noticias/barradir', $this->data, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('noticias/lista', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('noticias/jsnews');
    }
    
    public function ver($id = null)
    {
        $this->data["noticia"] = $this->mdnews->getNoticia($id); 
        
        $this->data["barraDir"] = $this->load->view('noticias/barradir', $this->data, true);
             
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('noticias/noticia', $this->data);        
        $this->load->view('template/footer');
    }
}

