<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outsourcing extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();       
        $this->data["pagina"] = NULL ;
        $this->load->model("Outsourcing_model","mdout");
        $this->data["pathbase"] = base_url("images/conteudo/");
        $this->data["tituloBarra"] = (object)array("titulo" => "Outsourcing", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> PORTAL SERVICE", "url" => PORTAL_SERVICE),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
    }
    
    //OUTSOURCING
    //Página principal de Outsourcing (Lista)
    public function index($ga = false)
    {
        $this->data["topMarcador"] = 4;        
        
        $this->data["outsourcings"] = $this->mdout->getAllOutsourcings();
        $this->data["solucoes"] = $this->mdout->getAllSolucoes();
                          
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
        
        if($ga)
        {
            $this->data["js"] = $this->load->view('js/jsGoogleConversion', array("valor_conversao" => 4500), true);
        }
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/index', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);     
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer', $this->data);
        $this->load->view('js/jsAccordionOutsourcing');
    }
    
    //Montando Conteúdo Interno de Outsourcing
    public function ver($id = null)
    {
        $this->data["topMarcador"] = 0;
        
        $this->data["outsourcing"] = $this->mdout->getOutsourcing($id);
        $this->data["solucoes"] = $this->mdout->getAllSolucoes();
        
        $this->data["tituloBarra"]->titulo = $this->data["outsourcing"]->titulo;
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["outsourcing"]->titulo) < 41)? $this->data["tituloBarra"]->fontSize : '20px';
                
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
        $this->data["nossas_marcas"] = $this->load->view('template/suppliers', null, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/montarCategorias', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);  
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer');
    }
    
    //SOLUCOES
    //Página principal de Outsourcing (Lista)
    public function solucoes($slug = null)
    {       
        if($slug !== null):            
        
            $this->data["topMarcador"] = 0;
            $this->data["solucoes"] = $this->mdout->getAllSolucoes();
            $this->data["tituloBarra"] = (object)array("titulo" => $this->data["solucoes"]["$slug"]->titulo);
            $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'20px');
            $this->data["bread"] = $this->data["solucoes"]["$slug"]->nome;
                    
            $this->data["conteudo"] = $this->load->view("solucoes/$slug", $this->data, true);
            
            $this->load->view('template/header');
            $this->load->view('template/navbar', $this->data);
            $this->load->view('template/barrasup', $this->data);        
            $this->load->view('solucoes/index', $this->data);        
            $this->load->view('template/footer');
            
        else:
            
            redirect(base_url("outsourcing"),"refresh");
            
        endif;
    }
    
    //ASSISTÊNCIA
    //Página de Assistência
    public function assistencia()
    {
                
        $this->data["topMarcador"] = 0;
        $this->data["pagina"] = "assistencia";
        $this->data["tituloBarra"]->titulo = meta($this->data["pagina"])->title;
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        
        $this->data["barraDir"] = $this->load->view("assistencia/barradir", $this->data, true);
                
        //Fazer a logica de cadastro de email do rodapé com Jquery, sem refresh
        
        $this->load->view('template/header', array("pagina"=>"assistencia"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('assistencia/assistencia', $this->data);                
        $this->load->view('template/footer');
        $this->load->view('js/jsMostrarAssist', $this->data);        
    }   
}