<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos_model extends CI_Model {

    /**
     *  Site Reis Office
     *  William Feliciano     
     */
    function __construct() {
        $this->db = $this->load->database('oracle', TRUE);
        $this->mysql = $this->load->database('default', TRUE);
    }

    //PRODUTOS   
    //pega os produtos do menu
    public function get_menu() {
        $result = array();
        
        $marca = "";
        if(isset($this->session->marca) && !empty($this->session->marca))
            $marca = "AND P.CHAVE_MARCA = (SELECT CHAVE FROM REIS_OFFICE.MARCAS WHERE MARCA = '". $this->session->marca ."')";

        // FAMILIA
        $sql = "SELECT F.CHAVE AS CHAVE_FAMILIA, F.FAMILIA, ". oracle_translate("F.FAMILIA") ." FAMILIA_URL FROM REIS_OFFICE.FAMILIA_PRODUTOS F WHERE (chave=36885  or chave = 36985 or chave = 78046  ) and CHAVE IN( SELECT L.CHAVE_FAMILIA FROM REIS_OFFICE.GRUPO_PRODUTOS G, REIS_OFFICE.LINHA_PRODUTOS L, REIS_OFFICE.PRODUTOS P WHERE G.CHAVE_LINHA = L.CHAVE AND P.CHAVE_GRUPO = G.CHAVE AND P.SEM_ECOMMERCE = 'NAO') ORDER BY F.FAMILIA";
        $rst = $this->db->query($sql)->result(); {
            foreach ($rst as $familia) {
                // LINHA
                $sql = "SELECT L.CHAVE , NVL(LF.LINHA_WEB, L.LINHA) AS LINHA
                        , ". oracle_translate("NVL(LF.LINHA_WEB, L.LINHA)") ." LINHA_URL
                        FROM REIS_OFFICE.LINHA_PRODUTOS L, REIS_OFFICE.LINHA_PRODUTOS_FOTOS LF 
                        WHERE L.CHAVE = LF.CHAVE_LINHA 
                        AND L.CHAVE_FAMILIA = " . $familia->CHAVE_FAMILIA . " AND lower(NVL(LF.LINHA_WEB, L.LINHA)) != 'outros suprimentos canon' 
                        AND NVL(LF.LINHA_WEB, L.LINHA) != 'Revelador' AND L.CHAVE IN (SELECT P.CHAVE_LINHA 
                                                                                        FROM REIS_OFFICE.PRODUTOS P 
                                                                                        WHERE P.SEM_ECOMMERCE = 'NAO' 
                                                                                        AND LF.EXIBIR_WEB = 'SIM' $marca) ORDER BY LF.SEQUENCIA";
                //SELECT G.CHAVE_LINHA FROM REIS_OFFICE.GRUPO_PRODUTOS G, REIS_OFFICE.PRODUTOS P WHERE P.CHAVE_GRUPO = G.CHAVE AND P.SEM_ECOMMERCE = 'NAO'
                $familia->LINHAS = $this->db->query($sql)->result();
                if ($familia->LINHAS) {
                    foreach ($familia->LINHAS as $linha) {
                        // CATEGORIA
                        $sql = "SELECT NVL(GF.GRUPO_WEB, GP.GRUPO) GRUPO, MIN(GF.SEQUENCIA) SEQUENCIA, GP.CHAVE AS CHAVE
                                , ". oracle_translate("NVL(GF.GRUPO_WEB, GP.GRUPO)") ." GRUPO_URL
                                FROM REIS_OFFICE.GRUPO_PRODUTOS_FOTOS GF, REIS_OFFICE.GRUPO_PRODUTOS GP 
                                WHERE GF.CHAVE_GRUPO = GP.CHAVE 
                                AND GF.EXIBIR_WEB = 'SIM' 
                                AND GP.CHAVE_LINHA = ". $linha->CHAVE ."
                                AND GP.CHAVE NOT IN (79366,79368,79370,79371)
                                AND GP.CHAVE IN (SELECT P.CHAVE_GRUPO 
                                                    FROM REIS_OFFICE.PRODUTOS P 
                                                    WHERE P.SEM_ECOMMERCE = 'NAO' 
                                                    AND P.SERVICO = 'NAO' 
                                                    AND P.PRODUTO = 'SIM' 
                                                    AND P.FORA_DE_LINHA = 'NAO' $marca)                                
                                GROUP BY NVL(GF.GRUPO_WEB, GP.GRUPO), GP.CHAVE
                                , ". oracle_translate("NVL(GF.GRUPO_WEB, GP.GRUPO)") ."
                                ORDER BY SEQUENCIA";
                        //echo $sql; exit;
                        $linha->CATEGORIAS = $this->db->query($sql)->result();
                        //pega todos os produtos daquela linha
                    }
                }
            }
        }
        
        return $rst;
    }

    public function get_produtos($familia, $linha, $categoria) {
        $chave_categ = $chave_linha = $chave_familia = false;

        // Pega a chave do grupo
        if(!empty($categoria))
        {
            $rst = $this->get_produto_categoria($categoria);
            $chave_categ = $rst->CHAVE;            
        }

        // pega a chave da linha
        if(!empty($linha))
        {
            $rst = $this->get_produto_linha($linha);
            $chave_linha = $rst->CHAVE;
        }

        // pega a chave da familia
        if(!empty($familia))
        {
            $rst = $this->get_produto_familia($familia);
            $chave_familia = $rst->CHAVE;
        }
                
        $this->db->select("PRODUTOS.CPROD, PRODUTOS.PORTAL_DESCRICAO_CURTA, LP.LINHA, FP.FAMILIA, NVL(GF.GRUPO_WEB, G.GRUPO) CATEGORIA");
        $this->db->select("NVL(PRODUTOS.PORTAL_DESCRICAO, PRODUTOS.DESCRICAO) PRODUTO", false);

        $this->db->join("REIS_OFFICE.GRUPO_PRODUTOS G", "PRODUTOS.CHAVE_GRUPO = G.CHAVE");
        $this->db->join("REIS_OFFICE.GRUPO_PRODUTOS_FOTOS GF", "GF.CHAVE_GRUPO = G.CHAVE");
        $this->db->join("REIS_OFFICE.PORTAL_PRODUTOS PP", "PRODUTOS.CPROD = PP.CHAVE_PRODUTO");
        $this->db->join("REIS_OFFICE.FAMILIA_PRODUTOS FP", "PRODUTOS.CHAVE_FAMILIA = FP.CHAVE");
        $this->db->join("REIS_OFFICE.LINHA_PRODUTOS LP", "PRODUTOS.CHAVE_LINHA = LP.CHAVE");

        $this->db->where("PRODUTOS.SERVICO = 'NAO' AND PRODUTOS.PRODUTO = 'SIM' AND PRODUTOS.SEM_ECOMMERCE = 'NAO' AND PRODUTOS.FORA_DE_LINHA = 'NAO'");
        $this->db->where("GF.EXIBIR_WEB = 'SIM'");
        
        if($chave_categ)
            $this->db->where("PRODUTOS.CHAVE_GRUPO = $chave_categ");

        if($chave_linha)
            $this->db->where("PRODUTOS.CHAVE_LINHA = $chave_linha");

        if($chave_familia)
            $this->db->where("PRODUTOS.CHAVE_FAMILIA = $chave_familia");
        
        if(isset($this->session->marca) && !empty($this->session->marca))
            $this->db->where("PRODUTOS.CHAVE_MARCA = (SELECT CHAVE FROM REIS_OFFICE.MARCAS WHERE MARCA = '". $this->session->marca ."')");

        //$this->db->order_by("DBMS_RANDOM.RANDOM");
        $this->db->order_by("PP.LOG_DATA", "ASC");
        $totalProd = $this->db->count_all_results("REIS_OFFICE.PRODUTOS", false);
        $sqlProd = $this->db->get_compiled_select();
        
        // Define a pagina
        $pg = (isset($this->session->produto_pagina)) ? $this->session->produto_pagina : 1;
        $pg_fim = $pg * 16;
        $pg_ini = $pg_fim - 15;        
        
        $sql = "SELECT * FROM (SELECT T1.*, rownum rn FROM ($sqlProd) T1 WHERE rownum <= $pg_fim) T2 WHERE rn >= $pg_ini";
        $rst = $this->db->query($sql)->result();
        
        return array($totalProd, $rst);
    }
    
    public function get_produto_fotos($cprod, $binario = true)
    {
        if($binario)
            $this->db->select("FOTO");
        $this->db->select("DESCRICAO, TIPO, CHAVE");
        $this->db->where("CHAVE_PRODUTO ", $cprod);
        $this->db->order_by("TIPO", "ASC");
        $rst = $this->db->get("REIS_OFFICE.PRODUTOS_FOTOS")->result();        
        return $rst;
    }
    
    public function get_foto_produto($chave)
    {
        $this->db->select("FOTO");
        $this->db->where("CHAVE = $chave");
        $rst = $this->db->get("REIS_OFFICE.PRODUTOS_FOTOS")->row();        
        return $rst;
    }

    public function get_produto_por($codigo = false, $descricao = false) {
        $this->db->select("PRODUTOS.CPROD, PRODUTOS.PORTAL_DESCRICAO_CURTA, PRODUTOS.CHAVE_GRUPO, PRODUTOS.CHAVE_LINHA, PRODUTOS.CHAVE_FAMILIA");
        $this->db->select("NVL(PRODUTOS.PORTAL_DESCRICAO, PRODUTOS.DESCRICAO) PRODUTO", false);

        $this->db->join("REIS_OFFICE.GRUPO_PRODUTOS G", "PRODUTOS.CHAVE_GRUPO = G.CHAVE");
        $this->db->join("REIS_OFFICE.GRUPO_PRODUTOS_FOTOS GF", "GF.CHAVE_GRUPO = G.CHAVE");        
        
        $this->db->where("PRODUTOS.SERVICO = 'NAO' AND PRODUTOS.PRODUTO = 'SIM' AND PRODUTOS.SEM_ECOMMERCE = 'NAO' AND PRODUTOS.FORA_DE_LINHA = 'NAO'");
        $this->db->where("GF.EXIBIR_WEB = 'SIM'");
        if ($codigo)
            $this->db->where("PRODUTOS.CPROD = $codigo");
        else
            $this->db->where("UPPER(NVL(PRODUTOS.PORTAL_DESCRICAO, PRODUTOS.DESCRICAO)) LIKE UPPER('%" . oracle_translate($descricao) . "%')");
        $rst = $this->db->get("REIS_OFFICE.PRODUTOS")->row();
        if ($rst) {
            // Pega a categoria (grupo)

            $this->db->select("GP.CHAVE, GP.GRUPO CATEGORIA");
            $this->db->where("GP.CHAVE = $rst->CHAVE_GRUPO");
            $rst->CATEGORIA = $this->db->get("REIS_OFFICE.GRUPO_PRODUTOS GP")->row();
            // Pega a linha

            $this->db->select("LP.CHAVE, LP.LINHA");
            $this->db->where("LP.CHAVE = $rst->CHAVE_LINHA");
            $rst->LINHA = $this->db->get("REIS_OFFICE.LINHA_PRODUTOS LP")->row();
            // Pega a familia
            $this->db->select("FP.CHAVE, FP.FAMILIA");
            //$this->db->select("PRODUTOS.PORTAL_DESCRICAO, PRODUTOS.DESCRICAO");
            $this->db->where("FP.CHAVE = $rst->CHAVE_FAMILIA");
            $rst->FAMILIA = $this->db->get("REIS_OFFICE.FAMILIA_PRODUTOS FP")->row();         
        }
        return $rst;
    }

    public function get_produto_detalhes($codigo) {

        return false;
    }
    
    
    public function get_produto_por_detalhe($detalhe_produto){  // pega detalhes de um unico produto
               
        $this->db->select("CODIGO_INTERNO, PP.PORTAL_DESCRICAO_LONGA, PRODUTOS.PORTAL_DESCRICAO_LONGA PORTAL_DESCRICAO_LONGA_ANTIGA, PESO_BRUTO, PESO_LIQUIDO, GRUPO, CPROD, CHAVE_MARCA");    
        $this->db->select("NVL(PRODUTOS.PORTAL_DESCRICAO, PRODUTOS.DESCRICAO) PRODUTO", false);
        $this->db->join("REIS_OFFICE.PORTAL_PRODUTOS PP", "PRODUTOS.CPROD = PP.CHAVE_PRODUTO");
        $resultado = $this->db->get_where("REIS_OFFICE.PRODUTOS", "CPROD = $detalhe_produto")->row();
        if($resultado)
        {
            if(strlen($resultado->PORTAL_DESCRICAO_LONGA->load()) <= 230)
                $resultado->PORTAL_DESCRICAO_LONGA = $resultado->PORTAL_DESCRICAO_LONGA_ANTIGA;
            
            $resultado->FABRICANTE = $this->get_produto_fabricante($resultado->CHAVE_MARCA);
            $resultado->BIBLIOTECA = $this->get_produto_biblioteca($resultado->CPROD);
            $resultado->FOTOS = $this->get_produto_fotos($resultado->CPROD, false);
        }
        return $resultado;
    }
    
    public function get_produto_fabricante($codigo)
    {
        $this->db->select("MARCA AS FABRICANTE");
        $this->db->where("CHAVE = $codigo");
        $rst = $this->db->get("REIS_OFFICE.MARCAS")->row();
        return $rst;
    }
    
    public function get_produto_biblioteca($codigo, $codigo_arquivo = false)
    {
        if($codigo_arquivo)
        {
            $this->db->select("NOME_ARQUIVO, TAMANHO_ARQUIVO, CHAVE, ARQUIVO");
            $this->db->where("CHAVE = $codigo_arquivo AND PRIVADO = 'NAO'");
            $rst = $this->db->get("REIS_OFFICE.BIBLIOTECA_PRODUTOS")->row();
            return $rst;
        }
        else
        {
            $this->db->select("NOME_ARQUIVO, TAMANHO_ARQUIVO, CHAVE");
            $this->db->where("CHAVE_PRODUTO = $codigo AND PRIVADO = 'NAO'");
            $rst = $this->db->get("REIS_OFFICE.BIBLIOTECA_PRODUTOS")->result();
            return $rst;
        }
    }
    
    public function get_produto_familia($chave)
    {
        if(!empty($chave))
        {
            $this->db->select("FP.CHAVE, FP.FAMILIA");
            if(is_numeric($chave))
                $this->db->where("CHAVE = $chave");
            else
                $this->db->where(oracle_translate("FP.FAMILIA") ." LIKE UPPER('". url_decodificar($chave) ."')");
            $rst = $this->db->get("REIS_OFFICE.FAMILIA_PRODUTOS FP")->row();
            if($rst)
                return $rst;
        }
        
        // Nao foi informada ou nao foi encontrada
        return (object)array("CHAVE" => false, "FAMILIA" => "");
        
        /*$this->db->where("CHAVE = $chave");
        $rst = $this->db->get("REIS_OFFICE.FAMILIA_PRODUTOS")->row();
        if($rst)
            return $rst->FAMILIA;        */
    }
    
    public function get_produto_linha($chave)
    {
        if(!empty($chave))
        {
            $this->db->select("LP.CHAVE, LP.LINHA");
            if(is_numeric($chave))
                $this->db->where("LP.CHAVE = $chave");
            else
                $this->db->where(oracle_translate("UPPER(LP.LINHA)") ." LIKE UPPER('". url_decodificar($chave) ."')");
            $rst = $this->db->get("REIS_OFFICE.LINHA_PRODUTOS LP")->row();
            if($rst)
                return $rst;
            else {
                $this->db->select("LPF.CHAVE_LINHA AS CHAVE, LPF.LINHA_WEB AS LINHA");
                if(is_numeric($chave))
                    $this->db->where("LPF.CHAVE_LINHA = $chave");
                else
                    $this->db->where(oracle_translate("UPPER(LPF.LINHA_WEB)") ." LIKE UPPER('". url_decodificar($chave) ."')");
                $rst = $this->db->get("REIS_OFFICE.LINHA_PRODUTOS_FOTOS LPF")->row();
                if($rst)
                    return $rst;
            }
        }
        
        // Nao foi informada ou nao foi encontrada
        return (object)array("CHAVE" => false, "LINHA" => "");
        
        /*$this->db->where("CHAVE = $chave");
        $rst = $this->db->get("REIS_OFFICE.LINHA_PRODUTOS")->row();
        if($rst)
            return $rst->LINHA;*/
    }
    
    public function get_produto_categoria($chave)
    {
        if(!empty($chave))
        {
            $this->db->select("GP.CHAVE, GP.GRUPO AS CATEGORIA");            
            if(is_numeric($chave))
                $this->db->where("GP.CHAVE = $chave");
            else
                $this->db->where(oracle_translate("UPPER(GP.GRUPO)") ." LIKE UPPER('". url_decodificar($chave) ."')");
            $rst = $this->db->get("REIS_OFFICE.GRUPO_PRODUTOS GP")->row();
            if($rst)
                return $rst;
            else {
                $this->db->select("GPF.CHAVE_GRUPO AS CHAVE, GPF.GRUPO_WEB AS CATEGORIA");
                if(is_numeric($chave))
                    $this->db->where("GPF.CHAVE_GRUPO = $chave");
                else
                $this->db->where(oracle_translate("UPPER(GPF.GRUPO_WEB)") ." LIKE UPPER('". url_decodificar($chave) ."')");
                $rst = $this->db->get("REIS_OFFICE.GRUPO_PRODUTOS_FOTOS GPF")->row();
                if($rst)
                    return $rst;
            }
        }
        
        // Nao foi informada ou nao foi encontrada
        return (object)array("CHAVE" => false, "CATEGORIA" => "");
        
        /*$this->db->where("CHAVE = $chave");
        $rst = $this->db->get("REIS_OFFICE.GRUPO_PRODUTOS")->row();
        if($rst)
            return $rst->GRUPO;*/
    }
    
    public function get_marcas()
    {
        $this->db->where("CHAVE IN (7,8,9,20,39)");
        $this->db->order_by("MARCA", "ASC");
        $rst = $this->db->get("REIS_OFFICE.MARCAS")->result();
        return $rst;
    }
    
    public function get_banners()  
    {       
        $this->mysql->from("site_banners");
        $this->mysql->where(array("tipo" => 3, "ativo" => 1));
        $qry = $this->mysql->get()->result();
        if($qry)
        {
            foreach($qry as $item)
            {
                if(strtolower(substr($item->url, 0, 4) != "http"))
                    $item->url = base_url(strtolower($item->url));
                else
                    $item->url = strtolower($item->url);
            }
        }
        
        return $qry;   
    }
    
    public function get_produtos_destaque()  
    {       
        $this->mysql->from("site_destaques");
        $this->mysql->where(array("tipo" => 3, "ativo" => 1));
        $qry = $this->mysql->get()->result();
        if($qry)
        {
            foreach($qry as $item)
            {
                $item->PRODUTO = $this->get_produto_por($item->img);
            }
        }
        return $qry;   
    }
 }
