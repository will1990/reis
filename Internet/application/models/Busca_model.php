<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Busca_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->oracle = $this->load->database('oracle', TRUE);
        $this->portal = $this->load->database('portal', TRUE);
    }
    
    public function buscarProdutos($termo = null)
    {        
        $sql = "SELECT PRODUTOS.CPROD, PRODUTOS.PORTAL_DESCRICAO_CURTA, PRODUTOS.DESCRICAO, PRODUTOS.PORTAL_DESCRICAO 
                FROM REIS_OFFICE.PRODUTOS
                JOIN REIS_OFFICE.GRUPO_PRODUTOS G ON PRODUTOS.CHAVE_GRUPO = G.CHAVE 
                JOIN REIS_OFFICE.GRUPO_PRODUTOS_FOTOS GF ON GF.CHAVE_GRUPO = G.CHAVE
                WHERE PRODUTOS.SERVICO = 'NAO' 
                AND PRODUTOS.PRODUTO = 'SIM' 
                AND PRODUTOS.SEM_ECOMMERCE = 'NAO' 
                AND PRODUTOS.FORA_DE_LINHA = 'NAO' 
                AND GF.EXIBIR_WEB = 'SIM'
                AND (UPPER(PRODUTOS.PORTAL_DESCRICAO_CURTA) LIKE UPPER('%$termo%') 
                OR UPPER(PRODUTOS.PORTAL_DESCRICAO) LIKE UPPER('%$termo%') 
                OR UPPER(PRODUTOS.PRODUTO) LIKE UPPER('%$termo%')
                OR UPPER(PRODUTOS.CODIGO) LIKE UPPER('%$termo%')
                OR UPPER(PRODUTOS.CODIGO_INTERNO) LIKE UPPER('%$termo%'))";
        
        $resultados = $this->oracle->query($sql)->result();
               
        if(!empty($resultados))
        {
            return $resultados;
        }
        else
        {
            return false;
        }
    }
    
    /*public function buscarNoticias($termo = null)
    {
        $sql = "SELECT cd_noticia, ds_chamada_noticia, ds_integra_noticia,
                INSTR(ds_integra_noticia, 'Reis Office') AS posicao FROM noticia 
                WHERE INSTR(ds_integra_noticia, 'Reis Office')>0 OR ds_chamada_noticia LIKE 'Reis Office'";
        
        $resultados = $this->portal->query($sql)->result();
        
        if(!empty($resultados))
        {
            return $resultados;
        }
        else
        {
            return false;
        }
    }*/
}
