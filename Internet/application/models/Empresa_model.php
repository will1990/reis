<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    }
    
    //EMPRESA    
    //Prêmios
    //Obter os Slides do Carousel (Prêmios)
    public function getPremios()
    {       
        $this->db->select("*");
        $this->db->from("site_premios_solucoes");
        $this->db->where("tipo", 1);
        $qry = $this->db->get();
        return $qry->result();
    }
}