<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Sistema 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    public function __construct()
    {        
        $this->CI =& get_instance();
    }
    
    public function enviarEmail($remetente, $destinatario, $view = "emailView") 
    {       
        $this->CI->load->library('email');

        $mail_config["protocol"] = "smtp";
        $mail_config["smtp_host"] = "mail.reisoffice.com.br";
        $mail_config["smtp_user"] = "spamtrap";
        $mail_config["smtp_pass"] = "Ro12333321";
        $mail_config["smtp_port"] = "587";
        $mail_config["mailtype"] = "html";
        $mail_config['crlf'] = "\r\n";
        $mail_config['newline'] = "\r\n";
        $this->CI->email->initialize($mail_config);

        $this->CI->email->from($remetente->email, html_entity_decode($remetente->nome));
        $this->CI->email->to($destinatario->email);
        if(!empty($destinatario->cc)){
             $this->CI->email->cc($destinatario->cc);
        }
        $this->CI->email->subject(html_entity_decode($destinatario->assunto));
        
        $msg = $this->CI->load->view("email/$view", $destinatario->mensagem, TRUE);
                
        $this->CI->email->message($msg);
        
        if (isset($destinatario->anexos))
        {          
            $this->CI->email->attach($destinatario->anexos['full_path']);
        }        
        
        if ($this->CI->email->send()) 
        {
            $debug = html_entity_decode($this->CI->email->print_debugger(array("headers")));
            if(isset($destinatario->anexos)):
                unlink($destinatario->anexos['full_path']);
            endif;
            return TRUE;
        }
        else
        {
            $debug = html_entity_decode($this->CI->email->print_debugger(array("headers")));
            return FALSE;
        }
    }
}
