<?php

// Funcao que formata os campos para serem mostrados ou salvos
function formatar($string, $tipo = "")
{
    if(empty($string))
        return false;
    
    // Limpa texto deixando somewnte os numeros
    $string = preg_replace("#[^0-9]#", "", $string);
    
    // Caso nao informe o tipo, detecta alguns padroes
    if (!$tipo)
    {
        switch (strlen($string))
        {
            case 10: 	$tipo = 'fone'; break;
            case 8: 	$tipo = 'cep';  break;
            case 11: 	$tipo = 'cpf';  break;
            case 14:    $tipo = 'cnpj'; break;
        }
    }
    
    // Formata no tipo escolhido
    switch($tipo)
    {
        case 'fone':
            if(strlen($string) == 10)
                $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . '-' . substr($string, 6);
            else
                $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 5) . '-' . substr($string, 7);
            break;
        case 'cep':
            $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
            break;
        case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
            break;
        case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12, 2);
            break;
        case 'dt2bd':
            $string = substr($string, -4) . '-' . substr($string, 2, 2) . '-' . substr($string, 0, 2);
            break;
        case 'bd2dt':
            $string = substr($string, -2) . '/' . substr($string, 4, 2) . '/' . substr($string, 0, 4);
            break;
        case 'st':
            $string = substr($string, 0, 2) .':'. substr($string, 2, 2);
            break;
        case 'ft':
            $string = substr($string, 0, 2) .':'. substr($string, 2, 2) .':'. substr($string, 4, 2);
            break;
    }
    return $string;
}

function somente_numeros($string)
{
    return preg_replace("#[^0-9]#", "", $string);
}

$orig = array("ç", "ã", "õ", "á", "à", "é", "í", "ó", "ú");
$mask = array("%cd%", "%at%", "%ot%", "%aa%", "%ac%", "%ea%", "%ia%", "%oa%", "%ua%");

function url_encode($url)
{
    global $orig;
    global $mask;
    //return str_replace($orig, $mask, urlencode($url));    
    return remover_acentos($url);
}

function url_decode($url)
{
    global $orig;
    global $mask;
    $url = urldecode($url);
    $url = str_replace($mask, $orig, $url);
    return $url;
}

function remover_acentos($string) {
    /*$string = preg_replace("/[áàâãä]/", "a", $string);
    $string = preg_replace("/[ÁÀÂÃÄ]/", "A", $string);
    $string = preg_replace("/[éèê]/", "e", $string);
    $string = preg_replace("/[ÉÈÊ]/", "E", $string);
    $string = preg_replace("/[íì]/", "i", $string);
    $string = preg_replace("/[ÍÌ]/", "I", $string);
    $string = preg_replace("/[óòôõö]/", "o", $string);
    $string = preg_replace("/[ÓÒÔÕÖ]/", "O", $string);
    $string = preg_replace("/[úùü]/", "u", $string);
    $string = preg_replace("/[ÚÙÜ]/", "U", $string);
    $string = preg_replace("/[ç]/", "c", $string);
    $string = preg_replace("/[Ç]/", "C", $string);
    $string = preg_replace("/[ñ]/", "n", $string);
    $string = preg_replace("/[Ñ]/", "N", $string);*/
    //$string = preg_replace("/[][><}{)(:;,!?*%~^`@]/+", "", $string);
    //$string = preg_replace("/ /", "_", $string);
    //$string = str_replace("/", "", $string);
    
    //$string = 'ÁÍÓÚÉÄÏÖÜËÀÌÒÙÈÃÕÂÎÔÛÊáíóúéäïöüëàìòùèãõâîôûêÇç';
    $string = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
    
    return $string;
}

function url_codificar($str)
{
    $str = str_replace(" / ", "_", $str);
    $str = str_replace("/", "__", $str);    
    $str = str_replace("(", "|_", $str);
    $str = str_replace(")", "_|", $str);    
    $str = remover_acentos($str);    
    $str = urlencode($str);
    //$str = str_replace("+", "--", $str);    
    return strtolower($str);
}

function url_decodificar($str)
{
    $str = str_replace("__", "/", $str);
    $str = str_replace("_", " / ", $str);        
    $str = str_replace("|_", "(", $str);
    $str = str_replace("_|", ")", $str);
    //$str = str_replace("--", "+", $str);
    $str = urldecode($str);
    return $str;
}

function oracle_translate($cmp)
{
    return "TRANSLATE($cmp, 'ÁÇÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕËÜáçéíóúàèìòùâêîôûãõëü', 'ACEIOUAEIOUAEIOUAOEUaceiouaeiouaeiouaoeu')";
}

function meta($index )//Inserir nas tags metas do header
{
    $metas = array();
    $metas["home"] = array("title" => "Venda e distribuição de equipamentos para impressão em todo Brasil "
                                ,"description" => "Líder em Outsourcing, Equipamentos e Soluções para Impressão ✓ Mais de 30 anos de tradição ✓ Líder de Mercado ✓ Solicite Orçamento sem Custo",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["atas"] = array("title" => "Atas e Registros de Preços - Licitações"
                                ,"description" => "Disponibilizamos as Stas de Resgistros de peço",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["assistencia"] = array("title" => "Assistência Técnica para Outsourcing e Revendas"
                                ,"description" => "Veja como funciona Assistência Técnica para Outsourcing e Revendas ✓ Portal Service Chamados Técnicos Suporte",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["eventos"] = array("title" => "Cronograma de Eventos"
                                ,"description" => "Conheça todos os eventos que a ReisOffice promove,suas datas e descrições ✓ Eventos ✓ Parceiros ✓ Funcionários",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["patrimonio"] = array("title" => "Identificação de Patrimônio"//atas das marcas 
                                 ,"description" => "Identificação de Patrimônio Identifique seus ativos de forma simples e rápida Software de edição",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["noticia"] = array("title" => "Listagem de Notícias"
                                ,"description" => "Conheça todas as notícias relacionandas aos produtos e serviços da ReisOffice Notícias",
                                "keywords" => " Revenda, distribuidora oficial Canon, Brother, Kyocera, Oki, Olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing.");
    $metas["etica"] = array("title" => "Conheça os princípios do nosso Código de Ética"
                                ,"description" => 'O sucesso da Reis Office está pautado em seu profissionalismo, determinação e esforço em alcançar suas metas. Para que tudo isso seja possível, foi desenvolvido o "Código de Ética Reis Office", que pauta todas as ações dos funcionários e fornecedores, além de ressaltar o comprometimento da empresa com a honestidade, respeito aos demais e à lei.',
                                "keywords" => "Revenda, distribuidora oficial canon, brother, Kyocera, oki, olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing / código de ética ");
    $metas["premios"] = array("title" => "Prêmios"
                                ,"description" => "Reis Office dedica-se a oferecer as melhores soluções para o cliente, através de uma equipe qualificada e produtos de alta qualidade e tecnologia. Busca melhorar suas práticas e responsabilidade social corporativa em uma empresa ecologicamente correta",
                                "keywords" => "uso consciente, responsabilidade ambiental, edifício sustentável, meio ambiente, reutilização de água, prêmios recebidos, Revenda, distribuidora oficial canon, brother, Kyocera, oki, olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing");
    $metas["showroom"] = array("title" => "Instalações e Show-Room"
                                ,"description" => ": Visite nosso showroom. Dispomos de instalações modernas e com grande diversidade de equipamentos das marca Brother, Canon, Kyocera, oki e olivetti. Conheça também nosso acervo de máquinas de escrever.",
                                "keywords" => "Revenda, distribuidora oficial canon, brother, Kyocera, oki, olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing / showroom/ instalações");
    $metas["social"] = array("title" => "Sustentabilidade e Responsabilidade Sócio Ambiental"
                               ,"description" => "Reis Office dedica-se a oferecer as melhores soluções para o cliente, através de uma equipe qualificada e produtos de alta qualidade e tecnologia. Busca melhorar suas práticas e responsabilidade social corporativa em uma empresa ecologicamente correta",
                                "keywords" => "uso consciente, responsabilidade ambiental, edifício sustentável, meio ambiente, reutilização de água, prêmios recebidos, Revenda, distribuidora oficial canon, brother, Kyocera, oki, olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing");
    $metas["ambiente"] = array("title" => "Meio Ambiente"
                               ,"description" => "Reis Office dedica-se a oferecer as melhores soluções para o cliente, através de uma equipe qualificada e produtos de alta qualidade e tecnologia. Busca melhorar suas práticas e responsabilidade social corporativa em uma empresa ecologicamente correta",
                               "keywords" => "uso consciente, responsabilidade ambiental, edifício sustentável, meio ambiente, reutilização de água, prêmios recebidos, Revenda, distribuidora oficial canon, brother, Kyocera, oki, olivetti, líder de mercado, equipamentos, suprimentos, soluções para impressão, locação, outsourcing");
    $metas["qualidade"] = array("title" =>"Política de Qualidade e Compromissos Reis Office",
                                "description" => "A Reis Office tem um compromisso com a qualidade em todas as etapas da prestação de serviço aos seus clientes. Por isso, estabeleceu uma política de qualidade que define as diretrizes para monitoramento e desenvolvimento do negócio.",
                                "keywords" => "Politica de qualidade/ compromissos Reis office/ diretrizes/ monitoramento");
    $metas["outsourcing/index"] = array ("title" => "Locação, outsourcing de impressão e gerenciamento de documentos"
                                ,"description" => "Soluções de impressão, outsourcing e locação de equipamentos para toda São Paulo. Contamos com softwares customizados para necessidade da empresa, sistema de GED – gerenciamento eletrônico de documentos, impressão na nuvem entre outros. Veja as soluções e serviços de impressão que ajuda a otimizar o tempo e reduzir os custos da empresa.",
                                "keywords"=> "soluções de impressão, locação, locar, outsourcing de impressão, softwares, gerenciamento remoto, aluguel de impressora, GED, gerenciamento de documentos, impressão na nuvem, serviço de impressão, prestação de serviço. 
                                Obs. E se necessário colocar os nomes de todas as soluções que temos.
                                ");
    $metas["governo/index"] = array("title" => "Venda e distribuição de equipamentos para impressão em todo Brasil "
                                ,"description" => "A Reis Office conta com uma equipe especializada e exclusiva para o atendimento de órgãos e empresas públicas. Confira nossas soluções e serviços que se encaixam em sua necessidade. Além  de equipamentos e suprimentos.",
                                 "keywords" => "Governo/ Licitações/ Soluções / serviços / outsourcing / locação / locar/ terceirização/"
    );
    
    
    if(isset($metas[$index]))
        return (object)$metas[$index];
    else
        return (object) $metas["home"];
}

function getMarcasSeo() //Inserir na tag de produtos referente a sua marca
{
        $marcas = array (
            "BROTHER" => "Seja uma revenda Brother através da Reis Office, temos 10 anos consecutivos com a premiação de maior Distribuidor oficial Brother. Dispomos de produtos como impressora a laser P/B "
                        . "e Colorida, multifuncional laser P/B e colorida, impressora a jato de tinta, multifuncional jato de tinta Brother, Rotuladores e etiquetadoras, Scanner Brother, impressoras térmicas Brother e máquina de corte."
                        . " Suprimentos como: toner Brother, cartucho de tinta Brother, fitas de etiquetas, fusores, roletes e peças Brother.", 
            "KYOCERA" =>"Seja uma revenda Kyocera através da Reis Office, temos 4 anos consecutivos com a premiação de maior Distribuidor oficial Kyocera. Dispomos de produtos como impressora a laser P/B e Colorida,"
                        . " multifuncional laser P/B e colorida. Suprimentos como: toner Kyocera, roletes, kit de manutenção e peças kyocera",
            "OKI" => "Seja uma revenda OKI através da Reis Office, somos Distribuidor oficial OKI. Dispomos de produtos como impressora a laser led P/B e Colorida, multifuncional laser led P/B "
                    . "e colorida, matriciais, multifuncionais e impressoras para área média e gráfica. Suprimentos como: toner OKI, kits de manutenção e peças OKI.",
            "CANON" => "Seja uma revenda Canon através da Reis Office, temos 10 anos com a premiação de maior Distribuidor oficial Canon. Dispomos de produtos como impressora e multifuncional laser P/B, impressora a jato de tinta, multifuncional jato de tinta Canon,"
                        . " plotter e impressoras de grande formato Canon, Scanner Canon e câmera Canon. Suprimentos como: toner Canon, cartucho de tinta"
                        . " Canon, roletes, Kits de manutenção e peças Canon.",
            "OLIVETTI" => "Seja uma revenda Olivetti através da Reis Office, temos mais de 30 anos como Distribuidor oficial Olivetti. Dispomos de modelos diversos de calculadoras com e sem bobinas.",
            "OCE" => "Seja uma revenda Canon através da Reis Office, temos 10 anos com a premiação de maior Distribuidor oficial Canon. Dispomos de produtos como impressora e multifuncional laser P/B, impressora a jato de tinta, multifuncional jato de tinta Canon,"
                        . " plotter e impressoras de grande formato Canon, Scanner Canon e câmera Canon. Suprimentos como: toner Canon, cartucho de tinta"
                        . " Canon, roletes, Kits de manutenção e peças Canon."
        );
        return $marcas;
}

/*Verificar se URL Existe*/
function urlExists($url) {

    $validar = get_headers($url);
    $validar = explode(" ",$validar[0]);
    $validar = $validar[1];
    
    if($validar == "302" || $validar == "200"):
        return true;
    else:
        return false;
    endif;    
}
