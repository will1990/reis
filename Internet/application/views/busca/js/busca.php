<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#rstBusca").DataTable({
            ordering: false,
            stateSave: true,
            paging: true,
            searching: true,
            sorting: false,
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
            "oLanguage": { "sUrl": "<?= base_url('assets/DataTables-1.10.13/ptBR_busca.txt')?>" }
        });
    });
</script>