<div class="container">
    <link href="<?= base_url('assets/custom/css/estiloBusca.css') ?>" rel="stylesheet" type="text/css"/>
    <br/>
    <div class="row">
        <div class="span4">
            <h4 class="dstqResul">ENCONTRADOS <?=$qtdeResAll?> RESULTADOS PARA:</h4>
        </div>
        <div class="span8">
            <h2 class="dstqTermo">"<?=$termo?>"</h2>
        </div>        
    </div>    
    <hr/>   
    <?php
    if(!empty($produtos)):
        ?>
        <div class="row">
            <!--<h4 class="span12 resulTopics"><font style="font-weight: normal;">(<?//=$qtdeResProd?>)</font>&nbsp;Produtos:</h4>-->
            <h4 class="span12 resulTopics">Produtos:</h4>
        </div>        
        <div class="row">
            <div class="span12">
                <table class="table" id="rstBusca" width="100%">
                    <thead><tr><td></td></tr></thead>
                    <tbody>            
                    <?php
                    foreach($produtos as $produto):
                        ?>
                        <tr>
                            <td>
                                <div class="media">
                                    <a class="pull-left" style="width: 160px;" href="<?= base_url('produtos////'.$produto->CPROD) ?>">
                                        <img class="media-object img-responsive text-center" style="max-width: 150px; max-height: 90px; left:50%; transform: translateX(-50%); position: relative;" src="<?= base_url('produtos/imagens/'.$produto->CPROD.'/image.jpg') ?>">                
                                    </a>
                                    <div class="media-body">
                                        <a href="<?= base_url('produtos////'.$produto->CPROD) ?>">
                                            <h4 class="media-heading"><?=$produto->PORTAL_DESCRICAO?></h4>

                                            <!-- Nested media object -->
                                            <div class="media">
                                                <?=$produto->DESCRICAO?>
                                            </div>
                                        </a>
                                        <br/>
                                    </div>
                                </div>
                                <br/>
                            </td>
                        </tr>
                        <?php                        
                    endforeach;     
                    //print_R($resultados);
                    ?>                    
                    </tbody>
                </table>
                <br/>
            </div>
        </div>        
        <?php
    endif;
    /*if(!empty($noticias)):
        ?>
        <div class="row">
            <h4 class="span12 resulTopics"><font style="font-weight: normal;">(0)</font>&nbsp;Noticias:</h4>
        </div>
        <div class="row">
            <div class="span12">
                <table class="table" id="rstBusca" width="100%">
                    <thead><tr><td></td></tr></thead>
                    <tbody>            
                    <?php
                    foreach($noticias as $noticia):
                        ?>
                        <tr>
                            <td>
                                <div class="media">
                                    <!--<a class="pull-left" style="width: 160px;" href="<?//= base_url('produtos/0/0/0/'.$produto->CPROD) ?>">
                                        <img class="media-object img-responsive text-center" style="max-width: 150px; max-height: 90px; left:50%; transform: translateX(-50%); position: relative;" src="<?//= base_url('produtos/imagens/'.$produto->CPROD.'/image.jpg') ?>">                
                                    </a>-->
                                    <div class="media-body">
                                        <a href="<?= base_url() ?>">
                                            <h4 class="media-heading"><?=$noticia->ds_chamada_noticia?></h4>

                                            <!-- Nested media object -->
                                            <div class="media">
                                                <?=$noticia->ds_integra_noticia?>
                                            </div>
                                        </a>
                                        <br/>
                                    </div>
                                </div>
                                <br/>
                            </td>
                        </tr>
                        <?php                        
                    endforeach;     
                    //print_R($resultados);
                    ?>                    
                    </tbody>
                </table>
                <br/>
            </div>          
        </div>
        <?php
    endif;*/
    ?>
</div>
