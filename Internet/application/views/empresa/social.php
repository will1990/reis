<div class="container">
    <h4 class="titEmp">RESPONSABILIDADE SOCIAL</h4>
    <?= $navbarEsq ?>
    
    <!-- Replicar conteúdo social na mesma estrutura do Meio Ambiente-->
    <!-- Ajustar melhor textos e imagens na view atual -->
    <div class="span8b">        
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Responsabilidade Social</li>
        </ul>
        <br/>
        
        <p style="text-align:justify">
            <img src="<?=base_url('images/visual/empresa/abrtf.jpg')?>" style="float:left; margin-right:30px; margin-bottom:30px; border: 1px solid #EEE; padding:4px; width: 200px;">
            A Reis Office, como importante empresa de tecnologia do país, tem consciência de seu papel na sociedade. Por esse motivo, 
            a Reis Office e seus fornecedores têm compromisso com o desenvolvimento da cultura, da educação e da sociedade, de um modo geral, 
            buscando sempre garantir a melhoria na qualidade de vida das pessoas.
            Os programas sociais da Reis Office têm o reconhecimento da The Rotary Foundation (ABRTF) que conferiu à Reis Office o selo de Empresa Cidadã por seu apoio a programas sociais.

            <br/><br/>
            Confira abaixo alguns dos programas sociais que tem impacto positivo na qualidade de vida de centenas de pessoas todos os dias:
        </p>       
        <div style="width:100%; height:1px; display:block; clear:left;"></div>
        <br/><br/>
        <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloAmbienteSocial.css")?>" />
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseObra">Obra Social Instituto Nossa Senhora das Dores</a>
                    </h4>
                </div>
                <div id="collapseObra" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/creche1.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/creche2.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/creche3.jpg')?>"/></div>
                        </div>
                        <br/>
                        <p class="conteudoSessao">
                            Buscando sempre fazer a diferença, apoiando ou desenvolvendo projetos socioambientais que visem uma melhoria na qualidade de vida das pessoas, 
                            a Reis Office patrocina de forma recorrente a creche da Obra Social Instituto Nossa Senhora das Dores, na comunidade carente de São Rafael, em Guarulhos (SP).
                            Confira abaixo alguns dos projetos realizados.
                        </p>
                        <br/>
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span12" style="border: 1px solid #CCC; border-radius: 4px;">  
                                    <div class="span3">                                       
                                        <img class="img-responsive" src="<?=base_url('images/visual/empresa/doacao.jpg')?>" alt="">
                                    </div>
                                    <div class="span9">
                                        <h4>Doação de alimentos, utensílios e itens de higiene pessoal</h4>
                                        <p style="text-align: justify; padding-right: 5px;">Com o objetivo de ajudar com as despesas mensais da Creche da Comunidade São Rafael, 
                                        a Reis Office criou uma importante campanha interna entre seus colaboradores: o projeto social “Colaborador Solidário, Empresa Solidária”.
                                        Os colaboradores que quiserem participar dessa ação podem contribuir com doações entre R$ 1,00 e R$ 5,00 por mês, descontado em folha de pagamento. 
                                        O valor total arrecadado entre os funcionários que autorizam a contribuição é dobrado pela Reis Office visando a compra de alimentos, utensílios de uso básico e produtos de higiene pessoal.
                                        Periodicamente são disponibilizadas aos colaboradores as notas fiscais que comprovam e informam a destinação dos recursos.
                                        </p>
                                    </div>                                    
                                </li>
                            </ul>
                            <ul class="thumbnails">
                                <li class="span12" style="border: 1px solid #CCC; border-radius: 4px;">
                                    <div class="span3">
                                        <img class="img-responsive" src="<?=base_url('images/visual/empresa/cestas.jpg')?>" alt="">
                                    </div>
                                    <div class="span9">
                                        <h4>Doação de cestas básicas</h4>
                                        <p style="text-align: justify; padding-right: 5px;">A Reis Office vende todos os papelões e papéis destinados à descarte para cooperativas de reciclagem. 
                                        O valor arrecadado é dobrado pela empresa e integralmente revertido para a compra de cestas básicas que são destinadas a famílias carentes e instituições de caridade.
                                        A Creche São Rafael é beneficiada por este projeto e repassa a doação às pessoas mais necessitadas.
                                        </p>
                                    </div>
                                </li>                                
                            </ul>
                            <ul class="thumbnails">
                                <li class="span12" style="border: 1px solid #CCC; border-radius: 4px;">
                                    <div class="span3">
                                        <img class="img-responsive" src="<?=base_url('images/visual/empresa/melhorias.jpg')?>" alt="">
                                    </div>
                                    <div class="span9">
                                        <h4>Melhorias no Edifício da Creche São Rafael</h4>
                                        <p style="text-align: justify; padding-right: 5px;">A manutenção do espaço, em que diariamente convivem centenas de crianças de 2 a 14 anos, 
                                        foi imprescindível para que continuassem existindo condições básicas de segurança no local de aprendizado dos jovens e para que os colaboradores possam trabalhar com tranquilidade em um ambiente seguro e adequado.
                                        Na ocasião, a Reis Office patrocinou a mão de obra para troca das telhas da creche, uma vez que o antigo telhado oferecia risco às crianças e aos colaboradores.
                                        </p>
                                    </div>
                                </li>                                
                            </ul>
                        </div>                        
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseParq">Miniparque Florestal Educativo</a>
                    </h4>
                </div>
                <div id="collapseParq" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque1.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque2.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque3.jpg')?>"/></div>
                        </div>
                        <br/>
                        <p class="conteudoSessao">
                            Buscando educar e conscientizar a juventude, preparando terreno para que no futuro a sociedade tenha incutido os valores da responsabilidade socioambiental, a Reis Office criou e mantém o projeto Miniparque Florestal Educativo, localizado em Senhora de Oliveira (MG).
                            A área de 15.000 m² foi transformada em local para desenvolvimento de atividades práticas de educação ambiental, recuperação de nascente de rios e preservação da fauna e flora locais.
                            O projeto conta com a participação de alunos e professores da Escola Municipal Martinho José Magalhães, localizada no bairro rural de Prudentes. Além da doação de uniformes, os alunos aprendem a preservar o meio ambiente cuidando e plantando árvores nativas, como: pau brasil, ipê roxo, aroeira, sete cascas, cedro, pinheiros e outras.
                        </p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapsePC">Doação de Microcomputadores</a>
                    </h4>
                </div>
                <div id="collapsePC" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span6"><img src="<?=base_url('images/visual/empresa/pcs1.jpg')?>"/></div>
                            <div class="span6"><img src="<?=base_url('images/visual/empresa/pcs2.jpg')?>"/></div>                            
                        </div>
                        <br/>
                        <p class="conteudoSessao">
                            Por acreditar que a educação é a base para construção de uma sociedade socialmente mais justa, a Reis Office apoia projetos que visam o desenvolvimento cultural e a inclusão social. 
                            Iniciativas como a doação de microcomputadores para a escola Martinho José Magalhães, no município de Senhora de Oliveira (MG) são exemplos de participação ativa para a melhoria da vida das pessoas.
                        </p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseAtv">Patrocínio de atividades culturais</a>
                    </h4>
                </div>
                <div id="collapseAtv" class="panel-collapse collapse">
                    <div class="panel-body">                        
                        <p class="conteudoSessao">
                            A Reis Office acredita que o acesso à cultura proporciona o aprendizado de valores importantes para a consciência social. 
                            O patrocínio de atividades culturais, como a Peça Teatral Rapunzel, organizada pelos alunos e profissionais da escola Martinho José Magalhães, 
                            no município de Senhora de Oliveira (MG) é exemplo de iniciativa que deve ser multiplicada e apoiada por empresas de todo o país.
                        </p>
                    </div>                    
                </div>
            </div>
        </div>
        <br/><br/>
    </div>
</div>


