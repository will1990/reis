<div class="container">   
    <h4 class="titEmp">CONHEÇA OS PRINCÍPIOS DO NOSSO CÓDIGO DE ÉTICA</h4>
    <?= $navbarEsq ?>

    <div class="span8b">
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Código de Ética</li>
        </ul>
        <br/>

        <p style="text-align:justify">
            O sucesso da Reis Office está pautado em seu profissionalismo, determinação e esforço em alcançar suas metas. Para que tudo isso seja possível, foi desenvolvido o "Código de Ética Reis Office", que pauta todas as ações dos funcionários e fornecedores, além de ressaltar o comprometimento da empresa com a honestidade, respeito aos demais e à lei. 
            <br/>
            <br/>
            Os princípios que norteiam o código de ética são: 
        </p>
        <div style="width:100%; height:20px; display:block; clear:left;"></div>
        <div class="bs-example" data-example-id="list-group-custom-content" style="border:0px; margin:0px;">
            <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloTopicsReis.css")?>" />
            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Respeitar todos os relacionamentos;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Aceitar e tratar todos os colaboradores como iguais;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Conduzir todos os negócios com honestidade e integridade;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Enfrentar os desafios com humildade e coragem;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Garantir máxima qualidade nos produtos e serviços;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Divulgar as informações com clareza e precisão, mantendo a confidencialidade, quando necessário;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Respeitar e cumprir as leis vigentes no país;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Respeitar o meio ambiente e cumprir as regulamentações sanitárias</li>
            </ul>
        </div>

        <div style="width:100%; height:1px; display:block; clear:left;"></div>
    </div>        
</div>
<br/><br/>