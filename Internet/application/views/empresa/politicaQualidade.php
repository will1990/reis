<div class="container">   
    <h4 class="titEmp">POLÍTICA DE QUALIDADE E COMPROMISSOS REIS OFFICE</h4>
    <?= $navbarEsq ?>

    <div class="span8b">
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Política de Qualidade</li>
        </ul>
        <br/>

        <p style="text-align:justify">
            A Reis Office tem um compromisso com a qualidade em todas as etapas da prestação de serviço aos seus clientes. Por isso, estabeleceu uma política de qualidade que define as diretrizes para monitoramento e desenvolvimento do negócio: 
        </p>
        <div style="width:100%; height:20px; display:block; clear:left;"></div>
        <div class="bs-example" data-example-id="list-group-custom-content" style="border:0px; margin:0px;">
            <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloTopicsReis.css")?>" />
            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Garantir a satisfação dos clientes, oferecendo soluções completas em impressão, cópia, transmissão, digitalização e gerenciamento de documentos.</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Melhorar continuamente a eficácia do sistema de gestão, visando melhorar cada vez mais a qualidade dos produtos e serviços.</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Valorizar a parceria com os clientes e fornecedores.</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Manter a transparência e ética profissional em todos os processos.</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Valorizar o patrimônio humano com treinamentos, desenvolvimento e respeito.</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i> Estabelecer práticas sustentáveis e uso consciente dos recursos, respeitando o meio ambiente.</li>
            </ul>
        </div>

        <div style="width:100%; height:1px; display:block; clear:left;"></div>
    </div>
</div>
<br/><br/>
