<div class="container">
    <h4 class="titEmp">MEIO AMBIENTE</h4>
    <?= $navbarEsq ?>
    
    <div class="span8b">        
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Meio Ambiente</li>
        </ul>
        <br/>
        
        <p style="text-align:justify">
            <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" style="float:left; margin-right:30px; margin-bottom:30px; border: 1px solid #EEE; padding:4px; width: 200px;"> 
            A Reis Office, como importante empresa de tecnologia do país, tem consciência de seu papel na sociedade. Por esse motivo, a Reis Office e seus fornecedores têm um compromisso com a cultura, educação e uso consciente dos recursos, garantindo uma melhora na qualidade de vida. 
            <br/><br/>
            A preservação ambiental foi institucionalizada como parte da missão, e incorporada às atividades diárias, seguindo os três princípios básicos de proteção ambiental, definidos pelo Pacto Mundial das Nações Unidas:        
        </p>        
        <div style="width:100%; height:1px; display:block; clear:left;"></div>
        <span class="span4c" style="width:280px;"></span>
        <div class="bs-example" data-example-id="list-group-custom-content" style="border:0px; margin:0px;">
            <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloTopicsReis.css")?>" />
            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i>
                    Apoiar uma abordagem preventiva aos desafios ambientais; 
                </li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i>
                    Promover a responsabilidade ambiental;
                </li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i>
                    Encorajar tecnologias que não agridem o Meio Ambiente.
                </li>
            </ul>
        </div>
        <div style="width:100%; height:1px; display:block; clear:left;"></div>
        <br/><br/>
        <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloAmbienteSocial.css")?>" />
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseEdif">Edifício Sustentável</a>
                    </h4>
                </div>
                <div id="collapseEdif" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p class="conteudoSessao">
                            Orientada por sua missão, a Reis Office preza pelo respeito ao meio ambiente e responsabilidade social corporativa, 
                            fator determinante para a construção de sua sede de maneira sustentável.
                            O prédio, que foi inaugurado em 2012, foi cuidadosamente projetado para otimizar recursos e minimizar os impactos, desde sua construção às operações após inaugurado.
                            Dentre as diversas soluções que garantem a sustentabilidade do edifício, destacam-se:
                        </p>
                        <br/>
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span12" style="border: 1px solid #CCC; border-radius: 4px;">  
                                    <div class="span4">
                                        <img class="img-responsive visible-desktop" style="float:left; margin-top: 25px;" src="<?=base_url('images/visual/empresa/energiaSolar1.png')?>" alt="">
                                        <img class="img-responsive visible-phone visible-tablet" style="float:left; margin-bottom: 10px;" src="<?=base_url('images/visual/empresa/energiaSolar1.png')?>" alt="">
                                    </div>
                                    <div class="span8">
                                        <h4>Abastecimento por Energia Solar Fotovoltaica</h4>
                                        <p style="text-align: justify; padding-right: 5px;">A Reis Office abastece parte do seu consumo de energia elétrica com um sistema próprio de energia solar fotovoltaica, responsável por gerar 2.550KWh/mês.
Segundo a ANEEL esta iniciativa foi pioneira em mais de 25 cidades do estado de São Paulo, sendo a Reis Office a primeira empresa a implantar um sistema com tal capacidade. Para efeito de comparação, o consumo médio das residências brasileiras é de 157KWh/mês, ou seja, a energia produzida pela Reis Office é suficiente para abastecer 16 casas populares.
As 78 placas fotovoltaicas de 325w instaladas no telhado contribuem para abastecer cerca de 20% do consumo da empresa e permitem que a Reis Office deixe de emitir anualmente 4 toneladas de CO², contribuindo assim para minimizar os efeitos do aquecimento global.
Ao iniciar tal projeto, a Reis Office não toma apenas para si o papel de minimizar os impactos ambientais, mas estimula os outros para que tomem parte nesta iniciativa e para que ajudem a espalhar esta mensagem consciente.
                                        </p>
                                    </div>                                    
                                </li>
                            </ul>
                        </div>                        
                        <div class="row-fluid">                            
                            <ul class="thumbnails">
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Aquecimento de água por meio da energia solar</h4>
                                        <p style="text-align: justify;">O prédio conta com um sistema para aproveitamento da energia solar no aquecimento da água que é utilizada na copa, cozinha e vestiários.</p>
                                    </div>
                                </li>                            
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Telhado Ecológico</h4>
                                        <p style="text-align: justify;">Telhas brancas com isolante térmico fazem a cobertura do edifício, refletem os raios solares e diminuem a temperatura interna gerando economia de energia com o sistema de refrigeração.</p>
                                    </div>
                                </li>
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Captação e reúso de água</h4>
                                        <p style="text-align: justify;">Existe um sistema completo para captação, filtragem e redistribuição de água da chuva e de cada ar-condicionado, permitindo o reuso nos sanitários, na irrigação de plantas, na limpeza e na lavagem do edifício.</p>
                                    </div>
                                </li>
                            </ul>
                            <ul class="thumbnails">
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Ventilação e luz natural</h4>
                                        <p style="text-align: justify;">Grandes janelas em toda as faces do edifício proporcionam total aproveitamento dos recursos naturais e diminuem significativamente o uso de energia com refrigeração e iluminação dos ambientes durante todo o ano.</p>
                                    </div>
                                </li>
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Válvulas de descarga com duplo acionamento</h4>
                                        <p style="text-align: justify;">O sistema Dual Flux gera um potencial de até 60% de economia de água e, consequentemente, menor emissão e necessidade de tratamento de esgoto: o acionamento do botão pequeno libera meia descarga para dejetos líquidos. O botão grande libera descarga completa, para dejetos sólidos.</p>
                                    </div>
                                </li>
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Torneiras com fechamento automático</h4>
                                        <p style="text-align: justify;">As torneiras são equipadas com arejadores e sistema de fechamento automático, que proporciona conforto para os usuários, evita desperdício e gera economia de até 70% no consumo de água.</p>
                                    </div>
                                </li>
                            </ul>
                            <ul class="thumbnails">
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Tratamento de esgoto e efluentes</h4>
                                        <p style="text-align: justify;">Todo o esgoto gerado na edificação é conduzido através de tubulação própria para uma estação interna de tratamento biológico que o transforma em água limpa, adequada para o reuso na limpeza e lavagem do edifício.</p>
                                    </div>
                                </li>
                                <li class="span4">
                                    <div class="thumbnail">
                                        <img src="<?=base_url('images/visual/empresa/introAmbiente.jpg')?>" alt="">
                                        <h4>Construção sustentável</h4>
                                        <p style="text-align: justify;">Na construção foram utilizados materiais que respeitam o meio ambiente, tais como revestimentos cerâmicos de cores claras, drywall, lâmpadas econômicas, dentre outros, que proporcionaram conforto, economia e menor impacto ambiental.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <li class="span12" style="border: 1px solid #CCC; border-radius: 4px;">  
                                    <div class="span4">
                                        <img class="img-responsive visible-desktop" style="float:left; margin-top: 30px;" src="<?=base_url('images/visual/empresa/coleta1.jpg')?>" alt="">
                                        <img class="img-responsive visible-phone visible-tablet" style="float:left; margin-bottom: 10px;" src="<?=base_url('images/visual/empresa/coleta1.jpg')?>" alt="">
                                    </div>
                                    <div class="span8">
                                        <h4>Coleta Seletiva</h4>
                                        <p style="text-align: justify; padding-right: 5px;">A Reis Office estimula e apoia seus colaboradores a praticarem a coleta seletiva de lixo, pois cuidar do meio ambiente é responsabilidade de todos. Buscando envolver os colaboradores neste projeto, foi elaborada uma cartilha que explica em detalhes a finalidade da coleta seletiva e sua importância. 
Com a implantação deste projeto, anualmente evitamos o corte de 18 árvores adultas, ajudando assim a reduzir a presença de CO² na atmosfera, já que cada árvore no seu ciclo de vida consome cerca de 160kg de CO². Evita-se também o despejo de 3,5 toneladas de lixo nos aterros, uma vez que anualmente o volume de papel e papelão descartado na empresa é de 2,5t e o volume de outros resíduos é de 1t.
Papéis e papelões são vendidos a cooperativas de reciclagem e o valor arrecadado é dobrado pela empresa e integralmente revertido para a compra de cestas básicas que são destinadas a famílias carentes e instituições de caridade.
Já os resíduos orgânicos são encaminhados à um processo de compostagem para que possam virar adubo de boa qualidade, que é utilizado em plantações e também distribuído aos colaboradores da empresa. 

                                        </p>
                                    </div>                                    
                                </li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseParq">Miniparque Florestal Educativo</a>
                    </h4>
                </div>
                <div id="collapseParq" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque1.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque2.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/miniParque3.jpg')?>"/></div>
                        </div>
                        <br/>
                        <p class="conteudoSessao">
                            Buscando educar e conscientizar a juventude, preparando terreno para que no futuro a sociedade tenha incutido os valores da responsabilidade socioambiental, a Reis Office criou e mantém o projeto Miniparque Florestal Educativo, localizado em Senhora de Oliveira (MG).
                            A área de 15.000 m² foi transformada em local para desenvolvimento de atividades práticas de educação ambiental, recuperação de nascente de rios e preservação da fauna e flora locais.
                            O projeto conta com a participação de alunos e professores da Escola Municipal Martinho José Magalhães, localizada no bairro rural de Prudentes. 
                            Além da doação de uniformes, os alunos aprendem a preservar o meio ambiente cuidando e plantando árvores nativas, como: pau brasil, ipê roxo, aroeira, sete cascas, cedro, pinheiros e outras.
                        </p>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseRes">Reserva Florestal Piranga</a>
                    </h4>
                </div>
                <div id="collapseRes" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/reserva1.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/reserva2.jpg')?>"/></div>
                            <div class="span4"><img src="<?=base_url('images/visual/empresa/reserva3.jpg')?>"/></div>
                        </div>
                        <br/>
                        <p class="conteudoSessao">
                            No ano de 2005, a Reis Office assumiu o compromisso de preservação ambiental de uma área de mata nativa, situada no município de Piranga, 
                            localizado no interior do Estado de Minas Gerais. A reserva florestal tem 40 hectares, e conta com grande biodiversidade, 
                            representando um refúgio seguro para inúmeras espécies de animais e vegetais.
                        </p>
                    </div>                    
                </div>
            </div>
        </div>
        <br/><br/>
    </div>
</div>