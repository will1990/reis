<div class="container"> 
    <h4 class="titEmp">QUEM SOMOS - TRAJETÓRIA, CERTIFICAÇÕES E PREMIAÇÕES</h4>
    <?= $navbarEsq ?> 

    <div class="span8b">        
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Quem Somos</li>
        </ul>
        <br/>

        <p style="text-align:justify">
            <img src="<?=base_url('images/visual/empresa/fachada.jpg')?>" style="float:left; margin-right:30px; margin-bottom:30px; border: 1px solid #EEE; padding:4px;"> 
            A Reis Office foi criada em 1984, com o objetivo de atender a uma demanda crescente por equipamentos de escritório, comercializando máquinas de escrever, calculadoras e tele impressoras. Com serviços diferenciados de vendas e assistência técnica, a empresa rapidamente consolidou sua marca e tornou-se sinônimo de confiança e qualidade. 

            Atenta às evoluções do mercado, a Reis Office passou a comercializar equipamentos mais modernos, com tecnologia de ponta, das melhores marcas disponíveis no mercado. Dessa forma, tornou-se uma empresa líder em soluções completas para impressão, digitalização, transmissão e armazenamento de documentos, com a melhor relação custo/benefício. 

            Desde então, a Reis Office dedica-se a oferecer as melhores soluções para o cliente, através de uma equipe qualificada e produtos de alta qualidade e tecnologia. </p>
        <div style="width:100%; height:1px; display:block; clear:left;"></div>
        <span class="span4c" style="width:280px;">           	</span>
        <div class="bs-example" data-example-id="list-group-custom-content" style="border:0px; margin:0px;">
            <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloTopicsReis.css")?>" />
            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i><strong>Certificações</strong><br>
                    Em 2005, a Reis Office obteve a certificação ISO 9001:00, concedida pela Fundação Carlos Alberto Vanzolini,certificado, SQ-M01843-4715-05. Essa certificação assegura um monitoramento e melhora constante. Atualmente, possuímos a ISO 9001:08 que é utilizada por mais de 750 mil organizações, em 161 países, e define o padrão para gestão de qualidade e sistemas em geral. </li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i><strong>Missão</strong><br>
                    Satisfazer as necessidades de nossos clientes, oferecendo soluções completas que desenvolvam continuamente o seu alto grau de satisfação e respeitando o meio ambiente com responsabilidade social corporativa;</li>
                <li class="list-group-item"><i class="icon-caret-right" style="color:#e47b38"></i><strong>Visão</strong><br>
                    Ser uma empresa atuante, que opere com foco no cliente e com o máximo de qualidade, eficiência e confiabilidade, mantendo sempre a ética e a constância de propósitos.</li>
            </ul>
        </div>
        <br>

        <div style="width:100%; height:1px; display:block; clear:left;"></div>

        <h2> LINHA DO TEMPO</h2>
        
        <iframe src='//cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1LgVjzifNNtkiasb6hgxoGep6zT01de3LIHQ4VNRQzHY&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' frameborder='0'></iframe>
    </div>
</div>
<br/><br/>