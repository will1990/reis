<aside class="span4b" style="margin-left:0px;"> 
    <div class="wrap" style="padding:10px 5px 5px 0px;">
        <div class="bs-example" data-example-id="vertical-button-group" style="text-transform:none;">
            <div class="btn-group-vertical" role="group" aria-label="Vertical button group" style="text-transform:none;">
                
                <div  class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                    <a id="empresa" href="<?=base_url("empresa")?>" class="btn btn-default2" style="font-weight: 400; width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Quem Somos </a>
                </div>

                <div class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                    <a id="codigo-de-etica" href="<?=base_url("empresa/etica")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Código de Ética  </a>
                </div>

                <div class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                    <a id="politica-de-qualidade" href="<?=base_url("empresa/qualidade")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Política de Qualidade  </a>
                </div>
                
                <div class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                    <a id="premios" href="<?=base_url("empresa/premios")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Prêmios  </a>
                </div>
                
                <div class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                    <a id="show-room" href="<?=base_url("empresa/showroom")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Showroom</a>
                </div>

                <div class="btn-group" role="group" style="border-bottom: 1px solid #DDD">
                    <a id="ambiente" href="<?=base_url("empresa/ambiente")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Meio Ambiente  </a>
                </div>
                
                <div class="btn-group" role="group">
                    <a id="social" href="<?=base_url("empresa/social")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> Responsabilidade Social  </a>
                </div>
                
            </div>
        </div>
    </div>
</aside>
