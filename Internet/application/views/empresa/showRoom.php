<div class="container">   
    <h4 class="titEmp">INSTALAÇÕES E SHOWROOM</h4>
    <?= $navbarEsq ?>

    <div class="span8b">
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
            <li data-original-title=""><a href="<?= base_url('empresa') ?>">Empresa</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Showroom</li>
        </ul>
        <br/>

        <p style="text-align:justify">
            Entre as instalações modernas da nova sede da Reis Office, destaca-se o Showroom, que conta com diversos equipamentos modernos das marcas Brother, Canon, HP, Kyocera e Oki. Agendando uma visita, nossos clientes têm a oportunidade de conhecer funções e características das máquinas na prática, sendo auxiliados por nossos vendedores e técnicos. 
            <br/>
            <br/>
            Partindo para um ponto de vista cultural, também é possível observar e conhecer algumas máquinas de escrever, equipamento primordial para o início do segmento de impressão e tecnologia. 
            <br/>
            <br/>
        </p>
        
        <h4 class="modal-title" id="myModalLabel">TOUR 360º</h4>
        <br>
        <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1spt-BR!2sbr!4v1443619551886!6m8!1m7!1siJ0CrwUwdngAAAQuoQG8Kw!2m2!1d-23.47500057949789!2d-46.54608494438975!3f258.62!4f-10.379999999999995!5f0.7820865974627469" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div style="width:100%; height:1px; display:block; clear:left;"></div>
    </div>
</div><!--/container-->
<br/><br/>