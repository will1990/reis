<html>
    <head>
        <title>Follow Me</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
        
            body{
                background-color: #537f95;
            }
            
            .backForm{
                background-color: #FFFFFF;
                width: 800px;
            }        
        
        </style>
        
    </head>
    <body bgcolor="#FFFFFF">
        <center>
            <table id="Tabela_01" width="800" height="1960" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_01.jpg')?>" width="400" height="359" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_02.jpg')?>" width="400" height="359" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_03.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_04.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_05.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_06.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_07.jpg')?>" width="400" height="266" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_08.jpg')?>" width="400" height="266" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_09.jpg')?>" width="400" height="269" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_10.jpg')?>" width="400" height="269" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_11.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_12.jpg')?>" width="400" height="267" border="0" style="display:block" alt="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_13.jpg')?>" width="400" height="265" border="0" style="display:block" alt="">
                    </td>
                    <td>
                        <img src="<?=base_url('images/landing/Follow-Me/lps_14.jpg')?>" width="400" height="265" border="0" style="display:block" alt="">
                    </td>
                </tr>
            </table>
        </center>
        
        <div class="container backForm" style="margin-top: -25px;">
            <?=$form?>
        </div>
    
    </body>
    
    <script type="text/javascript">
        var _gaq = _gaq || [];
        var pluginUrl =  '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
        _gaq.push(['_setAccount', 'UA-1326185-3']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</html>