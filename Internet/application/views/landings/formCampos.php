<form method="POST" class="form-horizontal" action="<?=base_url("contato/salvar")?>" id="formCont">
    <script type="text/javascript">
        /* Máscaras ER */
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        function mtel(v){
            v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
            v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
            return v;
        }
        function id( el ){
                return document.getElementById( el );
        }
        window.onload = function(){
                id('txtTelForm').onkeyup = function(){
                        mascara( this, mtel );
                }
        }       
    </script>
    <div class="form-group">
        <div class="col-md-offset-1 col-md-10">
            <label class="control-label txtForm" for="comment">* Nome:</label>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <input class="form-control input-sm" required="" type="text" name="txtNomeForm"/>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <label class="control-label txtForm" for="comment">* Email:</label>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <input class="form-control input-sm" required="" type="email" name="txtEmailForm"/>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <label class="control-label txtForm" for="comment"> Empresa / Orgão:</label>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <input class="form-control input-sm" type="text" name="txtEmpreForm"/>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <label class="control-label txtForm" for="comment">* Telefone:</label>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <input class="form-control input-sm" required="" type="text" id="txtTelForm" name="txtTelForm" maxlength="15"/>            
        </div>
        <div class="col-md-offset-1 col-md-10">
            <label class="control-label txtForm" for="comment">Observações:</label>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <textarea class="form-control" name="txtObsForm" rows="3" id="comment"></textarea>
        </div>
    </div>    
    <input type="hidden" name="txtNomeLanding" value="<?=$nome?>"/>
    <input type="hidden" name="txtMethodLanding" value="page"/>
    <div class="form-group text-right">
        <div class="col-md-offset-1 col-md-10">
            <input class="btn <?=$botao_classe?>" type="submit" value="<?=$botao_texto?>"/>
        </div>
    </div>
    <input type="hidden" name="controller" value="landings"/>
</form>