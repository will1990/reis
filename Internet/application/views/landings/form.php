<form style="margin-top: <?= $form_top_margin?>;" method="POST" class="form-horizontal" action="<?=base_url("contato/salvar")?>" id="formCont">
    <script type="text/javascript">
        /* Máscaras ER */
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        function mtel(v){
            v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
            v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
            return v;
        }
        function id( el ){
                return document.getElementById( el );
        }
        window.onload = function(){
                id('txtTelForm').onkeyup = function(){
                        mascara( this, mtel );
                }
        }       
    </script>
    <div class="form-group">
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label" for="comment">* Nome:</label>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <input class="form-control" required="" type="text" name="txtNomeForm"/>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label" for="comment">* Email:</label>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <input class="form-control" required="" type="email" name="txtEmailForm"/>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label" for="comment"> Empresa / Orgão:</label>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <input class="form-control" type="text" name="txtEmpreForm"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-2 col-md-2 col-lg-2">
            <label class="control-label" for="comment">* Telefone:</label>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <input class="form-control" required="" type="text" id="txtTelForm" name="txtTelForm" maxlength="15"/>            
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <label class="control-label" for="comment">Observações:</label>
        </div>
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <textarea class="form-control" name="txtObsForm" rows="3" id="comment"></textarea>
        </div>
    </div>
    <br/><br/>
    <input type="hidden" name="txtNomeLanding" value="<?=$nome?>"/>    
    <div class="form-group text-right">
        <div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-lg-6">
            <input class="btn <?=$botao_classe?>" type="submit" value="<?=$botao_texto?>"/>
        </div>
    </div>
    <input type="hidden" name="controller" value="landings"/>
</form> 










