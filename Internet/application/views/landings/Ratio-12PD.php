<html>
    <head>
        <title>Ratio 12PD</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>        
            body{
                background-color: #fff;                
            }
            .ratio{
                background: url(<?=base_url('images/landing/Ratio-12PD/fundo-ratio.png')?>) no-repeat;
                background-size: 100% auto; 
                height: 100%;
                min-height: 1400px;
            }           
            
            .botao{
                margin-bottom: 7px;
            }
            
            @media screen and (min-width: 992px) {
            
                .formulario{
                    background: url(<?=base_url('images/landing/Ratio-12PD/fundo-form.png')?>) no-repeat;
                    background-size: 104% 104%;
                    width: 275px;                    
                    margin-top: -9px;
                    padding-top: 78px;           
                }
                
                .txtForm{
                    color: #fff;
                    font-weight: normal;
                    font-size: 9pt;
                }
            }
            
            @media screen and (min-width: 1200px) {
            
                .formulario{
                    background: url(<?=base_url('images/landing/Ratio-12PD/fundo-form.png')?>) no-repeat;
                    background-size: 100% 103%;
                    width: 337px;
                    margin-top: -12px;
                    padding-top: 72px;                    
                }

                .txtForm{
                    color: #fff;
                    font-weight: normal;
                    font-size: 11pt;
                }
            }
        </style>
    </head>
    <body>
        <div class="container-fluid ratio">
            <div class="col-md-offset-8 col-md-3">
                <div class="col-md-offset-1 col-md-11">
                    <div class="formulario">
                        <?=$formCampos?>
                    </div>                   
                </div>
            </div>            
        </div>
    </body>
</html>