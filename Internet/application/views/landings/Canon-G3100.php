<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>MAXX Tinta - A qualidade  de impressão CANON, com sistema de alto rendimento.</title>
        <link rel="stylesheet" type="text/css" href="<?= base_url('images/landing/Canon-G3100/css/style.css')?>">
        <style>
            .backForm{
                background-color: #FFFFFF;
                width: 800px;
            }             
        </style>
    </head>
    <body>
        <div class="red-ribbon">
            <div class="container">
                <img src="<?= base_url('images/landing/Canon-G3100/images/logo-canon.png')?>" alt="">
            </div>
        </div>
        <!-- HEADER -->
        <div class="header">
            <div class="container">
                <span class="maxx-tinta"><strong>MAX</strong>X <strong>TINTA</strong> G3100</span>
                <div class="impressora-canon"></div>
            </div>
        </div>
        <!-- ZOOm -->
        <div class="zoom">
            <div class="container">
                <div class="zoom-overlay"></div>
                <div class="garrafa"></div>
                <div class="bico-overlay"></div>
                <span class="title">
                    <strong>Visor dos níveis de tinta:  </strong>
                    <br>
                    você sabe a hora de recarregar
                </span>
                <div class="nivel-01"></div>
                <div class="nivel-02"></div>
                <div class="nivel-03"></div>
                <div class="nivel-04"></div>
            </div>
        </div>
        <!-- RENDIMENTO -->
        <div class="rendimento">
            <div class="container">	
                <div class="timer7000">
                    <div class="png7000"></div>
                </div>

                <div class="timer6000">
                    <div class="png6000"></div>
                </div>

                <div class="over-papel"></div>
                <div class="rendimento-papel"></div>
                <!-- <div class="folha-01"></div> -->
                <div class="folha-02"></div>
                <div class="folha-03"></div>
                <div class="folha-04"></div>
            </div>
            <div class="rendimento-titulo">
                <div class="container">	
                    <strong>Maior volume de impressão</strong><br>e surpreendente custo por página
                </div>
            </div>
        </div>
        <div class="pigmento">

            <div class="container">	
                <h1 class="title">Sistema híbrido de impressão: pigmento e corante</h1>
            </div>
            <div class="container">

                <!-- COMPARISON SLIDER -->

                <figure class="cd-image-container">
                    <img src="<?= base_url('images/landing/Canon-G3100/images/img-original.jpg')?>" alt="Original Image">
                    <span class="cd-image-label" data-type="original">Preto Pigmento</span>

                    <div class="cd-resize-img"> <!-- the resizable image on top -->
                        <img src="<?= base_url('images/landing/Canon-G3100/images/img-modified.jpg')?>" alt="Modified Image">
                        <span class="cd-image-label" data-type="modified">Preto Corante</span>
                    </div>

                    <span class="cd-handle"></span> <!-- slider handle -->
                </figure> <!-- cd-image-container -->
                <img id="clique-e-arraste" src="<?= base_url('images/landing/Canon-G3100/images/clique-e-arraste.png')?>" class="clique" alt="">
            </div>
            <div class="pigmento-features">
                <h2>Tinta pigmento para mais definições em preto.</h2>
                <img src="<?= base_url('images/landing/Canon-G3100/images/pigmento-icons.png')?>" alt="">
            </div>
        </div>
        <div class="conectividade">
            <div class="container">	
                <div class="conectividade-title">
                    <h1>Imprima direto de</h1>
                    <h2>dispositivos móveis<span style="float: right; font-size: 12px;">3</span><br>ou computador</h2>
                </div>
                <div class="impressora-canon-2"></div>
                <div class="impressora-canon-2-2"></div>
                <div class="overlay"></div>
                <div class="conectividade-papel"></div>
                <div class="phone"></div>
                <div class="wifi"></div>
            </div>
        </div>
        <div class="tarja-magento">
            <div class="container">
                <div class="devices-magento"></div>
                <div class="devices-title">
                    <h1>Conexão direta com</h1>
                    <h2>dispositivos móveis<span style="float: right; font-size: 12px;">3</span></h2>
                </div>
            </div>
        </div>

        <!-- BAIXO CUSTO -->
        <div class="baixo-custo">
            <!-- MAGENTO -->
            <div class="tarja-laranja">
                <div class="container">
                    <div class="baixo-custo-title">
                        <h1>Agora você pode imprimir</h1>
                        <h2>quantas fotos quiser!</h2>
                    </div>
                    <div class="impressora-canon-3"></div>
                    <div class="over-papel-2">
                    </div>
                    <div class="baixo-desc">
                        <p>
                            Com a nova linha de impressoras Canon Maxx Tinta, ficou muito mais fácil e barato imprimir  fotos armazenadas  em seu celular, tablet, câmera digital ou computador,  com qualidade fotográfica.
                        </p>

                    </div>
                    <div class="baixo-desc2">
                        <div>
                            <span>
                                Impressão <br>
                                sem bordas
                                <span style="font-size: 11px; float: right">&nbsp; 2</span>

                            </span>

                            <div class="ico borda"></div>
                        </div>
                        <div>
                            <span><strong>USB de alta </strong><br>velocidade </span>
                            <div class="ico usb"></div>
                        </div>
                        <div>
                            <span>Modo<br>silencioso</span>
                            <div class="ico modo"></div>
                        </div>
                    </div>
                    <div class="timer2000">
                        <div class="png2000"></div>
                    </div>			
                </div>
            </div>
        </div>
    
        <div class="facil-carregar">
            <div class="raiox-overlay"></div>
            <div class="container">
                <span class="title">
                    <strong>Design moderno e compacto,</strong>
                    <br>
                    com sistema de tinta integrado
                </span>
            </div>
        </div>
        <div class="scanner-copiadora">
            <div class="container">
                <div class="title">
                    Imprima, copie e escaneie <br>
                    <strong>COM BAIXO CUSTO E ALTA QUALIDADE</strong>
                </div>
                <div class="feature-3"></div>
            </div>
            <div class="impressora-canon-4"></div>
            <div style="width: 800px;margin: 0 auto;height: 300px; position: relative;">
                <div class="scanner-flash">
                </div>
            </div>
        </div>        
        <div class="faixa-maxx-tinta">
            <img src="<?= base_url('images/landing/Canon-G3100/images/footer.png')?>" alt="">
        </div>
        
        <!-- SCRIPTS -->
        <script src="<?= base_url('images/landing/Canon-G3100/js/jquery-3.0.0.min.js')?>"></script>
        <script src="<?= base_url('images/landing/Canon-G3100/js/main.js')?>"></script>
        
    </body>
    <?=$iFrame?>
</html>
