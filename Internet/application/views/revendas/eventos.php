<div class="container">
    <div class="row"> 
        <div class="span8">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?=base_url("revendas")?>">Revendas</a> <span class="divider">/</span></li>
                <li class="active" data-original-title="">Eventos</li>
            </ul>
            <br/><br/>
            <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloEventosReis.css")?>"/>
            <ul class="event-list" id="eventos_reis" >
            <?php
            $meses = array("JAN","FEV","MAR","ABR","MAI","JUN","JUL","AGO","SET","OUT","NOV","DEZ");
            foreach ($eventos as $ordem):
                foreach ($ordem as $evento):                                    
                    list($dia, $mes, $ano) = explode("/", $evento->DATAS[0]->DATA_EVENTO);
                    $mes = $meses[$mes-1];
                    ?>
                    <li>
                        <time style="height:111px;"> 
                            <span class="day"><?=$dia?></span>
                            <span class="month"><?=$mes?></span>                                
                        </time>
                        <img src="<?=base_url("images/visual/revendas/$evento->FABRICANTE.jpg")?>" height="110" style="padding:0px;" />
                        <div class="info">
                            <h2 class="title"><?=$evento->TITULO?></h2>
                            <?php
                                $total = count($evento->DATAS);
                                if($total == 1):
                                    ?>
                                    <p class="desc" style="color:#7f7e7e"><?=$evento->DATAS[0]->DATA_EVENTO?> - Das <?=$evento->DATAS[0]->HORA_INICIO?> às <?=$evento->DATAS[0]->HORA_TERMINO?></p>
                                    <?php
                                else:                                    
                                    ?>
                                    <p class="desc" style="color:#7f7e7e"><?=$evento->DATAS[0]->DATA_EVENTO?> a <?=$evento->DATAS[$total-1]->DATA_EVENTO?> - Das <?=$evento->DATAS[$total-1]->HORA_INICIO?> às <?=$evento->DATAS[$total-1]->HORA_TERMINO?></p>
                                    <?php
                                endif;
                            ?>
                            <p class="desc" style="color:#7f7e7e">Local: <?=$evento->LOCAL?> / Formato: <?=$evento->FORMATO?></p>
                            <a href="https://portal.reisoffice.com.br/index.php/revenda/evento/detalhes/<?=$evento->ID?>" target="_blank" class="btn btn-large btn-inverse" style="text-transform:uppercase; cursor:pointer; margin-left:5px; font-size:12px; padding:5px 10px 5px 10px;">Saiba mais</a>                                                  
                        </div>                        
                    </li> 
                    <?php
                endforeach;
            endforeach;
            ?>
            </ul>
            <br/><br/><br/>                  
        </div>
        <?=$barraDir?>
    </div>
</div>