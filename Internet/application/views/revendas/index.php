<div class="container">
    <div class="row">        
        <div class="span8">            
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>
                <li class="active" data-original-title="">Revendas</li>
            </ul>
            <br/>
            <p style="text-align:justify">
                <br/>
                Em um ambiente empresarial cada vez mais competitivo e uma necessidade cada vez mais presente pela redução de custos e aumento de receitas, as empresas cada vez mais buscam agregar valor aos seus produtos e serviços. 
            </p>
            <p style="text-align:justify">
                Empresa certificada e reconhecida pelo mercado, a Reis Office oferece às revendas a melhor relação custo-benefício do segmento, atendendo com rapidez e qualidade em todo o território nacional e sempre buscando implementar estratégias que facilitem e rentabilizem a atuação de seus parceiros. </p>
            <p style="text-align:justify">
                Muito mais que apenas uma distribuidora de produtos, a Reis Office oferece equipes de suporte técnico certificadas pelos principais fabricantes, treinamentos, variedade de produtos, venda de peças e até mesmo consultoria para grandes projetos, além de soluções exclusivas para aumentar o valor dos seus contratos. </p>
            <p style="text-align:justify">
                Que outro parceiro oferece vantagens como essas? Confira abaixo alguns dos diversos diferenciais que a Reis Office oferece e descubra porque somos o seu parceiro ideal. <br>
                <br/>
            </p>
            <div style="width:100%; height:25px; display:block; clear:left;"></div>
            <div class="row-fluid">
                <div class="span4">
                    <a href="<?= PORTAL_REVENDAS ?>" target="_blank">
                        <img style="border:1px solid #EEE;" src="<?=base_url('images/visual/revendas/rev1.jpg')?>" alt="Portal de Revendas com Conteúdo Exclusivo">
                    </a>
                </div>
                <div class="span4">
                    <a href="<?= base_url('produtos') ?>">
                        <img style="border:1px solid #EEE;" src="<?=base_url('images/visual/revendas/rev2.jpg')?>" alt="Catálogo de Produtos Reis Office">
                    </a>
                </div>
                <div class="span4">
                    <a href="<?= base_url('revendas/eventos') ?>">
                        <img style="border:1px solid #EEE;" src="<?=base_url('images/visual/revendas/rev3.jpg')?>" alt="Eventos Exclusivos para Revendas Parceiras">
                    </a> 
                </div>
            </div>
            <br/><br/>
            <h3>PARCEIRO IDEAL</h3>
            
            <div id="slider">
                <!-- Top part of the slider -->                
                <div id="carousel-bounding-box">
                    <div class="carousel slide" id="myCarousel">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <?php
                            foreach ($solucoes as $id => $solucao):
                                $active = $id == 0 ? 'active' : '';
                                ?>
                                <div class="<?=$active?> item" data-slide-number="<?=$id?>">
                                <img src="<?=$pathbase.$solucao->img?>"></div>
                                <?php
                            endforeach;                               
                            ?>                    
                        </div><!-- Carousel nav -->
                        <a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>
                    </div>
                </div>             
            </div>

            <br/>
            <div class="hidden-phone" id="slider-thumbs">                
                <ul class="thumbnails">
                    <?php
                    foreach ($solucoes as $id => $solucao):
                        ?>
                        <li class="span1">
                            <a class="thumbnail carousel-selector" data-slide="<?=$id?>"><img src="<?=$pathbase.$solucao->thumb?>"></a>
                        </li>
                        <?php
                    endforeach;
                    ?>         
                </ul>                
            </div>           
            
            <p style="color:#E47B38; text-transform:none; font-size:18px; border:none; margin:0px; padding-left:10px; padding-right:10px;padding-bottom:0px">&nbsp;</p>
            <p style="color:#E47B38; text-transform:none; font-size:18px; border:none; margin:0px; padding-left:10px; padding-right:10px;padding-bottom:0px">&nbsp;</p>
            <p style="color:#E47B38; text-transform:none; font-size:18px; border:none; margin:0px; padding-left:10px; padding-right:10px;padding-bottom:0px">&nbsp;</p>           
            
        </div>
        <?=$barraDir?>
    </div>
</div>
<br/><br/>