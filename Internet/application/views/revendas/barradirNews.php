<aside class="span4">   
    <h3>NOTÍCIAS</h3>
    <?php    
    foreach ($noticias as $noticia):        
        ?>
        <p> 
            <?=$noticia->nm_noticia?>
            <br/>
            <a title="<?=$noticia->nm_noticia?>" href="<?=base_url("noticias/ver/$noticia->cd_noticia")?>"> 
                <?=$noticia->ds_chamada_noticia?>
                <br />
            </a>
            <a href="<?=base_url("noticias/ver/$noticia->cd_noticia")?>" class="btn btn-large btn-inverse" style="text-transform:uppercase; font-size:12px; padding:5px 10px 5px 10px">Saiba Mais</a>
        </p> 
        <?php
    endforeach;
    ?>
</aside>
