<div class="container">               
    <br/>
    <ul class="breadcrumb">
        <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>
        <li data-original-title=""><a href="<?=base_url("revendas")?>">Revendas</a> <span class="divider">/</span></li>
        <li class="active" data-original-title="">Webnars</li>
    </ul>
    <br/><br/>
    <table class="table table-bordered" id="tabWebnars" width="100%">
        <thead>
            <tr>
                <td style="width: 60px">Marca</td>
                <td>Treinamento</td>
                <td style="width: 75px">Vídeo</td>
            </tr>            
        </thead>
        <tbody>
            <?php 
            if(!empty($videos)):
                foreach ($videos as $video):
                    ?>
                    <tr>
                        <td style="width: 110px; text-align: center; padding-top: 25px;"><?=$video[0]?></td>                        
                        <td><?=$video[1]?></td>
                        <td style="width: 150px; text-align: center; padding-top: 15px;"><?=$video[2]?></td>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
</div>
<br/><br/>