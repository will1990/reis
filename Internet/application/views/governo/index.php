<div class="container">
    <div class="row"> 
        <div class="span8">
            <section class="wrap" style="padding-left:0px;">
                <br/>
                <ul class="breadcrumb">
                    <li data-original-title=""><a href="#">Home</a> <span class="divider">/</span></li>
                    <li class="active" data-original-title="">Governo</li>
                </ul>
                <br/>
                <p style="text-align:justify">
                    A Reis Office tem uma equipe especializada exclusivamente dedicada ao atendimento de órgãos e empresas públicas, que possibilita o acesso a soluções que se encaixam em sua necessidade 
                    <br/><br/>
                    Os profissionais do departamento são treinados, capacitados e com a vasta experiência no atendimento ao Governo, proporcionando informações atualizadas sobre os melhores equipamentos do mercado 
                    <br/><br/>
                    Todos os serviços e soluções oferecidos pela Reis Office são de alta qualidade e de marcas renomadas no mercado (Canon, Brother, Okidata, Olivetti, Kyocera, Elgin, HP, entre outras.), refletindo o nosso compromisso em oferecer apenas o melhor, seja para esfera pública, ou privada. 
                    <br/><br/>
                    Nossa equipe de assistência técnica é altamente treinada e tem tecnologias de ponta à disposição. Sempre pronta para atender sua solicitação, garantimos um atendimento rápido e eficaz, solucionando qualquer problema que possa acontecer. 
                    <br/><br/>
                    Além de equipamentos e suprimentos, oferecemos serviços de locação, outsourcing, gerenciamento eletrônico de documentos, entre outros. 
                </p>
                <p  style="text-align:justify; padding-top:10px">
                    <a style="color:#F60" href="<?= base_url('outsourcing')?>" title="Outsourcing e Terceirização de Serviços de Impressão">
                        Clique aqui caso queira saber mais sobre como a Reis Office trabalha com <strong>Outsourcing e Terceirização de Serviços de Impressão</strong>
                    </a>
                </p>
                                
                <div style="width:100%; height:20px; display:block; clear:left;"></div>
                
                <div class="row-fluid">
                    <?php
                    foreach ($destaques as $destaque):
                        ?>
                        <div class="span4" style="border:1px solid #eee;"> 
                            <a href="<?=$destaque->url?>" class="thumbnail" style="padding:0px; border:0px;">
                                <img src="<?=$pathbase.$destaque->img?>" alt="" width="212" height="212" style="padding:10px; border:0px;">
                            </a>
                            <h2 style="color:#E47B38; text-transform:none; font-size:18px; border:none; margin:0px; padding-left:10px; padding-right:10px; padding-bottom:0px"><?=$destaque->titulo?></h2>
                            <a class="btn btn-large btn-inverse" href="<?=$destaque->url?>" style="height:30px; line-height:30px;  margin:10px; padding:5px 15px 5px 15px; font-size:14px;">SAIBA MAIS</a>
                        </div>
                        <?php
                    endforeach;
                    ?>
                </div>
            </section>
        </div>  
        <br/>
        <?=$barraDir?>
    </div>
</div>