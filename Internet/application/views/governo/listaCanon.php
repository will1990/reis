<form id="canon" method="POST" action="<?= base_url('home/contato')?>">
    <p>
        <input type="submit" class="btn btn-default" name="submit" value="SOLICITAR PARTICIPAÇÃO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
    </p>
    
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="SCANNER DRM160II"></td>
            <td height="35">SCANNER DRM160II</td>
            <td>DRM160II</td>
            <td>2</td>
            <td>R$ 4.120,00</td>
            <td rowspan="2">FUNPEC</td>
            <td rowspan="2">01/07/2017</td>
            <td rowspan="2">PE 16/2016</td>
        </tr>        
    </table>
    <br/>
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="TINTA AMARELA PFI102Y 130ML IPF500 600 605 650 700 710 720 755 750 710 MFP"></td>
            <td height="35">TINTA AMARELA PFI102Y 130ML IPF500 600 605 650 700 710 720 755 750 710 MFP</td>
            <td>PFI 102Y</td>
            <td>25</td>
            <td>R$ 258,33  </td>
            <td rowspan="5">PREFEITURA DE JUIZ DE FORA</td>
            <td rowspan="5">05/08/2017</td>
            <td rowspan="5">PE 08/2016</td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="TINTA CIANO PFI102C 130ML IPF500 600 605 650 700 710 720 755 750 710 750 MFP"></td>
            <td height="35">TINTA CIANO PFI102C 130ML IPF500 600 605 650 700 710 720 755 750 710 750 MFP</td>
            <td>PFI 102C</td>
            <td>25</td>
            <td>R$ 257,08  </td>            
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="TINTA MAGENTA PFI102M 130ML IPF500 600 605 700 710 720 CANON"></td>
            <td height="35">TINTA MAGENTA PFI102M 130ML IPF500 600 605 700 710 720 CANON</td>
            <td>PFI 102M</td>
            <td>25</td>
            <td>R$ 290,00  </td>            
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="TINTA PRETA MATTE PFI102MBK IPF500 600 605 650 700 710 720 750 755 710 750 MFP"></td>
            <td height="35">TINTA PRETA MATTE PFI102MBK IPF500 600 605 650 700 710 720 750 755 710 750 MFP</td>
            <td>PFI 102MBK</td>
            <td>35</td>
            <td>R$ 290,00  </td>            
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="TINTA PRETA PFI102BK 130ML IPF500 600 605 650 700 710 720 750 755 710 750 MFP"></td>
            <td height="35">TINTA PRETA PFI102BK 130ML IPF500 600 605 650 700 710 720 750 755 710 750 MFP</td>
            <td>PFI 102BK</td>
            <td>30</td>
            <td>R$ 258,33  </td>            
        </tr>
    </table>
    <br/>
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="CARTUCHO PRETO PRETO PARA MP240 MP260 MP480 9ML 2974B017AA"></td>
            <td height="35">CARTUCHO PRETO PRETO PARA MP240 MP260 MP480 9ML 2974B017AA</td>
            <td>PG210BK</td>
            <td>5</td>
            <td>R$ 105,00</td>
            <td rowspan="2">COMPANHIA DE PESQUISA DE RECURSOS MINERAIS - CPRM</td>
            <td rowspan="2">11/09/2017</td>
            <td rowspan="2">PE 007/2016</td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="CARTUCHO TINTA COLOR PARA MP240 MP260 MP480 9ML 2976B017AA"></td>
            <td height="35">CARTUCHO TINTA COLOR PARA MP240 MP260 MP480 9ML 2976B017AA</td>
            <td>CL211C</td>
            <td>5</td>
            <td>R$ 182,00</td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <input type="hidden" name="govMarca" value="Canon"/>
    <p>
        <input type="submit" class="btn btn-default" name="submit" value="SOLICITAR PARTICIPAÇÃO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
    </p>
</form>


