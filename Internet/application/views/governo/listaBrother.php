<form id="brother" method="POST" action="<?= base_url('home/contato') ?>">
    <p>
        <input type="submit" class="btn btn-default" name="submit" value="SOLICITAR PARTICIPAÇÃO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
    </p>    
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="FITA ADESIVA, TIPO TÉRMICA, FITA NA COR BRANCA, MEDINDO 12 MM DE LARGURA E 8M DE COMPRIMENTO, PARA ROTULADORA ELÉTRICA P-TOUCH, BROTHER, MODELO M231"></td>
            <td height="35">FITA ADESIVA, TIPO TÉRMICA, FITA NA COR BRANCA, MEDINDO 12 MM DE LARGURA E 8M DE COMPRIMENTO, PARA ROTULADORA ELÉTRICA P-TOUCH, BROTHER, MODELO M231</td>
            <td>M231</td>
            <td>200</td>
            <td>R$ 38,00</td>
            <td rowspan="2">HOSPITAL UNIVERSITÁRIO DA USP</td>
            <td rowspan="2">17/03/2017</td>
            <td rowspan="2">25/2016</td>
        </tr>        
    </table>
    <br/>
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="CAIXA PARA RESÍDUOS DE TONER PARA IMPRESSORA BROTHER, 50.000 PÁGINAS. CÓDIGO WT-320CL"></td>
            <td height="35">CAIXA PARA RESÍDUOS DE TONER PARA IMPRESSORA BROTHER, 50.000 PÁGINAS. CÓDIGO WT-320CL</td>
            <td>WT320CL</td>
            <td>1</td>
            <td>R$ 498,00  </td>
            <td rowspan="8">PREFEITURA DE JUIZ DE FORA</td>
            <td rowspan="8">28/03/2017</td>
            <td rowspan="8">271/2016</td>
        </tr>
    </table>
    <br/>
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="ETIQ CONTINUA DE 62MM X 30,48M QL550"></td>
            <td height="35">ETIQ CONTINUA DE 62MM X 30,48M QL550</td>
            <td>DK2205</td>
            <td>20</td>
            <td>R$ 92,16</td>
            <td rowspan="8">FUNDAÇÃO NACIONAL DA SAUDE - FUNASA</td>
            <td rowspan="8">06/10/2017</td>
            <td rowspan="8">PE 02/2016</td>
        </tr>
    </table>
    <br/>
    <table border="0" cellpadding="3" cellspacing="3" >
        <tr bgcolor="#e9eef2">
            <td width="8%" bgcolor="#e9eef2"><strong>SELECIONE</strong></td>
            <td width="30%" bgcolor="#e9eef2"><strong>MATERIAL</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>MODELO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>QTDE</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>VALOR UNITÁRIO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>ÓRGÃO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>TÉRMINO CONTRATO</strong></td>
            <td width="5%" bgcolor="#e9eef2"><strong>EDITAL</strong></td>
        </tr>
        <tr>
            <td align="center"><input type="checkbox" name="govProduto" value="SCANNER DE DOCUMENTOS DE ALTA VELOCIDADE 24 PPM 48 IPM"></td>
            <td height="35">SCANNER DE DOCUMENTOS DE ALTA VELOCIDADE 24 PPM 48 IPM</td>
            <td>ADS2000</td>
            <td>5</td>
            <td>R$ 1.805,00</td>
            <td rowspan="8">UNIVERSIDADE TECNOLOGICA FEDERAL DO PARANÁ</td>
            <td rowspan="8">09/11/2017</td>
            <td rowspan="8">PE 091/2016</td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <input type="hidden" name="govMarca" value="Brother"/>
    <p>
        <input type="submit" class="btn btn-default" name="submit" value="SOLICITAR PARTICIPAÇÃO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
    </p>
</form>


