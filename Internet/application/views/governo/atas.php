<div class="container">
    <div class="row">        
        <div class="span8">
            <section class="wrap" style="padding-left:0px;">
                <br/>
                <link href="<?= base_url('assets/custom/css/estiloGovernoReis.css')?>" rel="stylesheet"/>
                <ul class="breadcrumb">
                    <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
                    <li data-original-title=""><a href="<?= base_url('governo')?>">Governo</a> <span class="divider">/</span></li>
                    <li class="active" data-original-title="">Atas</li>
                </ul>
                <br/>
                <p style="text-align:justify">Para facilitar o processo de solicitação dos órgãos públicos, disponibilizamos as atas de registros de preços que já foram aprovadas. Basta selecionar a marca que possui os equipamentos e suprimentos de seu interesse e verificar quais já estão disponíveis. Nossa equipe esta à disposição para auxiliar caso exista alguma dúvida nesse processo. </p>
                <p style="text-align:justify"><br/>
                </p>
                <p style="text-align:justify"><span style="color:#E77230; text-transform:none; font-size:18px; border:none; margin:0px;  padding-right:10px;">Selecione a marca que tem interesse</span></p>
                <p style="text-align:justify">Possuímos uma ampla gama de soluções e serviços de impressão para reduzir o tempo e custos da sua empresa, com qualidade superior na prestação do serviço.</p>
                <p style="text-align:justify"> Trabalhando com as melhores marcas de cada segmento, somos premiados em diversas categorias, o que prova nossa dedicação em sempre oferecer os melhores produtos para cada negócio. </p>
                <p style="text-align:justify"> Com uma equipe comercial capacitada e flexível, estudaremos sua empresa, analisando as necessidades, para que possa oferecer serviços e produtos personalizados e adequados para você.</p>
                <p style="text-align:justify"> Também mantemos uma equipe de assistência técnica e pós venda pronta para solucionar suas dúvidas, realizar reparos e manutenção nos seus equipamentos de maneira rápida, reafirmando nosso compromisso com os clientes. </p>
                <br/><br/>
                <div class="row-fluid">
                    <div class="span3 img-responsive marca">
                        <a href="<?= base_url('governo/atas/brother') ?>">
                            <img src="<?= base_url('images/visual/governo/brother.jpg') ?>" id="imgBrother"/>
                        </a>
                    </div>
                    <div class="span3 img-responsive marca">
                        <a href="<?= base_url('governo/atas/canon') ?>">
                            <img src="<?= base_url('images/visual/governo/canon.jpg') ?>" id="imgCanon"/>
                        </a>
                    </div>
                    <div class="span3 img-responsive marca">
                        <a href="<?= base_url('governo/atas/kyocera') ?>">
                            <img src="<?= base_url('images/visual/governo/kyocera.jpg') ?>" id="imgKyocera"/>
                        </a>
                    </div>
                    <div class="span3 img-responsive marca">
                        <a href="<?= base_url('governo/atas/oki') ?>">
                            <img src="<?= base_url('images/visual/governo/oki.jpg') ?>" id="imgOki"/>
                        </a>
                    </div>
                </div>         
            </section>
        </div>
        <br/>
        <?=$barraDir?>
    </div>
</div>