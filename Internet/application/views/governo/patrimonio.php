<div class="container">
    <div class="row">        
        <div class="span8">
            <section class="wrap" style="padding-left:0px;">
                <br/>
                <ul class="breadcrumb">
                    <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
                    <li data-original-title=""><a href="<?= base_url('governo')?>">Governo</a> <span class="divider">/</span></li>
                    <li class="active" data-original-title="">Identificação de Patrimônio</li>
                </ul>
                <br/>
                <p style="text-align:justify">
                    <span style="color:#7f7e7e; text-transform:none; font-size:18px; border:none; margin:0px;">
                        Identifique seu ativos de forma simples e rápida
                    </span>
                </p>

                <p style="text-align:justify">
                    <img src="<?= base_url('images/visual/governo/etiquetas1.png') ?>"  style="margin-left:10px; width:100%;float:right">
                    Com a linha de rotuladores eletrônicos PT-9700PC e PT-9800PCN, você tem a solução ideal área identificar os ativos de sua empresa. Com seu Software de edição PT-Editor, que já acompanha o equipamento, pode-se criar etiquetas diversas, incluindo códigos de Barras, números da série, logotipos, importar imagens da internet e molduras decorativas, com diversos tamanhos e tipos de fontes. Com o sistema de auto-numeração, você consegue imprimir de 1 a 5.000 com apenas um click! 
                </p>
                <p style="text-align:justify">
                    A linha PT-9700PC e PT-9800PCN trabalha com as fitas da linha TZ, que possuem uma lâmina de proteção, que as tornam extremamente resistentes, protegendo o impresso de impactos, água, produtos químicos, atritos, entre outros.
                </p>
                <br/>
                <table style="float:left" width="343" height="189" border="0" cellpadding="3" cellspacing="3">
                    <tr>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon1.png') ?>"    alt=""/></td>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon2.png') ?>"    alt=""/></td>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon3.png') ?>"    alt=""/></td>
                    </tr>
                    <tr>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon4.png') ?>"    alt=""/></td>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon5.png') ?>"    alt=""/></td>
                        <td valign="top"><img src="<?= base_url('images/visual/governo/icon6.png') ?>"    alt=""/></td>
                    </tr>
                </table>
                <p><img   src="<?= base_url('images/visual/governo/etiquetas2.jpg') ?>" width="338" height="335"  alt=""/></p>
                <p style="text-align:justify">&nbsp;</p>
                <br/>
            </section>
        </div>
        <br/>
        <?=$barraDir?>
    </div>
</div>

