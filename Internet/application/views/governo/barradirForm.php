<aside class="span4"> 
    <div class="wrap" style="padding-left:0px;">
        <p style="text-align:justify"> Se deseja solicitar um orçamento ou obter maiores informações entre em contato conosco pelo formulário, ou através do e-mail: <span class="label label-warning">licitacoes@reisoffice.com.br</span></p>
    </div>
    <h4>Entre em contato</h4>
    <form method="post" action="<?=  base_url("contato/salvar") ?>">        
        <div class="controls controls-row">
            <fieldset disabled>
            </fieldset>
        </div>
        <div class="controls controls-row">
            <input id="nome" name="govNome" required="" type="text" class="form-control" placeholder="Digite seu nome *">
        </div>        
        <div class="controls controls-row">
            <input id="email" name="govEmail" required="" type="email" class="form-control" placeholder="Digite seu e-mail *">
        </div>
        <div class="controls controls-row">
            <input name="govFone" type="text" required="" id="formsFone" class="form-control" maxlength="15" placeholder="DDD+Telefone">
        </div>
        <div class="controls controls-row">
            <input id="empresa" required="" name="govEmpresa" type="text" class="form-control" placeholder="Sua Empresa / Orgão *">
        </div>
        <div class="controls controls-row" style="width:374px;">
            <input id="material" required="" name="govMaterial" type="text" class="form-control" placeholder="Equipamento / Material *" />
        </div>
        <div class="controls controls-row" style="width:374px;">
            <input id="quantidade" name="govQtde" type="text" class="form-control" placeholder="Quantidade *" />
        </div>
        <div class="controls controls-row"> </div>
        <div class="controls controls-row">
            <textarea name="govObs" class="span6" placeholder="Digite aqui suas observações" rows="10" style="width:364px;"></textarea>
        </div>
        <input id="btn_submit" type="submit" class="btn btn-default" name="submit" value="ENVIAR CONTATO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
        <input type="hidden" name="controller" value="governo"/>
    </form>
</aside>