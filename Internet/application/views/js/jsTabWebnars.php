<script type="text/javascript">
    $(function() {
        $("#tabWebnars").dataTable({
            ordering: false,
            paging: true,
            "oLanguage": { "sUrl": "<?= base_url('assets/DataTables-1.10.13/pt_BR.txt')?>" }
        });
    });
</script>