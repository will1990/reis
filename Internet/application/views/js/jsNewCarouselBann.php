<script type="text/javascript">
jQuery(document).ready(function($) {
 
        $('#myCarousel').carousel({
                interval: 7000
        });
 
        //Handles the carousel thumbnails
        $('.carousel-selector').click( function(){
            var id = $(this).data("slide");
            $('#myCarousel').carousel(id);
        });
 
 
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid', function (e) {
            var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
        });
});
</script>