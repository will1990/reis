<script type="text/javascript">
    $(function() {
        $("#dtNoticias").dataTable({
            ordering: false,
            stateSave: true,
            paging: true,
            searching: false,
            sorting: false,
            "oLanguage": { "sUrl": "<?= base_url('assets/DataTables-1.10.13/pt_BR.txt')?>" }
        });
    });
</script>
