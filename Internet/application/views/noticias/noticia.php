<div class="container">
    <div class="row">
        <div class="span8"> 
            <br>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?=base_url("noticias")?>">Not&iacute;cias</a> <span class="divider">/</span></li>
                <li class="active" data-original-title=""><?=$noticia->nm_noticia?></li>
            </ul>
            <h2><?=$noticia->nm_noticia?></h2>
     
      
            <!-- Options -->
            <div class="blog-options">
                <span> <?=$noticia->data?></span>
            </div>
            
            <!-- /Options -->
            <p> <?=$noticia->noticia?> </p>
            <br/><br/>
        </div>
        <?=$barraDir?>
    </div>
</div>