<div class="container">
    <div class="row">
        <div class="span8"> 
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>                
                <li class="active" data-original-title="">Not&iacute;cias</li>
            </ul>
            <br/>
            <table class="" id="dtNoticias">
                <thead><tr><td></td></tr></thead>
                <tbody>
                    <?php foreach ($noticias as $noticia): ?>                    
                        <tr><td>
                                <a style="text-decoration:none;" href="<?=base_url("noticias/ver/$noticia->cd_noticia/". url_codificar(remover_acentos($noticia->nm_noticia)) .".html")?>"  title="<?=$noticia->nm_noticia?>">
                            <div class="wrap blog">

                                <div style="border:1px solid #EEE; float:left; margin-right:15px; height: 89px; width: 88px;">
                                    <img class="center-block" height="87" style="position: relative; top: 50%; transform: translateY(-50%); float: none;" src="<?=$noticia->img?>" alt="" >
                                </div>

                                <h4><?=$noticia->nm_noticia?></h4>
                                <span class="data">Publicado: <?=$noticia->data?> </span>
                                <br/>
                                <p>
                                    <?=$noticia->ds_chamada_noticia?>
                                </p>
                            </div>
                        </a>                    
                        </td></tr>
                    <?php endforeach; ?>         
                </tbody>
            </table>
            <br/><br/>
        </div>
        <?=$barraDir?>
    </div>
</div>