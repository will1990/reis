<div class="container">
    <div class="row">        
        <div class="span8">
            <section class="wrap">
                <br/>
                <ul class="breadcrumb">
                    <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
                    <li data-original-title=""><a href="<?= base_url("outsourcing") ?>">Outsourcing</a> <span class="divider">/</span></li>
                    <li class="active" data-original-title="" style="color:#e27b38">Assistência Técnica</li>
                </ul>
                <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloTopicsReis.css")?>" />
                <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloAssistReis.css")?>" />
                <br/>
                <p style="text-align:justify">A Reis Office conta com uma equipe de Assistência Técnica altamente qualificada, competente e com vasta experiência no seu segmento de atuação. Os técnicos recebem treinamentos constantes e estão sempre aptos a encontrar a melhor solução para a sua empresa, independente do tamanho. 
                    <br>
                    <br>

                    Atenta às necessidades de um atendimento rápido e qualificado, a Reis Office foi a primeira empresa de impressão a implantar um sistema on-line para os serviços de assistência técnica. Através do Portal Service é possível acompanhar todos os contratos vigentes, solicitar suprimentos e agendar atendimento técnico. 
                    <br>
                    <br>

                    Além disso, todos os técnicos possuem à sua disposição smartphones com conexão a internet e acesso ao sistema on-line, permitindo que as solicitações de atendimento sejam visualizadas de forma rápida, segura e diferenciadas, garantindo aos clientes o melhor serviço.</p>
                <p style="text-align:justify"><a style="color:#F60" href="<?= base_url("outsourcing") ?>" title="Outsourcing e Terceirização de Serviços de Impressão">Clique aqui caso queira saber mais sobre como a Reis Office trabalha com <strong>Outsourcing e Terceirização de Serviços de Impressão</strong></a></p>
                <br/>
                <br/>
                <div style="display:block; margin:0 auto; min-height:100px; width:100%;">
                    <div class="span3 sessao" style="margin:0 auto;">
                        <div id="outsourcing_img" class="theme-color-orange-bg block" onClick="descricao_produto('v1');">
                            <h3 style="font-size:21px; border:0px; padding:0px; margin:0px;">ASSISTÊNCIA TÉCNICA</h3>
                            <h4 style="font-size:28px; font-weight:normal; color:#FFF">OUTSOURCING</h4>
                            <i class="icon-wrench" style="color:#FFF"></i>
                            <p>Caso você já tenha feito uma locação com a Reis Office</p>
                            <p style="color:#FFF;"><strong>Clique e saiba mais</strong></p>
                        </div>
                    </div>
                    <div style="width:22px; float:left; height:1px;"></div>
                    <div class="span3 sessao">
                        <div id="evento_img" class="theme-color-red-bg block" onClick="descricao_produto('v2');">
                            <h3 style="font-size:21px; border:0px; padding:0px; margin:0px;">ASSISTÊNCIA TÉCNICA</h3>
                            <h4 style="font-size:28px; font-weight:normal; color:#FFF">REVENDAS</h4>
                            <i class="icon-retweet" style="color:#FFF"></i>
                            <p>Você já é uma revenda de produtos e serviços?</p>
                            <p style="color:#FFF;"><strong>Clique e saiba mais</strong></p>
                        </div>
                    </div>
                </div>                
                
                
                <div style="display:none; clear:left;" id="v1">                    
                    <a name="outsourcing"></a>                    
                    <br/><br/>                    
                    <h2>OUTSOURCING</h2>
                    <div class="toggle" style="display:block;">                   
                        <div class="myToggler"> <span class="togglerSign">-</span>Portal Service</div>
                        <div class="mySlider">O Portal Service é um serviço online exclusivo oferecido pela Reis Office, que permite mais praticidade aos clientes que desejam entrar em contato conosco.
                            <br/>
                            <br/>

                            Acessando o site, com apenas alguns cliques é possível solicitar suprimentos, abrir chamados técnicos, consultar ordens de serviço, pesquisar soluções para problemas via FAQ, entre diversas outras opções.
                            <br/>
                            <br/>

                            A Reis Office é a única empresa do ramo que disponibiliza um portal online de relacionamento aos seus clientes, facilitando o contato e diminuindo o tempo de espera em chamados e solicitações!
                            <br/>
                            <br/>

                            Confira mais benefícios do Portal Service: 
                            <br/>
                            <br/>

                            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Visualização dos contratos vigentes;</li>
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Histórico de faturas;</li>
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Solicitação de suprimentos;</li>
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Agendamento de atendimento técnico;</li>
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Acompanhamento do volume de páginas impressas.</li>
                            </ul>   

                            <br/>
                            <br/>

                            Para saber mais sobre o Portal Service: <a class="btn btn-large btn-inverse" href="<?=PORTAL_SERVICE?>" style="height:30px; line-height:30px; margin-bottom:7px; padding:5px 15px 5px 15px; font-size:14px;"><i class="icon-desktop"></i> CLIQUE AQUI</a>.


                        </div>
                        
                        <div class="myToggler"> <span class="togglerSign">-</span>Contrato de Manutenção</div>
                        <div class="mySlider"> Para garantir que seus equipamentos estejam sempre em perfeitas condições, a Reis Office oferece a possibilidade de "Contratos de Manutenção" *. Dessa forma, nossos profissionais especializados farão um acompanhamento técnico, garantindo que as máquinas operem da melhor forma possível. <br>
                            <br/>

                            Com os contratos, é possível optar por planos que incluem o fornecimento de peças e suprimentos, fazendo com que a sua empresa reduza os custos e otimize a produção. Confira as opções de planos de fornecimento: <br>
                            <br/>

                            <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> <strong>Prata:</strong> Inclui peças, mão de obra especializada e cilindros </li>
                                <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> <strong>Ouro:</strong> Inclui peças, mão de obra especializada, cilindros, revelador e toners.</li>
                            </ul>   

                            <br/>
                            <br/>


                            <strong>*Atenção:</strong> os contratos de manutenção estão disponíveis apenas para equipamentos Canon, Brother e Kyocera, sob condições específicas. </div>
                       
                        <div class="myToggler"> <span class="togglerSign">-</span>Chamados Técnicos</div>
                        <div class="mySlider">Para garantir o melhor atendimento aos clientes, a Reis Office mantém uma equipe de profissionais especializados e atualizados com treinamentos dos maiores fabricantes da indústria de tecnologia.
                            <br/>
                            <br/>

                            Contando com locomoção própria e smartphones conectados ao sistema, os técnicos possuem mais autonomia e agilidade para resolver chamados e atender solicitações, dando mais interatividade entre clientes e suporte e diminuindo o tempo de espera.
                            <br/>
                            <br/>

                            Além disso, revolucionamos o processo de atendimento a chamados técnicos, nos tornando a primeira empresa do mercado de impressão a implantar um sistema online de abertura e fechamento de chamados, solicitação e consulta de peças.
                            <br/>
                            <br/>

                            Com a implantação deste novo procedimento, todos os técnicos têm à sua disposição um smartphone com acesso ao sistema integrado. Através dele, é possível consultar o histórico do cliente e a disponibilidade de peças em tempo real.
                            <br/>
                            <br/>

                            Para saber mais sobre o Portal Service: <a class="btn btn-large btn-inverse" href="<?=PORTAL_SERVICE?>" style="height:30px; line-height:30px; margin-bottom:7px; padding:5px 15px 5px 15px; font-size:14px;"><i class="icon-desktop"></i> CLIQUE AQUI</a></div>
                        
                        <div class="myToggler"> <span class="togglerSign">-</span>Benefícios Conquistados</div>
                        <div class="mySlider">
                            <div class="bs-example" data-example-id="list-group-custom-content" style="border:0px; margin:0px;">                                
                                <ul class="list-group" style="padding:0px; border:0px; margin:0px;">
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Aumento da produtividade;</li>
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Redução de custo operacional;</li>
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Maior controle dos chamados e cumprimento de prazo;</li>
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Aumento da segurança do cliente, a partir da utilização de senha;</li>
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Aumento da capacidade produtiva do técnico;</li>
                                    <li class="list-group-item" data-original-title=""><i class="icon-caret-right" style="color:#e47b38"></i> Agilidade e satisfação dos clientes.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>




                <div style="display:none; clear:left;" id="v2">
                    <a name="revendas"></a>
                    <br/><br/>
                    <h2 style="color:#537f95">REVENDA</h2>
                    <div class="toggle" style="display:block;">                         
                        <div class="myToggler"> <span class="togglerSign">-</span>Garantia</div>
                        <div class="mySlider"> Você comprou um equipamento conosco que está apresentando problemas? Entre em contato com a nossa assistência técnica ou venha com o equipamento à nossa sede para que seja feita a avaliação técnica do equipamento e o problema possa ser solucionado.<br>

                            A garantia dos equipamentos é a chamada “Garantia Balcão”, em que os clientes precisam trazer os equipamentos até nossa assistência técnica. Este procedimento é similar ao que é praticado no mercado em relação a equipamentos eletrônicos ou automóveis, por exemplo.<br>

                            Caso a revenda necessite de uma visita técnica é necessário entrar em contato para verificar a possibilidade e os valores do serviço de deslocamento de nosso técnico.<br>

                            <strong>Para entrar em contato direto com a Assistência Técnica, ligue 11 2442-2600 e escolha a opção 1.
                            </strong> 
                        </div>
                        
                        <div class="myToggler"> <span class="togglerSign">-</span>Suporte a revendas</div>
                        <div class="mySlider"> Disponibilizamos um serviço de suporte a revendas via telefone. Utilizando este serviço exclusivo seus técnicos poderão tirar dúvidas sobre um problema ou procedimento técnico até mesmo quando estiverem “em campo”, durante visita aos seus clientes. <br>
                            <strong>Basta entrar em contato via telefone pelo 11 2442-2600 e falar com o setor de Suporte a Revendas.
                            </strong> </strong></div>
                        
                        <div class="myToggler"> <span class="togglerSign">-</span>Portal de Revendas</div>
                        <div class="mySlider"> 

                            O Portal de Revendas foi criado para facilitar o acesso a informações de manuais, treinamentos e outras informações técnicas e comerciais. Todos os seus funcionários podem e obter diversas informações. <br>

                            <strong>Cadastre-se no Portal de Revendas e tenha acesso a conteúdo exclusivo: <a href="<?=PORTAL_REVENDAS?>" target="_blank">http://portal.reisoffice.com.br</a></strong>

                        </div>
                        
                        <div class="myToggler"> <span class="togglerSign">-</span>Estacionamento para clientes</div>
                        <div class="mySlider">Prezando por mais conforto aos nossos clientes que desejam atendimento da assistência técnica, a Reis Office dispõe de um estacionamento próprio com 600m². Desta forma evitamos possíveis problemas com segurança e oferecemos mais comodidade e qualidade no atendimento. 

                            <div style="display:block; width:100%; height:20px; clear:left"></div>

                        </div>                        
                    </div>
                </div>
            </section>
            <br/>
        </div>
        <?=$barraDir?>
    </div>
</div>