<div class="section" style="background: #537f95; padding-bottom: 30px;">
    <div class="container">      
        <!-- Carousel Atual -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">          
            
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php 
                foreach ($banners as $id => $banner)
                {                    
                    $active = $id == 0 ? 'active' : '' ;
                    ?>
                    <div class="item <?=$active?>">                        
                        <div class="left-data-app" style="width:50%;">
                            <h3 class="titBanners"><?=$banner->titulo?></h3>                            
                            <br/><br/>
                            <p class="textBanners"><?=$banner->texto?></p>                            
                            <a class="btn btn-large btn-inverse" style="float:right;" href="<?=$banner->url?>">SAIBA MAIS</a>                            
                        </div>
                        <div class="right-data-app" style="width:50%;">
                            <img src="<?=$pathbase.$banner->img?>" style="height: 370px;">                       
                        </div>
                    </div>
                    <?php
                }              
                ?>                
            </div>         
            
            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>         
            <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
            
        </div>            
            
    </div>
</div>

<div class="container" style="margin-top: 15px;"> 
    <div class="row blocks-2">
        <?php
        foreach($destaques as $id => $destaque)
        {
            if($id <= 3)
            {
                ?>
                <div class="span3b">
                    <div class="block-2">
                        <h3 style="border:0px; padding:0px; margin:0px;"><img src="<?=$pathbase.$destaque->img?>"> <?=$destaque->titulo?></h3>
                        <p><?=$destaque->texto?></p>
                        <a class="btn btn-large btn-inverse" href="<?=$destaque->url?>" style="padding:10px 15px; font-size:16px;">VEJA MAIS</a> 
                    </div>
                </div>
                <?php
            }
        }
        ?>        
    </div>
    
    <div class="row wrap" style="margin-bottom:20px;"> 
        <div class="span8" style="width:780px; ">
            <h2><a class="corTitulos" href="<?=base_url("noticias")?>">&Uacute;LTIMAS NOT&Iacute;CIAS</a></h2>
            <div class="row">
                <?php
                foreach ($noticias as $noticia)
                {
                    ?>
                    <div class="span4 blog-corp" style="margin-bottom:20px;margin-left:20px;">
                        <div class="row">
                            <div class="span1"> 
                                <i style="width:88px; min-height:88px; border:1px solid #EEE; display:block;">
                                    <img class="blogimg" height="88" src="<?=$noticia->img?>" style=" max-height:87px; float:none; border:0px;" />
                                </i> 
                                <br><br>
                            </div>
                            <div class="span3">
                                <h3 style="line-height:25px; margin-bottom:8px; padding-bottom:5px;">
                                    <a href="<?=base_url("noticias/ver/$noticia->cd_noticia")?>"  title="<?=$noticia->nm_noticia?>" style="text-align:justify">
                                        <?=$noticia->ds_chamada_noticia?>...
                                    </a>
                                </h3>
                                <span class="date"><?=$noticia->data?></span>
                                <p style="text-align:justify"> </p>
                            </div>
                        </div>
                    </div>
                    <?php
                }        
                ?>        
            </div>
        </div>  
        <div class="span4c">
            <h3 class="corTitulos">SHOW ROOM 360&ordm;</h3>
            <div id="carousel-example-generic"> 
                <div role="listbox" style="border:1px solid #eeeeee; padding:10px; height:294px; width:94%;">
                    <div class="item active">
                        <a href="<?=base_url("empresa/showroom")?>"><img src="<?=base_url("images/showroom1.jpg")?>" width="100%" style="border:none"></a>
                    </div>         
                </div>
                <a class="carousel-control left" href="#carousel-example-generic" role="button" data-slide="prev">&lsaquo;</a> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a> <a class="carousel-control right" href="#carousel-example-generic" role="button" data-slide="next">&rsaquo;</a> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a> 
            </div>
        </div>
    </div>    
    
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>
    
</div>

