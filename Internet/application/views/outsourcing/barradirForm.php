<aside class="span4">
    
    <div style="background-color:#537f95; width:95%;  padding:15px; color:#FFF; font-size:16px;">  
        <span>
            <strong>
                <h2 style="background-color:#537f95; padding:1px; margin-bottom:2px; color:#FFF; font-size:16px;">
                    <strong>SOLUÇÕES ESPECÍFICAS PARA OUTSOURCING</strong>
                </h2>                    
            </strong>
        </span>
        <p style="color:#FFF; text-align:justify;"><br>
            Confira abaixo nossas soluções específicas para o seu negócio:</p>
        <div class="controls controls-row" style="width:344px;">
            <select class="span6" style="width:344px" onChange="javascript:redirecionar(this.value);" name="select">
                <option value="">Selecione seu ramo de atividade</option>
                <?php
                foreach ($solucoes as $idx => $solucao):                    
                    ?>
                    <option value="<?=$idx?>"><?=$solucao->nome?></option>                   
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="wrap">
        <p style="text-align:justify">
            <br>
            Para cada ramo de atividade, a Reis Office possui diversos cases de sucesso. Selecione no combo uma de nossas soluções disponíveis.
        </p>
    </div>
    <h4>Entre em contato</h4>    
    <form name="form1" id="form_contato" method="post" action="<?= base_url("contato/salvar") ?>">                     
        <div class="controls controls-row">
            <fieldset disabled>
            </fieldset>
        </div>
        <div class="controls controls-row">
            <input name="outNome" required="" type="text" class="form-control" placeholder="Digite seu nome">
        </div>
        <div class="controls controls-row">
            <input name="outEmail" required="" type="email" class="form-control" placeholder="Digite seu e-mail">
        </div>
        <div class="controls controls-row">
            <input id="formsFone" required="" name="outFone" type="text" class="form-control" maxlength="15" placeholder="DDD+Telefone">            
            <input id="cnpj" required="" type="text" placeholder="CPF ou CNPJ" name="outCNPJ" OnKeyUp="mascCnpjCpf(this);" maxlength="18">
        </div>        
        <div class="controls controls-row">
            <textarea name="outMsg" class="span6" placeholder="Digite aqui sua mensagem" rows="10" style="width:364px;"></textarea>
        </div>
        <input id="btn_submit" type="submit" class="btn btn-default" name="btn_submit" value="ENVIAR CONTATO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
        <input type="hidden" name="controller" value="outsourcing"/>
    </form>    
    <div class="wrap">
        <div class="fb-page" data-href="https://www.facebook.com/ReisOfficeProducts/" data-width="378" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/ReisOfficeProducts/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ReisOfficeProducts/">Reis Office Products</a></blockquote></div>
    </div>    
</aside>
