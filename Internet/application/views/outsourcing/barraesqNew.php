<aside class="span4b" style="margin-left:0px;">
    <div class="wrap" style="padding:0px 15px 0px 0px; border-right: 1px solid #ccc;">
        <h2 class="titulosOut tituloOutMenu">SERVIÇOS:</h2>
        <div class="wrap" style="padding:10px 10px 0px 0px;">
            <div class="accordion" id="accordion2">
                <?= $menuCompleto ?> 
            </div>     
        </div>
        <div class="areaSolucoes">
            <p class="txtSolucoes">Locações específicas para o seu negócio:</p>
            <div class="row-fluid">
                <select class="selectSolucoes" onChange="javascript:redirecionar(this.value);" name="select">
                    <option value="">Selecione seu ramo de atividade</option>
                    <?php
                    foreach ($solucoes as $idx => $solucao):                    
                        ?>
                        <option value="<?=$idx?>"><?=$solucao->nome?></option>                   
                        <?php
                    endforeach;
                    ?>
                </select>
            </div>
        </div>        
    </div>      
    <div style="margin-right: 21px;">
        <div class="fb-page" data-href="https://www.facebook.com/ReisOfficeProducts/" data-tabs="timeline" data-height="70" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
            <blockquote cite="https://www.facebook.com/ReisOfficeProducts/" class="fb-xfbml-parse-ignore">
                <a href="https://www.facebook.com/ReisOfficeProducts/">Reis Office Products</a>
            </blockquote>
        </div>
    </div>
    <br/><br/>
</aside>