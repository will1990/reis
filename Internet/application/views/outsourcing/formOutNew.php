<div class="row-fluid well">
    
    <h4 align="center" class="titulosOut tituloOutForm">PARA SABER MAIS SOBRE NOSSAS SOLUÇÕES, ENTRE EM CONTATO</h4>
    <form style="margin-bottom: 40px;" method="post" action="<?= base_url("contato/salvar") ?>">
        <div class="offset1 span5">
            <input type="text" class="form-control span12" required="" placeholder="*Digite seu Nome" name="outNome"/>
            <input type="email" class="form-control span12" required="" placeholder="*Digite seu Email" name="outEmail"/>
            <input type="text" class="form-control span8" required="" maxlength="15" id="formsFone" placeholder="*DDD + Telefone" name="outFone"/>
            <input type="text" class="form-control span8" required="" id="cnpj" placeholder="*CPF ou CNPJ" name="outCNPJ" OnKeyUp="mascCnpjCpf(this);" maxlength="18"/>
        </div>
        <div class="span5">
            <textarea name="outMsg" class="span12" required="" placeholder="*Digite aqui sua mensagem" rows="5"></textarea>
            
            <input class="btn btn-default btn-small btnForm" type="submit" value="Enviar"/>
            <input type="hidden" name="controller" value="outsourcing"/>
        </div>
    </form>
    
</div>