<link href="<?= base_url("assets/custom/css/estiloOutsourcingReis.css")?>" rel="stylesheet" type="text/css"/> 
<div class="container" style="padding-top: 25px;">
    <!--<div class="row-fluid" >-->
        <?=$barraesq?>
        <div class="span8b">        
            <div class="conteudoTexto">
                <p>A Reis Office possui uma variedade de soluções e serviços de impressão para otimizar o tempo e 
                    reduzir os custos da sua empresa, com alta qualidade na prestação do serviço.
                <p/>
                <p>
                    Com uma equipe comercial capacitada e flexível, a Reis Office irá estudar sua empresa, 
                    analisando as necessidades, para oferecer serviços e produtos personalizados e adequados.
                </p>
                <p>
                    A equipe de assistência técnica e pós-venda está sempre pronta para solucionar suas dúvidas, 
                    realizar reparos e manutenção nos seus equipamentos.
                </p>
            </div>        
            <hr/>        
            <h3 class="titulosOut tituloOutPrincipal">SOLUÇÕES EM DESTAQUE</h3>
            <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">                
                        <div class="item active">                            
                            <div class="row-fluid">                          
                                <a class="offset1 span1" href='<?= base_url("outsourcing/ver/4/" . url_codificar(remover_acentos("Impressão na Nuvem")) .".html")?>'>
                                    <img alt="impressão na nuvem" src="<?= base_url("images/visual/outsourcing/nuvem.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/4/" . url_codificar(remover_acentos("Impressão na Nuvem")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">Impressão na nuvem</h5>
                                        <p class="conteudoTextoBann">A impressão na nuvem é uma solução desenvolvida para reduzir seus custos com impressões desnecessárias, manter o sigilo de documentos confidenciais, gerar relatórios personalizados e muito mais.</p>
                                    </a>
                                </div>                                    
                                <a class="span1" href='<?= base_url("outsourcing/ver/5/" . url_codificar(remover_acentos("Gerenciamento Remoto")) .".html")?>'>
                                    <img alt="gerenciamento remoto" src="<?= base_url("images/visual/outsourcing/gerenciamento.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/5/" . url_codificar(remover_acentos("Gerenciamento Remoto")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">Gerenciamento Remoto</h5>
                                        <p class="conteudoTextoBann">Com a Reis Office, é possível gerenciar remotamente todo o parque de impressões da sua empresa de forma rápida e completa, possibilitando o acompanhamento de filas de impressão, status dos consumíveis e equipamentos.</p>
                                    </a>
                                </div>                                          
                            </div>
                            <br/><br/>
                            <div class="row-fluid">                             
                                <a class="offset1 span1" href='<?= base_url("outsourcing/ver/2/" . url_codificar(remover_acentos("Gerenciamento Eletrônico de Documentos")) .".html")?>'>
                                    <img alt="gerenciamento eletrônico de documentos" src="<?= base_url("images/visual/outsourcing/ged.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/2/" . url_codificar(remover_acentos("Gerenciamento Eletrônico de Documentos")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">GED</h5>
                                        <p class="conteudoTextoBann">O GED (Gerenciamento Eletrônico de Documentos) é muito mais do que apenas digitalizar e arquivar seus documentos. Com esta solução, criamos fluxos de trabalho personalizados que facilitam sua rotina, poupando tempo com a busca de arquivos físicos e agilizando os processos gerais dos departamentos.</p>
                                    </a>
                                </div>                                    

                                <a class="span1" href='<?= base_url("outsourcing/ver/3/" . url_codificar(remover_acentos("Contabilização")) .".html")?>'>
                                    <img alt="contabilização" src="<?= base_url("images/visual/outsourcing/contabilização.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/3/" . url_codificar(remover_acentos("Contabilização")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">Contabilização</h5>
                                        <p class="conteudoTextoBann">Atualmente, o Gerenciamento de impressões é uma demanda crescente de empresas que querem otimizar seus recursos, e diminuir os custos.</p>
                                    </a>
                                </div>                       
                            </div>                            
                        </div>
                        <div class="item">                  
                            <div class="row-fluid">                          
                                <a class="offset1 span1" href='<?= base_url("outsourcing/ver/6/" . url_codificar(remover_acentos("Dados Variáveis")) .".html")?>'>
                                    <img class="media-object img-responsive" width="100px" alt="dados variáveis" src="<?= base_url("images/visual/outsourcing/dados.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/6/" . url_codificar(remover_acentos("Dados Variáveis")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">Dados Variáveis</h5>
                                        <p class="conteudoTextoBann">Ideal para ações individualizadas, a impressão de dados variáveis possibilita que cada cópia do impresso tenha uma personalização diferente.</p>
                                    </a>    
                                </div> 

                                <a class="span1" href='<?= base_url("outsourcing/ver/1/" . url_codificar(remover_acentos("Locação")) .".html")?>'>
                                    <img class="media-object img-responsive" width="100px" alt="locação" src="<?= base_url("images/visual/outsourcing/locacao.png")?>">
                                </a>
                                <div class="span4">
                                    <a style="text-decoration: none;" href='<?= base_url("outsourcing/ver/1/" . url_codificar(remover_acentos("Locação")) .".html")?>'>
                                        <h5 class="media-heading tituloOutBann">Locação</h5>
                                        <p class="conteudoTextoBann">Com um ambiente empresarial cada vez mais competitivo, e uma necessidade constante de redução de custos e aumento de produtividade, as empresas estão buscando diferentes alternativas para otimizar os processos.</p>
                                    </a>
                                </div>                                        
                            </div>
                            <div class="row-fluid">
                                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                            </div>                            
                        </div>                                
                    </div>         

                    <!-- Controls -->
                    <a class="left carousel-control" style="background-color: transparent; color: #000;" href="#myCarousel" data-slide="prev">&lsaquo;</a>         
                    <a class="right carousel-control" style="background-color: transparent; color: #000;" href="#myCarousel" data-slide="next">&rsaquo;</a>

                </div>
            </div>
            <br/><br/>            
            <?=$formOut?>
        </div>
    <!--</div>-->
</div>