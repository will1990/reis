<link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloOutsourcingReis.css")?>" />

<div class="container">
<div class="row">
    <div class="span7">
    <br/><font size="5" style="color: #537f95;font-weight:500;">Precisa de fatura ou de segunda via de boleto?</font><br/><br/>
    <button class="accordion"><font size="5">Possui contrato de locação conosco</font></button>
    
    <div class="panel">
            <p style="color:#B28F4C">&raquo;Acesse o nosso Portal Service, vá ao Menu Financeiro e faça impressão de sua fatura e boleto.
            <a style="color:#B28F4C" href="<?=  base_url("arquivos/boletos/PASSO_A_PASSO_PORTAL_SERVICE.pdf")?>" target="_blank"><br/>Clique e confira o passo a passo.</a></p>
            <p style="color:#B28F4C">&raquo;Para atualizar o seu boleto, acesse <a style="color:#B28F4C" href="https://www.itau.com.br/servicos/boletos/atualizar/" target="_blank"><b>https://www.itau.com.br/servicos/boletos/atualizar/</b></a><br/>
            <a  style="color:#B28F4C" href="<?=  base_url("arquivos/boletos/Precisa_de_fatura_ou_segunda_via_de_boleto.pdf")?>"target="_blank">Clique e confira o passo a passo.</a><br/><br/></p>
            <p style="color:#B28F4C">&raquo;Ou então solicite a segunda via de sua fatura ou boleto, através dos nossos canais de atendimento:<br/><br/>
            <b>Telefone:(11)2442-2613<br/>
                E-mail: cobranca03@reisoffice.com.br</b></p>
    </div>
    <button class="accordion"><font size="5" >É pessoa Fisica ou revenda?</font></button>
    <div class="panel">
        <p style="color:#B28F4C">&raquo;Entre em contato conosco nos nossos canais de atendimento:<br/> 
        <b>Telefone:(11)2442-2613<br/>
            E-mail: cobranca03@reisoffice.com.br</b></p>
        <p style="color:#B28F4C">&raquo;Atualize seu boleto Itaú diretamento no site:<a style="color:#B28F4C" href="https://www.itau.com.br/servicos/boletos/atualizar/" target="_blank"><b>https://www.itau.com.br/servicos/boletos/atualizar/</b></a><br/></p>
        <p><b>Confira o passo a passo para atulizar boleto no site do Itaú.</b></p>
        <p><b>Opçao 01:Digite a Representação Numérica<br/>
        <img src="<?=  base_url("images/boleto01-01.png")?>" alt="Confira o passo a passo para atulizar boleto no site do Itaú">
        Opção 02:Digite Agência/Código do Cedente e Nosso Número</b><br/></p>
        <img src="<?=  base_url("images/boleto02-01.png")?>" alt="Confira o passo a passo para atulizar boleto no site do Itaú">
    </div>
    <hr>
    <p style="color: #537f95;font-size: 21px;">Orientações sobre boletos</p>
        <p style="color: #315160">O uso do boleto proporciona comodidade a quem realiza o pagamento. Pensando nisso, a REIS OFFICe traz orientações com boas práticas sobre os pagamentos.<br/><br/>
            No ato do pagamento <b>verifique todas as informações demonstradas</b> em seu canal de atendimento do banco (CNPJ, nome da empresa, valor, etc).<br/><br/>
            <b>Trabalhamos apenas com os bancos Itaú e Banco do Brasil</b> e qualquer boleto emitido por outra instituição deve ser ignorado.<br/><br/>
        <b>Utilizamos apenas nosso e-mail corporativo para envio de boletos (@reisoffice.com.br).</b><br/><br/>
        A REIS OFFICE tem estimulado seus parceiros a trabalharem com o DDA (Débito Direto Autorizado), produto desenvolvido pela Febraban para reduzir as irregularidades no meio bancário.<br/><br/>
        Em caso de dúvidas relacionadas ao pagamento, entre em contato conosco para mais informações.<br/><br/>
        Atenciosamente,</p><br/><br/>
        
        <p style="text-transform: uppercase"><b>Departamento Financeiro</b>
        <p style="color: #e47b38;"><b>Tel.:11 2442-2670<br/>cobranca@reisoffice.com.br</b></p>
    </div>
    
    
    <div class="span5">
        <img src="<?=base_url("images/Boletos-01.png")?>" alt="Faturas e segunda via de boletos">
    </div>

</div>
<br/><br/>
</div>