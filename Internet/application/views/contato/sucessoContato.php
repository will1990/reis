<div class="container">
    <div class="row" style="padding-top: 20px; padding-bottom: 20px;">
        <div class="span7 text-right">
            <p style="color: #e47b38; font-weight: normal; font-size: 25pt; padding-top: 60px;">
                Cadastro Realizado
            </p>
            <p style="color: #000; font-size: 1.5em; line-height: 27px; padding-top: 5px;">
                <?=$descricao?>
            </p>
        </div>
        <div class="span5">
            <img class="img-responsive" src="<?= base_url("images/visual/cadRealizado.png")?>"/>
        </div>        
    </div>
</div>