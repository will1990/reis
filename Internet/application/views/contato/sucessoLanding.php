<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="padding-top: 150px; padding-bottom: 20px;">
                <div class="col-md-7 text-right">
                    <p style="color: #e47b38; font-weight: normal; font-size: 25pt; padding-top: 60px;">
                        Cadastro Realizado
                    </p>
                    <p style="color: #000; font-size: 1.5em; line-height: 27px; padding-top: 0px;">
                        Aguarde Contato dos nossos Consultores
                    </p>
                    <p style="font-size: 1.2em; color: #000;">
                        Acesse: <a href="" target="_blank">www.reisoffice.com.br</a>
                    </p>
                </div>
                <div class="col-md-5">
                    <img class="img-responsive" src="<?= base_url("images/visual/cadRealizado.png")?>"/>
                </div>        
            </div>
        </div>
    </body>
</html>