<div class="container">
    <div class="row"> 
        <br/>
        <div class="span12">
            <ul class="breadcrumb" style="margin-bottom:20px">
                <li data-original-title=""><a href="<?=  base_url() ?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?=  base_url('home/contato') ?>">Contato</a> <span class="divider">/</span></li>
                <li class="active" data-original-title="">Trabalhe Conosco</li>
            </ul>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="span6">
            <h4><font color="#e47b38"> </font></h4>
            <p>Tem interesse em trabalhar na Reis Office? Para enviar-nos seu Curriculo, basta preencher os dados abaixo e anexar um documento com as informações completas.</p>
            <div id="note"></div>
            <div id="fields" class="contact-form">
                <?php
                if($this->session->flashdata("arquivo"))
                {
                    echo '<div class="alert alert-danger">'.$this->session->flashdata("arquivo").'</div>';                    
                }
                ?>
                <form enctype="multipart/form-data" method="post" name="form_upload" id="form_upload" action="<?= base_url("contato/salvar") ?>">
                    <div class="controls controls-row">
                        <input style="width:253px" name="trabNome" class="span3" type="text" placeholder="Nome (Obrigatório)" required="">
                        <input name="trabEmail" class="span3" type="email" placeholder="E-mail (Obrigatório)" required="">
                    </div>
                    <div class="controls controls-row">
                        <input id="formsFone" class="span3"  name="trabFone" type="text" class="form-control" required="" maxlength="15" placeholder="DDD+Telefone">
                    </div>
                    <div class="controls controls-row"></div>
                    <div class="controls controls-row">
                        <input style="padding-bottom:5px" type="file" name="trabArq" id="fileToUpload">
                    </div>
                    <div class="controls controls-row"></div>
                    <div class="controls controls-row">
                        <textarea style="width:555px" id="mensagem" name="trabMsg" class="span6" placeholder="Digite aqui sua mensagem" rows="10"></textarea>
                    </div>
                    <input id="submit2" type="submit" class="btn btn-default" name="actTrabalhe" value="ENVIAR CONTATO" style="line-height:35px; height:40px; padding-left:10px; padding-right:10px;">
                    <input type="hidden" name="controller" value="trabalhe"/>
                </form>
            </div>            
        </div>
        
        <div class="span6">
            <img src="<?=base_url('images/visual/trabalhe.jpg')?>" style="padding: 4px; border: 1px solid #EEE;">
        </div>
        
    </div>
    <br/><br/>
    <?=$nossas_marcas?> 
</div>