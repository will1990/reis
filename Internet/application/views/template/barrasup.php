<div style="background:#537f95; width:100%; clear:left; min-height:67px; color:#FFF; overflow:hidden">
    <div style="width:1180px; margin:0 auto; display:block;">
        <div style="color:#FFF; width:55%; line-height:67px; text-transform:uppercase; float:left;">
            <?php if ($tituloBarra->titulo == "Produtos"):?>
            <p style="font-size:<?=$tituloBarra->fontSize?>; padding-top:18px; font-weight:norma; line-height:30px">
                <?=$tituloBarra->titulo?>
            </p>
            <?php else:?>
            <h1 style="font-size:<?=$tituloBarra->fontSize?>; padding-top:0px; font-weight:normal; line-height:30px">
                <?=$tituloBarra->titulo?>
            </h1>
            <?php endif;?>
        </div>
        <div style="font-size:34px; color:#FFF; width:45%; line-height:50px; float:left;" align="right"> 
            <?php if(isset($botoesBarra) && $botoesBarra): ?>
                <?php foreach($botoesBarra as $botao): ?>
                    <a class="btn btn-large btn-inverse" href="<?=$botao->url?>" style="height:30px; line-height:30px; margin-bottom:7px; padding:5px 15px 5px 15px; font-size:14px;"><?=$botao->titulo?></a> 
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?= !empty($imgBarra)? $imgBarra : "" ?>
<?= !empty($formBarra)? $formBarra : "" ?>
