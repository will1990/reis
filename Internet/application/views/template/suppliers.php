<br/>
<div class="row our-clients">
    <div class="span12">
        <p class="sub-titulo-h3">Nossas Marcas</p>
    </div>

    <style>
        .client-img img {
            filter: url(filters.svg#grayscale); /* Firefox 3.5+ */
            filter: gray; /* IE6-9 */
            -webkit-filter: grayscale(1); /* Google Chrome & Safari 6+ */
            opacity: 0.4;
            filter: alpha(opacity=40);
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }

        .client-img img:hover {
            filter: none;
            -webkit-filter: none;
        }
    </style>

    <div class="span2">
        <div class="client-img"><a href="<?= base_url("produtos/BROTHER")?>"><img src="<?= base_url("images/logos/brother.jpg")?>" alt="" style="margin:0 auto; display:block" ></a></div>
    </div>
    <div style="width:49px; float:left; height:1px;"></div>
    <div class="span2">
        <div class="client-img"><a href="<?= base_url("produtos/CANON")?>"><img src="<?= base_url("images/logos/canon.jpg")?>" alt="" style="margin:0 auto; display:block" ></a></div>
    </div>
    <div style="width:49px; float:left; height:1px;"></div>
    <div class="span2">
        <div class="client-img"><a href="<?= base_url("produtos/KYOCERA")?>"><img src="<?= base_url("images/logos/kyocera.jpg")?>" alt="" style="margin:0 auto; display:block" ></a></div>
    </div>
    <div style="width:49px; float:left; height:1px;"></div>
    <div class="span2">
        <div class="client-img"><a href="<?= base_url("produtos/OKIDATA")?>"><img src="<?= base_url("images/logos/oki.jpg")?>" alt="" style="margin:0 auto; display:block" ></a></div>
    </div>
    <div style="width:49px; float:left; height:1px;"></div>
    <div class="span2">
        <div class="client-img"><a href="<?= base_url("produtos/OLIVETTI")?>"><img src="<?= base_url("images/logos/ollivetti.jpg")?>" alt="" style="margin:0 auto; display:block" ></a></div>
    </div>
</div>
<hr style="visibility: hidden">
<p style="text-transform: uppercase ;font-size:20px; padding-top:9px; padding-bottom:9px; color:#999; font-weight:normal">Líder em Outsourcing, Equipamentos e Soluções para Impressão</p>
<hr style="visibility: hidden">