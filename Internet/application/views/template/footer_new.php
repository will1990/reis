<footer style="margin-top: 0">
    <div class="container" style="background-color:#e47b38;">
        <br/>
        <div class="row">
            <div class="span3b" style="border: 0px; margin-bottom: 20px;">
                <p class="sub-titulo-h3-rodape">SIGA-NOS</p>
                <p>A Reis Office está presente nas principais rede sociais, siga-nos e fique por dentro de todas as novidades.</p>
                <br/>
                <div class="socials">
                    <a target="_blank" href="https://www.facebook.com/ReisOfficeProducts"><img src="<?= base_url("images/visual/Facebook.png")?>" alt="Facebook Reis Office" style="width: 15%"/></a> 
                    <a target="_blank" href="https://twitter.com/reisofficereal"><img src="<?= base_url("images/visual/Twitter.png")?>" alt="Twitter Reis Office" style="width: 15%"/></a> 
                    <a target="_blank" href="https://plus.google.com/+reisoffice/posts"><img src="<?= base_url("images/visual/G+.png")?>" alt="Google Plus Reis Office" style="width: 15%"/></a> 
                    <a target="_blank" href="https://www.linkedin.com/company/reis-office-products-ltda-?trk=biz-companies-cym"><img src="<?= base_url("images/visual/Linkedin.png")?>" alt="Linkedin Reis Office" style="width: 15%"/></a>  
                    <a target="_blank" href="https://www.youtube.com/user/ReisOfficeProducts"><img src="<?= base_url("images/visual/Youtube.png")?>" alt="Youtube Reis Office" style="width: 15%"/></a>
                </div>
                <br/>
                <p style="font-size: 0.8em; color: #885332;"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"></a>
                    <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Site</span> de 
                    <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.reisoffice.com.br" property="cc:attributionName" rel="cc:attributionURL" style="color: #885332;">Reis Office</a> 
                    possui Licen&ccedil;a
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/" style="color: #885332;">: Creative Commons</a>
                    Baseado em <a xmlns:dct="http://purl.org/dc/terms/" href="Reisoffice.com.br" rel="dct:source" style="color: #885332;">Reisoffice.com.br</a>. 
                </p>
            </div>
            <div class="span3b" style="border: 0px; margin-bottom: 20px;">
                <p class="sub-titulo-h3-rodape">NEWSLETTER</p>
                <p>Cadastre seu e-mail e receba nosso informativo com conteudos exclusivos sobre o mercado de outsourcing de impressão e muito mais.</p>
                <br/>
                <form class="form-search">
                    <input type="text" class="input-medium">
                    <button type="submit" style="display: inline; background-color: #537f95; color: white; width: 50px; height: 29px; border: none; margin-top: 0px; margin-left:0px; border-radius:0 10px 0 0;">
                        <i class="icon-circle-arrow-right"></i>
                    </button>
                </form>
                <br/>
                <p>
                    <img src="<?= base_url("images/visual/footer/sistemaGestao.png")?>" alt="Sistema de Gestão" style="width: 90px"/>
                    <img src="<?= base_url("images/visual/footer/empresaCidada.png")?>" alt="Selo Empresa Cidadã" style="width: 90px"/>
                </p>
            </div>
            <div class="span3b" style="border: 0px; margin-bottom: 20px;">
                <p class="sub-titulo-h3-rodape">SITEMAP</p>
                <p>
                    <a href="<?= base_url() ?>">Blog</a><br/>
                    <a href="<?= base_url() ?>">Boletos/Faturas</a><br/>
                    <a href="<?= base_url() ?>">Contato</a><br/>
                    <a href="<?= base_url() ?>">Loja virtual</a><br/>
                    <a href="<?= base_url() ?>">Notícias</a><br/>
                    <a href="<?= base_url() ?>">Outsourcing</a><br/>
                    <a href="<?= base_url() ?>">Produtos</a><br/>
                    <a href="<?= base_url() ?>">Revenda</a><br/>
                    <a href="<?= base_url() ?>">Serviços</a><br/>
                    <a href="<?= base_url() ?>">Suporte técnico</a><br/>
                    <a href="<?= base_url() ?>">Trabalhe conosco</a><br/>
                    <a href="javascript:void(0)" data-toggle="popover" data-placement="top" data-content="
                        <a href='<?= base_url() ?>' style='color:#575756'>Business Inteligence</a><br/>
                        <a href='<?= base_url() ?>' style='color:#575756'>Firewall</a><br/>
                        <a href=''<?= base_url() ?>' style='color:#575756'>Portal Service</a><br/>
                        <a href='<?= base_url() ?>' style='color:#575756'>Representantes</a><br/>
                        <a href=''<?= base_url() ?>' style='color:#575756'>Webmail</a><br/>
                       " title="" data-original-title="Área Restrita">Área restrita</a>
                </p>
            </div>
            <div class="span3b" style="border: 0px; margin-bottom: 20px;">
                <p class="sub-titulo-h3-rodape">ENDEREÇO</p>
                <p>
                    <i class="icon-map-marker"></i> Rua Francisco Antunes, 598<br/>
                    <i class="icon-map-marker" style="color: transparent"></i> Guarulhos - SP<br/>
                    <i class="icon-map-marker" style="color: transparent"></i> CEP: 07040-010<br/>
                    <br/>
                    <i class="icon-map-marker" style="color: transparent"></i> ATENDIMENTO<br/>
                    <i class="icon-map-marker" style="color: transparent"></i> De segunda à sexta<br/>
                    <i class="icon-map-marker" style="color: transparent"></i> das 08:00 às 18:00<br/>
                    <br/>
                    <i class="icon-map-marker" style="color: transparent"></i> CONTATO<br/>
                    <i class="icon-phone" style=""></i> 55 (11) 2442-2600<br/>
                    <i class="icon-envelope-alt" style=""></i> cac@reisoffice.com.br<br/>
                </p>
            </div>
        </div>
    </div>
</div>
    
</footer>

<script type="text/javascript" src="<?= base_url('assets/jQuery-2.2.4/jquery-2.2.4.js')?>"></script>

<script type="text/javascript" src="<?= base_url('assets/bootstrap/js/bootstrap.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/DataTables-1.10.13/js/jquery.dataTables.min.js')?>"></script>

<script type="text/javascript" src="<?= base_url('assets/bootstrap/bootbox.min.js')?>"></script>

<script type="text/javascript" src="<?= base_url('assets/SweetAlert/sweet-alert.min.js')?>"></script>
<!--fancyBox3-->
<script src="<?= base_url("assets/fancyapps/source/jquery.fancybox.js")?>"></script>
<!-- Script responsável pelo Form de Cadastro de Email, presente no rodapé -->
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCkt-5A5wY2aEFv_q2bHozTo3fiV4-c7-Y"></script>
<script type="text/javascript">
    //function validEmail(){  
    $("#formEmail").on("submit", function(){  
        $.ajax({
            type: "POST",
            url: "<?= base_url('contato/cadEmail') ?>", 
            data: {
                txtEmail: $("#txtEmail").val(),
                origemEmail: $("#origemEmail").val()
            },
            dataType: "text",  
            cache: false,
            success:
            function(data){
                bootbox.alert({                   
                    size: "small",
                    message: data
                });                
            }
        });// you have missed this bracket
        return false;
    });        
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({
            placement : 'bottom'
        });
        
        $('[data-toggle="popover"]').popover({html:true});
        
        $('[data-fancybox="images"]').fancybox();
        
        $(".produto-pagina").on("click", function(e){
            e.preventDefault();
            pag = $(this).data("pagina");
            url = $(this).data("href");
            
            console.log(pag)
            console.log(url)
            console.log("<?= $this->session->produto_pagina?>")
            
            if(url != "#")
            {
                $.ajax({
                    url: "<?= base_url("produtos/produto_pagina")?>/"+ pag,
                    success: function(data) {
                        if(data == "1")
                            window.location.href = url;
                    }
                })
            }
        })
    });
    
    var myApp;
    myApp = myApp || (function() {
        var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myPleaseWaitDialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processando...</h4></div><div class="modal-body"><div class="progress progress-striped active"><div class="progress-bar" style="width: 85%"></div></div></div></div></div></div>');
        return {
            showPleaseWait: function(msg) {
                if(msg != null)                            
                    pleaseWaitDiv.find('h4').html(msg);
                pleaseWaitDiv.modal('show');
            },
            hidePleaseWait: function () {
                pleaseWaitDiv.modal('hide');
            }
        };
    })();
    //}    
</script>

<?= (isset($js)) ? $js : "" ?>

<script src="https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">
_uacct = "UA-1326185-3";
urchinTracker();
</script>

<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038922210;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038922210/?guid=ON&amp;script=0"/>
</div>
</noscript>

<script language="javascript" DEFER="DEFER">
(function(a,e,c,f,g,h,b,d){var k={ak:"1038922210",cl:"zu06CPik-WYQ4uOy7wM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");

_googWcmGet('number1', '(11) 2442-2600');
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

