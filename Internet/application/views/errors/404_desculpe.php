<div class="container">
    <div class="row"> 
        <div style="background: url(<?= base_url("images/conteudo/pag_erro_back.png")?>) center center no-repeat; height: 211px; padding: 20px">
            <p style="color: #e47b38; 
                    font-size: 2.5em; 
                    font-weight: normal; 
                    line-height: 40px; 
                    text-align: right; 
                    padding-right: 510px;
                    padding-top: 20px">
                O link que está tentando acessar<br/>pode ter sido modificado.            
            </p>
            <p style="color: #000; 
                    font-size: 1.5em; 
                    font-weight: normal; 
                    line-height: 27px; 
                    text-align: right; 
                    padding-right: 510px;
                    padding-top: 20px">
                Por favor, pesquise o que procura no <b>campo de busca</b><br/>ou no <b>menu</b>, ambos no topo da página.
            </p>
        </div>
    </div>
</div>
