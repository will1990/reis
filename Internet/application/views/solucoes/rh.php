<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos</strong><br/>
        <br>
    </span></p>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Manipular e manter documentos pessoais são alguns dos grandes desafios de qualquer empresa. Encontrar documentos de forma rápida é uma das maiores dificuldades que as empresas com alto turnover possuem. 
    Quando uma empresa é acionada judicialmente, esta documentação precisa ser encontrada o quanto antes, de forma organizada, facilitando os recursos cabíveis. </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Para acabar com este problema, a Reis Office oferece serviços que melhoram sua produtividade. Digitar em um campo de pesquisa e trazer todas as informações relacionadas a este campo consultado é um dos grandes desafios das empresas, e isto já é possível hoje com as soluções de GED. Com ela podemos pesquisar campos específicos e trazer toda a documentação e cadastro de um funcionário, por exemplo. 
    Com isso, reduzimos o tempo de pesquisa e agilizamos os processos que eventualmente as empresas são submetidas. Com nossas soluções de digitalização de documentos, conseguimos dar inteligência e agilidade em todo o ciclo de vida do documento, otimizando os processos da empresa, eliminando manipulação de documentos evitando erros e desperdícios. </p>
<hr size="1">
<p style="text-align:justify;  color:#db7b3e">- <strong>Soluções de Workflow de documentos</strong><br>
    <br>
</p>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Algumas empresas possuem dificuldades no processo admissional, pois a matriz fica longe da filial. Ou seja, os documentos dos candidatos precisam ser enviados por correio e alguns por e-mail. Porém ambos os métodos não são eficientes, o correio dispõe de uma logística pessoal para o envio e não é instantâneo além do risco da perda do documento, o e-mail por sua vez não é organizado via fluxo (Workflow) sendo assim o usuário precisa digitalizar e enviar para outro usuário os anexos, porém estes mesmos anexos possuem limitações em tamanho (MB).</p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Visualizando este cenário a Reis Office desenvolveu uma solução prática para a inserção e distribuição destes documentos via Workflow. Cada unidade desta empresa terá acesso ao portal WEB CAPTURE da NSI para inserir e indexar os documentos dos candidatos basta digitalizar os documentos para uma pasta e depois fazer upload do arquivo na WEB. Com esta solução a empresa não necessitará instalar o software de captura em todas as unidades, apenas utilizar a versão WEB e a entrega do documento é instantânea. </p>
<hr size="1">


<p style="text-align:justify; color:#db7b3e">- <strong>Soluções para fluxo de trabalho</strong><br>
    <br>
</p>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Todas as empresas possuem uma demanda no setor de Recursos Humanos, o processo documental deste setor é primordial, pois armazena todas as informações relacionadas ao vinculo empregatício. Muitas vezes estas informações se perdem ou não são localizadas facilmente, pela quantidade e longevidade do processo, além dos processos cadastrais na Admissão existem os documentos recorrentes do vínculo empregatício assim como o processo demissional. </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Pensando na melhoria deste processo, a Reis Office desenvolveu uma solução prática e viável. Criamos três fluxos de trabalho: O primeiro é o Admissional, o segundo Recorrente e o terceiro Demissional. Com a solução de digitalização por fluxos a empresa digitaliza ao longo dos vínculos empregatícios dela com os seus colaboradores, facilitando a inserção e busca destes documentos, reduzindo a perda em 100%.</p>
<p style="text-align:justify">&nbsp;</p>



