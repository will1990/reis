<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p>
<br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p>Digitalização de 5.000 relatórios de bordo para empresa de transportes. Motoristas e cobradores produzem relatórios de horário de saída e chegada todos os dias, sistema parecido com a folha de ponto. Departamento de Recursos Humanos necessita de acesso digital e rápido dos horários para compor as horas trabalhadas e gerar a folha de pagamento.        </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Utilizar Autostore para indexar o relatório e realizar o OCR Zonal nos campos de documento estruturado. Esta solução reduzirá a busca deste documento drasticamente para alguns minutos, melhoria na leitura, evitando perdas e checagem manual. <br>
</p>
<p style="text-align:justify">&nbsp;</p>