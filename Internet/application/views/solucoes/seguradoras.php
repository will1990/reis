<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p>
<br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p>As seguradoras possuem uma demanda documental no processo de vistoria. Este processo por sua vez é conclusivo e obrigatório para todos os clientes e novos segurados. O processo é criado á partir de fotos e descrições técnicas, o processo é manual e todas as fotos tiradas pela câmera passam por um processo de indexação manual depois de serem descarregadas no computador. Algumas fotos não estão sendo indexadas e costumam ser misturadas a outros processos de vistoria.</p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">A proposta de melhoria para este projeto é a utilização do software de captura NSI Mobile. O NSI Mobile é um aplicativo capaz de capturar as imagens a partir da câmera do celular, além disso, ele indexa e envia o arquivo para o setor envolvido via ReisOffice GED, o processo é definitivamente simples e rápido, diminuindo o tempo de operação dos técnicos e responsáveis pela liberação. <br>
</p>
<p style="text-align:justify">&nbsp;</p>