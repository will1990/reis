<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p>
<br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Uma empresa de logística necessita de agilidade em seu processo de negócio, pois todas as entregas e pedidos são efetuadas via papel, ou seja, independente da origem do processo, a empresa imprime o pedido para que seja encaminhado a entrega ao destino. Além disso, depois de checado o documento precisa ser assinado, o que gera outra demanda que é o armazenamento. </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Analisando este projeto a Reis Office propôs a eliminação do papel em parte do processo, os pedidos que eram impressos começaram a serem capturados eletronicamente via AutoCapture, ou seja eles serão adicionados no GED e indexados na origem. Para o setor de entregas foi criado um formulário digital que possibilita a assinatura do destinatário no próprio Smartphone ou Tablet via NSi Mobile. Utilizando este processo todos os documentos assinados serão encaminhados automaticamente para a empresa fazer o controle de entrega.  </p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify">&nbsp;</p>