<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p>
<br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Muitos Escritórios de Contabilidade fazem a gerencia contábil para mais de uma empresa, ou seja, seu número de clientes cresce a cada dia. Por conta disso o controle e gestão documental precisa ser organizado, além disso, toda empresa possui os seguintes processos: 
    <br>
    - Fiscal (Gestão de impostos)
    <br>
    - NF (Venda e Compra)
    <br>
    - Folha de Pagamento
    <br>
    - IR (Físico e Jurídico)
    <br>
    - Controles e Balanços (Financeiro)
    <br>
    - Contratos - Inscrições e gestão patrimonial </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Para atendermos este cliente vamos propor uma solução embarcada para direcionar cada documento para o devido Workflow, além disso, organizar todos os documentos destes clientes separadamente (Bibliotecas) utilizando o AutoCapture para documentos eletrônicos como DANFE e guias de impostos. <br>
    A solução visa diminuir o tempo de busca ao documento e armazenamento seguro.</p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify">&nbsp;</p>