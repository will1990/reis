<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p><br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Nos escritórios de Advocacia os documentos possuem um valor inestimável, por se tratarem de documentos processuais os mesmos precisam ser armazenados com total segurança. Quando o documento é gerado eletronicamente ele passa por diversas operações judiciais, certamente ele será armazenado novamente só que em PAPEL, começando o processo da guarda física do documento. Porém a busca do documento é complementar ao trabalho do advogado e nem sempre ele necessita alterar o documento apenas pesquisar informações. A pesquisa demora, pois o documento está armazenado fisicamente e é difícil de encontrá-lo.
</p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">O processo documental de um escritório de advocacia é grande, todo o trabalho fornecido gera demanda em papel e armazenamento. Com a prática solução do ReisOffice GED + Autostore pode-se coordenar todas a digitalizações, indexações e armazenamentos, em segundos. A pesquisa sobre a informação é veloz e não gera esperas por parte dos advogados ou bibliotecários do escritório. Sendo assim aperfeiçoam-se todos os processos com apenas um CLIQUE!  </p>
<hr size="1">
<p style="text-align:justify;  color:#db7b3e">- <strong>Soluções para Peticionamento eletrônico</strong><br>
    <br>
</p>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">De acordo com a lei 11.419/06, autorizada pela resolução nº 04/08, os escritórios de advocacia podem protocolar as petições iniciais e intermediárias de forma eletrônica. Entretanto, existem limites para o tamanho do arquivo, entre outras especificações.</p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">O peticionamento eletrônico ajudou a agilizar os processos no sistema judiciário brasileiro e, para se adequar a esta tecnologia, oferecemos equipamentos compatíveis com o sistema. Os arquivos são criados de acordo com a legislação vigente, poupando tempo e garantindo os resultados desejados.  </p>
<p style="text-align:justify">&nbsp;</p>