<p style="text-align:justify"><span style="text-align:justify;  color:#db7b3e">- <strong>Soluções para gerenciamento eletrônico de documentos e fluxo de trabalho</strong></span></p>
<br/>
<p style="text-align:justify"><strong>Problemas Identificados</strong></p>
<p style="text-align:justify">Todas as concessionárias possuem um processo documental extenso por conta da demanda de vendas e serviços que oferecem. Os processos mais importantes deste segmento são os de compra de veículos e venda. Portanto é muito importante armazenar de forma segura todas as informações destes processos, pois ambos utilizam documentos pessoais como CPF, RG, Renavan, Chassis e todos os documentos relacionados ao carro. 
    <br>
    Este processo é extremamente moroso, pois todos os arquivos e fichas são guardados manualmente depois de serem copiados. </p>
<p style="text-align:justify"><strong>Soluções</strong></p>
<p style="text-align:justify">Visualizando este cenário a proposta da Reis Office é digitalizar os documentos na origem, ou seja, fazer a gestão de todos os documentos deste processo no momento em que ele se inicia, pois uma vez que o operador ou vendedor digitalizar os documentos do processo, o mesmo pode ser devolvido para o dono, assim como o software armazenará a informação instantaneamente com os dados dos clientes. Além de reduzir o tempo em que se organiza estes documentos o projeto aumenta o nível de segurança da informação.  </p>
<p style="text-align:justify">&nbsp;</p>
<p style="text-align:justify">&nbsp;</p>