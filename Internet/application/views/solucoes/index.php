<div class="container">
    <aside class="span4b" style="margin-left:0px;"> 
        <h4 class="titEmp">SOLUÇÕES</h4><br/>            
        <link rel="stylesheet" href="<?=base_url("assets/custom/css/estiloSolucoesReis.css")?>" />

        <div class="bs-example" data-example-id="vertical-button-group" style="text-transform:none;">

            <div class="btn-group-vertical" role="group" aria-label="Vertical button group" style="text-transform:none;">

                <?php
                foreach ($solucoes as $idx => $solucao):
                    ?>                                
                    <div class="btn-group" role="group" style="border-bottom:1px solid #DDD">
                        <a href="<?=base_url("outsourcing/solucoes/$idx")?>" class="btn btn-default2" style="font-weight: 400;width:auto; line-height: 1.42857143;padding: 9px 12px; margin-bottom: 0px;border: 1px solid transparent;"> <i class="icon-chevron-right"></i> <?=$solucao->nome?></a>
                    </div>
                    <?php
                endforeach;
                ?>       

            </div>
        </div>
        <p style="text-align:justify"> <a class="btn btn-large btn-inverse" href="<?=base_url("outsourcing")?>" style="padding:10px 15px; font-size:16px;">VOLTAR PARA OUTSOURCING</a></p>

    </aside>
    <div class="span8b">
        <br/>
        <ul class="breadcrumb">
            <li data-original-title=""><a href="<?=base_url()?>">Home</a> <span class="divider">/</span></li>
            <li class="active" data-original-title=""><a href="<?=base_url("outsourcing")?>">Outsourcing</a> <span class="divider">/</span></li>
            <li class="active" data-original-title="">Soluções <span class="divider">/</span></li>
            <li class="active" data-original-title=""><?=$bread?></li>
        </ul>
        <br/>
        <?=$conteudo?>
        <br/><br/>
    </div>
</div>

