<div class="container">
    <div class="row"> 
        <?=$menuEsq?>
        <div class="span8b">
           <?php if($menu_categoria): ?>
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= url_decode($menu_categoria)?></font></h1>
            <?php endif; ?>
            <?php if($produtos): ?>                
                <?php foreach($produtos as $key => $item): ?>
                <a href="<?= base_url("produtos/$chave_familia/$chave_linha/$chave_categoria/$item->CPROD")?>">  
                        <div class="span3cm wrap" style="padding-right:8px"> 
                            <div style="width:215px; height:218px; border:1px solid #EEE; display:block; text-align: center; vertical-align: middle">
                                <img style='max-height:215px;' class='center-block' src='<?= base_url("produtos/imagens/$item->CPROD/imagem.jpg")?>' alt='<?= $item->PORTAL_DESCRICAO_CURTA?>'/>
                            </div>
                            <ul style="width:215px; height:100px;" class="socials-member">		  
                                <h4 style="text-transform:none; color:#db7b3e; font-weight:normal; display:inline"><?= $item->PRODUTO?></h4>
                                <br>
                                <span class="job" style=" display:inline"></span>
                            </ul>
                        </div>
                    </a>
                    <?php if(empty($menu_familia) && $key == 4) { break; } ?>
                <?php endforeach; ?>
                <?php if(empty($menu_familia)): ?>
                    <a class="btn btn-primary pull-right" href="<?= base_url("produtos")?>">Ver Mais >></a>
                <?php endif; ?>
            <?php endif; ?>                
            <br/><br/>            
        </div>
        <div class="span2">
            <button type="button" class="btn btn-large btn-inverse">DESTAQUES</button>
        </div>
    </div>  
</div>