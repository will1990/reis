<div class="container">
    <div class="row"> 
        <?= $menuEsq ?>

        <div class="span8b">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title="">
                    <a href="<?= base_url("produtos") ?>">Orçamento</a>      
                </li>
            </ul>

            <div class="row">
                <div class="span8b">
                    <h1 style="font-size:17.5px"><font color="#e47b38">Orçamento Nº <?= $orcamento->ID?></font></h1>  
                    
                    <p>
                        Seu orçamento foi enviado com sucesso.<br/>
                        Aguarde que em breve um de nossos consultores entrará em contato.
                    </p>

                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <td width="10%">Foto</td>
                                <td width="75%">Descrição</td>
                                <td width="10%">Quantidade</td>
                            </tr>                            
                            <?php if ($orcamento):?>
                                <?php foreach ($orcamento->ITENS as $id => $item):?>
                                    <tr id=" tr_detalhe_orcamento">
                                        <td style="vertical-align: middle; text-align: center"><img src="<?= base_url("produtos/imagens/$item->COD_PRODUTO/imagem_$item->COD_PRODUTO.jpg")?>" width="50"></td>
                                        <td style="vertical-align: middle"><?= $item->PRODUTO->PRODUTO?></td>
                                        <td style="vertical-align: middle; text-align: center"><?= $item->QUANTIDADE?></td>
                                    </tr>
                                <?php endforeach;?>
                            <?php endif;?>
                        </thead>
                    </table>
                    
                    <h1 style="font-size:17.5px"><font color="#e47b38">Checkout</font></h1>  
                        
                    <div class="form-group">
                        <strong>Nome:</strong> <?= $orcamento->DETALHE->NOME?><br/>
                        <strong>E-mail:</strong> <?= $orcamento->DETALHE->EMAIL?><br/>
                        <strong>Telefone:</strong> <?= $orcamento->DETALHE->TELEFONE?><br/>
                        <strong>CPF/CNPJ:</strong> <?= $orcamento->DETALHE->CPFCNPJ?><br/>
                        <strong>CEP:</strong> <?= $orcamento->DETALHE->CEP?><br/>
                        <strong>Cidade:</strong> <?= $orcamento->DETALHE->CIDADE?><br/>
                        <strong>Estado:</strong> <?= $orcamento->DETALHE->ESTADO?><br/>
                        <strong>Observações:</strong><br/><?= $orcamento->DETALHE->OBSERVACOES->load()?>
                    </div>                    
                </div>
            </div>
            
        </div>
    </div>
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>

</div>