<div class="container">
    <div class="row"> 
        <?= $menuEsq ?>

        <div class="span8b">
            <br/>
            <ul class="breadcrumb"> 
                <li data-original-title=""><a href="<?= base_url() ?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title="">
                    <a href="<?= base_url("produtos") ?>">Orçamento</a>      
                </li>
            </ul>

            <div class="row-fluid">
                
                <p style="margin:20px 0 20px 0px; font-size:17.5px; color:#e47b38; text-transform: uppercase; font-weight: bold;">Preencha seus dados que entraremos em contato.</p>
                
                <form action="<?= base_url("orcamento/index") ?>" method="post" id="formOrcamento" name="formOrcamento">
                    <div class="span6">                        
                        <input type="text" class="form-control span10" name="nome" placeholder="Nome (Obrigatório)" required=""/>                            
                        <input type="text" maxlength="18" class="form-control span6" id="cnpj" name="CNPJ" OnKeyUp="mascCnpjCpf(this);" placeholder="CPF/CNPJ" required=""/>                  
                        <input type="text" class="form-control span7" id="input-estado" name="estado" readonly/>                            
                            
                        <select class="span7"  id="estados" name="estados" required/> >
                            <?php foreach ($estados as $estado): ?>
                                <option value="<?= $estado->UF ?>"><?= $estado->UF?></option>
                            <?php endforeach; ?>
                        </select>                        
                    </div>
                    <div class="span6">
                        <input type="email" class="form-control span10" name="email" placeholder="E-mail (Obrigatório)" required=""/>
                        <input type="text"  maxlength="15" class="form-control span6" name="telefone" id="formsFone" required="" placeholder="DDD+Telefone"/>                        
                        <input type="text" class="form-control span10" id="input-municipio" name="municipio"value="" readonly/>
                        
                        <select class="span10" name="municipios" required/>
                                 
                        </select>
                    </div>
                    <div class="row-fluid">
                        <select class="offset2 span8 offset2" name="conheceu" required="" >
                            <?php foreach ($optChc as $item): ?>
                                <option value="<?= $item ?>"><?= $item ?></option>
                            <?php endforeach; ?>
                        </select> 
                        
                        <textarea class="span12" rows="6" name="observacao" placeholder="Digite aqui sua mensagem"></textarea>             
                    </div>
                    <button style="text-align: right" type="submit" onsubmit="myApp.showPleaseWait()" class="btn btn-large btn-inverse" >Enviar</button>
                </form>
            </div>
                
            <!-- contato -->
            <!-- tabelas de produtos de orcamentos-->
            <div class="span8b">                                              
                <h1 style="font-size:17.5px"><font color="#e47b38">Produtos para orçamento</font></h1>  
                <table class="table table-bordered ">
                    <thead>
                        <tr>
                            <td width="5%">Excluir</td>
                            <td width="10%">Foto</td>
                            <td width="75%">Descrição</td>
                            <td width="10%">Quantidade</td>
                        </tr>

                        <?php if ($orcamentos): ?>
                            <?php foreach ($orcamentos as $id => $item): ?>
                                <tr id=" tr_detalhe_orcamento">
                                    <td style="vertical-align: middle; text-align: center"> <a href="<?= base_url("orcamento/excluir/$item->COD_PRODUTO") ?> ">Deletar</a></td>
                                    <td style="vertical-align: middle; text-align: center"><img src="<?= base_url("produtos/imagens/$item->COD_PRODUTO/imagem_$item->COD_PRODUTO.jpg") ?>" width="50"></td>
                                    <td  style="vertical-align: middle"><?= $item->PRODUTO->PRODUTO ?>
                                    </td>
                                    <td style="vertical-align: middle; text-align: center"><input value="<?= $item->QUANTIDADE ?>" class="orc-qtde" data-orcamento="<?= $item->ID_ORCAMENTO ?>" data-produto="<?= $item->COD_PRODUTO ?>" style="width: 50px; text-align: center"></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </thead>
                </table>
                <div align="center">
                    <a href="<?= base_url("produtos") ?>" class="btn btn-large btn-inverse" style="text-align: center; padding: 5px 19px; font-size:15px; text-transform:uppercase; margin-right: 90px;">Ver mais Produtos</a>
                </div>
            </div>
        </div>

        
    </div>
    <?= (isset($nossas_marcas)) ? $nossas_marcas : "" ?>
</div>


<!-- Modal--> 
<div class="modal hide fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div  class="modal-dialog modal-lg">
            <div class="modal-content">
                <form style="position: absolute" class="Orcinput"  action="<?= base_url("orcamento/insertOrc") ?>" method="post" onsubmit="return checkCheckBox(this)">
                    <div class="modal-body orca_color">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div>
                            <img style="width:80px;" align="right" src="<?=  base_url("images/CARRINHO_l-01.png")?>" />
                        </div>
                        <h4 class="modal-title"><font color="#6b4242"> Vimos que deixou produto em seu carrinho</font></h4></br>
                        <hr style="visibility: hidden"/>
                        <div>
                            <input style="margin-bottom: 10px" type="checkbox" name="nao_encontrou" id="notfound" value="1" ><font color="#653030" >&nbsp;Não encontrou o que precisava</font><br>
                            <input style="margin-bottom: 10px" type="checkbox" name="falta_inf" id="falta_inf" value="1"><font color="#653030">&nbsp;Falta de informação</font><br>
                            <input style="margin-bottom: 10px" type="checkbox" name="outros" id="outros" value="1"><font color="#653030">&nbsp;Outros &nbsp;<input  class="campo_color"type="text" name="outros"></font><br>
                        </div>
                        <hr style="visibility: hidden"/>
                        <div>                            
                            <label style="text-align: center; font-weight: bold">Caso queria mais informações, deixe seu e-mail:</label>
                            <input style="margin-left: 30px; width: 330px " type="email" id="email" name="email" value=""required/>  
                            <hr style="visibility: hidden"/>
                        
                            <input type="submit" style="padding: 5px 19px; font-size:15px; text-transform:uppercase; margin:auto auto auto 150px;" onsubmit="rastreio()"id="button-modal" class= "btn btn-primary btn-lg" value="Enviar" aria-hidden="true" />    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

