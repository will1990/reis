<script type="text/javascript">
    var rastreamento = "SAIDA";

    $(document).ready(function(){
        $(".orc-qtde").change(function(){
            orca = $(this).data("orcamento");
            prod = $(this).data("produto");
            qtde = $(this).val();
            myApp.showPleaseWait();
            $.ajax({
                url: "<?= base_url("orcamento/atualizar_quantidade")?>/"+ orca +"/"+ prod +"/"+ qtde,
                success: function(data){                    
                    myApp.hidePleaseWait();
                    if(data != "1")                        
                        alert("Erro ao atualizara quantidade.");
                },
                error: function(x, y, err){
                    myApp.hidePleaseWait();
                    console.log(x)
                }
            });
        });
        
       /* $('#myModal').on('show', function () {
            //alert('tttt');
           rastreio(); 
        })
        
        var contador = 0;        
        
        $("a").on("click", function(e){
            if(contador == 0)
            {
                var target = $(this).attr("href");
                
                if(target.indexOf("produtos") < 0 && target.indexOf("#") < 0)
                {       
                    e.preventDefault();
                    rastreamento = "NAVEGACAO";
                    rastreio();
                    $("#myModal").modal("show");
                    contador++;
                  //  alert(target);
                    //return false;
                }
                else
                {
                    rastreamento = "PRODUTOS";
                    rastreio();
                }
            }
        });*/
        
        /*$("html").mouseleave(function(){            
            rastreio();
            $("#myModal").modal("show");
            $("html").off("mouseleave");
        })*/
        
        getGeolocation();
    });
    
    function rastreio()
    {
        // Enviar a tag de rastreamento
       
        $.ajax({
            url: "<?= base_url("orcamento/rastreamento")?>/"+ rastreamento,
            success: function(){},
            error: function(x, y, err){
                console.log(x);
            }
        });
    }
    
    function checkCheckBox(f)
    {
        if (!f.nao_encontrou.checked && !f.falta_inf.checked && !f.outros.checked)
        {
            swal('Selecione algumas opções...');
            return false;
        }
        else
        {
            swal('Suas informações foram enviadas com sucesso.');            
            return true;
        }
    }

    $('#estados').change(function(){
        $('#municipios').load('<?=  base_url("orcamento/get_estado")?>'+$('#estados').val() );
    })

    function getGeolocation(){
        if ( navigator.geolocation )
        {
            navigator.geolocation.getCurrentPosition( function( parmetro ){
                console.log(parmetro)
                $.ajax({
                    url: "http://maps.googleapis.com/maps/api/geocode/json?latlng="+ parmetro.coords.latitude +","+ parmetro.coords.longitude +"&sensor=true",
                    dataType: "json",
                    success: function(data){
                        mun = data.results[0].address_components[4].long_name;
                        est = data.results[0].address_components[5].long_name;
                        $("#input-municipio").val(mun);
                        $("#input-estado").val(est);
                    },
                    error: function(x, y, err){
                        console.log(x);
                    }
                });
            });
        }
    }
</script>
