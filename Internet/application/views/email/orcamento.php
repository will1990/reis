<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Pedido de Orçamento</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>ORÇAMENTO Nº <?= $orcamento->ID?></h1>

	<div id="body">
            <p><b>Data e Hora: <?= $orcamento->DATA?></b></p>

            <p>Como conheceu: <?= $orcamento->DETALHE->COMO_CONHECEU?></p>
            <code><?= $orcamento->DETALHE->OBSERVACOES->load()?></code>

            <p>Dados Cadastrais</p>
            <table cellspacing='0' border='1' width='95%'>
                <tbody>
                    <tr>
                        <td width="15%">Nome</td>
                        <td width="35%"><?= $orcamento->DETALHE->NOME?></td>
                        <td width="15%">E-mail</td>
                        <td width="35%"><?= $orcamento->DETALHE->EMAIL?></td>
                    </tr> 
                    <tr>
                        <td width="15%">Fone</td>
                        <td width="35%"><?= $orcamento->DETALHE->TELEFONE?></td>
                        <td width="15%">CPF/CNPJ</td>
                        <td width="35%"><?= $orcamento->DETALHE->CPFCNPJ?></td>
                    </tr>
                </tbody>
            </table>
            <table cellspacing='0' border='1' width='95%'>
                <tbody>
                    <tr>
                        <td width="15%">CEP</td>
                        <td width="20%"><?= $orcamento->DETALHE->CEP?></td>
                        <td width="15%">Cidade</td>
                        <td width="30%"><?= $orcamento->DETALHE->CIDADE?></td>
                        <td width="10%">UF</td>
                        <td width="10%"><?= $orcamento->DETALHE->ESTADO?></td>
                    </tr>
                </tbody>
            </table>
            
            <?php if($orcamento->CLIENTE): ?>
                <h1>Cliente cadastrado</h1>
                <p>
                    Nome: <?= $orcamento->CLIENTE->NOME?>
                    Última Compra: Nome: <?= $orcamento->CLIENTE->ULTCOMPRA?>
                    Consultor: <?= ($orcamento->VENDEDOR) ? $orcamento->VENDEDOR->NOME : "NÃO IDENTIFICADO"?>
                </p>
            <?php endif; ?>

            <p>Produtos Selecionados</p>
            <code>
                <table cellspacing='0' border='1' width='95%'>
                    <thead>
                        <tr>
                            <td width="90%">Descrição</td>
                            <td width="10%">Quantidade</td>
                        </tr>                    
                    </thead>
                    <tbody>
                        <?php foreach($orcamento->ITENS as $item): ?> 
                            <tr id=" tr_detalhe_orcamento">
                                <td style="vertical-align: middle"><?= $item->PRODUTO->PRODUTO?></td>
                                <td style="vertical-align: middle; text-align: center"><?= $item->QUANTIDADE?></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </code>
	</div>

	<p class="footer">Solicitação enviada através do site da Reis Office</p>
</div>

</body>
</html>