<div class="container">
    <div class="row"> 
        <?=$menuEsq?>
        <div class="span8b">
            <!-- Carousel Atual -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="background: #537f95;">          

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php foreach ($banners as $id => $banner): ?>
                        <?php $active = $id == 0 ? 'active' : '' ; ?>
                        <div class="item <?=$active?>" style="padding:20px;">                        
                            <div class="left-data-app img-responsive" style="width:35%;">
                                <img src="<?= base_url("images/conteudo/$banner->img")?>" style="height: 250px; padding-bottom: 20px">
                            </div>
                            <div class="right-data-app" style="width:60%;">
                                <h3 class="titBanners"><?=$banner->titulo?></h3>                            
                                <br/><br/>
                                <p class="textBanners"><?=$banner->texto?></p>                            
                                <a class="btn btn-large btn-inverse" style="float:right;" href="<?=$banner->url?>"><?=$banner->texto_botao?></a>                            
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>         

                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>         
                <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>

            </div> 
            <br/>
            <?php if($produtos): ?>                
                <?php foreach($produtos as $key => $produto): ?>
                    <?php $item = $produto->PRODUTO ?>
                    <a href="<?= base_url("produtos/". url_codificar($item->FAMILIA->FAMILIA) ."/". url_codificar($item->LINHA->LINHA) ."/". url_codificar($item->CATEGORIA->CATEGORIA) ."/$item->CPROD/". url_codificar($item->PRODUTO))?>">  
                        <div class="span3cm wrap" style="padding-right:8px"> 
                            <div style="width:215px; height:218px; border:1px solid #EEE; display:block; text-align: center; vertical-align: middle">
                                <img style='max-height:215px;' class='center-block' src='<?= base_url("produtos/imagens/$item->CPROD/". url_codificar($item->PRODUTO) .".jpg")?>' alt='<?= $item->PORTAL_DESCRICAO_CURTA?>'/>
                            </div>
                            <ul style="width:215px; height:100px;" class="socials-member">		  
                                <h4 style="text-transform:none; color:#db7b3e; font-weight:normal; display:inline"><?= $item->PRODUTO?></h4>
                                <br>
                                <span class="job" style=" display:inline"></span>
                            </ul>
                        </div>
                    </a>
                    <?php if(empty($menu_familia) && $key == 15) { break; } ?>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="alert alert-info">Nenhum produto encontrado para a categoria selecionada.</div>
            <?php endif; ?>                
            <br/><br/>            
        </div>
    </div>
    
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>
    
</div>
