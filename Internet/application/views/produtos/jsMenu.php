<script type="text/javascript">
    $(document).ready(function(){
        $('.menu-categoria').on('show', function(obj) {
            id = $(obj.currentTarget).data("id");
            $("#header"+id).css("background-color", "#f2f2f2");
            $("#headerLink"+id).css("color", "#e47b38").css("text-decoration", "underline");
        });
        $('.menu-categoria').on('hide', function(obj) {
            id = $(obj.currentTarget).data("id");
            $("#header"+id).css("background-color", "#fff");
            $("#headerLink"+id).css("color", "#a2a2a2").css("text-decoration", "none");
        });
        $('#modalOrcamento').on('show', function () {
            var codigo = $('#btnOrc').data('produto');
            $.ajax({
                url: "<?= base_url("orcamento/adicionar")?>/"+ codigo,
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    $('#orc_mensagem').html(data.mensagem);
                },
                error: function(x, y, err) {
                    console.log(x);
                }
            });
        });
        $('#btnOrc').on('click', function () {
            var codigo = $(this).data('produto');
            $.ajax({
                url: "<?= base_url("orcamento/adicionar")?>/"+ codigo,
                dataType: "json",
                success: function(data) {
                    //console.log(data);
                    //$('#orc_mensagem').html(data.mensagem);
                    window.location.href = "<?= base_url("orcamento")?>";
                },
                error: function(x, y, err) {
                    console.log(x);
                }
            });
        })
    });
</script>