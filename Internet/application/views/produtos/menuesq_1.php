<style>
    .accordion-inner a:hover {
        color: #e47b38;
    }
    .menu-ativo {
        background-color: #f2f2f2;
    }
    .menu-ativo a {
        color: #e47b38;
        text-decoration: underline;
    }

</style>
<div class="span4b">  
    
    <?php foreach($menu as $familia): ?>    
        <?php if($familia->LINHAS): ?>
            <br/>
            <span style="margin-bottom: 50px; padding-bottom: 45px;font-size: 17.5px; color: rgb(228, 123, 56);">
                <?php if ($familia->FAMILIA == "EQUIPAMENTOS" && !empty($menu_categoria)):?>
                <h2 class="default1"><?= $familia->FAMILIA?></h2>
                <?php elseif($familia->FAMILIA == "SUPRIMENTOS"):?>    
                <h2 class="default1"><?= $familia->FAMILIA?></h2>
                <?php elseif($familia->FAMILIA == "PEN DRIVES / CARTOES DE MEMORIA"):?>
                <p class="default1"><?= $familia->FAMILIA?></p><br/>
                <?php elseif ($familia->FAMILIA == "EQUIPAMENTOS"):?>
                <h1 class="default1"><?= $familia->FAMILIA?></h1><br/>
                <?php endif;?>
                <br/>
            </span>
            <div class="accordion" id="accordion_<?= $familia->CHAVE_FAMILIA ?>">
                <?php foreach($familia->LINHAS as $linha): ?>
                    
                    <div class="accordion-group">
                        <?php if(count($linha->CATEGORIAS) > 1): ?>
                            <div class="accordion-inner <?= ($linha->CHAVE == $chave_linha) ? "menu-ativo" : ""?>" id="header<?= $linha->CHAVE?>" style="<?= ($linha->CHAVE == $chave_linha) ? "" : "background-color: #fff"?>;">
                                <a class="menu-linha" id="headerLink<?= $linha->CHAVE?>" data-toggle="collapse" data-parent="#accordion_<?= $familia->CHAVE_FAMILIA ?>" href="#collapse<?= $linha->CHAVE?>">
                                    <i class="icon-chevron-right"></i> <?= $linha->LINHA?><span class="caret pull-right" style="margin-top: 10px; margin-right: 0px"></span>
                                </a>
                            </div>
                            <div id="collapse<?= $linha->CHAVE?>" data-id="<?= $linha->CHAVE?>" class="accordion-body collapse menu-categoria <?= ($linha->CHAVE == $chave_linha) ? "in" : ""?>">
                                <?php foreach($linha->CATEGORIAS as $categ): ?>
                                    <div class="accordion-inner <?= ($categ->CHAVE == $chave_categoria) ? "menu-ativo" : ""?>">
                                        <a class="accordion-toggle" style="" data-toggle="" data-parent="" 
                                            href="<?= base_url("produtos/". url_codificar($familia->FAMILIA_URL) ."/". url_codificar($linha->LINHA_URL) ."/". url_codificar($categ->GRUPO_URL))?>">
                                            <?php if (!empty($menu_categoria)):?>
                                            <i class="icon-double-angle-right"></i> <?= $categ->GRUPO?>
                                            <?php else:?>
                                            <i class="icon-double-angle-right"></i> <h3 class="submenus0"> <?= $categ->GRUPO?> </h3>
                                            <?php endif?>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <div class="accordion-inner <?= ($linha->CHAVE == $chave_linha) ? "menu-ativo" : ""?>" id="header<?= $linha->CHAVE?>" style="<?= ($linha->CHAVE == $chave_linha) ? "" : "background-color: #fff"?>;">
                                <a class="menu-linha" id="headerLink<?= $linha->CHAVE?>" data-toggle="" data-parent=""
                                   href="<?= base_url("produtos/". url_codificar($familia->FAMILIA_URL) ."/". url_codificar($linha->LINHA_URL) ."/". url_codificar($linha->CATEGORIAS[0]->GRUPO_URL))?>">
                                    <i class="icon-chevron-right"></i> <?= $linha->LINHA?>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
        
    <!-- Categories widget -->
    <div  class="wrap" style="margin-bottom:0px;">     
        <hr>	 
        <h4><font color="#537f95"><b>MARCA</b></font></h4><br>
        <select onchange="document.location.href='<?= base_url("produtos")?>/'+this.value">
            <option value="">Selecione uma Marca</option>
            <?php foreach($marcas as $item): ?>
                <option value="<?= $item->MARCA?>" <?= ($item->MARCA == $this->session->marca) ? "selected" : ""?>><?= $item->MARCA?></option>
            <?php endforeach; ?>
        </select>
    </div>    
    <br/>
    <a href="<?= base_url("revendas/webnars")?>" target="_blank">
        <img src="<?=base_url('images/visual/revendas/acessoPortal.png')?>" width="290" alt=""/>
    </a>
    <br/><br/>
    <div class="fb-page" data-href="https://www.facebook.com/ReisOfficeProducts/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/ReisOfficeProducts/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ReisOfficeProducts/">Reis Office Products</a></blockquote></div>
</div>