<div class="container">
    <div class="row"> 
        <?=$menuEsq?>
        <div class="span8b">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?= base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?= base_url("produtos")?>">Produtos</a>
                    <?= ($this->session->marca) ? "<span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/".$this->session->marca) ."'>". $this->session->marca ."</a>" : ""?> 
                    <?= ($menu_familia) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia)) ."'>". $menu_familia."</a>" : ""?>
                    <?= ($menu_linha) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia) ."/". url_codificar($menu_linha)) ."'>". $menu_linha ."</a>" : ""?>
                    <?= ($menu_categoria) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia) ."/". url_codificar($menu_linha) ."/". url_codificar($menu_categoria)) ."'>". $menu_categoria ."</a>" : ""?>
                    
                </li>
            </ul>
           <?php if($menu_categoria): ?>
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= url_decode($menu_categoria)?></font></h1>
            <?php elseif($menu_linha): ?>    
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= url_decode($menu_linha)?></font></h1>
            <?php elseif($menu_familia): ?>    
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= url_decode($menu_familia)?></font></h1>
            <?php endif; ?>
            <?php if($produtos): ?>
                <?php foreach($produtos as $key => $item): ?>
                    <a href="<?= base_url("produtos/". url_codificar($item->FAMILIA) ."/". url_codificar($item->LINHA) ."/". url_codificar($item->CATEGORIA) ."/$item->CPROD/". url_codificar($item->PRODUTO))?>">  
                        
                        <div class="span3cm wrap" style="padding-right:8px"> 
                            <div style="width:215px; height:218px; border:1px solid #EEE; display:block; text-align: center; vertical-align: middle">
                                <img style='max-height:215px;' class='center-block' src='<?= base_url("produtos/imagens/$item->CPROD/". url_codificar($item->PRODUTO) .".jpg")?>' alt='<?= $item->PORTAL_DESCRICAO_CURTA?>'/>
                            </div>
                            <ul style="width:215px; height:100px;" class="socials-member">		  
                                <h4 style="text-transform:none; color:#db7b3e; font-weight:normal; display:inline"><?= $item->PRODUTO?></h4>
                                <br>
                                <span class="job" style=" display:inline"></span>
                            </ul>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>                
            <br/><br/>            
        </div>
        <div class="span8b pull-right" style="text-align: center">
            <?php $paginas = ceil($produtos_total / 16) ?>
            <?php for($p = 0; $p < $paginas; $p++): ?>
                <?php $classe = ($p == $this->session->produto_pagina - 1) ? "btn-default" : "btn-primary" ?>
                <?php $url_pagina = ($this->session->marca) ? base_url("produtos/". $this->session->marca) : base_url("produtos/". url_codificar($menu_familia) ."/". url_codificar($menu_linha) ."/". url_codificar($menu_categoria))?>
                <a class="btn <?= $classe?> produto-pagina" data-toggle="tab" href="javascript:void(0)" data-pagina="<?= $p+1?>" data-href="<?= ($classe == "btn-primary") ? current_url() : "#"?>"><span style="padding: 15px; line-height: 30px; font-size: 1.2em;"><?= $p+1?></span></a>
            <?php endfor; ?>
        </div>
    </div>
        
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>
    
</div>
