<div class="container">
    <div class="row"> 
        <?= $menuEsq ?>

        <div class="span8b">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?= base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?= base_url("produtos")?>">Produtos</a>
                    <?= ($this->session->marca) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/".$this->session->marca) ."'>". $this->session->marca ."</a>" : ""?> 
                    <?= ($menu_familia) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia)) ."'>". $menu_familia ."</a>" : ""?> 
                    <?= ($menu_linha) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia) ."/". url_codificar($menu_linha)) ."'>". $menu_linha ."</a>" : ""?>
                    <?= ($menu_categoria) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/". url_codificar($menu_familia) ."/". url_codificar($menu_linha) ."/". url_codificar($menu_categoria)) ."'>". $menu_categoria ."</a>" : ""?>
                </li>
            </ul>
            <?php if($produto): ?>
                <div class="row">
                    <div class="span8">
                        <h2 style="border:0px; margin-bottom:20px; padding:0px; font-size:26px; line-height:26px; font-weight:normal; color:#e47b38; text-transform: none;">
                            <span itemprop="identifier" content="mpn:25908">
                                <span itemprop="name"><h1 class="produto-font"><?= urldecode($titulo_descricao)?></span></h1>
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="span4">
                        <div id="myCarousel" class="carousel slide" style="margin-top: 0">
                            <!-- Carousel items -->
                            <div class="carousel-inner" style="border:1px solid #eeeeee; height: 380px;">
                                <?php $primeiro = 0 ?>
                                <?php foreach($detalhes->FOTOS as $key => $item): ?>
                                    <?php if($item->TIPO != "THUMBNAIL"): ?>
                                        <div class="item <?= ($primeiro == 0) ? "active" : ""; $primeiro++?>"> 
                                            <a data-fancybox = "images" href="<?= base_url("produtos/imagem/$item->CHAVE/0/". url_codificar($detalhes->PRODUTO) ."_$key.jpg")?>" data-caption="<?= $detalhes->PRODUTO?>">
                                                <img src="<?= base_url("produtos/imagem/$item->CHAVE/1/". url_codificar($detalhes->PRODUTO) ."_$key.jpg")?>" itemprop="image" alt="<?= $detalhes->PRODUTO?>"> 
                                            </a>                                    
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                            <!-- Carousel nav -->
                            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="span5">
                        <div style="width:100%; display:block; clear:left;">
                            <a class="btn btn-large btn-inverse" href="javascript:void(0)" id="btnOrc" data-produto="<?= $codigo?>" style="line-height:25px; height:25px; float:right">
                                <span itemprop="availability" content="in_stock">SOLICITE UM ORÇAMENTO</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="span8b">
                        <div class="accordion" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                        Caracteristicas <span class="togglerSign">＋</span>
                                    </a>
                                </div>
                                <div id="collapseOne" class="accordion-body collapse in">
                                    <div class="accordion-inner">
                                        <?= (is_object($detalhes->PORTAL_DESCRICAO_LONGA)) ? $detalhes->PORTAL_DESCRICAO_LONGA->load() : $detalhes->PORTAL_DESCRICAO_LONGA;?>
                                        <hr/>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Código </td>
                                                    <td>&nbsp;<?= $codigo ;?></td>
                                                </tr>
                                                <tr>
                                                    <td>Código Interno </td>
                                                    <td> &nbsp;<?php echo $detalhes->CODIGO_INTERNO ;?></td>
                                                </tr>
                                                <tr>
                                                    <td>Peso Liquido* </td>
                                                    <td>&nbsp; <?php echo $detalhes->PESO_LIQUIDO ;?>&nbsp;KG* <sup>Sujeito à conferência</sup></td>
                                                </tr>
                                                <tr>
                                                    <td>Peso Bruto* </td>
                                                    <td>&nbsp; <?php echo $detalhes->PESO_BRUTO ;?>&nbsp;KG*<sup>Sujeito à conferência</sup></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        Marca <span class="togglerSign">＋</span>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <span itemprop="brand"><?= ($detalhes->CPROD == 26068) ? "REIS OFFICE" : $detalhes->FABRICANTE->FABRICANTE?></span>
                                    </div>
                                </div>
                            </div>
                            <?php if($detalhes->BIBLIOTECA): ?>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                                            Downloads <span class="togglerSign">＋</span>
                                        </a>
                                    </div>
                                    <div id="collapse3" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <table class="table table-condensed">
                                                <tbody>
                                                    <tr>
                                                        <td class="style1">Nome</td>
                                                        <td class="style1">Tamanho</td>
                                                    </tr>
                                                    <?php foreach($detalhes->BIBLIOTECA as $item): ?>
                                                        <tr>
                                                            <td><a class="text_produtos" href="<?= base_url("produtos/download/$item->CHAVE")?>"><?= $item->NOME_ARQUIVO?></a></td>
                                                            <td><div align="right"><a class="text_produtos" href="<?= base_url("produtos/download/$item->CHAVE")?>"><?= $item->TAMANHO_ARQUIVO?>kb</a></div></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <br/><br/>
        </div>
    </div>
    
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>
    
</div>

<div id="modalOrcamento" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form name='orcamento' method="post" action="<?= base_url("produtos/orcamento/$codigo")?>">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 id="myModalLabel">Solicitar Orçamento</h4>
        </div>
        <div class="modal-body" style="height: 150px; max-height: 150px">
            <p style="border:0px; margin-bottom:20px; padding:0px; font-size:16px; line-height:16px; font-weight:normal; color:#e47b38; text-transform: none;">
                <span itemprop="identifier" content="mpn:25908">
                    <span itemprop="name"><?= $titulo_descricao?></span>
                </span>
            </p>            
            <p id="orc_mensagem"></p>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-large btn-default2">Ver mais produtos</button>
            <a href="<?= base_url("orcamento")?>" class="btn btn-large btn-inverse">Finalizar Orçamento</a>
        </div>
    </form>
</div>

