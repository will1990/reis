<div class="container">
    <div class="row">
        <?=$menuEsq?>
        <div class="span8b">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?= base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?= base_url("produtos")?>">Produtos</a>                
                <?= ($menu_familia) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$menu_familia") ."'>". urldecode($menu_familia) ."</a>" : ""?>
                <?= ($menu_linha) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$menu_familia/$menu_linha") ."'>". urldecode($menu_linha) ."</a>" : ""?>
                <?= ($menu_categoria) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$menu_familia/$menu_linha/$menu_categoria") ."'>". urldecode($menu_categoria) ."</a>" : ""?>
                </li>
            </ul>
            <?php if($menu_categoria): ?>
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= urldecode($menu_categoria)?></font></h1>
            <?php endif; ?>
            <?php if($produtos): ?>                
                <?php foreach($produtos as $item): ?>
                    <a href="<?= base_url("produtos/$item->CPROD/$item->PRODUTO")?>">  
                        <div class="span3cm wrap" style="padding-right:8px"> 
                            <div style="width:215px; height:218px; border:1px solid #EEE; display:block;">
                                <img style='max-height:215px;' class='center-block' src='<?= base_url("produtos/imagens/$item->CPROD/". urlencode($item->PRODUTO) .".jpg")?>' alt='<?= $item->PORTAL_DESCRICAO_CURTA?>'/>
                            </div>
                            <ul style="width:215px; height:100px;" class="socials-member">		  
                                <h4 style="text-transform:none; color:#db7b3e; font-weight:normal; display:inline"><?= $item->PRODUTO?></h4>
                                <br>
                                <span class="job" style=" display:inline"></span>
                            </ul>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
            <br/><br/>
        </div>
    </div>
</div>