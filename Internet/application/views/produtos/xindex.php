<div class="container">
    <div class="row"> 
        <?=$menuEsq?>
        <div class="span8b">
            <br/>
            <ul class="breadcrumb">
                <li data-original-title=""><a href="<?= base_url()?>">Home</a> <span class="divider">/</span></li>
                <li data-original-title=""><a href="<?= base_url("produtos")?>">Produtos</a>
                    <?= ($this->session->marca) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/".$this->session->marca) ."'>". $this->session->marca ."</a>" : ""?> 
                    <?= ($menu_familia) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$chave_familia") ."'>". $menu_familia ."</a>" : ""?> 
                    <?= ($menu_linha) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$chave_familia/$chave_linha") ."'>". $menu_linha ."</a>" : ""?>
                    <?= ($menu_categoria) ? " <span class='divider'>/</span></li> <li data-original-title=''><a href='". base_url("produtos/$chave_familia/$chave_linha/$chave_categoria") ."'>". $menu_categoria ."</a>" : ""?>
                </li>
            </ul>
            <?php if($menu_categoria): ?>
                <h1 style="font-size:17.5px"><font color="#e47b38"><?= url_decode($menu_categoria)?></font></h1>
            <?php endif; ?>
            <?php if($produtos): ?>                
                <?php $paginas = ceil(count($produtos) / 8); ?>
                <?php $cont = $pagina = 0; ?>
                <?php $active = 'active'; ?>
                <div class="tab-content">
                    <?php foreach($produtos as $key => $item): ?>
                        <?= ($cont == 0) ? "<div class='tab-pane $active' id='p_$pagina'>" : ""?>
                        <a href="<?= base_url("produtos/$chave_familia/$chave_linha/$chave_categoria/$item->CPROD")?>">  
                            <div class="span3cm wrap" style="padding-right:8px"> 
                                <div style="width:215px; height:218px; border:1px solid #EEE; display:block; text-align: center; vertical-align: middle">
                                    <img style='max-height:215px;' class='center-block' src='<?= base_url("produtos/imagens/$item->CPROD/". remover_acentos($item->PRODUTO) .".jpg")?>' alt='<?= $item->PORTAL_DESCRICAO_CURTA?>'/>
                                </div>
                                <ul style="width:215px; height:100px;" class="socials-member">		  
                                    <h4 style="text-transform:none; color:#db7b3e; font-weight:normal; display:inline"><?= $item->PRODUTO?></h4>
                                    <br>
                                    <span class="job" style=" display:inline"></span>
                                </ul>
                            </div>
                        </a>
                        <?php $cont++; ?>
                        <?php if($cont == 8) { echo "</div>"; $cont = 0; $pagina++; } ?>
                    <?php endforeach; ?>
                    <?= ($cont > 0) ? "</div>" : ""?>
                </div>                
                <?php for($p = 0; $p <= $pagina; $p++): ?>
                <!--<a class="btn btn-primary" data-toggle="tab" href="$p_<?= p?>"><span style="padding: 15px; line-height: 30px; font-size: 1.2em;"><?= $p+1?></span></a>-->
                <?php endfor; ?>
            <?php else: ?>
                <div class="alert alert-info">Nenhum produto encontrado para a categoria selecionada.</div>
            <?php endif; ?>                
            <br/><br/>            
        </div>
    </div>
        
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>
    
</div>
