<div style="width: 100%">
    <a href="<?= base_url("empresa/showroom")?>"><img src="<?= base_url("images/visual/Banner-01.png") ?>"></a>
</div>

<div class="container" style="margin-top: 15px;"> 
    <div class="row blocks-2">
        <div class="span12">
            <p style="font-size:24px; color:#575756; font-weight:normal; padding-bottom: 10px; border-bottom: 1px solid #d0d0d0">Serviços</p>
        </div>
        <?php foreach($destaques as $id => $destaque): ?>
            <div class="span3b" style="border: 0px">
                <div class="">
                    <div class="img-responsive" style="text-align: center">
                        <?php 
                            switch($id)
                            {
                                case 0:
                                    $img = "Revendas.png";
                                    break;
                                case 1:
                                    $img = "Outsourcing.png";
                                    break;
                                case 2:
                                    $img = "Licitacoes.png";
                                    break;
                                case 3:
                                    $img = "Loja_virtual.png";
                                    break;
                            }
                        ?>
                        <a href="<?=$destaque->url?>"><img src="<?= base_url("images/visual/".$img) ?>" style="height: 120px"></a>
                    </div>                
                    <p style="font-size:24px; color:#537f95; font-weight:bold; padding: 10px 0px 15px 0px; margin:0px; text-align: center; text-transform: uppercase">
                        <?=$destaque->titulo?>
                    </p>
                    <p style="font-size:1.2em; color:#666; text-align: center;">
                        <?=$destaque->texto?>
                    </p>
                </div>
            </div>    
        <?php endforeach; ?>
    </div>
    
    <div class="row blocks-2">
        <div class="span12">
            <p style="font-size:24px; color:#575756; font-weight:normal; padding-bottom: 10px; border-bottom: 1px solid #d0d0d0">Produtos</p>
        </div>    
        <div id="myCarousel" class="carousel slide hidden-phone">
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item">
                    <div class="span3" style="border: 0px">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/multifuncional")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Multifuncionais.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/impressoras")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Impressoras.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/scanner")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Scanners.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px;">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/calculadora")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Calculadores.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="span3" style="border: 0px">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/rotulador")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Rotularores.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px;">
                        <div class="">
                            <a href="<?= base_url("produtos/equipamentos/camera digital")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Cameras_Digitais.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px;">
                        <div class="">
                            <a href="<?= base_url("produtos/suprimentos")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Suprimentos.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                    <div class="span3" style="border: 0px;">
                        <div class="">
                            <a href="<?= base_url("produtos/suprimentos/kit+manutencao_esteiras_fusores")?>">
                                <div class="dest-image">
                                    <img src="<?= base_url("images/visual/Kit_manutencao.png") ?>">
                                </div>                
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
        <div class="row visible-phone">
            <div class="span3" style="border: 0px">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/multifuncional")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Multifuncionais.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/impressoras")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Impressoras.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/scanner")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Scanners.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px;">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/calculadora")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Calculadores.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>                
            <div class="span3" style="border: 0px">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/rotulador")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Rotularores.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px;">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/equipamentos/camera digital")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Cameras_Digitais.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px;">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/suprimentos")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Suprimentos.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
            <div class="span3" style="border: 0px;">
                <div class="" style="padding: 15px">
                    <a href="<?= base_url("produtos/suprimentos/kit+manutencao_esteiras_fusores")?>">
                        <div class="dest-image">
                            <img src="<?= base_url("images/visual/Kit_manutencao.png") ?>">
                        </div>                
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <?=(isset($nossas_marcas)) ? $nossas_marcas : "" ?>    
</div>

