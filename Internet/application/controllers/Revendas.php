<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define("EVTPATH", "https://portal.reisoffice.com.br/revenda/ws/eventos_site");
define("VIDPATH", "https://portal.reisoffice.com.br/revenda/ws/video_treinamento_site");
//define("VIDPATH", "http:/\/localhost/Portal/revenda/ws/video_treinamento_site");

class Revendas extends CI_Controller
{
    /** 
     *  Site Reis Office
     *  William Feliciano 
     */
    
    function __construct() 
    {
        parent::__construct();       
        $this->data['pagina'] = null;
        $this->load->model("Revenda_model","mdrev");
        $this->data["pathbase"] = base_url("images/conteudo/");
                
        $this->data["tituloBarra"] = (object)array("titulo" => "Revendas", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        $this->data["font"] = '30px';
    }
    
    //REVENDAS
    //Páginas
    //Index
    public function index()
    {
        $this->data["topMarcador"] = 3;        
        $this->data["solucoes"] = $this->mdrev->getSolucoes();
                
        $this->data["barraDir"] = $this->load->view('revendas/barradirForm', $this->data, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('revendas/index', $this->data);
        $this->load->view('js/jsMascFonesForms', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('js/jsNewCarouselBann', $this->data);
    }
    
    //Eventos
    public function eventos()
    {
        $this->data["topMarcador"] = 0;
        $this->data["tituloBarra"]->titulo = "Cronograma de Eventos Reis Office";
        
        $this->data["eventos"] = $this->prepEventos(EVTPATH);       
        
        $this->data["noticias"] = $this->mdrev->getNoticias();
        
        $this->data["barraDir"] = $this->load->view('revendas/barradirNews', $this->data, true);
        
        $this->load->view('template/header', array("pagina" => "eventos"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('revendas/eventos', $this->data);        
        $this->load->view('template/footer');
    }
    
    //Webnars
    public function webnars(){
        
        $this->data["topMarcador"] = 0;
        $this->data["tituloBarra"]->titulo = "Treinamentos Online";
        
        $this->data["videos"] = $this->prepEventos(VIDPATH)['data'];
              
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('revendas/webnars', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('js/jsTabWebnars', $this->data);
    }
    
    //Processamento
    //Preparar os dados dos Eventos e Webnars
    public function prepEventos($url = null)
    {
        //$arq = fopen($url,"r");
        $ch = curl_init();
        // informar URL e outras funções ao CURL

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // acessar a URL
        $conteudo = curl_exec($ch);
                        
        return (array) json_decode($conteudo);
    }   
    
    //*** ???    
    public function treinamento($nomeView = null)
    {
        $this->data["topMarcador"] = 3;
        $this->data["tituloBarra"] = "Revendas";
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        
        //$this->load->view("treinamento/$nomeView");
        
        $this->load->view('template/footer');     
    }
    
}

