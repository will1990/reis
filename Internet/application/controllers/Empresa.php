<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct()
    {       
        parent::__construct();
        $this->load->model("Empresa_model","mdemp");
        $this->data['pagina'] = null;
        $this->data["pathbase"] = base_url("images/conteudo/");

        $this->data["navbarEsq"] = $this->load->view('empresa/menuesq', $this->data, true);
        
        $this->data["tituloBarra"] = (object)array("titulo" => "Empresa", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        $this->data["topMarcador"] = 2;         
    }
       
    //EMPRESA
    //Quem Somos
    public function index()
    {   
        /*$this->load->library('parser');
        $data = array(
        'meta_title' => "Líder em Outsourcing e Soluções para Impressão | Reis Office",
        'meta_description' => "Home"
        );
        $this->parser->parse('template/header', $data);*/
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/quemSomos', $this->data);        
        $this->load->view('template/footer');       
    }
    
    //Código de ética
    public function etica()
    {   
        $this->data["pagina"] = "etica";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"etica"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/codigoEtica', $this->data);        
        $this->load->view('template/footer');  
    }
    
    //Política de Qualidade
    public function qualidade()
    {   $this->data["pagina"] = "qualidade";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"qualidade"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/politicaQualidade', $this->data);        
        $this->load->view('template/footer');  
    }
    
    //Prêmios
    public function premios()
    {
        $this->data["premios"] = $this->mdemp->getPremios();      
        
        $this->data["pagina"] = "premios";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"premios"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/premios', $this->data);        
        $this->load->view('template/footer');
        $this->load->view('js/jsNewCarouselBann', $this->data);
    }
    
    //Showroom
    public function showroom()
    {   
        $this->data["pagina"] = "showroom";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"showroom"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/showRoom', $this->data);        
        $this->load->view('template/footer');  
    }
    
    //Meio Ambiente
    public function ambiente()
    {   
        $this->data["pagina"] = "ambiente";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"ambiente"));
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/meioAmbiente', $this->data);        
        $this->load->view('template/footer');
    }
    
    //Responsabilidade Social
    public function social()
    {   
        $this->data["pagina"] = "social";
        $this->data["tituloBarra"] = (object)array("titulo" => meta($this->data["pagina"])->title, "fontSize" => "30px");
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        $this->load->view('template/header', array("pagina"=>"social"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view('empresa/social', $this->data);        
        $this->load->view('template/footer');
    }
}