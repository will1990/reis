<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loja extends CI_Controller 
{
    /***
     * Controle de redirecionamento
     */
    
    public function index()
    {
        redirect(LOJA_VIRTUAL);
    }    
}