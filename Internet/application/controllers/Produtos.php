<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

    /**
     *  Saite Reis Office
     *  William Feliciano     
     */
    
    
    function __construct() {
        parent::__construct();
        $this->load->library("parser");
        $this->load->model("Produtos_model", "m_produtos");
        $this->data['pagina'] = 'produto';
        //$this->data["pathbase"] = base_url("images/conteudo/");
        $this->data["topMarcador"] = 6;

        $this->data["nossas_marcas"] = $this->load->view('template/suppliers', null, true); 
        $this->data["tituloBarra"] = (object) array("titulo" => "Produtos", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object) array("titulo" => "<i class='icon-calendar'></i> EVENTOS", "url" => base_url("revendas/eventos")),
            (object) array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object) array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
    }
    
    public function index($familia = '', $linha = '', $categoria = '',  $codigo = '', $nome = '')
    {    
        $this->load->library('parser');
        if($familia != "" && strpos("BROTHER;CANON;KYOCERA;OKIDATA;OLIVETTI", $familia) !== false)
        {    
            /*$this->data["menu"] = $this->m_produtos->get_menu_marca($familia);           
            echo "<pre>"; print_r($this->data["menu"]); exit;
            $produto = $this->m_produtos->get_produto_por($familia);  
            
            if($produto)
            {
                $this->session->produto_selecionado = $familia;
                redirect(base_url("produtos/". url_encode($produto->FAMILIA->FAMILIA) ."/". url_encode($produto->LINHA->LINHA) ."/". url_encode($produto->CATEGORIA->CATEGORIA) ."/". url_encode($produto->PRODUTO)));
            }*/            
            $this->session->marca = $familia;
            $familia = '';
        }
        elseif($familia == "")
            $this->session->marca = $familia;
        elseif(is_numeric($familia))
        {
            $check_familia = $this->m_produtos->get_produto_familia($familia);
            if(!$check_familia->CHAVE)
            {
                $link = $this->m_produtos->get_produto_por($familia);
                redirect(base_url("produtos/". url_codificar($link->FAMILIA->FAMILIA) ."/". url_codificar($link->LINHA->LINHA) ."/". url_codificar($link->CATEGORIA->CATEGORIA) ."/$familia/". url_codificar($link->PRODUTO)));
                exit;
            }
        }

        $this->data["menu"] = $this->m_produtos->get_menu();
        $this->data["marcas"] = $this->m_produtos->get_marcas();
        
        $data_familia = $this->m_produtos->get_produto_familia($familia);
        $this->data["chave_familia"] = $data_familia->CHAVE;
        $this->data["menu_familia"] = $data_familia->FAMILIA; //pega a familia 
       // print_r( $this->data["menu_familia"] = $data_familia->FAMILIA);
        $data_linha = $this->m_produtos->get_produto_linha($linha);        
        $this->data["chave_linha"] = $data_linha->CHAVE;
        $this->data["menu_linha"] = $data_linha->LINHA;
        //print_r($this->data["menu_linha"] = $data_linha->LINHA);
        $data_categoria = $this->m_produtos->get_produto_categoria($categoria);
        $this->data["chave_categoria"] = $data_categoria->CHAVE;
        $this->data["menu_categoria"] = $data_categoria->CATEGORIA;
        
        $this->data["menu_categoria"] = $data_categoria->CATEGORIA;       
        if(!isset($this->session->produto_url) || $this->session->produto_url != "$familia/$linha/$categoria")
        {
            $this->session->produto_url = "$familia/$linha/$categoria";
            $this->session->produto_pagina = 1;
        }
                
        $this->data["menuEsq"] = $this->load->view('produtos/menuesq_1', $this->data, true);
        $this->data["js"] = $this->load->view('produtos/jsMenu', $this->data, true);

        // Verifica como buscar os produtos arrumar
        if(!empty($codigo))
        {
            $view = "produto";            
            $this->data["codigo"] = $codigo;
            $this->data["produto"] = $this->m_produtos->get_produto_por($this->data["codigo"]);
            $this->data["detalhes"] = $this->m_produtos->get_produto_por_detalhe($this->data["codigo"]);
            $this->data["titulo_descricao"] = $this->data["produto"]->PRODUTO;
            
            $this->data["chave_familia"] = $this->data["produto"]->FAMILIA->CHAVE;
            $this->data["chave_linha"] = $this->data["produto"]->LINHA->CHAVE;
            $this->data["chave_categoria"] = $this->data["produto"]->CATEGORIA->CHAVE;
            $this->data["menu_familia"] = $this->data["produto"]->FAMILIA->FAMILIA;
            $this->data["menu_linha"] = $this->data["produto"]->LINHA->LINHA;
            $this->data["menu_categoria"] = $this->data["produto"]->CATEGORIA->CATEGORIA;

        } 
        elseif(empty($familia) && empty($linha) && empty($categoria) && empty($this->session->marca))
        {
            $view = "home";
            $this->data["banners"] = $this->m_produtos->get_banners();
            $this->data["produtos"] = $this->m_produtos->get_produtos_destaque();
            $this->data["titulo_descricao"] = "Produtos";
        }
        else 
        {
            $view = "index";
            $produtos = $this->m_produtos->get_produtos($familia, $linha, $categoria);
            $this->data["produtos"] = $produtos[1];
            $this->data["produtos_total"] = $produtos[0];
            //print_r($this->data);
        }
        
        /* tag SEO */
        // $this->parser->parse // https://www.codeigniter.com/userguide3/libraries/parser.html
        $this->parser->parse('template/header', $this->tagSeo($this->data, $codigo,$categoria));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);
        $this->load->view("produtos/$view", $this->data);
        $this->load->view('template/footer', $this->data);        
    }        
    
public function tagSeo($data , $codigo){
    if(!empty($data['detalhes'])){
        $menuLinha = $data["menu_linha"];
        $marca = $data["detalhes"]->FABRICANTE->FABRICANTE;
            
        $marcas = getMarcasSeo();
        $tituloDescri = !empty($data["titulo_descricao"]) ? $data["titulo_descricao"] : $data["menu_categoria"];
        $codInter = !empty($data['detalhes']) ?  $data['detalhes']->CODIGO_INTERNO : '';
            $rst = array
                (
                    'meta_title' =>   "$tituloDescri",
                    'meta_description'=> !empty($marcas[$marca])? $marcas[$marca] : '',
                    "meta_subject" => " Venda, Aluguel de Copiadoras, Impressoras, são paulo (SP)-Guarulhos -".$data["titulo_descricao"]."",
                    'meta_keywords' => "$menuLinha  $codigo  ". $data["menu_familia"]." ".$data["menu_categoria"]." $marca $codInter"
                );
        }
    else 
    {   
        $rst = array
            (
                'meta_title' => "",
                'meta_description'=>" Distribuidora oficial Brother, Canon, Kyocera, Oki e olivetti, a Reis office conta com uma infinidade de produtos como impressora, multifuncional, scanner, plotter, câmera e calculadoras. Além dos suprimentos que vão desde toner até peças. Confira nossos produtos.",
                "meta_subject" => " Venda, Aluguel de Copiadoras, Impressoras, são paulo (SP)-Guarulhos  ",
                'meta_keywords' => " Multifuncional, Plotter, toner, cartucho, impressora, fragmentadora, scanner, suprimentos, canon, brother, Kyocera, oki, olivetti"
            );
    }
    return $rst;
}

public function imagens($cprod, $prod, $tamanho = "PRINCIPAL") {
        $fotos = $this->m_produtos->get_produto_fotos($cprod);
        $img = false;
        $sem_foto = file_get_contents(APPPATH . "../images/conteudo/notImg.jpg");
        
        if($fotos)
        {
            foreach($fotos as $foto)
            {
                if($foto->TIPO == $tamanho)
                    if(!empty($foto->FOTO))
                        $img = $foto->FOTO;
                    else
                        $img = $sem_foto;
            }

            if (!$img)
                $img = $fotos[0]->FOTO;
        }
        else
        {
            $img = $sem_foto;
        }
        
        header("Content-type: image/jpeg");
        
        $im = imagecreatefromstring($img);
        
        if($fotos)
        {
            $ims = imagescale($im, 215, -1, IMG_BICUBIC_FIXED);
            imagepng($ims);
        }
        else
        {
            imagepng($im);
        }
    }
    
    public function imagem($chave, $redim = true, $prod = "Imagem_RO.jpg")
    {
        $foto = $this->m_produtos->get_foto_produto($chave);
        $img = false;
        $sem_foto = file_get_contents(APPPATH . "../images/conteudo/notImg.jpg");
        
        if($foto)
        {
            if(!empty($foto->FOTO))
                $img = $foto->FOTO;
            else
                $img = $sem_foto;
        }
        else
        {
            $img = $sem_foto;
        }

        header("Content-type: image/jpeg");
        
        $im = imagecreatefromstring($img);
        
        if($foto)
        {
            if($redim)
                $ims = imagescale($im, 380, -1, IMG_BILINEAR_FIXED);
            else
                $ims = $im;
            imagepng($ims);
        }
        else
        {
            imagepng($im);
        }
    }
    
    public function download($chave)
    {
        $arq = $this->m_produtos->get_produto_biblioteca(0, $chave);
        if($arq)
        {
            $this->load->helper("download");
            force_download($arq->NOME_ARQUIVO, $arq->ARQUIVO);
        }
        else
        {
            $this->index();
        }
    }
    
    public function produto_pagina($pagina)
    {
        $this->session->produto_pagina = $pagina;
        echo '1';
    }
}
