<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outsourcing extends CI_Controller 
{
    /** 
     *  Site Reis Office
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();       
        $this->data["pagina"] = NULL ;
        
        $this->load->model("Outsourcing_model","mdout");
        
        $this->data["pathbase"] = base_url("images/conteudo/");
        $this->data["tituloBarra"] = (object)array("titulo" => "Outsourcing", "fontSize" => "30px");
        $this->data["botoesBarra"] = array(
            (object)array("titulo" => "<i class='icon-calendar'></i> PORTAL SERVICE", "url" => PORTAL_SERVICE),
            (object)array("titulo" => "<i class='icon-rss'></i> NOTICIAS", "url" => base_url("noticias")),
            (object)array("titulo" => "<i class='icon-chevron-right'></i> ASSIST&Ecirc;NCIA T&Eacute;CNICA", "url" => base_url("outsourcing/assistencia"))
        );
        
        $this->data["solucoes"] = $this->mdout->getAllSolucoes();
    }
    
    //OUTSOURCING
    //Página principal de Outsourcing (Lista)
    public function index($ga = false)
    {
        $this->data["topMarcador"] = 4;        
        
        $this->data["outsourcings"] = $this->mdout->getAllOutsourcings();
                                  
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
                
        $this->load->view('template/header', array("pagina"=>"outsourcing/index"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/index', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);     
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer', $this->data);
        $this->load->view('js/jsAccordionOutsourcing');
    }
    
    //Montando Conteúdo Interno de Outsourcing
    public function ver($id = null)
    {
        $this->data["topMarcador"] = 0;
        
        $this->data["outsourcing"] = $this->mdout->getOutsourcing($id);
                
        $this->data["tituloBarra"]->titulo = $this->data["outsourcing"]->titulo;
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["outsourcing"]->titulo) < 41)? $this->data["tituloBarra"]->fontSize : '20px';
                
        $this->data["barraDir"] = $this->load->view('outsourcing/barradirForm', $this->data, true);
        $this->data["nossas_marcas"] = $this->load->view('template/suppliers', null, true);
        
        $this->load->view('template/header');
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('outsourcing/montarCategorias', $this->data);
        $this->load->view('js/jsRedirectOutsourcing', $this->data);  
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
        $this->load->view('template/footer');
    }
    
    //SOLUCOES
    //Página principal de Outsourcing (Lista)
    public function solucoes($slug = null)
    {       
        if($slug !== null):            
        
            $this->data["topMarcador"] = 0;
            $this->data["tituloBarra"] = (object)array("titulo" => $this->data["solucoes"]["$slug"]->titulo);
            $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'20px');
            $this->data["bread"] = $this->data["solucoes"]["$slug"]->nome;
                    
            $this->data["conteudo"] = $this->load->view("solucoes/$slug", $this->data, true);
            
            $this->load->view('template/header');
            $this->load->view('template/navbar', $this->data);
            $this->load->view('template/barrasup', $this->data);        
            $this->load->view('solucoes/index', $this->data);        
            $this->load->view('template/footer');
            
        else:
            
            redirect(base_url("outsourcing"),"refresh");
            
        endif;
    }
    
    //ASSISTÊNCIA
    //Página de Assistência
    public function assistencia()
    {                
        $this->data["topMarcador"] = 0;
        $this->data["pagina"] = "assistencia";
        $this->data["tituloBarra"]->titulo = meta($this->data["pagina"])->title;
        $this->data["tituloBarra"]->fontSize = (strlen($this->data["tituloBarra"]->titulo) < 36)? '30px' :((strlen($this->data["tituloBarra"]->titulo) < 46)? '25px':'22px');
        
        $this->data["barraDir"] = $this->load->view("assistencia/barradir", $this->data, true);
                
        //Fazer a logica de cadastro de email do rodapé com Jquery, sem refresh
        
        $this->load->view('template/header', array("pagina"=>"assistencia"));
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);        
        $this->load->view('assistencia/assistencia', $this->data);                
        $this->load->view('template/footer');
        $this->load->view('js/jsMostrarAssist', $this->data);        
    }
    
    //NOVA
    
    public function nova()
    {
        $this->data["topMarcador"] = 4;
        
        $menu = $this->mdout->getMenu();
        $this->data["menuCompleto"] = $this->montarMenu($menu);
        
                        
        $this->data["imgBarra"] = $this->load->view('outsourcing/imgBarraNew', null, true);
        $this->data["formBarra"] = $this->load->view('outsourcing/formBarraNew', array("descricao" => "Receba mais informações sobre nossas soluções:"), true);
        $this->data["barraesq"] = $this->load->view('outsourcing/barraesqNew', $this->data, true);
        $this->data["formOut"] = $this->load->view('outsourcing/formOutNew', $this->data, true);
        
        $this->load->view('template/header', $this->data);
        $this->load->view('template/navbar', $this->data);
        $this->load->view('template/barrasup', $this->data);    
        $this->load->view('outsourcing/indexNew', $this->data);
        $this->load->view('template/footer');
        $this->load->view('js/jsRedirectOutsourcing', $this->data);  
        $this->load->view('js/jsMascFonesForms', $this->data);
        $this->load->view('js/jsMascCnpjForms', $this->data);
    }

    public function montarMenu($menu, $id_pai = 0)
    {
        //if($id_pai !== 0):
        //    echo 'filho';
        //    exit;
        //endif;
        
        if($id_pai !== 0):
            $view = "<div id='collapse$id_pai' data-id='' class='accordion-body collapse menu-categoria'>";
        else:
            $view = "<div class='accordion-group'>";
        endif;
        
        foreach($menu[$id_pai] as $key => $linha):
                
            if($id_pai !== 0):
                $aclass = 'accordion-toggle';
                $icon = '<i class="icon-double-angle-right"></i>';
            else:
                $aclass = 'menu-linha';
                $icon = '';
            endif;
              
            $data = (isset($menu[$key])) ? "data-toggle='collapse' data-parent='#accordion2'" : " ";
            $ahref = (isset($menu[$key])) ? "#collapse$key" : base_url("outsourcing/ver/$linha->id_outsourcing/".url_codificar($linha->titulo));
            $icon = (isset($menu[$key])) ? "<i class='icon-chevron-right'></i>" : $icon;
            $span = (isset($menu[$key])) ? "<span class='caret pull-right' style='margin-top: 6px;'></span>" : "";
            
            
            
            $view .= "<div class='accordion-inner' id='header'>
                            <a class='$aclass fontMenu' id='headerLink' $data href='$ahref'>
                                $icon $linha->titulo $span
                            </a>
                      </div>";     
                
            if(isset($menu[$key])):                
                $view .= $this->montarMenu($menu, $key);
            endif;   
        endforeach;
        
        $view .= "</div>";
                      
        return $view;
    }
}