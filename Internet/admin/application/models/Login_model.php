<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
*   Site / Administração
*   William Feliciano     
**/

class Login_model extends CI_Model
{
    function __construct() 
    {
        $this->db = $this->load->database('default', TRUE);
    }
    
    //LOGIN
    //Consulta para Obter Informações do Usuário para Login
    public function getLogin($obj)
    {   
        if($obj->id == 0)
            $this->db->where("email = '$obj->user' AND senha = MD5('$obj->pass')");
        else        
            $this->db->where("id_empregado", $obj->id);
        $this->db->where("ativo", 1);
        $rst = $this->db->get("usuarios")->row();  
        if($rst)
        {
            if($obj->id != 0)
            {
                $rst->nome = $obj->nome;
                $rst->email = $obj->email;
            }
        }
        
        return $rst;
    }
    
    //Registra o ultimo Dia e Horário que o Login do Usuário foi realizado
    public function registroLogin($id)
    {
        $this->db->set("dt_login", "NOW()", false);
        $this->db->where("id", $id);
        $this->db->update("usuarios");
    }
    
    //Consulta para Obter informações do Grupo que pertence o Usuário
    public function getGrupo($id = null)
    {
        $this->db->select("nome");
        $this->db->where("id", $id);
        $qry = $this->db->get("grupos");
        $query = $qry->row();
        return $query->nome;
    }
    
    public function chk_terceiro($user, $pass)
    {
        
    }
}

