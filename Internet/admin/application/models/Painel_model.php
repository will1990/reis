<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel_model extends CI_Model 
{   
    function __construct() 
    {        
        $this->db = $this->load->database('portal', true);
    }
    
    public function get_numeros()
    {
        $result["total_noticias"] = $this->db->count_all("noticia");
        
        $this->db->where("dt_noticia >= DATE_SUB(NOW(),INTERVAL 30 DAY)");
        $result["total_noticias_novas"] = $this->db->count_all_results("noticia");
        
        $this->db->where("dt_noticia <= DATE_SUB(NOW(),INTERVAL 365 DAY)");
        $result["total_noticias_antigas"] = $this->db->count_all_results("noticia");
        
        $result["total_categorias"] = $this->db->count_all("noticia_categoria");
        
        return (object)$result;
    }
}