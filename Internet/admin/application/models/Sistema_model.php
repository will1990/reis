<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_model extends CI_Model
{
    function __construct() 
    {        
        $this->db = $this->load->database('default', TRUE);
    }
    
    //MÉTODOS
    //Obtem Método da Tabela MÉTODO
    public function getMetodo($classe, $metodo)
    {
        $this->db->where("classe", $classe);
        $this->db->where("metodo", $metodo);
        return $this->db->get("metodos");
    }
    
    public function get_metodo($id)
    {
        $this->db->where("id", $id);
        return $this->db->get("metodos")->row();
    }
    
    //Obtem o ID do Método para Comparações e Montagem de Views
    public function getInfoMetodo($metodo, $grupo, $acao)
    {
        /*$this->db->select("id, icon, url, parametro");
        $this->db->where("classe", $classe);
        $this->db->where("metodo", $metodo);
        $qry = $this->db->get("metodos");
        $query = $qry->row();
               
        if($query != null):            
            $query->url = remontarUrl($query->url);
            return $query;
        else:
            return null;
        endif; */
        $this->db->where("id_metodo = $metodo AND id_grupo = $grupo AND $acao = 1");
        $rst = $this->db->get("permissoes_grupos")->row();
        if($rst)
        {
            $met = $this->get_metodo($metodo);
            $sub_metodo = ($met->metodo != "index") ? "_".$met->metodo : "";
            $sub_metodo = str_replace("editar", "", $sub_metodo);
            $rst->url = base_url("$met->classe/$acao". $sub_metodo);
            $rst->acao = strtoupper($acao);
        }
        return $rst;
    }
    
    //Adiciona Novo Método a Tabela MÉTODO
    public function addMetodo($classe, $metodo)
    {
        $this->db->set("classe", $classe);
        $this->db->set("metodo", $metodo);
        $this->db->set("parametro", "1");
        $this->db->set("url", "$classe/$metodo");
        $this->db->set("modulo", "ADMIN");
        $this->db->insert('metodos');
    }
    
    //TIPOS
    //Obtem todos os tipos de Determinada Categoria ou Todos Existentes dependendo do Parâmetro
    public function getCategs($tipo = null, $entrada = null)
    {
        $this->db->select("*");
        $this->db->from("site_tipos");
        $this->db->where("tipo", $tipo);
        
        if($entrada !== null)
        {
            if(is_numeric($entrada) !== false)
            {
                $this->db->where("id", $entrada);
            }
            else
            {
                $this->db->where("valor", $entrada);
            }
            $qry = $this->db->get();
            return $qry->row();
        }
        else
        {
            $qry = $this->db->get();
            return $qry->result();
        }      
    }
        
    //PERMISSÕES
    //Verifica as permissões do Usuário Logado
    public function getPermUser($data = null)
    {       
        $this->db->select("*");
        $this->db->where("id_usuario", $data->user_id);
        $this->db->where("id_metodo", $data->metodo);
        $this->db->where("liberar", 1);
        $query = $this->db->get("permissoes_usuarios");
          
        if($query->result_id->num_rows > 0)
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Verifica as permissões do Grupo deste Usuário
    public function getPermGroup($data = null)
    {
        $this->db->select("*");        
        $this->db->where("id_grupo", $data->id_grupo);
        $this->db->where("id_metodo", $data->metodo);
        $this->db->where("liberar", 1);
        $query = $this->db->get("permissoes_grupos");
                      
        if($query->result_id->num_rows > 0)
        { 
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //MENU
    //Obter as Classes para o Menu Principal da Sidebar (Agrupado)
    public function getCategsMenu()
    {
        $this->db->select("classe");
        $this->db->from("metodos");        
        $this->db->where("menu >", 0);
        $this->db->group_by("classe");
        $this->db->order_by("id", "ASC");
        $qry = $this->db->get();      
        return $qry->result();             
    }
    
    //Obter Classes/Categorias Pai para o Menu (Individual)
    public function getCategMenu($classe = null, $nivel = null)
    {
        $this->db->select("*");
        $this->db->from("metodos");
        if($classe !== null)
        {
            $this->db->where("classe", $classe);
        }        
        if($nivel !== null)
        {
            $this->db->where("menu", $nivel);
        }
        $qry = $this->db->get();
        
        if($qry->result_id->num_rows > 0)
        {
            $qry = $qry->row();
            $qry->url = ($qry->url !== '#')? explode("/", $qry->url)[3] : $qry->url ;
                        
            return $qry;
        }
        else
        {            
            return (object)array("metodo" => $classe);
        }
    }
    
    //Obter os SubMenus para o Menu Principal da Sidebar
    public function getSubCategsMenu($classe = null, $param = null, $nivel = null)
    {
        $this->db->select("*");
        $this->db->from("metodos");
        if($classe !== null)
        {
            $this->db->where("classe", $classe);
        }
        if($param !== null)
        {
            $this->db->where("parametro", $param);
        }
        if($nivel !== null)
        {
            $this->db->where("menu", $nivel);
        }
        $this->db->order_by("parametro", "ASC");
        $qry = $this->db->get();
        
        if($qry->result_id->num_rows > 0)
        {
            $resul = $qry->result();
            foreach ($resul as $res)
            {
                $res->url = remontarUrl($res->url);
            }
            return $resul;
        }
        else
        {
            return null;
        }
    }
    
    //Obter todos os Métodos permitidos Para o Usuário ou Grupo
    public function getMetodosPermitidos($tabela = null, $coluna= null, $id = null )
    {        
        $this->db->select("id_metodo");
        $this->db->from($tabela);
        $this->db->where($coluna);
        $this->db->where("liberar", 1);
        $qry = $this->db->get();
        $query = $qry->result();
        
        $resul = array();
        foreach ($query as $qry)
        {
            $resul[] .= $qry->id_metodo;
        }
        return $query;
    }
    
    //GERAL - VIEWS
    //Obter Título de Barra superior através do Tipo de Listagem
    public function getType($tipo = null)
    {
        if($tipo != null)
        {
            $this->db->select("*");
            $this->db->from("site_tipos");
            $this->db->where("id", $tipo);
            $qry = $this->db->get();
            $qry = $qry->result();
            return $qry[0];            
        }
        else
        {
            return null;
        }
    }
    
    public function get_menu()
    {
        $this->db->order_by("id_pai","ASC");
        $this->db->order_by("ordem","ASC");
        $rst = $this->db->get_where("menu", "ativo = 1")->result();
        $arr_menu = array();
        foreach($rst as $item)
        {
            $arr_menu[$item->id_pai][$item->id] = array("titulo" => $item->titulo
                                                      , "classe" => $item->classe
                                                      , "metodo" => $this->get_metodo($item->id_metodo));
        }
        
        return $arr_menu;
    }
    
    public function show_menu($arr_menu, $id_pai = 0)
    {
        if($id_pai)
            $menu = "<ul class='treeview-menu'>";
        else
            $menu = "<ul class='sidebar-menu'><li class='header'>NAVEGAÇÃO PRINCIPAL</li>";
        
        foreach($arr_menu[$id_pai] as $id_menu => $item)
        {
            if($this->get_item_menu($item))
            {
                $active = ($this->router->class == $item["classe"]) ? "active" : "";
                $active_item = ($this->router->method == $item["metodo"]->metodo) ? "active" : "";
                $metodo = $item["metodo"];
                $dropdown = (isset($arr_menu[$id_menu])) ? "dropdown" : "";
                $ddlink = (isset($arr_menu[$id_menu])) ? "class='dropdown-toggle $active' data-toggle='dropdown'" : "";                    
                $link = "$metodo->classe/$metodo->metodo";
                
                $titulo = $item['titulo'];
                $icone = "fa-circle-o";
                if(!$id_pai)
                    list($icone, $titulo) = explode("|", $item['titulo']);
                
                $menu .= ($id_pai) ? "<li class='$active_item'><a href='". base_url($link) ."'><i class='fa $icone'></i> ". $titulo : "<li class='treeview $active'><a href='". base_url($link) ."'><i class='fa $icone'></i>". $titulo;

                if(isset($arr_menu[$id_menu]))                
                    $menu .= "<span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>". $this->show_menu($arr_menu, $id_menu);
                else
                    $menu .= "</a>";

                $menu .= "</li>";
            }
            else
            {
                if(isset($arr_menu[$id_menu]))
                {
                    $menu .= "<li class='treeview'>";
                    list($icone, $titulo) = explode("|", $item['titulo']);
                    $menu .= "<a href='#'><i class='fa $icone'></i>". $titulo ."<span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>";
                    $menu .= $this->show_menu($arr_menu, $id_menu);
                    $menu .= "</li>";
                }
            }
        }
        $menu .= ($id_pai) ? "</ul>" : "";
        
        return $menu;
    }
    
    public function get_item_menu($item)
    {
        $dados = $this->data["login"];
        
        if($item["metodo"])
        {
            $usuario = $this->getMetodosPermitidos("permissoes_usuarios", "id_metodo = ". $item["metodo"]->id ." AND id_usuario = $dados->user_id");
            $grupo = $this->getMetodosPermitidos("permissoes_grupos", "id_metodo = ". $item["metodo"]->id ." AND id_grupo = $dados->id_grupo");
            $permitidos = array_unique(array_merge($usuario, $grupo));
            if($permitidos)
                return true;
            else
                return false;
        }
        else
            return false;
    }
}
