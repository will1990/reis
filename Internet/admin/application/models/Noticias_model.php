<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias_model extends CI_Model 
{   
    function __construct() 
    {        
        $this->db = $this->load->database('portal', true);        
    } 
    
    public function get_noticias()
    {
        $this->db->order_by("dt_noticia", "desc");
        $rst = $this->db->get("noticia")->result();
        $result = array();
        if($rst)
        {
            foreach($rst as $item)
            {
                $result[] = $this->get_noticia($item->cd_noticia);
            }
        }
        return $result;
    }
    
    public function get_noticia($id)
    {
        $this->db->where("cd_noticia", $id);
        $rst = $this->db->get("noticia")->row();
        if($rst)
        {
            $this->db->where("cd_categoria = $rst->cd_categoria");
            $rst->categoria = $this->db->get("noticia_categoria")->row();
                
            // verifica se existe a imagem
            if(!empty($rst->nm_foto))
                if(file_exists(NOT_IMG_PATH . $rst->nm_foto))
                    $rst->url_foto =  base_url(IMG_PATH . $rst->nm_foto);
                else
                    $rst->url_foto = false;
            else
                $rst->url_foto = false;
        }
        return $rst;
    }
    
    public function get_categorias()
    {
        $this->db->order_by("nm_categoria", "asc");
        $rst = $this->db->get("noticia_categoria")->result();
        return $rst;
    }
    
    public function set_noticia()
    {
        $data = (object)$this->input->post();
        $dados = $this->data["login"];
                
        $this->db->set("nm_noticia", $data->titulo);
        $this->db->set("dt_noticia", "STR_TO_DATE('$data->data','%d/%m/%Y')", false);
        $this->db->set("ds_integra_noticia", $data->integra);
        $this->db->set("ds_chamada_noticia", $data->chamada);
        $this->db->set("cd_categoria", $data->categoria);
        
        if(isset($data->upload_data))
            $this->db->set("nm_foto", $data->upload_data["file_name"]);
        
        if(isset($data->excluir_foto))
        {
            $ft = $this->db->get_where("noticia", "cd_noticia = $data->id")->row();
            unlink(NOT_IMG_PATH . $ft->nm_foto);
            $this->db->set("nm_foto", "NULL", false);
        }
        
        if($data->id)
        {
            $this->db->where("cd_noticia = $data->id");
            if($this->db->update("noticia"))
                $this->session->set_flashdata("message", alert_message("Notícia atualizada com sucesso", "success"));
            else
                $this->session->set_flashdata("message", alert_message("Não foi possível atualizar a Notícia", "warning"));
            
            $id = $data->id;
        }
        else
        {
            if($this->db->insert("noticia"))
                $this->session->set_flashdata("message", alert_message("Notícia inserida com sucesso", "success"));
            else
                $this->session->set_flashdata("message", alert_message("Não foi possível inserir a Notícia", "warning"));
            
            $id = $this->db->insert_id();
        }
        
        redirect(base_url("noticias/editar/$id"));
    }
    
    public function del_noticia($id)
    {
        $result = array("result" => false, "msg" => "");
        
        $this->db->where("cd_noticia = $id");
        $noticia = $this->db->get("noticia")->row();
        
        if($noticia)
        {        
            $this->db->where("cd_noticia = $id");
            if($this->db->delete("noticia"))
            {
                $result["result"] = true;
                $result["msg"] = "Notícia, $noticia->nm_noticia, excluída com sucesso.";
            }
            else
            {
                $result["msg"] = "Não foi possível excluir esta notícia";
            }
        }
        else
            $result["msg"] = "Nunhem notícia encontrada para esta ação";
        
        return json_encode($result);
    }
    
    public function get_categoria($id)
    {
        $this->db->where("cd_categoria = $id");
        $rst = $this->db->get("noticia_categoria")->row();
        return $rst;
    }
    
    public function set_categoria()
    {
        $data = (object)$this->input->post();
        $dados = $this->data["login"];
                
        $this->db->set("nm_categoria", $data->titulo);
        
        if($data->id)
        {
            $this->db->where("cd_categoria = $data->id");
            if($this->db->update("noticia_categoria"))
                $this->session->set_flashdata("message", alert_message("Categoria atualizada com sucesso", "success"));
            else
                $this->session->set_flashdata("message", alert_message("Não foi possível atualizar a Categoria", "warning"));
            
            $id = $data->id;
        }
        else
        {
            if($this->db->insert("noticia_categoria"))
                $this->session->set_flashdata("message", alert_message("Categoria inserida com sucesso", "success"));
            else
                $this->session->set_flashdata("message", alert_message("Não foi possível inserir a Categoria", "warning"));
            
            $id = $this->db->insert_id();
        }
        
        redirect(base_url("noticias/editar_categorias/$id"));
    }
    
    public function del_categoria($id)
    {
        $result = array("result" => false, "msg" => "");
        
        $this->db->where("cd_categoria = $id");
        $rst = $this->db->get("noticia_categoria")->row();
        
        if($rst)
        {        
            $this->db->where("cd_categoria = $id");
            $noticias = $this->db->count_all_results("noticia");
            
            if($noticias == 0)
            {
                $this->db->where("cd_categoria = $id");
                if($this->db->delete("noticia_categoria"))
                {
                    $result["result"] = true;
                    $result["msg"] = "Categoria, $rst->nm_categoria, excluída com sucesso.";
                }
                else
                {
                    $result["msg"] = "Não foi possível excluir esta categoria";
                }
            }
            else
                $result["msg"] = "Ação cancelada. Existem $noticias notícias atreladas a esta categoria.";
        }
        else
            $result["msg"] = "Nunhem categoria encontrada para esta ação";
        
        return json_encode($result);
    }
}