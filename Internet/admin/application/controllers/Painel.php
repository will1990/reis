<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
                
        // Verifica se tem permissao nesta pagina
        $this->data["login"] = $this->sistema->checkLogin($this->router->class, $this->router->method);
        
        // Recupera a mensagem de status, se houver
	$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
                        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->data["login"]);
        
        $this->load->model("Painel_model","m_painel");
    } 
    
    public function index()
    {
        $painel = $this->m_painel->get_numeros();
        $this->data["total_noticias"] = $painel->total_noticias;
        $this->data["total_noticias_novas"] = $painel->total_noticias_novas;
        $this->data["total_noticias_antigas"] = $painel->total_noticias_antigas;
        $this->data["total_categorias"] = $painel->total_categorias;
        
        $this->data["header"] = $this->load->view('template/header', $this->data, true);
        $this->data["footer"] = $this->load->view('template/footer', $this->data, true);
        $this->data["navbar"] = $this->load->view('template/navbar', $this->data, true);
        $this->data["sidebar"] = $this->load->view('template/sidebar', $this->data, true);
        $this->data["content"] = $this->load->view('painel/home', $this->data, true);
        $this->load->view("template/conteudo", $this->data);
    }
}
