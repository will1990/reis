<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
    /**
     *  Site / Administração
     *  William Feliciano     
     */
    
    function __construct() 
    {
        parent::__construct();
                        
        // Recupera a mensagem de status, se houver
	$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
        
        // Carrega o modelo
        $this->load->model("Login_model","mdlogin");
    }
    
    //LOGIN
    //Formulário de Login - Interrompe sessões ao Executar Obrigatoriamente
    public function index()
    {      
        // Termina a sessão obrigatoriamente
        $this->logout(false);
        
        // Valida as informações do usuário        
        $this->form_validation->set_rules('loginEmpresa', 'Empresa', 'required');
        $this->form_validation->set_rules('loginUser', 'Usuário', 'required');
        $this->form_validation->set_rules('loginPass', 'Senha', 'required|callback_login_user');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        
        if($this->form_validation->run($this) !== FALSE)
        {    
            redirect(base_url("painel"));                     
        }
        else
        {            
            $this->data["header"] = $this->load->view("template/header", $this->data, true);
            $this->data["footer"] = $this->load->view("template/footer", $this->data, true);
            $this->load->view("login/login", $this->data);
        }
    }
    
    //Validação de Login - Abertura de Sessão
    public function login_user($pass)            
    { 
        $user = $this->input->post("loginUser");
        $emp = $this->input->post("loginEmpresa");
        
        if($emp > 0)
        {
            // Valida funcionario
            $url = 'http://intranet.reisoffice.com.br/registro/senhas/validar_usuario/'.$user.'/'.$pass.'/'.$emp;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FILETIME, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $json = curl_exec($ch);
            $obj = json_decode($json); 
        }        
        else
        {
            // Valida tercerizado
            $obj = (object)array("result" => true, "user" => $user, "pass" => $pass, "id" => 0);
        }
                
        $login = $this->validaLogin($obj);
                
        if($login->logged === false)
        {
            $this->form_validation->set_message("login_user", $login->error);
            return false;
        }
        else
        {
            $this->mdlogin->registroLogin($login->user_id);
            $this->session->set_userdata(array("is_logged_inter" => true, "dados_inter" => $login));
            return true;
        }
        
    }
    
    public function validaLogin($obj)
    {  
        $data = array(  "logged"        => FALSE,
                        "error"         => "",
                        "nome"          => "",
                        "user_id"       => "",
                        "id_tipo"       => "",
                        "id_grupo"      => "",
                        "nm_grupo"      => "",
                        "fname"         => ""   );
                            
        if($obj->result !== false)
        {
            $query = $this->mdlogin->getLogin($obj);
                                    
            if($query)
            {
                $result = $query;
                            
                $data['logged'] = true;
                $data['nome'] = $result->nome;
                $data['emp_id'] = $obj->id;
                $data['user_id'] = $result->id;
                $data['id_tipo'] = $result->id_tipo;
                $data['id_grupo'] = $result->id_grupo;
                $data['nm_grupo'] = $this->mdlogin->getGrupo($result->id_grupo);
                $data['fname'] = ucfirst(strtolower(explode(' ', $result->nome)[0]));        
            }
            else
            {        
                $data['error'] = "Acesso não Permitido";                
            }
        }
        else
        {
            $data["error"] = $obj->error;
        }
        return (object)$data;
    }
    
    //LOGOUT
    //Destroi a Sessão
    public function logout($redir = TRUE)
    {
        //$dados = $this->data["dados"];
        $this->session->unset_userdata(array("is_logged_inter", "dados_inter"));
        
        if($redir)
        {
            redirect(base_url("login"));
        }
    }
    
    //Redirecionamento e Tratamento em caso de Falta de Autoridade para a Operação desejada
    public function noaut()
    {
        $this->session->set_flashdata("erro", "Falta autoridade!");        
        if(!empty($_SERVER['HTTP_REFERER']))
        {
            header("Location: ".$_SERVER['HTTP_REFERER']."");            
        }
        else
        {
            redirect(base_url("painel"));
        }
    }    
}