<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller 
{
    /** 
     *  Site / Administração
     *  Fagner Valerio
     */
    
    function __construct() 
    {
        parent::__construct();
                
        // Verifica se tem permissao nesta pagina
        $this->data["login"] = $this->sistema->checkLogin($this->router->class, $this->router->method);
        
        // Recupera a mensagem de status, se houver
	$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
                        
        // Monta o Menu específico para o Usuário Logado
        $this->data["menu"] = $this->sistema->montarMenu($this->data["login"]);        
        
        define("IMG_PATH", "../arquivos/noticias/thumb/");
        define("NOT_IMG_PATH", APPPATH .'../'. IMG_PATH);
        
        $this->load->model("Noticias_model","m_noticias");
        
        $this->data["page_title"] = "Notícias";
        $this->data["page_description"] = "Notícias sobre a Reiso Office";
    } 
    
    public function index()
    {
        $this->data["noticias"] = $this->m_noticias->get_noticias();
        $this->data["botoes"] = $this->data["login"]->permissoes;
                
        $this->data["js"] = $this->load->view('noticias/js/lista', $this->data, true);        
        $this->data["header"] = $this->load->view('template/header', $this->data, true);
        $this->data["footer"] = $this->load->view('template/footer', $this->data, true);
        $this->data["navbar"] = $this->load->view('template/navbar', $this->data, true);
        $this->data["sidebar"] = $this->load->view('template/sidebar', $this->data, true);
        $this->data["content"] = $this->load->view('noticias/list_view', $this->data, true);
        $this->load->view("template/conteudo", $this->data);
    }
    
    public function novo()
    {
        $this->editar();
    }
    
    public function editar($id = 0)
    {
        $this->form_validation->set_rules('data', 'Titulo do Banner', 'required');
        $this->form_validation->set_rules('titulo', 'Titulo do Banner', 'required');        
        $this->form_validation->set_rules('chamada', 'Titulo do Banner', 'required');
        $this->form_validation->set_rules('integra', 'Titulo do Banner', 'required');
        $this->form_validation->set_rules('categoria', 'Titulo do Banner', 'required');
        $this->form_validation->set_rules('foto', 'Titulo do Banner', 'callback_foto_upload');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        
        if($this->form_validation->run() == false)
        {            
            $this->data["noticia"] = $this->m_noticias->get_noticia($id);
            $this->data["categorias"] = $this->m_noticias->get_categorias();

            //$this->data["js"] = $this->load->view('noticias/js/form', $this->data, true);        
            $this->data["header"] = $this->load->view('template/header', $this->data, true);
            $this->data["footer"] = $this->load->view('template/footer', $this->data, true);
            $this->data["navbar"] = $this->load->view('template/navbar', $this->data, true);
            $this->data["sidebar"] = $this->load->view('template/sidebar', $this->data, true);
            $this->data["content"] = $this->load->view('noticias/form_view', $this->data, true);
            $this->load->view("template/conteudo", $this->data);
        }
        else
        {
            $this->m_noticias->set_noticia();
        }
    }
    
    public function foto_upload($param)
    {
        $config['upload_path']          = NOT_IMG_PATH;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name']         = true;
        $config['max_size']             = 2048;
        $config['max_width']            = 1960;
        $config['max_height']           = 1024;

        if(empty($_FILES["foto"]["name"]))
            return true;
        else
        {        
            $this->load->library('upload', $config);        
        
            if(!$this->upload->do_upload("foto"))
            {
                $this->form_validation->set_message("foto_upload", $this->upload->display_errors());
                return false;
            }
            else
            {
                $_POST["upload_data"] = $this->upload->data();
                return true;
            }
        }
    }
    
    public function remover($id)
    {
        echo $this->m_noticias->del_noticia($id);
    }
    
    public function categorias()
    {
        $this->data["categorias"] = $this->m_noticias->get_categorias();
        $this->data["botoes"] = $this->data["login"]->permissoes;
                
        $this->data["js"] = $this->load->view('noticias/js/lista', $this->data, true);        
        $this->data["header"] = $this->load->view('template/header', $this->data, true);
        $this->data["footer"] = $this->load->view('template/footer', $this->data, true);
        $this->data["navbar"] = $this->load->view('template/navbar', $this->data, true);
        $this->data["sidebar"] = $this->load->view('template/sidebar', $this->data, true);
        $this->data["content"] = $this->load->view('noticias/list_categ_view', $this->data, true);
        $this->load->view("template/conteudo", $this->data);
    }
    
    public function novo_categorias()
    {
        $this->editar_categorias();
    }
    
    public function editar_categorias($id = 0)
    {
        $this->form_validation->set_rules('titulo', 'Titulo do Banner', 'required');        
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        
        if($this->form_validation->run() == false)
        {            
            $this->data["categoria"] = $this->m_noticias->get_categoria($id);
            
            //$this->data["js"] = $this->load->view('noticias/js/form', $this->data, true);        
            $this->data["header"] = $this->load->view('template/header', $this->data, true);
            $this->data["footer"] = $this->load->view('template/footer', $this->data, true);
            $this->data["navbar"] = $this->load->view('template/navbar', $this->data, true);
            $this->data["sidebar"] = $this->load->view('template/sidebar', $this->data, true);
            $this->data["content"] = $this->load->view('noticias/form_categ_view', $this->data, true);
            $this->load->view("template/conteudo", $this->data);
        }
        else
        {
            $this->m_noticias->set_categoria();
        }
    }
    
    public function remover_categorias($id)
    {
        echo $this->m_noticias->del_categoria($id);
    }
}
