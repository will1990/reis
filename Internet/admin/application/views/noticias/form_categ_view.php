<form class="form form-vertical" action="<?= base_url("noticias/editar_categorias")?>" method="post" enctype="multipart/form-data">
    <?= validation_errors()?>
    <?= $message?>
    <div class="box">
        <div class="box-body">                
            <div class="row">                                                      
                <section class="col-md-6 col-lg-6 form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3 text-right">Título</label>
                        <div class="col-md-8 col-lg-9">
                            <input class="form-control" value="<?= set_value("titulo", @$categoria->nm_categoria)?>" name="titulo" required/>
                        </div>
                    </div>                    
                </section>                              
            </div>        
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12 col-lg-12 form-actions">
                    <input type="hidden" name="id" value="<?= @$categoria->cd_categoria?>"/>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                    <a href="<?= base_url("noticias/categorias")?>" class="btn btn-default"><i class="fa fa-ban"></i> Cancelar</a>
                    <?php if(@$categoria->cd_categoria): ?>
                        <a href="<?= base_url("noticias/remover_categorias/".@$categoria->cd_categoria)?>" class="btn btn-danger btn-excluir pull-right" data-redir="<?= base_url("noticias/categorias")?>" data-text="Excluir <?= strtoupper(@$categoria->nm_categoria)?>?"><i class="fa fa-trash"></i> Excluir</a>
                    <?php endif; ?>    
                </div>
            </div>
        </div>
    </div>
</form>             