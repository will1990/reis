<div class="row">
    <div class="col-lg-12">
        <?= $message?>
    </div>
</div>
<div class="box">
    <div class="box-body table-responsive">
        <p><?= mostrar_botao($botoes, "NOVO")?></p>
        <table width="100%" id="tbCategorias" class="table table-bordered table-striped">
            <thead>                    
                <tr>
                    <th>Ações</th>
                    <th>Título</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($categorias as $item): ?>                
                    <tr>
                        <td style="vertical-align: middle">
                            <?= mostrar_botao($botoes, "EDITAR", null, $item->cd_categoria)?>
                            <?= mostrar_botao($botoes, "REMOVER", null, $item->cd_categoria, "data-redir='' data-text='Deseja excluir esta Notícia?'")?>
                        </td>
                        <td style="vertical-align: middle"><?=$item->nm_categoria?></td>
                    </tr>                        
                <?php endforeach; ?>            
            </tbody>
        </table>            
    </div>       
</div>