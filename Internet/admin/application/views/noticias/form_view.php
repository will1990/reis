<form class="form form-vertical" action="<?= base_url("noticias/editar")?>" method="post" enctype="multipart/form-data">
    <?= validation_errors()?>
    <?= $message?>
    <div class="box">
        <div class="box-body">                
            <div class="row">                                                      
                <section class="col-md-6 col-lg-6 form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3 text-right">Categoria</label>
                        <div class="col-md-8 col-lg-9">
                            <select class="form-control" name="categoria" required>
                                <option value="">Selecione</option>
                                <?php foreach($categorias as $item): ?>
                                    <option value="<?= $item->cd_categoria?>" <?= set_select("categoria", $item->cd_categoria, ($item->cd_categoria == @$noticia->cd_categoria) ? true : false)?>><?= $item->nm_categoria?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3 text-right">Título</label>
                        <div class="col-md-8 col-lg-9">
                            <input class="form-control" value="<?= set_value("titulo", @$noticia->nm_noticia)?>" name="titulo" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3">Data</label>                                
                        <div class="col-md-8 col-lg-9">
                            <input class="form-control data-br" value="<?= set_value("data", date("d/m/Y", strtotime(@$noticia->dt_noticia)))?>" name="data" required/>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3">Chamada</label>                                
                        <div class="col-md-8 col-lg-9">
                            <textarea class="form-control" name="chamada" rows="3" required><?= set_value("chamada", @$noticia->ds_chamada_noticia)?></textarea>
                        </div>
                    </div>
                </section>              
                <section class="col-md-6 col-lg-6 form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3">Imagem Destaque</label>                                
                        <div class="col-md-8 col-lg-9">
                            <input type="file" class="form-control" name="foto" />
                        </div>
                    </div>
                    <?php if(@$noticia->url_foto): ?>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-3">
                                Excluir imagem
                                <input type="checkbox" value="1" name="excluir_foto"/>
                            </label>                                
                            <div class="col-md-8 col-lg-9 img-responsive">
                                <img src="<?= $noticia->url_foto?>" />
                            </div>
                        </div>
                    <?php endif; ?>
                </section>              
            </div>        
            <div class="row">
                <section class="col-md-12 col-lg-12 form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12 col-lg-12">
                            <label class="control-label">Íntegra</label>
                        </div>
                    </div>
                    <div class="form-group">            
                        <div class="col-md-12 col-lg-12">
                            <textarea class="editor" style="width: 100%" name="integra" rows="30" required><?= set_value("integra", prepNoticia(@$noticia->ds_integra_noticia))?></textarea>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-md-12 col-lg-12 form-actions">
                    <input type="hidden" name="id" value="<?= @$noticia->cd_noticia?>"/>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                    <a href="<?= base_url("noticias/index")?>" class="btn btn-default"><i class="fa fa-ban"></i> Cancelar</a>
                    <?php if(@$noticia->cd_noticia): ?>
                        <a href="<?= base_url("noticias/remover/".@$noticia->cd_noticia)?>" class="btn btn-danger btn-excluir pull-right" data-redir="<?= base_url("noticias/index")?>" data-text="Excluir <?= strtoupper(@$noticia->nm_noticia)?>?"><i class="fa fa-trash"></i> Excluir</a>
                    <?php endif; ?>    
                </div>
            </div>
        </div>
    </div>
</form>             