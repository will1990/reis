<script type="text/javascript">
    //script responsavel pelas tabelas (DataTables)
    $(document).ready(function(){
        $('#tbBanners').DataTable({
            language: {
                url: "<?=base_url("assets/DataTables-1.10.15/lang/portugues-brasil.json")?>"
            },
            stateSave: true,
            paging: true,
            sorting: [[ 1, "asc" ]],
            columnDefs: [
                { targets: [0], sortable: false, searchable: false },
                { targets: [0,2,3,4], className: "text-center" },
                { targets: [2,3], type: "date-uk" }
            ]
        });   
    });    
</script>
