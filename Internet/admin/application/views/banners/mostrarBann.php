<div class="col-lg-12"> 
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Ver Banner</h2>
        </div><!-- /.box-header -->
        <div class="box-body">                
            <div class="row">                                                      
                <section class="col-md-6 col-lg-6 form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3 text-right">
                            Título:                                
                        </label>
                        <div class="col-md-8 col-lg-7">
                            <h4><?=$banner->titulo?></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-lg-3">Descrição: </label>                                
                        <div class="col-md-8 col-lg-7 well">
                            <?=$banner->texto?>
                        </div>
                    </div>
                    
                    <?php
                    
                    if(!empty($banner->dt_inicio) AND !empty($banner->dt_fim))
                    {
                        ?>
                        <hr/>
                        <div class="form-group">                                               
                            <div class="col-xs-12 col-md-12 col-lg-12 text-center">
                                <h4><b>Vigência</b></h4>                            
                            </div>                            
                        </div>
                        <div class="form-group">                        
                            <section class="col-xs-6 col-md-9 col-lg-6">
                                <label class="control-label col-md-6 col-lg-6">
                                    Início:                                
                                </label>
                                <div class="col-md-3 col-lg-6">
                                    <h4><?=$banner->dt_inicio?></h4>
                                </div>
                            </section>
                            <section class="col-xs-6 col-md-9 col-lg-6">
                                <label class="control-label col-md-6 col-lg-4">
                                    Término:                              
                                </label>
                                <div class="col-md-3 col-lg-8">
                                    <h4><?=$banner->dt_fim?></h4>
                                </div>
                            </section>
                        </div>                        
                        <?php
                    }
                    
                    if(!empty($banner->url))
                    {
                        ?>
                        <hr/>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-3 text-right">
                                URL do Botão (Saiba Mais):                                
                            </label>
                            <div class="col-md-8 col-lg-7 dist_sup">
                                <h4><?=$banner->url?></h4>
                            </div>
                        </div>
                        <?php
                    }
                    
                    ?>
                        
                </section>
                <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-lg-3 text-right">
                            Imagem:                                
                        </label>
                        <div class="col-md-4 col-lg-8 dist_sup">                            
                            <img class="img-responsive" src="<?=$banner->img?>" alt="Banner Index"/>
                        </div>
                    </div>
                </section>                
            </div>
            <hr/>
            <div class="col-lg-12">                
                <a href="<?=base_url("banners/listar/$label->valor")?>" class="btn btn-default btn-lg">Voltar</a>
            </div>
        </div>    
    </div>                 
</div>    



