<div class="col-lg-12"> 
    <form class="form form-vertical" action="<?=base_url("banners/novo/$tipo")?>" method="post" enctype="multipart/form-data">
        <div class="box box-primary">
            <div class="box-header">
                <h2 class="box-title">Novo Banner</h2>
            </div><!-- /.box-header -->
            <div class="box-body">                
                <div class="row">                                                      
                    <section class="col-md-6 col-lg-6 form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-3 text-right">
                                Título:                                
                            </label>
                            <div class="col-md-8 col-lg-7">
                                <input class="form-control" value="<?=$retorno['bannTitulo']?>" name="bannTitulo"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-3">Descrição: </label>                                
                            <div class="col-md-8 col-lg-7">
                                <textarea class="form-control" name="bannTexto" style="height: 150px;"><?=$retorno['bannTexto']?></textarea>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <div class="col-md-1 col-lg-2"></div>
                            <div class="col-xs-2 col-md-2 col-lg-1 text-center" style="margin-top: 10px;"><i class="fa fa-info-circle fa-3x"></i></div>
                            <div class="col-xs-10 col-md-9 col-lg-9">
                                <h4>Datas Opcionais</h4>
                                <h5>Caso preenchidas, o banner irá Ativar ou Desativar automaticamente</h5>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <section class="col-xs-6 col-md-9 col-lg-6">
                                <label class="control-label col-md-6 col-lg-6">
                                    Data Início:                                
                                </label>
                                <div class="col-md-3 col-lg-6">
                                    <input id="dtIniAdm" class="form-control" value="<?=$retorno['bannIni']?>" name="bannIni" style="width: 105px;"/>
                                </div>
                            </section>
                            <section class="col-xs-6 col-md-9 col-lg-6">
                                <label class="control-label col-md-6 col-lg-4">
                                    Data Término:                              
                                </label>
                                <div class="col-md-3 col-lg-8">
                                    <input id="dtFimAdm" class="form-control" value="<?=$retorno['bannFim']?>" name="bannFim" style="width: 105px;"/>
                                </div>
                            </section>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-lg-3 text-right">
                                URL do Botão (Saiba Mais):                                
                            </label>
                            <div class="col-md-8 col-lg-7 dist_sup">
                                <input class="form-control" value="<?=$retorno['bannUrl']?>" type="text" name="bannUrl"/>
                            </div>
                        </div>
                    </section>
                    <section class="col-md-6 col-lg-6 form-horizontal" style="border-left: 2px solid #ccc">                        
                        <div class="form-group">                                
                            <div class="col-lg-offset-3 col-md-4 col-lg-9 dist_sup">                                
                                <img id="mini_foto_new" class="img-responsive" src="<?=$pathBaseBann.'/notImg.jpg'?>"/>
                            </div>                      
                        </div>
                        <hr/>                        
                        <div class="form-group">                            
                            <label class="col-lg-3 text-right">
                                Escolha uma Imagem:
                            </label>
                            <div class="col-lg-9">                         
                                <input type="file" id="image" name="bannImg" class="btn btn-default" onchange="readURL(this,'mini_foto_new');" />                           
                            </div>
                        </div>
                    </section>
                    <input type="hidden" name="bannTipo" value="<?=$tipo?>"/>
                </div>
                <br/>
                <hr/>
                <div class="col-lg-12">
                    <input class="btn btn-primary btn-lg" type="submit" name="bannAct" value="Publicar"/>
                </div>                
            </div>    
        </div>
    </form>             
</div>    
    