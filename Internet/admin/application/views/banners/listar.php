<div class="row">
    <div class="col-lg-12">
        <?php
        if(!empty($this->session->flashdata('sucesso')))
        {
            echo '<div class="alert alert-info" role="alert"><h4>'.$this->session->flashdata('sucesso').'</h4></div>';
            $this->session->set_flashdata('sucesso', '');
        }

        if(!empty($this->session->flashdata('erro')))
        {
            echo '<div class="alert alert-danger" role="alert"><h4>'.$this->session->flashdata('erro').'</h4></div>';
            $this->session->set_flashdata('erro', '');
        }

        if(!empty($this->session->flashdata('feito')))
        {
            echo '<div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('feito').'</h4></div>';
            $this->session->set_flashdata('erro', '');
        }            
        ?>
    </div>
</div>
<div class="box">
    <div class="box-body table-responsive">
        <p><?= $conjunto->botaoAdd ?></p>
        <table width="100%" id="tbBanners" class="table table-bordered table-striped">
            <thead>                    
                <tr>
                    <th>Ações</th>
                    <th>Título</th>
                    <th>Data Inicio</th>                        
                    <th>Data Fim</th>
                    <th>Imagem</th>
                </tr>
            </thead>
            <tbody>
                <?php

                foreach($banners as $banner)
                {
                    $botaoDel = ($conjunto->botoesDel !== false) ? '<a href="'.base_url("banners/remover/$banner->id").'" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>': null ;
                    $botaoOnOffView = $banner->ativo > 0 ? '<i class="btn btn-primary btn-xs" style="width:32px;">ON</i>' : '<i class="btn btn-warning btn-xs" style="width:32px;">OFF</i>';
                    $botaoOnOff = ($conjunto->botoesOnOff !== false) ? '<a href="'.base_url("banners/onoff/$banner->id").'">'.$botaoOnOffView.'</a>' : null ;
                    ?>
                    <tr>
                        <td style="vertical-align: middle">
                            <a href="<?=base_url('banners/'.$conjunto->botoesEdit->url.'/'.$banner->id)?>"><i class="btn btn-default fa <?=$conjunto->botoesEdit->icon?> btn-xs"></i></a>
                            <?=$botaoDel?>
                            <?=$botaoOnOff?>
                        </td>
                        <td style="vertical-align: middle"><?=$banner->titulo?></td>
                        <td style="vertical-align: middle"><?=$banner->dt_inicio?></td>
                        <td style="vertical-align: middle"><?=$banner->dt_fim?></td>
                        <td style="vertical-align: middle"><img src="<?= $pathBaseBann .'/'. $banner->img?>" height="50"/></td>
                    </tr>                        
                    <?php 
                }

                ?>            
            </tbody>
        </table>            
    </div>       
</div>