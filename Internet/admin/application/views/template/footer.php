
<!-- jQuery 2.2.3 -->
<script src="<?=base_url('assets/jQuery-2.2.4/jquery-2.2.4.min.js')?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url('assets/bootstrap-3.3.7/js/bootstrap.min.js')?>"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function(event) {
     $.getScript('<?=base_url("assets/plugins/morris/morris.min.js")?>');
  });
</script>
<!-- Sparkline -->
<script src="<?=base_url('assets/plugins/sparkline/jquery.sparkline.min.js')?>"></script>
<!-- jvectormap -->
<script src="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url('assets/plugins/knob/jquery.knob.js')?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?=base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- datepicker -->
<script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js')?>"></script>
<script src="<?=base_url('assets/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js')?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>"></script>
<!-- Slimscroll -->
<script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?=base_url('assets/plugins/fastclick/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('assets/dist/js/app.min.js')?>"></script>

<!-- DataTables -->
<script type="text/javascript" src="<?=base_url('assets/DataTables-1.10.15/js/jquery.dataTables.min.js')?>"></script>
<!-- DataTables bootstrap -->
<script type="text/javascript" src="<?=base_url('assets/DataTables-1.10.15/js/dataTables.bootstrap.min.js')?>"></script>

<!-- jszip -->
<script type="text/javascript" src="<?=base_url('assets/JSZip-3.1.3/jszip.min.js')?>"></script>
<!-- pdfmake -->
<script type="text/javascript" src="<?=base_url('assets/pdfmake-0.1.27/build/pdfmake.min.js')?>"></script>
<!-- vfs_fonts -->
<script type="text/javascript" src="<?=base_url('assets/pdfmake-0.1.27/build/vfs_fonts.js')?>"></script>
<!-- autoFill -->
<script type="text/javascript" src="<?=base_url('assets/AutoFill-2.2.0/js/dataTables.autoFill.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/AutoFill-2.2.0/js/autoFill.bootstrap.min.js')?>"></script>
<!-- Buttons -->
<script type="text/javascript" src="<?=base_url('assets/Buttons-1.3.1/js/dataTables.buttons.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/Buttons-1.3.1/js/buttons.bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/Buttons-1.3.1/js/buttons.html5.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/Buttons-1.3.1/js/buttons.print.min.js')?>"></script>
<!-- responsiveBootstrap -->
<script type="text/javascript" src="<?=base_url('assets/Responsive-2.1.1/js/dataTables.responsive.min.js')?>"></script>
<!-- responsiveDatable -->
<script type="text/javascript" src="<?=base_url('assets/Responsive-2.1.1/js/responsive.bootstrap.min.js')?>"></script>
<!-- Select -->
<script type="text/javascript" src="<?=base_url('assets/Select-1.2.2/js/dataTables.select.min.js')?>"></script>
<!-- Sweetalert -->
<script type="text/javascript" src="<?=base_url('assets/Sweetalert/js/sweetalert.min.js')?>"></script>
<!-- include summernote css/js-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>

<script>
    $(document).ready(function() {
        $.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function (a){
                return parseInt(moment(a, "DD/MM/YYYY").format("X"), 10);
            },
            "date-uk-asc": function (a, b) {
                return a - b;
            },
            "date-uk-desc": function (a, b) {
                return b - a;
            }
        });
        
        $('.data-br').datepicker({            
            language: 'pt-BR',
            format: "dd/mm/yyyy",
            autoclose: true
        });
        
        $('.editor').summernote();
        
        $(".btn-excluir").on("click", function(e){
            e.preventDefault();
            btn = $(this);
            popText = $(this).data("text")
            popUrl = $(this).attr("href")
            popRedir = $(this).data("redir")
            swal({
                title: "Tem certeza?",
                text: popText,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true
            },
            function(isConfirm){
                if (isConfirm)
                {
                    $.ajax({
                        url: popUrl,
                        dataType: "JSON",
                        success: function(data) {
                            if(data.result)
                            {
                                if(popRedir != "")
                                    window.location.href = popRedir;
                                else
                                {
                                    $(btn).closest("tr").remove();
                                    swal(data.msg);
                                }
                            }
                            else
                            {
                                swal("Ops!", data.msg, "error");
                            }
                        }
                    });
                }
            });
        })
    });
</script>

<?= (@$js) ? $js : ''?>