 <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SA</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SiteA</b>dmin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <div class="navbar-custom-menu">
            <!-- Usuário Opções Barra -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">                            
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="hidden-xs"><?=$login->fname?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= base_url('images/logo.png') ?>" class="img-circle" alt="User Image">
                                <p><?=$login->nome?> <small><?=$login->nm_grupo?></small></p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Trocar Senha</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?=base_url('login/logout')?>" class="btn btn-default btn-flat">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </li>         
                </ul>
            </div>
        </div>
    </nav>