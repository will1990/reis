<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AdminLTE 2 | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link href="<?= base_url("assets/bootstrap-3.3.7/css/bootstrap.min.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- Font Awesome -->
<link href="<?= base_url("assets/Font-awesome-4.7.0/css/font-awesome.min.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link href="<?= base_url("assets/dist/css/AdminLTE.min.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="<?= base_url("assets/dist/css/skins/_all-skins.min.css") ?>" rel="stylesheet" type="text/css" /> 


<!-- DataTables bootstrap -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/DataTables-1.10.15/css/dataTables.bootstrap.min.css") ?>"/>

<!-- autoFill -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/AutoFill-2.2.0/css/autoFill.bootstrap.css") ?>"/>
<!-- buttonsBootstrap -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/Buttons-1.3.1/css/buttons.bootstrap.min.css") ?>"/>
<!-- responsiveBootstrap -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/Responsive-2.1.1/css/responsive.bootstrap.min.css") ?>"/>
<!-- selectBootstrap -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/Select-1.2.2/css/select.bootstrap.min.css") ?>"/>

<!-- Sweetalert -->
<link rel="stylesheet" type="text/css" href="<?= base_url("assets/Sweetalert/css/sweetalert.css") ?>"/>

<!-- iCheck -->
<link href="<?= base_url("assets/plugins/iCheck/flat/blue.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- Morris chart -->
<link href="<?= base_url("assets/plugins/morris/morris.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- jvectormap -->
<link href="<?= base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- Date Picker -->
<link href="<?= base_url("assets/plugins/datepicker/datepicker3.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- Daterange picker -->
<link href="<?= base_url("assets/plugins/daterangepicker/daterangepicker.css") ?>" rel="stylesheet" type="text/css" /> 
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?= base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") ?>" rel="stylesheet" type="text/css" /> 

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->