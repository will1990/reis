<!DOCTYPE html>
<html>
<head>
  <?= $header ?>
</head>
<body class="hold-transition skin-blue">
<div class="wrapper">

  <header class="main-header">
      <?= (isset($navbar)) ? $navbar : "" ?>
  </header>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div id="loginbox" style="margin-top: 50px;" class="mainbox col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4 col-sm-8 col-sm-offset-2">  
                <div class="row text-center">
                    <div class="col-lg-12">
                        <img src="<?=base_url('images/logo-reisoffice.png')?>" class="img-responsive col-xs-8 col-sm-7 col-md-6 col-lg-5" alt="ReisOffice"/>  
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px;">
                        <?= $message?>
                        <?= validation_errors()?>
                    </div>
                </div>
                <div class="panel panel-warning">            
                    <div class="panel-heading">                
                        <div class="panel-title">Sistema de Administração do Conteúdo do Site</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><!--<a href="#">Esqueceu a senha?</a>--></div>
                    </div>

                    <div style="padding-top:30px" class="panel-body">
                        <div style="display:none" id="login-alert" class="col-sm-12"></div>                
                        <form id="loginform" class="form-horizontal" action="<?=base_url('login')?>" method="post" role="form">
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                <select class="form-control" name="loginEmpresa">
                                    <option value="">Selecione</option>
                                    <option value="1" <?= set_select("loginEmpresa", "1")?>>Reis Office Comercial</option>
                                    <option value="2" <?= set_select("loginEmpresa", "2")?>>Reis Office Serviços</option>                                    
                                    <option value="0" <?= set_select("loginEmpresa", "0")?>>Empresa Tercerizada</option>
                                </select>
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input id="login-username" type="text" class="form-control" value="<?= set_value("loginUser")?>" name="loginUser" placeholder="Usuário">                                        
                            </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input id="login-password" type="password" class="form-control" value="<?= set_value("loginPass")?>" name="loginPass" placeholder="Senha">
                            </div>

                            <div style="margin-top:10px" class="form-group col-lg-12">               
                                <!-- Button -->
                                <div class="controls col-xs-4 col-sm-4 col-md-3 col-lg-3">
                                    <input type="submit" name="loginAct" class="btn btn-primary btn-lg" value="Login"/>                         
                                </div>
                            </div>                          
                        </form>
                    </div>                     
                </div>
                <!--<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>-->
            </div>    
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
      <strong>Reis Office Products Ltda.</strong> &copy; 2017
    <!-- <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved. -->
  </footer>

</div>
<!-- ./wrapper -->
<?= $footer?>
</body>
</html>