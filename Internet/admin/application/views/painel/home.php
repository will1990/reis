<div class="row">
    <div class="col-md-12">
        <?= $message?>
    </div>
</div>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= @$total_noticias?></h3>

                <p>Notícias</p>
            </div>
            <div class="icon">
                <i class="fa fa-newspaper-o"></i>
            </div>
            <a href="<?= base_url("noticias")?>" class="small-box-footer">Ver todas <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= @$total_noticias_novas?></h3>

                <p>Notícias nos últimos 30 dias</p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-plus-o"></i>
            </div>
            <a href="<?= base_url("noticias")?>" class="small-box-footer">Ver todas <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= @$total_noticias_antigas?></h3>

                <p>Notícias com + de 1 ano</p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-minus-o"></i>
            </div>
            <a href="<?= base_url("noticias")?>" class="small-box-footer">Ver todas <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= @$total_categorias?></h3>

                <p>Categorias de Notícias</p>
            </div>
            <div class="icon">
                <i class="fa fa-reorder"></i>
            </div>
            <a href="<?= base_url("noticias")?>" class="small-box-footer">Ver todas <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>