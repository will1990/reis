<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema 
{
    function __construct() 
    {
        $this->CI =& get_instance();        
        $this->CI->load->model("sistema_model", "mdsys");   
    }
    
    //LOGINS, MÉTODOS e PERMISSÕES
    //Verifica Login e as Permissoes do Usuário a cada Controller
    public function checkLogin($classe, $metodo)
    {
        // verifica se está logado
        $data = $this->CI->session->userdata("dados_inter");
                
        if(isset($data->logged) && $data->logged)
        {    
            // Verifica se o método é novo
            $this->verifMetodo($classe, $metodo);
            
            // Pega os dados do metodo
            $rst = $this->CI->mdsys->getMetodo($classe, $metodo);
            $route = $rst->row();
            
            // verifica se o metodo é privado e se tem premissao no mesmo            
            $data->metodo = $route->id;
            $data->permissoes = $this->permissaoBotoes($data);
                        
            if($route->parametro == 0)
            {                
                return $data;
            }
            elseif($route->parametro == 1 && ($this->CI->mdsys->getPermUser($data) !== false) OR ($this->CI->mdsys->getPermGroup($data) !== false))
            {         
                return $data;
            }            
            else
            {                
                redirect(base_url('login/noaut'));                
            }            
        }
        else
        {
            //$this->session->set_flashdata("message", "É preciso fazer o login para acessar esta página.");
            redirect(base_url('login/logout'));
        }
    }
    
    //Pega a URL, verifica se Metodo Existe e Salva caso não exista
    public function verifMetodo($classe, $metodo)
    {
        $query = $this->CI->mdsys->getMetodo($classe, $metodo); 
        
        if($query->result_id->num_rows == 0)
        {           
            $this->CI->mdsys->addMetodo($classe, $metodo);
        }        
    }
    
    //MENU
    //Monta o Menu da Sidebar de Acordo com as Permissões Exigidas pelo Usuário
    public function montarMenu($dados)
    {        
        //$usuario = $this->CI->mdsys->getMetodosPermitidos("permissoes_usuarios", "id_usuario", $dados->user_id );
        //$grupo = $this->CI->mdsys->getMetodosPermitidos("permissoes_grupos", "id_grupo", $dados->id_grupo );
        //$permitidos = array_unique(array_merge($usuario, $grupo));
        
        $menu = $this->CI->mdsys->get_menu();
        return $this->CI->mdsys->show_menu($menu);
        
        /*
        $query = $this->CI->mdsys->getCategsMenu();
        
        $arrmenu = array();
        foreach ($query as $qry)
        {
            $submenu = array();
            $subquery = $this->CI->mdsys->getSubCategsMenu($qry->classe, null, 2);
            $menu = $this->CI->mdsys->getCategMenu($qry->classe, 1);
            $menu->active = (explode("/", $_SERVER['REQUEST_URI'])[3]) === strtolower($qry->classe)? 'active' :'';  
            if($subquery !== null)
            {
                foreach ($subquery as $sbq)
                {                    
                    $submenu[] = (array_search($sbq->id, $permitidos)!== false)? $sbq : null ;
                }
            }
            else
            {
                $submenu = null;
            }          
            $arrmenu[] = array("nivel1" => $menu, "nivel2" => $submenu );
        }
        */
        //aplicar as permissoes no menu
        
        //return (object)$arrmenu;
        
        //echo '<pre>';
        //print_r($arrmenu);
        //print_r($grupo);
        //print_r($permitidos);
        //echo '</pre>';
        //exit;
        
    }
    
    //TABELAS e LISTAS
    //Monta o conjunto de Botões nas Tabelas de Visualização de Acordo com as Permissões
    public function montarConjuntoBotoes($dados = null)
    {       
        $btAdd = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'novo');
        $btsEdit = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'editar');                               
        $btsDel = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'remover');
        $qryOnOff = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'onoff');
        
        if($qryOnOff != null):
            $btsOnOff = $qryOnOff;
        else:
            $btsOnOff['id'] = 0;
            $btsOnOff = (object)$btsOnOff;
        endif;
        
        //print_R($qryOnOff);        
        //echo array_search($btsOnOff->id, $permitidos)!== false ? 'ok' : 'no';
        //exit;
        
        $botaoAddView = '<a href="'.$btAdd->url.'" class="btn btn-primary"><span class="fa fa-file"></span>&nbsp;&nbsp;'.$btAdd->acao.'</a>';
                
        $botaoEditYes = array();
        $botaoEditYes['id'] = '';
        $botaoEditYes['icon'] = 'fa-edit';
        $botaoEditYes['url'] = 'editar';
                
        $botaoEditNo = array();
        $botaoEditNo['id'] = '';
        $botaoEditNo['icon'] = 'fa fa-eye';
        $botaoEditNo['url'] = 'ver';       
        
        return (object)array(
            "botaoAdd" => ($btAdd !== false) ? $botaoAddView : null,
            "botoesEdit" =>  ($btsEdit !== false) ? (object)$botaoEditYes : (object)$botaoEditNo,
            "botoesDel" => ($btsDel !== false) ? true : false,
            "botoesOnOff" => ($btsOnOff !== false) ? true : false,
        );   
    }   
    
    public function permissaoBotoes($dados)
    {
        $btAdd = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'novo');
        $btEdit = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'editar');                               
        $btDel = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'remover');
        $btOnOff = $this->CI->mdsys->getInfoMetodo($dados->metodo, $dados->id_grupo, 'onoff');
        
        return (object)array(
            "botaoAdd" => $btAdd,
            "botaoEdit" =>  $btEdit,
            "botaoDel" => $btDel,
            "botaoOnOff" => $btOnOff
        );
    }
}