<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Classe com funções básicas para o sistema
 *
 * @author fagnervalerio
 */
class Sistema 
{
    public function __construct()
    {
        /*
        * Criando uma instância do CodeIgniter para poder acessar
        * banco de dados, sessionns, models, etc...
        */
        $this->CI =& get_instance();
    }
    
    public function enviarEmail($remetente, $destinatario) 
    {       
        $this->CI->load->library('email');

        $mail_config["protocol"] = "smtp";
        $mail_config["smtp_host"] = "mail.reisoffice.com.br";
        $mail_config["smtp_user"] = "spamtrap";
        $mail_config["smtp_pass"] = "Ro12333321";
        $mail_config["smtp_port"] = "587";
        $mail_config["mailtype"] = "html";
        $mail_config['crlf'] = "\r\n";
        $mail_config['newline'] = "\r\n";
        $this->CI->email->initialize($mail_config);

        $this->CI->email->from($remetente->email, html_entity_decode($remetente->nome));
        $this->CI->email->to($destinatario->email);
        if(!empty($destinatario->cc)){
             $this->CI->email->cc($destinatario->cc);
        }
        $this->CI->email->subject(html_entity_decode($destinatario->assunto));
        $msg = $this->CI->load->view("email/email", $destinatario->mensagem, TRUE);
        $this->CI->email->message($msg);
        
        if (isset($destinatario->anexos)){
            foreach ($destinatario->anexos as $anexo)
            {
                $this->CI->email->attach($anexo['full_path']);
            }
        }
        else
        {
            //echo "não há anexos";
        }
        //print_r($destinatario->mensagem);
        //print_r($destinatario->anexos);
        //exit;
        if ($this->CI->email->send()) {
            $debug = html_entity_decode($this->CI->email->print_debugger());
            file_put_contents(APPPATH . "logs/email_debug_ok.txt", $debug);
            //echo $debug;
            //exit;
            // Grava LOG
            //$this->log("EML", json_encode(array("msg"=> substr($debug,0,3000), "remetente"=>$remetente->email, "destinatario"=>$destinatario->email)));

            return TRUE;
        } else {
            $debug = html_entity_decode($this->CI->email->print_debugger());
            file_put_contents(APPPATH . "logs/email_debug_err.txt", $debug);
            //echo $debug;
            //exit;
            // Grava LOG
            //$this->log("EML", json_encode(array("msg"=>$debug, "remetente"=>$remetente->email, "destinatario"=>$destinatario->email)));

            return FALSE;
        }
    }
}
