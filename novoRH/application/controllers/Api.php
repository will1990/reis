<?php

class Api extends CI_Controller 
{
    /** APIs - novoRH
     * 
     *  William Feliciano 
     */    
    
    function __construct() 
    {
        parent::__construct();        
        $this->load->model("Apis_model", "mdapi");        
        //$this->load->library('sistema');        
    }
    
    //página padrao de api
    public function index()
    {
        echo '<center><h1>Nenhum parâmetro solicitado!</h1></center>';        
    }
    
    //retorna informações do colaborador através do Email
    public function infoemail($email = null)
    {
        if($email !== null)
        {
            $email = $this->mdapi->obtIdDeEmail($email."@reisoffice.com.br");
            if($email !== null)
            {
                $func = $this->mdapi->obtFuncDeId($email->id_empregado);
                $resul = array("codigo" => $func->id, "nome" => $func->nome, "matricula" => $func->matricula, "admissao" => $func->data_admissao, "email" => $email->valor);
                echo '<pre>'.json_encode($resul).'</pre>';
            }
            else
            {
                echo '<center><h1>Email não consta na Base de Dados!</h1></center>';
            }
        }
        else
        {
            redirect(base_url()."api", "refresh");
        }
    }
    
    //retorna informações do colaborador através da Matrícula    
    public function infomat($matricula = null, $posicao = null)
    {
        if($matricula !== null)
        {
            $func = $this->mdapi->obtFuncDeMatr($matricula, $posicao);            
            if($func !== null)
            {
                $resul = array("codigo" => $func->id, "nome" => $func->nome, "matricula" => $func->matricula, "admissao" => $func->data_admissao);
                $email = $this->mdapi->obtEmailDeId($func->id);                
                $resul["email"] = $email !== null ? $email->valor : "Nao consta email na Base de Dados" ;
                echo '<pre>'.json_encode($resul).'</pre>';
            }
            else
            {
                echo '<center><h1>Matrícula não consta na Base de Dados!</h1></center>';
            }   
        }
        else
        {
            redirect(base_url()."api", "refresh");
        }
    }
    
    //retorna validação de submits e forms
    public function validact()
    {
        if(!empty($this->input->post("actlogin")))
        {
            $dados = $this->input->post();
            if(is_numeric($dados["txtlogin"]) !== FALSE)
            {
                echo $this->validaMat($dados);
            }
            else
            {
                echo $this->validaEmail($dados);
            }
        }        
        else
        {
            echo '<h4>button name: actlogin</h4>';
            echo '<h4>input login matricula/email: txtlogin</h4>';
            echo '<h4>input password: txtsenha</h4>';
        }
    }
    
    //retorna a validação para Metricula Inserida em Form
    public function validaMat($dados = null)
    {
        if(!empty($dados))
        {
            $verif = 'no';
            $funcs = $this->mdapi->validFuncDeMatr($dados["txtlogin"]);
            if(!empty($funcs))
            {            
                foreach($funcs as $func)
                {                
                    if($this->validaSenha($dados["txtsenha"], $func->id) !== FALSE)        
                    {
                        $verif = $verif !== 'ok' ? 'ok' : $verif ;                    
                    }       
                }

                if($verif !== 'no')
                {
                    $fim = array("autent" => $verif, "erro" => '');                
                }
                else
                {
                    $fim = array("autent" => $verif, "erro" => 'Senha Incorreta!');                
                }                
            }
            else
            {
                $fim = array("autent" => $verif, "erro" => 'Matricula nao Localizada');
            }
            echo '<pre>'.json_encode($fim).'</pre>';
        }
        else
        {
            redirect(base_url()."api/validact","refresh");
        }
    }
    
    //retorna a validação para Email Inserido em Form
    public function validaEmail($dados = null)
    {
        if(!empty($dados))
        {
            $verif = 'no';
            $senhas = $this->mdapi->validFuncDeEmail($dados["txtlogin"].'@reisoffice.com.br');
            if(!empty($senhas))
            {
                foreach($senhas as $senha)
                {                
                    if($this->validaSenha($dados["txtsenha"], $senha->id_empregado) !== FALSE)
                    {                    
                        $verif = $verif !== 'ok' ? 'ok' : $verif ;
                    }                
                }

                if($verif !== 'no')
                {
                    $fim = array("autent" => $verif, "erro" => '');                
                }
                else
                {
                    $fim = array("autent" => $verif, "erro" => 'Senha Incorreta!');                
                }
            }
            else
            {
                $fim = array("autent" => $verif, "erro" => 'Matricula nao Localizada');
            }            
            echo '<pre>'.json_encode($fim).'</pre>';
        }
        else
        {
            redirect(base_url()."api/validact","refresh");
        }
    }
    
    //recebe a senha e faz a comparação
    public function validaSenha($senhaDig = null, $id = null)
    {
        $resul = $this->mdapi->validSenhaDeId($id);
        if($resul->senha === md5($senhaDig))
        {
            return true;
        }
        else
        {
            return false;
        }
    }    
    
    public function testeapi()
    {
        $this->load->view("outros/form");
    }
    
    public function flaumar()
    {
        $this->mdapi->carregar_funcionarios();
    }
    
    public function flaumar_dep()
    {
        $this->mdapi->corrigir_dependencias();
    }
    
    public function func_geral()
    {
        $this->mdapi->geral();
    }
}