<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('PATHXLS', APPPATH . '..\arquivos\RAMAIS.xls');

class Leitura extends CI_Controller {
      
    public function index()
    {
        echo 'Página de Carregamento dos Ramais';
        
        $ramais = $this->execRamais(); 
        //$retorno = $this->addRamais($ramais);
        
        
        echo '<pre>';
        print_r($ramais);
        echo '</pre>';
    }
    
    public function setcabRamal($n1, $n2, $n3, $n4)
    {
        return array(
            $n1 => "nome",
            $n2 => "funcao",
            $n3 => "ramal",
            $n4 => "skype"
        );
    }
    
    public function setparam($prow, $urow, $pcol, $ucol, $cab, $norows)
    {
        return array(
            "linha" => (object)array(
                "primeira" => $prow,
                "ultima"   => $urow,
                "ignorar"  => $norows
            ),
            "coluna" => (object)array(
                "primeira" => $pcol,
                "ultima"   => $ucol,
                "cabecalho" => $cab
            ),           
        );         
    }   
    
    public function execRamais()
    {
        $tratamento1 = $this->setparam(4, 62, 0, 3, $this->setcabRamal(0,1,2,3), array(4,6,11,25,30,35,41,50,54,58));
        $tratamento2 = $this->setparam(4, 65, 5, 8, $this->setcabRamal(5,6,7,8), array(4,33,37,60));
        $tratamento3 = $this->setparam(4, 59, 10, 13, $this->setcabRamal(10,11,12,13), array(4,8,12,15,28,43));
        $tratamento4 = $this->setparam(4, 16, 15, 18, $this->setcabRamal(15,16,17,18), array(4));
        
        $array1 = $this->mdler->lerxls(PATHXLS, $tratamento1);
        $array2 = $this->mdler->lerxls(PATHXLS, $tratamento2);
        $array3 = $this->mdler->lerxls(PATHXLS, $tratamento3);
        $array4 = $this->mdler->lerxls(PATHXLS, $tratamento4);
                
        $array = array_merge($array1, $array2, $array3, $array4);
        
        //echo '<pre>';
        //print_r($array);
        //echo '</pre>';
        
        foreach($array as $id => $pessoa)
        {            
            $pessoa['nomePesq'] = $this->nomePesq($pessoa['nome']);
            if($pessoa['nomePesq'] !== false):
                $pesqNome = $this->mdler->pesqPessoa($pessoa['nomePesq']);
                if(count($pesqNome) > 1):
                    $funcao = explode(" ",$pessoa['funcao']); 
                    $tam = count($funcao);
                    $pessoa['funcaoPesq'] = "%".$funcao[$tam-1]."%";
                    $pessoa['retornoFunc'] = ""; 
                    foreach ($pesqNome as $value):
                        $resul = $this->mdler->refinarPessoa($value->id, $pessoa['funcaoPesq']);                    
                        if(!empty($resul)):
                            $pessoa['retornoFunc'][] = $resul;          
                        endif;               
                    endforeach;                    
                else:
                    $pessoa['retornoPesq'] = $pesqNome;
                endif;                    
            else:
                $pessoa['retornoPesq'] = "Deu Ruim" ;
            endif;
            
            if(!empty($pessoa['retornoPesq'])):
                $pessoa['status'] = 1;
            elseif(!empty($pessoa['retornoFunc'])):
                if(count($pessoa['retornoFunc']) == 1):
                    $pessoa['status'] = 2;
                else:
                    $pessoa['status'] = 0;                
                endif; 
            else:
                $pessoa['status'] = 0;
            endif;            
            
            $pessoas[$id] = (object)$pessoa;            
        }      
        return $pessoas;
    }
    
    public function addRamais($ramais)
    {
        $nao = array(33,35,40,44,84,89,91,98);
        $retorno = "";
        foreach ($ramais as $id => $ramal):
            $resposta = "";
            if(in_array($id, $nao) == false):
                if($ramal->status == 1):
                    $rst = $this->mdler->setPessoa($ramal->retornoPesq[0]->id, $ramal);
                    $resposta = (object)array();
                    $resposta->final = $rst == true ? "Salvo": "Deu Ruim na hora de Salvar";
                elseif($ramal->status == 2):    
                    $rst = $this->mdler->setPessoa($ramal->retornoFunc[0][0]->id_empregado, $ramal);
                    $resposta = $ramal;
                    $resposta->final = $rst == true ? "Salvo Com Ressalva - Verificar": "Deu Ruim na hora de Salvar (2)";
                else:
                    $resposta = $ramal;
                    $resposta->final = "Fazer Manualmente Status 0!";
                endif;    
            else:
                $resposta = $ramal;
                $resposta->final = "Fazer Manualmente!";
            endif;
            $retorno[$id] = $resposta;
        endforeach;
        
        return $retorno;
    }
    
    public function nomePesq($nome)
    {
        if(strpos($nome, "/") !== false):
            $nome = false;
        else:
            if(strpos($nome, " ") !== false):
                $nome = "%".str_replace(" ", "% %", $nome)."%";
            else:
                $nome = "%".$nome."%";
            endif;
        endif;
        return $nome;
    }
}
