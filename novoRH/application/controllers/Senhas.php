<?php

class Senhas extends CI_Controller {

    /** Sistema de Login e Geração de Senhas - novoRH
     * 
     *  William Feliciano 
     */    
    
    function __construct() 
    {
        parent::__construct();        
        $this->load->model("Senhas_model", "mdsenha");
        
        $this->load->library('sistema');
        $this->load->library('form_validation');  
        
        $this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
    }
    
    public function index($retorno = "")
    {       
        $this->session->set_userdata("ROSenhasRetorno", $retorno);
        
        if($this->input->post())
        {
            $data = (object)$this->input->post();
                        
            // Verifica se informou a matricula
            if(isset($data->matricula))
            {
                if(!empty($data->matricula) && is_numeric($data->matricula))
                {
                    // Verifica se existe essa matricula na base de dados
                    $funcionario = $this->mdsenha->get_funcionario($data->empresa, $data->matricula);
                    if($funcionario)
                    {
                        if($funcionario->esqueci)
                        {
                            // Mostra a tela para esqueci a senha
                            $this->data["funcionario"] = $funcionario;                            
                            $tela = "recuperar_senha";                            
                        }
                        else
                        {
                            // Mostra a tela para completar o cadastro
                            $this->data["funcionario"] = $funcionario;
                            $tela = "obterSenhas";
                        }
                    }
                    else
                    {
                        // Mostra a tela com mensagem
                        $this->data["message"] = "<div class='alert alert-danger'>Nenhum funcionário encontrado com o Nº de Matrícula $data->matricula</div>";
                        $tela = "matricula";
                    }
                }
            }
            elseif(isset($data->codfunc))
            {
                $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|callback_email_reisoffice');
                $this->form_validation->set_rules('senha', 'Nova Senha', 'required|min_length[8]|max_length[16]');
                $this->form_validation->set_rules('csenha', 'Confirmar Senha', 'required|min_length[8]|max_length[16]|matches[senha]');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                
                if ($this->form_validation->run() == FALSE) 
                {
                    $funcionario = $this->mdsenha->get_funcionario($data->empresa, null, $data->codfunc);
                    $this->data["funcionario"] = $funcionario;
                    $tela = "obterSenhas";
                }
                else
                {
                    if($this->mdsenha->set_funcionario())
                    {
                        $this->data["titulo"] = "Cadastro Realizado!";
                        $this->data["mensagem"] = "Voce receberá um email com os dados de Login <br/><br/> Utilize seu login nos Sistemas da Reis Office";
                        $this->data["retorno_mensagem"] = "Retornando a Intranet...";
                        $this->data["retorno_link"] = (empty($this->session->userdata("ROSenhasRetorno"))) ? INTRANET : $this->session->userdata("ROSenhasRetorno");
                        $tela = "congratulations";
                    }
                    else
                    {
                        $tela = "matricula";
                    }
                }
            }
            elseif(isset($data->rec_email))
            {
                $this->form_validation->set_rules('rec_email', 'E-mail', 'required|valid_email|callback_email_reisoffice|callback_email_funcionario');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                
                if ($this->form_validation->run() == FALSE) 
                {
                    $funcionario = $this->mdsenha->get_funcionario($data->empresa, false, $data->rec_codfunc);
                    $this->data["funcionario"] = $funcionario;
                    $tela = "recuperar_senha";
                }
                else
                {
                    if($this->mdsenha->set_nova_senha())
                    {
                        $this->data["titulo"] = "Recupere sua senha!";
                        $this->data["mensagem"] = "Acesse seu e-mail e clique no link para gerar uma nova senha.";
                        $this->data["retorno_mensagem"] = "Redirecionando...";
                        $this->data["retorno_link"] = (empty($this->session->userdata("ROSenhasRetorno"))) ? INTRANET : $this->session->userdata("ROSenhasRetorno");
                        $tela = "congratulations";
                    }
                    else
                    {
                        $tela = "matricula";
                    }
                }
            }
            elseif(isset($data->alt_email))
            {
                $this->form_validation->set_rules('alt_email', 'E-mail', 'required|valid_email');
                $this->form_validation->set_rules('alt_asenha', 'Senha Atual', 'required|min_length[8]|max_length[16]');
                $this->form_validation->set_rules('alt_senha', 'Nova Senha', 'required|min_length[8]|max_length[16]');
                $this->form_validation->set_rules('alt_csenha', 'Confirmar Senha', 'required|min_length[8]|max_length[16]|matches[alt_senha]');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                
                if ($this->form_validation->run() == FALSE) 
                {
                    $funcionario = $this->mdsenha->get_funcionario($data->empresa, false, $data->alt_codfunc);
                    $this->data["funcionario"] = $funcionario;
                    $this->data["recuperar"] = 1;
                    $tela = "obterSenhas";
                }
                else
                {
                    if($this->mdsenha->set_nova_senha(true))
                    {
                        $this->data["titulo"] = "Senha Alterada!";
                        $this->data["mensagem"] = "Sua senha foi alterada com sucesso.";
                        $this->data["retorno_mensagem"] = "Redirecionando...";
                        $this->data["retorno_link"] = (empty($this->session->userdata("ROSenhasRetorno"))) ? INTRANET : $this->session->userdata("ROSenhasRetorno");
                        $tela = "congratulations";
                    }
                    else
                    {
                        redirect(INTRANET);
                    }
                }
            }
            else
                $tela = "matricula";
        }  
        else
        {
            $tela = "matricula";
        }
    
        $this->load->view("templates/header");
        $this->load->view("templates/barra");        
        $this->load->view("senhas/$tela", $this->data);
        $this->load->view("templates/footer");
    }
    
    public function email_reisoffice($email)
    {
        if(strpos(strtolower($email), "@reisoffice.com.br") === false)
        {
            $this->form_validation->set_message("email_reisoffice", "O E-mail deve pertencer ao domínio reisoffice.com.br");
            return false;
        }
        /*
        // Busca na base de e-mails do outlook
        $emails = file(APPPATH . "../uploads/contatos_090117.csv");
        
        if($emails)
        {
            foreach($emails as $eml)
            {
                list($email1, $nome, $email2) = explode(";", $eml);
                if(trim(strtolower($email)) == trim(strtolower($email1)) || trim(strtolower($email)) == trim(strtolower($email2)))
                    return true;
            }
            
            $this->form_validation->set_message("email_reisoffice", "O e-mail <$email> não foi encontrado na lista de contatos da Reis Office");
            return false;
        }
        else
        {
            $this->form_validation->set_message("email_reisoffice", "Não foi possível validar o seu e-mail. Contate o Depto. de T.I.");
            return false;
        }*/
    }
    
    public function email_funcionario($email)
    {
        if(!$this->mdsenha->chk_email_funcionario())
        {
            $this->form_validation->set_message("email_funcionario", "E-mail não pertence à Matrícula ou CPF informado");
            return false;
        }
        
        return true;
    }
    
    public function ativar($id, $hash)
    {
        $this->mdsenha->ativar_senha($id, $hash);
    }
    
    public function recuperar($id)
    {
        // Verifica se existe e se é para recuperar
        $funcionario = $this->mdsenha->get_funcionario(false, false, $id);
        if($funcionario)
        {
            if($funcionario->senha)
            {
                if($funcionario->senha->alterar_senha == 1)
                {
                    // Mostra a tela para esqueci a senha
                    $this->data["funcionario"] = $funcionario;
                    $this->data["recuperar"] = 1;
                    $tela = "obterSenhas";
                }
                else
                    redirect(INTRANET);
            }
            else
                redirect(INTRANET);
        }
        else
            redirect(INTRANET);
        
        $this->load->view("templates/header");
        $this->load->view("templates/barra");        
        $this->load->view("senhas/$tela", $this->data);
        $this->load->view("templates/footer");
    }
    
    public function validar_usuario($login = "", $senha = "", $empresa = 1)
    {
        $result = array();
        
        if($this->input->post())
        {
            $login = $this->input->post("usuario");
            $senha = $this->input->post("senha");
            $empresa = $this->input->post("empresa");
        }
        
        $funcionario = $this->mdsenha->chk_funcionario($login, $senha, $empresa);
        if($funcionario == "X")
        {
            $result["result"] = false;
            $result["error"] = "Senha inválida";
        }
        elseif($funcionario == "U")
        {
            $result["result"] = false;
            $result["error"] = "Colaborador não encontrado";
        }
        else
        {
            $result["id"] = $funcionario->id;
            $result["nome"] = $funcionario->nome;
            $result["email"] = $funcionario->email->valor;
            $result["matricula"] = $funcionario->matricula;
            $result["result"] = true;
            $result["error"] = "";
        }
        
        echo json_encode((object)$result);
    }
    
    public function retornar_id($user, $empresa)
    {
        if(strlen($user) == 11)
            $empresa = false;
        
        $rst = $this->mdsenha->get_funcionario($empresa, $user);
        if($rst)
            $result = (object)array("id" => $rst->id);
        else
            $result = (object)array("id" => 0);
        
        echo json_encode($result);
    }
    
    //VALIDAÇÃO DADOS
    //Verificação Normal ou Patrão
    public function validarDados()
    {
        $dados = $this->input->post();       
        if(strpos($dados["txtemail"],'.') !== FALSE)
        {            
            return $this->validNomeEmail($dados);
        }
        else
        {
            return $this->validaPatrao($dados);            
        }        
    }

    //Validando Dados - Normal    
    public function validNomeEmail($dados = null)
    {
        $name = $this->tirarAcentos(strtolower($dados["txtnome"]));
        //$nomes = explode(" ", $name);
        $email = explode(".", strtolower($dados["txtemail"]));
        $matricula = $dados["txtmatricula"];
                        
        /*if($nomes[0] === $email[0])
        {
            $verif = 'O sobrenome não coincide com o email informado!';
            foreach ($nomes as $nome)
            {                
                if(strtolower($email[1]) === strtolower($nome))
                {
                    $verif = true;                    
                }                
            }

            if($verif === true)
            { */               
                $rst = $this->mdsenha->obterFuncDados(array("id","nome"),"rh_empregados", array("matricula" => $matricula));
                if(count($rst)>1)
                {
                    $verif = 'O nome não coincide com a Matrícula Informada!';
                    foreach ($rst as $func)
                    {
                        $func->nome = $this->tirarAcentos($func->nome);
                        $verif = $func->nome === strtoupper($name) ? $func->id : $verif ;
                    }
                }
                else
                {
                    $rst = $rst[0];
                    $rst->nome = $this->tirarAcentos($rst->nome);

                    $verif = $rst->nome === strtoupper($name) ? $rst->id : 'O nome e/ou matrícula não coincide com o da Base de Dados!' ;
                }
            //}            
            return $verif;            
        /*}
        else
        {
            return 'O nome não coincide com o email informado!';
        } */       
    }
    
    //Validando Dados - Patrão
    public function validaPatrao($dados = null)
    {
        $nome = $this->tirarAcentos(strtolower($dados["txtnome"]));
        $nomes = explode(" ", $nome);
        $email = strtolower($dados["txtemail"]);
        $matricula = $dados["txtmatricula"];
        
        /*if(($email === $nomes[0])AND(strpos($email, '.') !== TRUE))
        {*/                    
            $rst = $this->mdsenha->obterFuncDados(array("id","nome","matricula"), "rh_empregados", array("matricula" => $matricula));
            $rst = $rst[0];
            $rst->nome = $this->tirarAcentos($rst->nome);
            
            if($rst->nome === strtoupper($nome) AND ($rst->matricula === $matricula))
            {  
                return $rst->id;
            }
            else
            {
                return 'O nome e/ou matrícula não coincide com o da Base de Dados!';
            }                
        /*}
        else
        {
            return 'O email não coincide com o nome informado';
        }*/
    }
    
    //Concluindo Validação    
    public function validarCaDados($id = null, $email = null)
    {
        $resul = $this->mdsenha->obterFuncDados("*", "rh_empregados_senhas", array("id_empregado" => $id))[0];
        if(empty($resul))       
        {
            //$this->mdsenha->newFuncDados(array("alterar_senha" => 1, "id_empregado" => $id, "ativo" => 0), "rh_empregados_senhas");
            $this->mdsenha->newFuncDados(array("id_empregado" => $id, "tipo_contato" => 13, "valor" => $email."@reisoffice.com.br"), "rh_empregados_contatos");
            redirect(base_url()."senhas/cadastrar/$id", "refresh");           
        }
        else
        {
            $cod = $resul->id;
            $this->mdsenha->updFuncDados(array("alterar_senha" => 1, "ativo" => 0), "rh_empregados_senhas", array("id" => $cod));
            redirect(base_url()."senhas/cadastrar/$id", "refresh");
        }
    }
    
    
    
    //FORM E VALIDAÇÃO DE SENHAS (2ªfase)
    //Cadastro    
    public function cadastrar1($id = null)
    {
        if(empty($id))
        {
            $id = $this->input->post("codfunc");
        }

        $this->data["funcionario"] = $this->mdsenha->obterFuncDados("*", "rh_empregados", array("id" => $id))[0];
        $this->data["email"] = $this->mdsenha->obterFuncDados("valor", "rh_empregados_contatos", array("id_empregado" => $id, "tipo_contato" => 13))[0];
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('txtsenha1', 'Primeira Senha', 'required');
            $this->form_validation->set_rules('txtsenha2', 'Confirmação de Senha', 'required|matches[txtsenha1]|callback_contarSenha['.$this->input->post("txtsenha2").']');                

            if ($this->form_validation->run() !== FALSE)
            {                
                $campos = array(
                    "alterar_senha" => 0, 
                    "ativo"         => 1,
                    "senha"         => md5($this->input->post("txtsenha2")),
                    "data_ativacao" => $this->horaCerta()
                );           
                if($this->mdsenha->updFuncDados($campos, "rh_empregados_senhas", array("id_empregado" => $id)))
                {
                    if($this->sistema->enviarEmail($this->prepRem(), $this->prepDest($this->data["funcionario"]->nome, $this->data["email"]->valor, $this->input->post("txtsenha2"))) !== FALSE)
                    {
                        redirect(base_url()."senhas/realizado", "refresh");
                    }
                    else
                    {
                        $this->session->set_flashdata('erro', "Erro ao enviar Email");
                        redirect(base_url()."senhas/index", "refresh");                        
                    }
                }
                else
                {
                    $this->session->set_flashdata('erro', "Erro de conexão com a Base de Dados, tente novamente mais Tarde!");
                    redirect(base_url()."senhas/index");
                }                
            }                
        }

        $this->load->view("templates/header");
        $this->load->view("templates/barra");        
        $this->load->view("senhas/obterSenhas", $this->data);
        $this->load->view("templates/footer");        
    }
    
    
    
    //FINALIZAÇÃO  
    //Mensagem 
    public function realizado()
    {
        $this->load->view("templates/header");
        $this->load->view("templates/barra");
        $this->load->view("senhas/congratulations");
        $this->load->view("templates/footer");
    }
    
    //Preparação Email
    public function prepDest($nome = null, $email = null, $senha = null)
    {
        /*$msg = "$nome, sua senha para acesso aos Sistemas Internos da Reis Office é:<br><pre>$senha</pre><br>"
                . "Por favor, ative a sua senha <a href=''>clicando aqui</a>"
        
        return (object) array(
            'email' => $email,
            'nome' => $nome,
            'mensagem' => array("titulo" => "Senha de Acesso", "mensagem" => $email),
            //'cc' => ' ', 
            'assunto' => 'Reis Office - Atualização Cadastral'            
        );*/
    }
    
    public function prepRem()
    {
        return (object) array(
            'email' => 'sistema.solicitacao@reisoffice.com.br',
            'nome'  => 'Login Sistemas'
        );
    }
    
    
    
    
    
    //Funções úteis de uso Geral
    function tirarAcentos($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }
    
    function contarSenha($senha = null)
    {
        if(strlen($senha)>16)
        {
            $this->form_validation->set_message('contarSenha', 'A senha deve Ser menor que 16 caracteres');
            return false;
        }
        elseif(strlen($senha)<7)
        {
            $this->form_validation->set_message('contarSenha', 'A senha deve ter no Mínimo 8 caracteres');
            return false;
        }
        else
        {
            return true;
        }
    }
    
    function horaCerta()
    {
        date_default_timezone_set('America/Sao_Paulo');
        return date('Y-m-d H:i:s', time());
    }
    
    //Teste de Visual da View do Email
    public function emailSenha()
    {
        $this->load->view("email/templateSenhas");
    }    
}

