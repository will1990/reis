<?php

class Painel extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();        
        $this->load->model("Painel_model", "m_pnel");
    }
    
     public function index()
    {
        $this->data['test'] = $this->m_pnel->obter_pessoa();
        
        $this->load->view("templates/header");
        $this->load->view("templates/barra");
        $this->load->view("templates/menu");
        $this->load->view("painel/index", $this->data);
        $this->load->view("templates/footer");
    }
}
    