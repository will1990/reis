<?php

class Funcionario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('language');
        //FileUpload
        $this->load->model("Funcionario_model", "m_func");
        $this->load->model("FileUpload", "m_arq");       
    }

    private $data;

    public function valida_form_dinamico() {
        if (!empty($_POST["metodo"])) {
            $metodo = $_POST["metodo"];
            if (!$this->m_func->$metodo()) {
                $this->$metodo();
            }else{
                //add operação bd
                echo "sucesso";
            }
        }
    }

       
    
    
    
    public function index($id = 0) {

        $data['empregado'] = $this->m_func->select_nome();
        $this->load->view("templates/header");
        $this->load->view("funcionario/index", $data);
        $this->load->view("templates/footer");
    }

    public function login() {
        $this->load->view("templates/loginHeader");
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/login", $data);
        $this->load->view("templates/footer");
    }

    public function funcionario_form() {
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/cadastroFuncionario", $data);
        $this->load->view("templates/footer");
        
    }

    public function contrato_form() {
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/contrato", $data);
        $this->load->view("templates/footer");
    }

    public function contato_form() {
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/contato", $data);
        $this->load->view("templates/footer");
    }

    public function endereco_form() {
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/endereco", $data);
        $this->load->view("templates/footer");
        $this->load->view("js/cep");
    }

    public function dependente_form() {
        $data = 0;
        $this->load->view("templates/header");
        $this->load->view("funcionario/dependente/cad_dependente", $data);
        $this->load->view("templates/footer");
    }
    public function documentos(){
         $data = 0;
        $this->load->view("templates/header");
        $this->load->view("arquivos/documentos", $data);
        $this->load->view("templates/footer");
        $this->load->view("js/dropzone");
    }
    public function fileUpload(){
        $this->m_arq->enviar();
    }
}
