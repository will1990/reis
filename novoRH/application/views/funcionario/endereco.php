<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="alert alert-danger">
                    <strong>Dados Incorretos!</strong> <?php echo validation_errors(); ?>
                </div>
                <h1> Cadastro de Endereço</h1>
                <form method="post" action="<?= base_url("funcionario/valida_form_dinamico") ?>">
                    <div class="form-group ">
                        <label class="control-label requiredField" for="logradouro">
                            Rua
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="logradouro" name="logradouro" placeholder="ex: rua das luas " type="text" />
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="num">
                            Numero
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="num" name="num" placeholder="ex: 64" type="text" value="<?php if (!empty($_POST["num"])) echo $_POST["num"]; ?>" value="<?php if (!empty($_POST["num"])) echo $_POST["num"]; ?>"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="bairro">
                            Bairro
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="bairro" name="bairro" placeholder="ex: Jardim Alguem Sem Nome" type="text"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="cidade">
                            Cidade
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="cidade" name="cidade" placeholder="ex: Guarulhos" type="text"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="estado">
                            Estado
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="estado" name="estado" placeholder="ex: São Paulo" type="text"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="cep">
                            CEP
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="cep" name="cep" placeholder="ex: 07140280" type="text" onblur="pesquisacep(this.value);" value="<?php if (!empty($_POST["cep"])) echo $_POST["cep"]; ?>"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label " for="referencia">
                            Referencia
                        </label>
                        <input class="form-control" id="referencia" name="referencia" type="text"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label " for="compl">
                            Complemento
                        </label>
                        <input class="form-control" id="compl" name="compl" type="text"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField">
                            Endere&ccedil;o Atual?
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <div class="">
                            <div class="radio">
                                <label class="radio">
                                    <input name="end_atual" type="radio" value="sim"/>
                                    sim
                                </label>
                            </div>
                            <div class="radio">
                                <label class="radio">
                                    <input name="end_atual" type="radio" value="n&atilde;o"/>
                                    n&atilde;o
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                cadastrar
                            </button>
                            <input type="hidden" name="metodo" value="endereco_form">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

