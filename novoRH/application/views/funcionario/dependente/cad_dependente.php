<!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: #ffffff !important;}.bootstrap-iso .form-control:focus { border-color: #eecd4c;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(238, 205, 76, 0.6); box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(238, 205, 76, 0.6);} .asteriskField{color: red;}</style>

<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
    <form method="post">
     <div class="form-group ">
      <label class="control-label requiredField" for="tipo_dependente">
       Tipo de Dependente
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="tipo_dependente" name="tipo_dependente" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="nascimento">
       Data de Nascimento
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="nascimento" name="nascimento" placeholder="DD/MM/YYYY" type="date"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="nome">
       Nome
      </label>
      <input class="form-control" id="nome" name="nome" type="text"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary " name="submit" type="submit">
        cadastrar
       </button>
      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>