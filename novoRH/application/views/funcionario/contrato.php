
<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<?php echo validation_errors(); ?>
<div id="resultado"></div>
<div class="bootstrap-iso">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <form method="post" id="formContrato" role="form" action="<?=base_url("funcionario/valida_form_dinamico")?>">
                    <div class="form-group ">
                        <label class="control-label requiredField" for="matricula">
                            Matricula
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="matricula" name="matricula" placeholder="ex: 101522" type="text" value="<?php if(!empty($_POST["matricula"]))echo $_POST["matricula"]; ?>"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="setor">
                            Setor
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <select class="select form-control" id="setor" name="setor">
                            <option value="escolha um setor">
                                escolha um setor
                            </option>
                            <option value="Second Choice">
                                Second Choice
                            </option>
                            <option value="Third Choice">
                                Third Choice
                            </option>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="cargo">
                            Cargo
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <select class="select form-control" id="cargo" name="cargo">
                            <option value="escolha um cargo">
                                escolha um cargo
                            </option>
                            <option value="Second Choice">
                                Second Choice
                            </option>
                            <option value="Third Choice">
                                Third Choice
                            </option>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label class="control-label requiredField" for="inicio">
                            Data de Inicio
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <input class="form-control" id="inicio" name="inicio" placeholder="DD/MM/YYYY" type="date" value="<?php if(!empty($_POST["inicio"]))echo $_POST["inicio"]; ?>"/>
                    </div>
                    <div class="form-group ">
                        <label class="control-label " for="termino">
                            Data de T&eacute;rmino
                        </label>
                        <input class="form-control" id="termino" name="termino" placeholder="MM/DD/YYYY" type="date" value="<?php if(!empty($_POST["termino"]))echo $_POST["termino"]; ?>"/>
                    </div>
                    <div class="form-group" id="div_atual">
                        <label class="control-label requiredField" for="atual">
                            Atual?
                            <span class="asteriskField">
                                *
                            </span>
                        </label>
                        <div class="">
                            <label class="radio-inline">
                                <input name="atual" type="radio" value="Sim"/>
                                Sim
                            </label>
                            <label class="radio-inline">
                                <input name="atual" type="radio" value="N&atilde;o"/>
                                N&atilde;o
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <button class="btn btn-primary " name="submit" type="submit">
                                cadastrar
                            </button>
                            <input type="hidden" name="metodo" value="contrato_form">
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
