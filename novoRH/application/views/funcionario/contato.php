<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<?php echo validation_errors(); ?>
<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
    <form method="post" action="<?=base_url("funcionario/valida_form_dinamico")?>">
     <div class="form-group ">
      <label class="control-label requiredField" for="tipo_cont">
       Tipo de Contato
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="tipo_cont" name="tipo_cont" placeholder="ex: recado" type="text" value="<?php if(!empty($_POST["tipo_cont"]))echo $_POST["tipo_cont"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="nome">
       Nome
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="nome" name="nome" placeholder="ex: Fulano do Ciclano Silva" type="text" value="<?php if(!empty($_POST["nome"]))echo $_POST["nome"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="bairro">
       Telefone
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="bairro" name="bairro" placeholder="ex: (11) 9089-0000" type="text" value="<?php if(!empty($_POST["bairro"]))echo $_POST["bairro"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="email">
       Email
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="email" name="email" placeholder="ex: fulano.ciclano@reisoffice.com.br" type="text" value="<?php if(!empty($_POST["email"]))echo $_POST["email"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="ramal">
       Ramal
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="ramal" name="ramal" placeholder="ex: 2012" type="text" value="<?php if(!empty($_POST["ramal"]))echo $_POST["ramal"]; ?>"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary " name="submit" type="submit">
        cadastrar
       </button>
       <input type="hidden" name="metodo" value="contato_form">
      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
