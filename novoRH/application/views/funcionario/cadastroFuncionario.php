
<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<?php echo validation_errors(); ?>
<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-4 col-sm-4 col-xs-12">
    <form method="post" action="<?=base_url("funcionario/valida_form_dinamico")?>">
     <div class="form-group ">
      <label class="control-label requiredField" for="name">
       Nome
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="name" name="nome" type="text" value="<?php if(!empty($_POST["nome"]))echo $_POST["nome"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="matricula">
       Matricula
       <span class="asteriskField">
        *
       </span>
      </label>
         <input class="form-control" id="matricula" name="matricula" placeholder="100001" type="text" value="<?php if(!empty($_POST["matricula"]))echo $_POST["matricula"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="email">
       Email
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="email" name="email" placeholder="your.mail@reisoffice.com.br" type="email" value="<?php if(!empty($_POST["email"]))echo $_POST["email"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="date">
       Data de Nascimento
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="date" name="nascimento" placeholder="DD/MM/YYYY" type="date" value="<?php if(!empty($_POST["nascimento"]))echo $_POST["nascimento"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="casamento" >
       Data de casamento

      </label>
      <input class="form-control" id="date1" name="casamento" placeholder="DD/MM/YYYY" type="date" value="<?php if(!empty($_POST["casamento"]))echo $_POST["casamento"]; ?>"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="data_admissao">
       Data de Admiss&atilde;o
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="data_admissao" name="data_admissao" placeholder="DD/MM/YYYY" type="date" value="<?php if(!empty($_POST["data_admissao"]))echo $_POST["data_admissao"]; ?>"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary " name="submit" type="submit">
        cadastrar
       </button>
       <input type="hidden" name="metodo" value="funcionario_form">
      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>
