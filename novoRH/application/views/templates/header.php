<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>         
        <meta name="viewport" content="width=device-width">
        <title>RH Reis Office</title>
            
        <!-- Inline CSS based on choices in "Settings" tab -->
        <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: #ffffff !important;}.bootstrap-iso .form-control:focus { border-color: #eecd4c;  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(238, 205, 76, 0.6); box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(238, 205, 76, 0.6);} .asteriskField{color: red;}</style>

        <!-- Ajax -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <!-- Bootstrap -->
        <link href="<?=base_url("assets/bootstrap/css/bootstrap.css") ?>" rel="stylesheet" type="text/css" />
        
        <!-- Jquery -->
        <link href="<?=base_url("assets/jquery/jquery-ui.min.css") ?>" rel="stylesheet" type="text/css" />
        
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?=base_url("assets/font-awesome-4.6.3/css/font-awesome.min.css")?>">
    
        <!-- Data picker -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        
        <!-- fullCalendar -->
        <link href="<?=base_url("assets/full_calendar/fullcalendar.min.css") ?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url("assets/full_calendar/fullcalendar.print.css") ?>" rel="stylesheet" type="text/css" media='print' />
        
        
        <!--<link rel="stylesheet" href="<?//=base_url("assets/font_awesome/font-awesome.min.css") ?>">-->
        
        <!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->


        <!-- Fancybox -->
        <style>            
                        
            .navbar-header button{
                color:#fff;
            }
            .fullscreen{
                width: 100%
            }
            main{
                padding-top:50px;
            }
            /*h3{
                color: #e47b38;			
            }*/
            label{
                color: #e47b38;			
            }
            .css_legenda_principal{
                margin-top: 4px;
                color: #FFFFFF;
                font-size: 30px;
            }
            .css_legenda_user{
                color: #FFFFFF;
            }
            .css_barra{
                background-color: #EE9A00;	
            }
            .img_logo{
                margin-top: 3px;
                width: 160px;                
            }
            
             
        </style>
    </head>     
        
    <body>
        <!--<div>-->