<div class="container">
    <div class="col-md-12">
        <div class="row text-center">                  
            <h1>Informações do Colaborador</h1>
            <br>            
        </div>

        <?=$message?>
        <?=validation_errors()?>
        
        <div class="col-xs-12 col-sm-5 col-md-5">
            <div class="well">
                <section class="text-center">
                    <?php if(!empty($funcionario->foto)): ?>
                        <img src="data:image/png;base64,<?=base64_encode($funcionario->foto)?>" class="img-rounded img_func" height="250">
                    <?php else: ?>
                        <img src="<?=base_url("images/semfoto.png")?>" class="img-rounded img_func" height="250">
                    <?php endif; ?>
                </section>
                <br/>
                <section class="well">
                    <h5>Nome: </h5>
                    <?php
                    if (!empty($funcionario->nome)) 
                    {
                        $nome = mb_detect_encoding($funcionario->nome) === 'ASCII' ? iconv('ASCII','UTF-8', $funcionario->nome): $funcionario->nome ;                        
                        echo '<h3 class="text-center">'.$nome.'</h3>';
                    }                    
                    ?>               
                </section>
                <section class="well">
                    <h5>Matrícula: </h5>
                    <?php
                    if (!empty($funcionario->matricula)) 
                    {                                              
                        echo '<h2 class="text-center">'.$funcionario->matricula.'</h2>';
                    }
                    ?>
                </section>                                
            </div>
            <section class="row visible-xs">
                <br/>
            </section> 
        </div>
        
        <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-7 col-md-offset-1 col-md-6">            
            <div class="row">
                <form action="<?=base_url("senhas/index")?>" method="POST">
                    <?php if(!isset($recuperar)): ?>                        
                        <h3>Por favor, complete o seu cadastro:</h3>
                        <br/>
                        <h5>E-mail:</h5>
                        <div class="form-group form-group-lg">                                       
                            <input class="form-control" id="email" name="email" type="email" placeholder="Email da Reis Office" value="<?=set_value("email")?>" />
                        </div>

                        <br/>
                        <h5>Senha Reis Office:</h5>
                        <div class="form-group form-group-lg">                                       
                            <input class="form-control" id="senha" name="senha" type="password" placeholder="Nova Senha" value="<?=set_value("senha")?>" />
                        </div>
                        <br/>
                        <div class="form-group form-group-lg">                           
                            <input class="form-control" id="senha" name="csenha" placeholder="Confirmar Senha" type="password" value="<?=set_value("csenha")?>" />
                        </div>
                        <h5 class="text-right">(Mínimo 8 caract. e Máximo 16 caract.)</h5>

                        <input type="hidden" name="codfunc" value="<?=$funcionario->id?>"/>
                        <br/>
                        <hr/>            
                        <div class="form-group">                  
                            <a href="<?=base_url("senhas")?>" class="btn btn-success btn-lg"><< Retornar</a>
                            <button class="btn btn-primary btn-lg pull-right" name="obtFunc" type="submit">Concluir</button>
                        </div>
                    <?php else: ?>
                        <h3>Altere sua senha:</h3>
                        <br/>
                        <h5>Senha Provisória: <small>(Enviada por e-amil pelo sistema)</small></h5>
                        <div class="form-group form-group-lg">                                       
                            <input class="form-control" id="asenha" name="alt_asenha" type="password" placeholder="Senha Provisória" value="<?=set_value("alt_asenha")?>" />
                        </div>
                        <br/>
                        <h5>Nova Senha:</h5>
                        <div class="form-group form-group-lg">                                       
                            <input class="form-control" id="senha" name="alt_senha" type="password" placeholder="Nova Senha" value="<?=set_value("alt_senha")?>" />
                        </div>
                        <br/>
                        <div class="form-group form-group-lg">                           
                            <input class="form-control" id="senha" name="alt_csenha" placeholder="Confirmar Senha" type="password" value="<?=set_value("alt_csenha")?>" />
                        </div>
                        <h5 class="text-right">(Mínimo 8 caract. e Máximo 16 caract.)</h5>

                        <input type="hidden" name="alt_codfunc" value="<?=$funcionario->id?>"/>
                        <input type="hidden" name="alt_email" value="<?=$funcionario->email->valor?>"/>
                        <input type="hidden" name="empresa" value="<?=$funcionario->id_empresa?>"/>
                        <br/>
                        <hr/>            
                        <div class="form-group">                  
                            <button class="btn btn-primary btn-lg pull-right" name="obtFunc" type="submit">Alterar Senha</button>
                        </div>                    
                    <?php endif; ?>
                </form>
            </div>                
        </div>    
    </div> <!-- Fecha o ".col-md-12" -->
</div>