<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row text-center">                 
            <h1>Informe seu Nº de Matricula</h1>
            <p>Caso não saiba seu nº de matrícula, verifique o seu crachá ou consulte o RH.</p>
            <br>           
        </div>          
        
        <?=$message?>        
              
        <div class="row">            
            <form class="" action="<?=base_url("senhas/index")?>" method="POST">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <br/>                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">Reis Office:</span>        
                        <select class="form-control" id="empresa" name="empresa" required>
                            <option value="">Selecione</option>
                            <option value="1">Comercial</option>
                            <option value="2">Serviços</option>
                        </select>
                    </div>                    
                </div>                
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <br/>                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">Matricula:</span>        
                        <input class="form-control" id="matricula" name="matricula" placeholder="Matricula" type="text"/>
                    </div>                    
                </div>                
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr/>            
                    <div class="form-group text-left">  
                        <button class="btn btn-primary btn-lg" name="obtFunc" type="submit">
                            Avançar >>
                        </button>                        
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>    
