<div class="container">
    <div class="col-md-12">
        <div class="row text-center">                  
            <h1><?=$titulo?></h1>
            <br/>            
        </div>
        <div class="row text-center">
            <h3><?=$mensagem?></h3>
        </div>
        <br/>
        <?php if(isset($retorno_mensagem) && !empty($retorno_mensagem)): ?>
            <div class="row text-center">
                <?=$retorno_mensagem?>
                <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>        				
            </div>
        <?php endif; ?>
    </div>
    <?php if(isset($retorno_link) && !empty($retorno_link)): ?>
        <meta http-equiv="refresh" content=5;url="<?=$retorno_link?>">
    <?php endif; ?>
</div>