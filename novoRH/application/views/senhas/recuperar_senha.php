<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row text-center">                 
            <h1>Informe seu E-mail</h1>
            <br>           
        </div>          
        
        <?= $message?>
        <?= validation_errors()?>
              
        <div class="row">            
            <form class="" action="<?=base_url("senhas/index")?>" method="POST">
                <h3><?= $funcionario->nome?></h3>
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <br/>                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">E-mail:</span>        
                        <input class="form-control" id="email" name="rec_email" placeholder="@reisoffice.com.br" type="email" required/>
                    </div>                    
                </div>                
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr/>            
                    <div class="form-group text-left">                  
                        <input type="hidden" name="rec_codfunc" value="<?= $funcionario->id?>"/>
                        <input type="hidden" name="empresa" value="<?=$funcionario->id_empresa?>"/>
                        <button class="btn btn-primary btn-lg" name="obtFunc" type="submit">
                            Avançar >>
                        </button>                        
                    </div>
                </div>                
            </form>
        </div>
    </div>
</div>    
