<div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row text-center">                 
            <h1>Insira seus Dados</h1>
            <br>           
        </div>  
        
        <div class="row">
            <?php
            if(validation_errors())
            {
                echo '<div class="col-lg-12"><div class="alert alert-danger" role="alert"><h4>'.validation_errors().'</h4></div></div>';
            }
            
            if(!empty($this->session->flashdata('erro')))
            {
                echo '<div class="col-lg-12"><div class="alert alert-warning" role="alert"><h4>'.$this->session->flashdata('erro').'</h4><?div></div>';                
            }
                
            ?>
        </div>
              
        <div class="row">            
            <form class="visible-sm visible-md visible-lg" action="<?=base_url("senhas/index")?>" method="POST">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <br/>                    
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">Nome Completo:</span>                   
                        <input class="form-control" id="name" name="txtnome" type="text" placeholder="Nome"/>
                    </div>
                    <br/>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">Matricula:</span>        
                        <input class="form-control" id="matricula" name="txtmatricula" placeholder="Matricula" type="text"/>
                    </div>
                    <br/>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="basic-addon1">Somente o Email:</span>      
                        <input class="form-control" id="email" name="txtemail" placeholder=""/>
                        <span class="input-group-addon" id="basic-addon1">@reisoffice.com.br</span>
                    </div>
                    <br/>
                </div>                
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <hr/>            
                    <div class="form-group text-left">                  
                        <button class="btn btn-primary btn-lg" name="obtFunc" type="submit">
                            Avançar >>
                        </button>                        
                    </div>
                </div>                
            </form>
            <form class="visible-xs" action="<?=base_url("senhas/index")?>" method="POST">
                <div class="col-xs-12">
                    <br/>
                    <div class="form-group form-group-lg">                                       
                        <input class="form-control" id="name" name="txtnome" type="text" placeholder="Nome"/>
                    </div>
                    <div class="form-group form-group-lg">                           
                        <input class="form-control" id="matricula" name="txtmatricula" placeholder="Matrícula" type="text"/>
                    </div>
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="email" name="txtemail" placeholder="email"/>
                        <span class="input-group-addon" id="basic-addon1" style="font-size: 10pt;">@reisoffice.com.br</span>
                    </div>                    
                    <br/>
                </div>
                <div class="col-xs-12">
                    <hr/>            
                    <div class="form-group text-left">                  
                        <input type="hidden" name="empresa" value="<?=$funcionario->id_empresa?>"/>
                        <button class="btn btn-primary btn-lg" name="obtFunc" type="submit">
                            Avançar >>
                        </button>                        
                    </div>
                </div>  
            </form>       
        </div>
    </div>
</div>    
