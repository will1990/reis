<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />        
        <meta name="viewport" content="width=device-width">
        <title>Intranet Reis Office</title>
       
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="<?= base_url("assets/conteudo/css/font-awesome.min.css") ?>">

        <!-- Fancybox -->

        <style>                       
            .navbar-header button{
                color:#fff;
            }
            .fullscreen{
                width: 100%
            }
            main{
                padding-top:50px;
            }
            h3{
                color: #e47b38;			
            }

            label{
                color: #e47b38;			
            }
            header{
                color: #e47b38;	
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }
        </style>
    </head> 
    
    <body>
        <div class="col-md-12 text-center">              
            <img src="http://www.reisoffice.com.br/images/logo.png" height="60" style="float:left" />
        </div>
        <div class="col-md-12">
            <h1><?=$titulo?></h1>
        </div>
        <br/>
        <div class="col-md-12">
            <h2>Suas Informações de Login</h2>
            <h3><b style="color:#000;"> Email:</b> <?=$email?> </h3>
            <h3><b style="color:#000;"> Senha:</b> <?=$senha?> </h3>                           
        </div>      
    </body>
    <footer>        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>        
    </footer>
</html>