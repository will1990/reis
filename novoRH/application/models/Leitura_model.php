<?php

class Leitura_model extends CI_Model 
{
    function __construct() {
        parent::__construct();
        $this->load->library('excel');
        
        $this->rh = $this->load->database('default', true);
    }  
    
    function lerxls($path = null, $tratamento = null)
    {        
        $objReader = new PHPExcel_Reader_Excel5();
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($path);
        $objPHPExcel->setActiveSheetIndex(0);
               
        $linha = $tratamento["linha"];
        $coluna = $tratamento["coluna"];
        
        $linhas = '';
        // navegar na linha
        for($row = $linha->primeira; $row <= $linha->ultima; $row++)
        {
            if(in_array($row, $linha->ignorar) !== false)
            {
                null;
            }
            else
            {
                // navegar nas colunas da respectiva linha
                for($col = $coluna->primeira; $col <= $coluna->ultima; $col++)
                {
                    /*if((in_array($coluna, $tratamento['colunas_validas']) !== false))
                    {*/
                        if(empty($coluna->cabecalho[$col]))
                        {
                            $linhas[$row][$col] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue();     
                        }
                        else
                        {
                            $linhas[$row][$coluna->cabecalho[$col]] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($col, $row)->getValue();                          
                        }
                        
                    /*}
                    elseif((in_array($coluna, $tratamento['colunas_datas']) !== false))
                    {
                        if($linha === $tratamento['cabecalho_tab'])
                        {
                            $linhas['cab'][$coluna] = utf8_decode($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue());                            
                        }
                        else
                        {
                            $data = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($coluna, $linha)->getValue();
                            $linhas[$linha][$coluna] = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($data));
                        }
                    }
                    else
                    {
                        null;
                    }*/
                }
            }
            
            
        }
        return $linhas;
    }
    
    public function pesqPessoa($nome)
    {
        return $this->rh->query("SELECT id, nome FROM rh_empregados WHERE nome LIKE '$nome' AND ativo = 1")->result();
    }
    
    public function refinarPessoa($id, $termo)
    {
        $sql = "SELECT a.id_empregado, empregado.nome, b.titulo cargo, c.titulo depto FROM rh_empregados_contratos AS a 
                LEFT JOIN rh_cargos AS b ON a.id_cargo = b.id 
                LEFT JOIN rh_departamentos AS c ON a.id_departamento = c.id
                LEFT JOIN rh_empregados AS empregado ON a.id_empregado = empregado.id 
                WHERE a.id_empregado = $id AND b.titulo LIKE '$termo' AND c.titulo LIKE '$termo'";                
        
        return $this->rh->query($sql)->result();        
    }
    
    
    public function setPessoa($id, $pessoa)
    {
        $this->rh->set("id_empregado", $id);
        $this->rh->set("ramal", $pessoa->ramal);
        $this->rh->set("skype", $pessoa->skype);
        if($this->rh->insert("rh_empregados_contatos_internos")):
            return true;
        else:
            return false;
        endif;
    }    
    
    
    
    /*public function set_funcionario($emp)
    {
        print_r($emp);
        
        $rst = $this->db->get_where("rh_empregados", "matricula = '". $emp[0] ."'")->row();
        if($rst)
        {
            echo "Funcionario encontrado:<br/>";
            
            // Seta os documentos
            //$this->set_documentos($rst->id, $emp);

            // Atualuiza a data de nascimento
            $this->db->set("nome", $emp[3]);
            $this->db->set("data_nascimento", formatar($emp[4],"dt2bd"));
            $this->db->where("id = $rst->id");
            if($this->db->update("rh_empregados"))
            {
                echo "Funcionario ". $emp[3] .": ATUALIZADO<hr/>";
            }
            else
            {
                echo "Erro ao atualizar funcionario ". $emp[3] .": ". $this->db->_error_message() ."<hr/>";
            }
        }
        else
        {
            echo "Funcionario novo...<br/>";
            
            $this->db->set("nome", $emp[3]);
            $this->db->set("matricula", $emp[0]);
            $this->db->set("data_admissao", formatar($emp[5],"dt2bd"));
            $this->db->set("data_nascimento", formatar($emp[4],"dt2bd"));
            $this->db->set("ativo", 1);
            if($this->db->insert("rh_empregados"))
            {
                $id = $this->db->insert_id();
                
                // Seta os documentos
                $this->set_documentos($id, $emp);
                
                // Define uma senha
                
                echo "Funcionario ". $emp[3] .": OK<hr/>";
            }
            else
            {
                echo "Erro ao inserir funcionario ". $emp[3] .": ". $this->db->_error_message() ."<hr/>";
            }            
        }
    }
    
    public function set_documentos($id, $emp)
    {
        // {1=rg, 2=cpf, 3=pis}
        $docs = array(1=>6,2=>2,3=>1);
        
        foreach($docs as $doctip => $idx)
        {
            if(!empty($emp[$idx]))
            {
                // Limpa o rg existente
                $this->db->where("id_empregado = $id AND tipo_documento = $doctip");
                $this->db->delete("rh_empregados_documentos");

                // Insere o novo doc
                $doc = somente_numeros($emp[$idx]);
                $this->db->set("id_empregado", $id);
                $this->db->set("tipo_documento", $doctip);
                $this->db->set("numero", $doc);
                $this->db->insert("rh_empregados_documentos");
            }
        }
    }*/
}

