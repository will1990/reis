<?php

class Painel_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);     
    }
    
    public function obter_pessoa()
    {
        $this->db->select("id, nome, matricula");
        $this->db->from("rh_empregados");
        $this->db->where("matricula", 77);
        $query = $this->db->get();
        $rst = $query->result();
        
        return $rst;
    }
}
