<?php

class Senhas_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->load->library('sistema');
    }
    
    public function get_funcionario($empresa = false, $matricula = false, $id = false)
    {
        if($matricula)
        {
            $this->db->where("matricula = '$matricula'");            
        }
        elseif($id)
        {
            $this->db->where("id = '$id'");
        }
        else
        {
            $this->db->where("1 = 2");
        }
        
        if($empresa)
        {
            $this->db->where("id_empresa = $empresa");
        }
        
        $this->db->where("ativo = 1");
        $rst = $this->db->get("rh_empregados")->row();        
        if($rst)
        {
            // verifica se já criou a senha e atualizou o e-mail
            $sen = $this->db->get_where("rh_empregados_senhas", "id_empregado = $rst->id AND ativo = 1")->row();
            $eml = $this->db->get_where("rh_empregados_contatos", "id_empregado = $rst->id AND tipo_contato = 13")->row();
            $rst->senha = $sen;
            $rst->email = $eml;
            
            if($sen && $eml)
                $rst->esqueci = true;
            else
                $rst->esqueci = false;
            /*{
                $this->session->set_flashdata("message", "<div class='alert alert-warning'>$rst->nome, você já atualizou o seu cadastro.</div>");
                redirect(base_url("senhas"));
                exit;
            }*/
        }
        
        return $rst;        
    }
    
    public function set_funcionario()
    {
        $data = (object)$this->input->post();
        $funcionario = $this->get_funcionario($data->empresa, false, $data->codfunc);
        
        // Elimina as senhas anteriores
        $this->db->set("ativo", 0);
        $this->db->where("id_empregado = '$data->codfunc'");
        $this->db->update("rh_empregados_senhas");
        
        // Salva a senha do funcionário
        $this->db->set("id_empregado", $data->codfunc);
        $this->db->set("senha", md5($data->senha));
        $this->db->set("data_senha", "NOW()", false);
        $this->db->set("alterar_senha", 0);
        $this->db->set("ativo", 1);
        $this->db->insert("rh_empregados_senhas");
        
        // Elimina os dados de email funcional (13)
        $this->db->where("id_empregado = '$data->codfunc' AND tipo_contato = 13");
        $this->db->delete("rh_empregados_contatos");
        
        // Salva o email do funcionário
        $this->db->set("id_empregado", $data->codfunc);
        $this->db->set("tipo_contato", 13);
        $this->db->set("valor", strtolower($data->email));
        $this->db->insert("rh_empregados_contatos");
        
        // Envia o E-mail
        $msg = "$funcionario->nome, sua senha para acesso aos Sistemas Internos da Reis Office é:<br><pre>$data->senha</pre><br>"
                . "Por favor, ative a sua senha <a href='". base_url("senhas/ativar/$data->codfunc/". md5($data->senha)) ."'>clicando aqui</a>";
        $remetente = (object) array(
            'email' => 'desenvolvimento@reisoffice.com.br',
            'nome'  => 'Gerenciamento de Senha'
        );
        $destinatario = (object) array(
            'email' => $data->email,
            'nome' => $funcionario->nome,
            'mensagem' => array("titulo" => "Senha de Acesso", "mensagem" => $msg),
            //'cc' => ' ', 
            'assunto' => 'Reis Office - Atualização Cadastral'            
        );
        if($this->sistema->enviarEmail($remetente, $destinatario))
        {
            return true;
        }
        else
        {
            $this->session->set_flashdata('message', "<div class='alert alert-info'>$funcionario->nome, seu cadastro foi atualizado, porém, não foi possível lhe enviar o e-mail. Entre em contato com o Depto. de T.I.</div>");
            redirect(base_url("senhas/index"));
        }
    }
    
    public function ativar_senha($id, $hash)
    {
        // Busca a senha do funcionario
        $this->db->where("id_empregado = $id AND senha = '$hash'");
        $rst = $this->db->get("rh_empregados_senhas")->row();
        if($rst)
        {
            $funcionario = $this->db->get_where("rh_empregados", "id = $id")->row();
                        
            if(!empty($rst->data_ativacao))
            {
                $this->session->set_flashdata('message', "<div class='alert alert-warning'>$funcionario->nome, sua senha já esta ativada</div>");
                redirect(base_url("senhas/index"));
            }
            else
            {
                $this->db->set("data_ativacao", "NOW()", false);
                $this->db->where("id = $rst->id");
                $this->db->update("rh_empregados_senhas");                
                
                $this->session->set_flashdata('message', "<div class='alert alert-success'>$funcionario->nome, sua senha foi ativada com sucesso</div>");
                redirect(base_url("senhas/index"));
            }
        }
        else
        {
            $this->session->set_flashdata('message', "<div class='alert alert-danger'>Dados inválidos para ativação de senha</div>");
            redirect(base_url("senhas/index"));
        }
    }
    
    public function chk_email_funcionario()
    {
        $data = (object)$this->input->post();
        $funcionario = $this->get_funcionario(false, false, $data->rec_codfunc);
        
        if($funcionario)
        {
            if($funcionario->email)
            {
                if($funcionario->email->valor == strtolower($data->rec_email))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }

    public function set_nova_senha($alterar = false)
    {
        $data = (object)$this->input->post();
        
        if($alterar)
        {
            $funcionario = $this->get_funcionario(false, false, $data->alt_codfunc);
            
            // Salva a senha do funcionário
            $this->db->where("id_empregado = $data->alt_codfunc AND ativo = 1");
            $this->db->set("senha", md5($data->alt_senha));
            $this->db->set("data_ativacao", "NOW()", false);
            $this->db->set("data_recuperacao", "NOW()", false);
            $this->db->set("alterar_senha", 0);
            $this->db->update("rh_empregados_senhas");
            
            // Envia o E-mail
            $msg = "$funcionario->nome, sua senha é:<p><pre>$data->alt_senha</pre></p>";
            $remetente = (object) array(
                'email' => 'desenvolvimento@reisoffice.com.br',
                'nome'  => 'Gerenciamento de Senha'
            );
            $destinatario = (object) array(
                'email' => $data->alt_email,
                'nome' => $funcionario->nome,
                'mensagem' => array("titulo" => "Senha de Acesso", "mensagem" => $msg),
                //'cc' => ' ', 
                'assunto' => 'Reis Office - Atualização Cadastral'            
            );
        }
        else
        {
            $funcionario = $this->get_funcionario(false, false, $data->rec_codfunc);
            
            // Cria uma nova senha que será enviada
            $nova_senha = substr(md5(date("Hs:is")), rand(0,20), 8);

            // Elimina as senhas anteriores
            $this->db->set("ativo", 0);
            $this->db->where("id_empregado = '$data->rec_codfunc'");
            $this->db->update("rh_empregados_senhas");

            // Salva a senha do funcionário
            $this->db->set("id_empregado", $data->rec_codfunc);
            $this->db->set("senha", md5($nova_senha));
            $this->db->set("data_senha", "NOW()", false);
            $this->db->set("alterar_senha", 1);
            $this->db->set("ativo", 1);
            $this->db->insert("rh_empregados_senhas");

            // Envia o E-mail
            $msg = "$funcionario->nome, sua senha provisória é:<p><pre>$nova_senha</pre></p>"
                    . "Por favor, altere sua senha <a href='". base_url("senhas/recuperar/$data->rec_codfunc") ."'>clicando aqui</a>";
            $remetente = (object) array(
                'email' => 'desenvolvimento@reisoffice.com.br',
                'nome'  => 'Gerenciamento de Senha'
            );
            $destinatario = (object) array(
                'email' => $data->rec_email,
                'nome' => $funcionario->nome,
                'mensagem' => array("titulo" => "Senha de Acesso", "mensagem" => $msg),
                //'cc' => ' ', 
                'assunto' => 'Reis Office - Atualização Cadastral'            
            );
        }
        
        if($this->sistema->enviarEmail($remetente, $destinatario))
        {
            return true;
        }
        else
        {
            $this->session->set_flashdata('message', "<div class='alert alert-info'>$funcionario->nome, seu cadastro foi atualizado, porém, não foi possível lhe enviar o e-mail. Entre em contato com o Depto. de T.I.</div>");
            redirect(base_url("senhas/index"));
        }
    }
    
    public function chk_funcionario($login, $senha, $empresa)
    {
        $funcionario = false;
        
        // Verifica se veio matricula ou cpf no login
        if(strlen($login) == 11)
        {
            $this->db->where("numero = '$login' AND tipo_documento = 2");
            $rst = $this->db->get("rh_empregados_documentos")->row();
            if($rst)
            {
                $funcionario = $this->get_funcionario(false, false, $rst->id_empregado);
            }
        }
        else
            $funcionario = $this->get_funcionario($empresa, $login);
        
        // Valida a senha
        if($funcionario)
        {
            if($funcionario->senha->senha == md5($senha))
                return $funcionario;
            else
                return "X";
        }
        else
            return "U";
    }
    
    public function obterFuncDados($colunas = '*', $tabela = null, $where = null)
    {
        $this->db->select($colunas);
        $this->db->from($tabela);
        $this->db->where($where);
        $qry = $this->db->get();
        return $qry->result();        
    }
    
    public function newFuncDados($valores = null, $tabela = null)
    {
        $this->db->set($valores);
        if ($this->db->insert($tabela))
        {
            return true;
        }
        else
        {
            return false;
        }   
    }   
    
    public function updFuncDados($valores = null, $tabela = null, $where = null)
    {
        $this->db->set($valores);
        $this->db->where($where);
        if ($this->db->update($tabela))
        {
            return true;
        }
        else
        {
            return false;
        }     
    }     
}

