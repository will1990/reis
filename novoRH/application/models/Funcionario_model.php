<?php

class Funcionario_model extends CI_Model {

        
    function __construct() 
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->db = $this->load->database('default', TRUE);         
    }   
    
    function select_nome(){
        //$query = $this->db->get_where('RH_EMPREGADOS', array('ID'=>200));
        $this->db->select("*");
        $this->db->from("RH_EMPREGADOS");
        $this->db->where("ID=200");
        $query = $this->db->get();
        $rst = $query->result();
        return $rst;
        //return $query->row_array();   
    }
    public function contrato_form() { //|numeric|greater_than[0.99]
        $this->form_validation->set_rules('matricula', 'Matricula', 'required');
        $this->form_validation->set_rules('setor', 'Setor', 'required');
        $this->form_validation->set_rules('cargo', 'Cargo', 'required');
        $this->form_validation->set_rules('inicio', 'Data de Inicio', 'required');
        $this->form_validation->set_rules('termino', 'Data de Termino', 'required');
        $this->form_validation->set_rules('atual', '"Atual ?"', 'required');
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            
        }
    }
    public function contato_form() { //|numeric|greater_than[0.99]
        $this->form_validation->set_rules('tipo_contato', 'Tipo de Contato', 'required');
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('telefone', 'Telefone', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('ramal', 'Ramal', 'required');
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            
        }
    }
    
    public function funcionario_form() { //|numeric|greater_than[0.99]
        $this->form_validation->set_rules('nome', 'nome', 'required');
        $this->form_validation->set_rules('matricula', 'matricula', 'required');
        $this->form_validation->set_rules('nascimento', 'Data de Nascimento', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('data_admissao', 'Data de Admissão', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function endereco_form() { //|numeric|greater_than[0.99]
        $this->form_validation->set_rules('logradouro', 'Rua', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required');
        $this->form_validation->set_rules('cep', 'Cep', 'required|numeric');
        $this->form_validation->set_rules('cidade', 'Cidade', 'required');
        $this->form_validation->set_rules('bairro', 'Bairro', 'required');
        $this->form_validation->set_rules('num', 'Numero', 'required|numeric');
        $this->form_validation->set_rules('end_atual', '"Endereço Atual?"', 'required');
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
