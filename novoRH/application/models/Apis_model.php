<?php

class Apis_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        //$this->db = $this->load->database('default', TRUE);
    }
    
    //em uso
    public function obtIdDeEmail($email = null)
    {
        $this->db->select("id_empregado, valor, descricao");
        $this->db->from("rh_empregados_contatos");
        $this->db->where("valor", $email);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[0];
    }
    
    
    public function obtEmailDeId($id = null)
    {
        $this->db->select("id_empregado, valor, descricao");
        $this->db->from("rh_empregados_contatos");
        $this->db->where("id_empregado", $id);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[0];
    }

    //em uso
    public function obtFuncDeId($id = null)
    {
        $this->db->select("id, nome, matricula, data_admissao");
        $this->db->from("rh_empregados");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[0];
    }
    
    //em uso
    public function obtFuncDeMatr($matricula = null, $posicao = null)
    {
        $posicao = $posicao !== null ? $posicao : 0 ;
        
        $this->db->select("id, nome, matricula, data_admissao");
        $this->db->from("rh_empregados");
        $this->db->where("matricula", $matricula);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[$posicao];
    }
    
    //em uso
    public function validFuncDeEmail($email = null)
    {
        $this->db->select("id_empregado, valor");
        $this->db->from("rh_empregados_contatos");
        $this->db->where("valor", $email);
        $qry = $this->db->get();
        return $qry->result();        
    }
    
    //em uso
    public function validFuncDeMatr($matricula = null)
    {
        $this->db->select("id");
        $this->db->from("rh_empregados");
        $this->db->where("matricula", $matricula);
        $qry = $this->db->get();
        return $qry->result();        
    }
    
    public function validFuncDeId($id = null)
    {
        $this->db->select("id, nome, matricula");
        $this->db->from("rh_empregados");
        $this->db->where("id", $id);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[0];
    }
    
    //em uso
    public function validSenhaDeId($id = null)
    {
        $this->db->select("senha");
        $this->db->from("rh_empregados_senhas");
        $this->db->where("id_empregado", $id);
        $qry = $this->db->get();
        $query = $qry->result();
        return $query[0];
    }
    
    public function carregar_funcionarios()
    {
        $sufixo = "_copy";
        $empresas = array("1" => "comercial", "2" => "service");
        
        // Para cada empresa carrega os funcionarios da base da flaumar
        foreach($empresas as $id_empresa => $empresa)
        {
            $funcionarios = file(APPPATH."../uploads/". $empresa ."_funcionarios.csv");            
            if($funcionarios)
            {
                print "Encontrados ". count($funcionarios) ." funcionarios da empresa REIS ". strtoupper($empresa) ."<br>";
            
                foreach($funcionarios as $funcionario)
                {
                    set_time_limit(15);
                    list($matricula, $nome, $nascimento, $admissao, $rg, $cpf) = explode(";", $funcionario);
                    
                    // Verifica se já existe o funcionário
                    $this->db->where("nome = '". utf8_decode($nome) ."' AND matricula = $matricula AND data_admissao = STR_TO_DATE('$admissao', '%d/%m/%Y') AND id_empresa = $id_empresa");
                    $ext = $this->db->get("rh_empregados".$sufixo)->row();
                    if($ext)
                        $id_empregado = $ext->id;
                    else
                        $id_empregado = 0;
                    
                    // Salva os dados do empregado
                    $this->db->set("nome", strtoupper($nome));
                    $this->db->set("matricula", intval($matricula));
                    $this->db->set("data_admissao", "STR_TO_DATE('$admissao', '%d/%m/%Y')", false);
                    $this->db->set("data_nascimento", "STR_TO_DATE('$nascimento', '%d/%m/%Y')", false);
                    $this->db->set("ativo", 1);
                    $this->db->set("id_empresa", $id_empresa);
                    
                    if($id_empregado)
                    {
                        $this->db->where("id = $id_empregado");
                        $rst = $this->db->update("rh_empregados".$sufixo);
                    }
                    else
                    {
                        $rst = $this->db->insert("rh_empregados".$sufixo);
                        $id_empregado = $this->db->insert_id();
                    }
                    
                    // Salva os documentos
                    if($id_empregado)
                    {
                        $this->db->where("id_empregado = $id_empregado AND tipo_documento IN (1,2)");
                        $this->db->delete("rh_empregados_documentos".$sufixo);
                        
                        for($d = 1; $d <= 2; $d++)
                        {
                            $this->db->set("id_empregado", $id_empregado);
                            $this->db->set("tipo_documento", $d);
                            $this->db->set("numero", ($d == 1) ? str_replace(array(".","-",","), "", $rg) : str_replace(array(".","-",","), "", $cpf));
                            $this->db->insert("rh_empregados_documentos".$sufixo);
                        }
                    }
                }
            }
            else
            {
                print "Nenhum funcionario carregado/arquivo não encontrado ($empresa)<br>";
            }
        }
    }
    
    public function corrigir_dependencias()
    {
        $this->db->select("r1.nome, r1.matricula, r1.id", false);
        $this->db->select("r2.nome nome2, r2.matricula matricula2, r2.id id2", false);
        $this->db->join("rh_empregados r2", "r1.nome = r2.nome AND r2.ativo = 1");
        $rst = $this->db->get("rh_empregados_copy r1")->result();
        if($rst)
        {
            foreach($rst as $item)
            {
                print "$item->nome -> $item->id2 -> $item->id<br>";
                // Corrige os contatos
                $this->db->where("id_empregado = $item->id2");
                $this->db->set("id_empregado", $item->id);
                $this->db->update("rh_empregados_contatos_copy");
                
                // Corrige as senhas
                $this->db->where("id_empregado = $item->id2");
                $this->db->set("id_empregado", $item->id);
                $this->db->update("rh_empregados_senhas_copy");
            }
        }
    }
    
    public function geral()
    {
        // Carrega dos dados do arquivo CSV
        $dados = file(APPPATH . "../uploads/geral_comercial.csv");
        $deps =  file(APPPATH . "../uploads/deps_comercial.csv");
        $ends =  file(APPPATH . "../uploads/end_comercial.csv");
        
        $bd = "RH_teste.";
        $id = 0;
        
        // Percorre os dados gerais
        foreach($dados as $func)
        {
            list($matricula,$nome,$nome_pai,$nome_mae,$titulo_eleitor,$zona,$secao,$cpf,$rg,$dt_adm,$dt_nasc,$pis,$ctps,$serie) = explode(";", $func);
            
            $this->db->select("id");
            $this->db->where("matricula = $matricula AND id_empresa = 1");
            $rst = $this->db->get($bd."rh_empregados")->row();
            if($rst)
            {
                $id = $rst->id;
                $adm = DateTime::createFromFormat("d/m/Y", $dt_adm);
                $nasc = DateTime::createFromFormat("d/m/Y", $dt_nasc);
                
                // Atualiza as informacoes padrao
                $this->db->set("data_adimissao", $adm->format("Y-m-d"), false);
                $this->db->set("data_nascimento", $nasc->format("Y-m-d"), false);
                $this->db->where("id = $id");
                $this->db->update($bd."rh_empregados");
                print "$nome - INFO PADRAO";
            }
            else
            {
                $adm = DateTime::createFromFormat("d/m/Y", $dt_adm);
                $nasc = DateTime::createFromFormat("d/m/Y", $dt_nasc);
                
                // Atualiza as informacoes padrao
                $this->db->set("matricula", $matricula);
                $this->db->set("nome", $nome);
                $this->db->set("data_adimissao", $adm->format("Y-m-d"), false);
                $this->db->set("data_nascimento", $nasc->format("Y-m-d"), false);
                $this->db->set("id_empresa", 1);
                $this->db->set("ativo", 1);
                $this->db->insert($bd."rh_empregados");
                $id = $this->db->insert_id();
                print "$nome - INFO PADRAO";
            }
            
            if($id > 0)
            {
                // Atualiza os documentos
                $docs = array();
                $docs[] = array("id_empregado" => $id, "tipo_documento" => 1, "numero" => $rg);
                $docs[] = array("id_empregado" => $id, "tipo_documento" => 2, "numero" => $cpf);
                $docs[] = array("id_empregado" => $id, "tipo_documento" => 3, "numero" => $pis);
                $docs[] = array("id_empregado" => $id, "tipo_documento" => 29, "numero" => $ctps, "complemento1" => $serie);
                if(!empty($titulo_eleitor))
                    $docs[] = array("id_empregado" => $id, "tipo_documento" => 28, "numero" => $titulo_eleitor, "complemento1" => $zona, "complemento2" => $secao);
                $this->db->where("id = $id");
                $this->db->delete($bd."rh_empregados_documentos");
                $this->db->insert($bd."rh_empregados_documentos", $docs);
                print " - DOCUMENTOS";
                
                // Atualiza pai e mae
                $this->db->set("id_empregado", $id);
                $this->db->set("tipo_dependente", 6);
                $this->db->set("nome", $nome_pai);
                $this->db->replace($bd."rh_empregados_dependentes");
                $this->db->set("id_empregado", $id);
                $this->db->set("tipo_dependente", 7);
                $this->db->set("nome", $nome_mae);
                $this->db->replace($bd."rh_empregados_dependentes");
                print " - PAI/MAE";
            }
        }
        
        // Percorre os dependentes        
        foreach($deps as $item)
        {
            $linha = explode(";", $item);

            $id_atual = 0;
            $dep_tipo = "";
            $dep_nome = "";
            $dep_nasc = "";

            if($linha[1] == "Empregado:")
            {
                if($id_atual > 0)
                {
                    // Salva os dados do dependente anterior
                    $this->db->set("tipo_dependente", $dep_tipo);
                    $this->db->set("nome", $dep_nome);
                    $this->db->set("data_nascimento", $dep_nasc->format("Y-m-d"));
                    $this->db->insert($bd."rh_empregados_dependentes");
                }
                
                $matricula = $linha[6];
                $emp = $this->db->get_where($bd."rh_empregados", "matricula = $matricula AND id_empresa = 1")->row();
                if($emp)
                {
                    $id_atual = $emp->id;
                }
            }

            if($linha[0] == "Nome:")
                $dep_nome = $linha[10];
            
            if($linha[0] == "Data Nascimento:")
            {
                $dep_nome = $linha[10];
            }


            print " - DEPENDENTES:";
        }
    }
}
